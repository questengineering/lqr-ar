#define appVersion="2.29"

[Setup]
AppCopyright=2013 Quest Integrity Group
AppName=LOTIS-MANTIS SMR AutoReporter {#appVersion}
AppVerName={#appVersion}
VersionInfoVersion={#appVersion}
VersionInfoCompany=Quest Integrity Group
VersionInfoDescription=LOTIS-MANTIS SMR AutoReporter
VersionInfoTextVersion={#appVersion}
VersionInfoCopyright=2013 Quest Integrity Group
VersionInfoProductName=LOTIS-MANTIS SMR AutoReporter
VersionInfoProductVersion={#appVersion}
OutputBaseFilename=SetupQIGAutoReporterSMR
MinVersion=0,5.01.2600sp3
AppMutex=AutoReporterLotisMantisSMR
DefaultDirName={pf}\Quest Integrity Group\AutoReporterSMR
UsePreviousAppDir=no
DisableDirPage=true
DisableProgramGroupPage=true
DefaultGroupName=Quest Integrity Group
AppPublisher=Quest Integrity Group
AppPublisherURL=
AppSupportURL=
AppVersion={#appVersion}
AppContact=
AppSupportPhone=
UninstallDisplayName=LOTIS-MANTIS SMR AutoReporter
AppID={{5fa011e5-2618-434f-9b55-fc8bfafcf2d8}
DisableFinishedPage=true
DisableReadyPage=true
ShowLanguageDialog=no
UninstallDisplayIcon={app}\AutoReporter.exe
OutputDir=.\

[Icons]
Name: {group}\LOTIS-MANTIS SMR AutoReporter; Filename: {app}\AutoReporter.exe; IconFilename: {app}\AutoReporter.exe; IconIndex: 0
Name: {commondesktop}\LOTIS-MANTIS SMR AutoReporter; Filename: {app}\AutoReporter.exe; IconFilename: {app}\AutoReporter.exe; IconIndex: 0

[Dirs]
Name: {userdocs}\Inspection Data; Permissions: users-full
Name: {userappdata}\AutoReporter\Templates; Permissions: users-full

[Files]
; Application Files
Source: ..\AutoReporter\bin\Release\*; DestDir: {app}; Flags: ignoreversion replacesameversion; Permissions: users-full
 Source: ..\AutoReporter\bin\Release\Dictionaries\*; DestDir: {app}\Dictionaries; Flags: ignoreversion replacesameversion; Permissions: users-full
; Database add - onlyifdoesntexist or confirmoverwrite depending on the behavior you'd like the installer to have when dealing with the old database.
Source: ..\AutoReporter\bin\Release\DB_local\*; DestDir: {app}\DB_local; Flags: confirmoverwrite uninsneveruninstall; Permissions: users-full

; Report Templates
Source: ..\AutoReporter\bin\Release\ReportTemplates\*.dot; DestDir: {userappdata}\AutoReporter\Templates; Flags: replacesameversion 

