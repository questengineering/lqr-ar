Module modProgram
    'Const DEFAULT_QTT_OFFICE = 1
    Private mCurrentOfficeID As Integer

    ' Constants for field ID's of items from the tblDefaultText_ListItems
    ' Each grouping provides values for a dropdown 
    Public Const DEF_TEXTLIST_PIPETUBE_MATERIAL = 1
    Public Const DEF_TEXTLIST_PIPETUBE_SURFACE = 2
    Public Const DEF_TEXTLIST_PIPETUBE_MANUFACTURER = 3
    Public Const DEF_TEXTLIST_INITIAL_DIRECTIONS = 5
    Public Const DEF_TEXTLIST_PLANT_PROCESS_TYPE = 6
    Public Const DEF_TEXTLIST_REFORMER_OEM = 7
    Public Const DEF_TEXTLIST_UNIT_TYPE = 11    'BURNER CONFIGURATION

    ' constants for inspection subfolders
    Const REPORT_FOLDER = "Report"
    Const MINWALL_FOLDER_FTIS = "Tables"
    Const MINWALL_FOLDER = "Tables"
    Const DRAWINGS_FOLDER = "Drawings"
    Const DATA_FOLDER = "Data"
    Const DELETEDPASSES_FOLDER = "xDeleted"
    Const GRAPHICS_FOLDER = "Graphics"
    Const GRAPHICS_3D_TUBE = "3D Tube Image"
    Const GRAPHICS_3D_REFORMER = "3D Reformer Model"   'Spare New has no 3D Reformer Model folder
    Const GRAPHICS_DIAMETER = "Diameter Graphs"
    Const GRAPHICS_OTHER = "Other Images"

    Const QIG_FOLDER = "QIG Only"
    Const QIG_DATA = "Data"
    Const QIG_REPORT = "Field Report"
    Const QIG_MISC = "Misc"
    Const QIG_TABLES = "Tables"

    Private m_MainForm As Main

    Public Class cOpenInspections
        ' simple collection class for cReportLinkedFile objects
        Inherits CollectionBase

        Public Property Item(ByVal Index As Integer) As InspectionDetails
            Get
                Return CType(List.Item(Index), InspectionDetails)
            End Get
            Set(ByVal Value As InspectionDetails)
                List.Item(Index) = Value
            End Set
        End Property

        Public Function Add(ByVal Item As InspectionDetails) As Integer
            Return List.Add(Item)
        End Function

    End Class

    Private m_OpenForms As cOpenInspections

    Public Property MainForm()
        Get
            MainForm = m_MainForm
        End Get
        Set(ByVal value)
            m_MainForm = value
        End Set
    End Property

    Public Function OpenQTTInspection(ByVal pInspectionID As Integer, ByVal pInspectionType As Integer, ByVal fIsNew As Boolean)
        Dim X As InspectionDetails

        Dim fFormFailed As Boolean
        If m_OpenForms Is Nothing Then
            m_OpenForms = New cOpenInspections
        End If
        For Each X In m_OpenForms
            If X.Inspection_ID = pInspectionID Then
                Try
                    X.Select()
                Catch Ex As Exception
                    fFormFailed = True
                End Try
                If Not fFormFailed Then
                    Return X
                    Exit For
                End If
            End If
        Next

        X = New InspectionDetails
        With X
            .Show()
            .OpenInspection(pInspectionID, fIsNew, True)
        End With
        m_OpenForms.Add(X)
        Return X

    End Function


    Public Function InspectionClosed(ByVal pInspectionID As Integer, ByVal pDetailsForm As Integer, ByVal pIsNew As Boolean, ByVal pWasDeleted As Boolean)
        ' can be called to notify that inspection was closed
        Dim X As InspectionDetails

        Dim iPos As Integer = 0

        If m_OpenForms Is Nothing Then
            m_OpenForms = New cOpenInspections
        End If
        'See if inspection is already in list collection
        For Each X In m_OpenForms
            If X.Inspection_ID = pInspectionID Then
                m_OpenForms.RemoveAt(iPos)
                iPos = iPos + 1
                Exit For
            End If
        Next
    End Function

    Public Function CurrentOpenInspections() As cOpenInspections
        If m_OpenForms Is Nothing Then
            m_OpenForms = New cOpenInspections
        End If

        CurrentOpenInspections = m_OpenForms
    End Function

    Property CurrentQTTOffice() As Integer
        Get
            'If not set, use default
            If mCurrentOfficeID = 0 Then
                mCurrentOfficeID = My.Settings.DefaultOfficeID
            End If
            'Return office
            CurrentQTTOffice = mCurrentOfficeID
        End Get
        Set(ByVal value As Integer)
            mCurrentOfficeID = value
        End Set
    End Property

    Function SetDefaultOffice(ByVal OfficeID As Integer, ByVal AlsoSetAsCurrent As Boolean)
        'TODO  Add default office feature
        My.Settings.DefaultOfficeID = OfficeID
        If AlsoSetAsCurrent Then mCurrentOfficeID = OfficeID
        Return True
    End Function

    Function AddAppendixItem(ByVal AppendixSectionID As Integer, ByVal ReportItemTypeID As Integer, ByVal SortOrder As Integer, ByVal PipeTubeID As Integer, ByVal RptBookMark As String) As Boolean
        'Called to add a single item to specified appendix
        Dim taApdxItems As New QTT_InspectionsDataSetsTableAdapters.vwAppendixItem_DetailsTableAdapter
        Dim PTI_ID As System.Nullable(Of Integer)

        Try
            If PipeTubeID <> 0 Then
                PTI_ID = PipeTubeID
            End If
            taApdxItems.sprocAddAppendixItem(AppendixSectionID, ReportItemTypeID, SortOrder, PTI_ID, RptBookMark)
        Catch ex As Exception
            MsgBox("The following exception occurred while attempting to add an Appendix Item to the Inspection database:" & vbNewLine & ex.Message)
        End Try

        Return True

    End Function

    Function DeleteAppendixItem(ByVal AppendixSectionID As Integer) As Boolean
        ' called to delete a single item from an Appendix
        Dim taApdxItems As New QTT_InspectionsDataSetsTableAdapters.vwAppendixItem_DetailsTableAdapter

        Try
            taApdxItems.sprocDeleteAppendixItem(AppendixSectionID)
        Catch ex As Exception
            MsgBox("The following exception occurred while attempting to delete an Appendix Item from the Inspection database:" & vbNewLine & ex.Message)
        End Try

        Return True

    End Function

    Function SetupInspectionFolders(ByVal InspectionFolderName As String, ByVal NumPasses As Integer, ByVal PassesInAlpha As Boolean,
                                    ByVal PassesCustom As Boolean, ByVal CustomPasses() As String, ByVal InspectionTypeID As Integer) As Boolean
        ' sets up folder tree for specified project
        Dim strCurFldr As String
        Dim intPass As Integer
        Dim strPassFldr As String
        Const LOTIS_SPARE = 8
        Const MANTIS_SPARE = 11
        Dim fIsSpare As Boolean = False

        'Flag if Spare/Uninstalled
        fIsSpare = (InspectionTypeID = LOTIS_SPARE Or InspectionTypeID = MANTIS_SPARE)

        With My.Computer.FileSystem
            ' create root folder as needed
            'KM 7/7/2011
            'KM 8/2/2011 Changed to MyDocuments
            Dim strRootPath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & My.Settings.RootDataFolder
            Dim strFullRoot As String = .CombinePath(strRootPath, InspectionFolderName)
            ' does root need to be created?
            If Not (.DirectoryExists(strFullRoot)) Then
                .CreateDirectory(strFullRoot)
            End If
            ' create subfolders -
            ' REPORT
            strCurFldr = (.CombinePath(strFullRoot, REPORT_FOLDER))
            If Not (.DirectoryExists(strCurFldr)) Then
                .CreateDirectory(strCurFldr)
            End If
            ' MINWALL
            strCurFldr = (.CombinePath(strFullRoot, MINWALL_FOLDER))
            If Not (.DirectoryExists(strCurFldr)) Then
                .CreateDirectory(strCurFldr)
            End If
            ' DRAWINGS (not Spare/Uninstalled)
            If Not fIsSpare Then
                strCurFldr = (.CombinePath(strFullRoot, DRAWINGS_FOLDER))
                If Not (.DirectoryExists(strCurFldr)) Then
                    .CreateDirectory(strCurFldr)
                End If
            End If
            ' QIG ONLY
            Dim strQIGRoot = (.CombinePath(strFullRoot, QIG_FOLDER))
            If Not (.DirectoryExists(strQIGRoot)) Then
                .CreateDirectory(strQIGRoot)
            End If
            ' QIG DATA
            Dim strQIGDataRoot = (.CombinePath(strQIGRoot, QIG_DATA))
            If Not (.DirectoryExists(strQIGDataRoot)) Then
                .CreateDirectory(strQIGDataRoot)
            End If
            ' QIG FIELD REPORT
            strCurFldr = (.CombinePath(strQIGRoot, QIG_REPORT))
            If Not (.DirectoryExists(strCurFldr)) Then
                .CreateDirectory(strCurFldr)
            End If
            ' QIG MISC
            strCurFldr = (.CombinePath(strQIGRoot, QIG_MISC))
            If Not (.DirectoryExists(strCurFldr)) Then
                .CreateDirectory(strCurFldr)
            End If
            ' QIG TABLES
            strCurFldr = (.CombinePath(strQIGRoot, QIG_TABLES))
            If Not (.DirectoryExists(strCurFldr)) Then
                .CreateDirectory(strCurFldr)
            End If
            ' DATA and GRAPHICS
            Dim strDataRoot = .CombinePath(strFullRoot, DATA_FOLDER)
            If Not (.DirectoryExists(strDataRoot)) Then
                .CreateDirectory(strDataRoot)
            End If
            Dim strGraphicsRoot = .CombinePath(strFullRoot, GRAPHICS_FOLDER)
            If Not (.DirectoryExists(strGraphicsRoot)) Then
                .CreateDirectory(strGraphicsRoot)
            End If
            'GRAPHICS
            If Not (.DirectoryExists(.CombinePath(strGraphicsRoot, GRAPHICS_3D_TUBE))) Then
                .CreateDirectory(.CombinePath(strGraphicsRoot, GRAPHICS_3D_TUBE))
            End If
            'Do not include 3D Reformer Model folder if New Spare report
            If Not fIsSpare Then
                If Not (.DirectoryExists(.CombinePath(strGraphicsRoot, GRAPHICS_3D_REFORMER))) Then
                    .CreateDirectory(.CombinePath(strGraphicsRoot, GRAPHICS_3D_REFORMER))
                End If
            End If
            If Not (.DirectoryExists(.CombinePath(strGraphicsRoot, GRAPHICS_DIAMETER))) Then
                .CreateDirectory(.CombinePath(strGraphicsRoot, GRAPHICS_DIAMETER))
            End If
            If Not (.DirectoryExists(.CombinePath(strGraphicsRoot, GRAPHICS_OTHER))) Then
                .CreateDirectory(.CombinePath(strGraphicsRoot, GRAPHICS_OTHER))
            End If
        End With

        Return True

    End Function

    Private Sub CreateDirectoriesIfDontExist(ByVal DirectoriesToCreate As List(Of String))

        For Each ThisDirectory As String In DirectoriesToCreate
            If Not (My.Computer.FileSystem.DirectoryExists(ThisDirectory)) Then
                My.Computer.FileSystem.CreateDirectory(ThisDirectory)
            End If
        Next
    End Sub

    Function UpdateInspectionPassesFolders(ByVal InspectionFolderName As String, ByVal NumPasses As Integer, ByVal PassesInAlpha As Boolean,
                                           ByVal PassesCustom As Boolean, ByVal CustomPasses() As String, ByVal InspectionTypeID As Integer)
        Dim strCurFldr As String
        Dim strAltPassFldr As String
        Dim strDelPassesFldr As String

        With My.Computer.FileSystem
            'Ensure the proper folder structure exists
            Dim strRootPath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & My.Settings.RootDataFolder
            Dim strFullRoot As String = .CombinePath(strRootPath, InspectionFolderName)
            'Create the subfolders of the Full Root
            Dim strReportFolder As String = .CombinePath(strFullRoot, REPORT_FOLDER)

            Dim strMinWallFolder As String = .CombinePath(strFullRoot, MINWALL_FOLDER)
            Dim strDrawingsFolder As String = .CombinePath(strFullRoot, DRAWINGS_FOLDER)
            Dim strQIGRoot As String = .CombinePath(strFullRoot, QIG_FOLDER)
            Dim strQIGDataRoot As String = .CombinePath(strQIGRoot, QIG_DATA)
            Dim strQIGFieldReportFolder As String = .CombinePath(strQIGRoot, QIG_REPORT)
            Dim strQIGMisc As String = .CombinePath(strQIGRoot, QIG_MISC)
            Dim strQIGTables As String = .CombinePath(strQIGRoot, QIG_TABLES)
            Dim strDataRoot As String = .CombinePath(strFullRoot, DATA_FOLDER)
            Dim strGraphicsRoot = .CombinePath(strFullRoot, GRAPHICS_FOLDER)
            Dim DirectoriesToCreate As New List(Of String)
            DirectoriesToCreate.Add(strFullRoot)
            DirectoriesToCreate.Add(strReportFolder)
            DirectoriesToCreate.Add(strMinWallFolder)
            DirectoriesToCreate.Add(strDrawingsFolder)
            DirectoriesToCreate.Add(strQIGRoot)
            DirectoriesToCreate.Add(strQIGDataRoot)
            DirectoriesToCreate.Add(strQIGFieldReportFolder)
            DirectoriesToCreate.Add(strQIGMisc)
            DirectoriesToCreate.Add(strQIGTables)
            DirectoriesToCreate.Add(strDataRoot)
            DirectoriesToCreate.Add(strGraphicsRoot)
            Call CreateDirectoriesIfDontExist(DirectoriesToCreate)

        End With

        Return True
    End Function
    Function CleanInvalidFilenameChars(ByVal strIn As String) As String
        ' Replace invalid filename characters with empty strings.
        ' invalid characters are: \ / : * ? < > | "  
        Dim strTemp As String
        ' strIn = "SDF/SD:F\sdfsf342*sdf?fsf""sdfs<sdfdsf>sdf|sf"
        strTemp = System.Text.RegularExpressions.Regex.Replace(strIn, "\:|/|\\|\*|""|<|>|\?|\|", "")

        Return strTemp
    End Function

    Public Function GetTubeRowLabel(ByVal intTubeRowNumber As Integer, ByVal fTubesRowsInAlpha As Boolean, ByVal fTubeRowCustom As Boolean, ByVal arrCustomTubesRows() As String) As String
        'Get first or last tube/row label based on type of numbering and number of tubes/rows

        If fTubesRowsInAlpha Then
            Return Microsoft.VisualBasic.Chr(64 + intTubeRowNumber)
        ElseIf fTubeRowCustom Then
            Return Trim(arrCustomTubesRows(intTubeRowNumber - 1))
        Else
            Return Format(intTubeRowNumber, "0")
        End If
    End Function

    Public Function GetDefaultFieldText(ByVal intInspectionType As Integer, ByVal strFieldCode As String) As String
        ' returns default text for specified field
        Dim taDefTextItems As New QTT_LookupsDataTableAdapters.tblDefaultTextEntriesTableAdapter
        Dim tblDefText As New QTT_LookupsData.tblDefaultTextEntriesDataTable
        taDefTextItems.FillBy_InspectionTypeAndFieldCode(tblDefText, intInspectionType, strFieldCode)

        ' read data and return 
        Dim sRetText As String = ""
        If tblDefText.Rows.Count > 0 Then
            Dim oRow As QTT_LookupsData.tblDefaultTextEntriesRow = tblDefText.Item(0)
            If Not (oRow.IsDefaultTextNull) Then
                sRetText = oRow.DefaultText
            End If
        End If
        Return sRetText

    End Function


    Public Function SetupInspectionAppendixes(ByVal InspectionID As Integer, ByVal InspectionType As Integer, ByVal NumberOfPasses As Integer, ByVal PassesInAlpha As Boolean, ByVal fCustomPasses As Boolean, ByVal strCustomPasses As String) As Boolean
        ' setup inspection appendix(es) and default appendix items by calling sprocs on db server

        Dim taAppendixes As New QTT_InspectionsDataSetsTableAdapters.AppendixesTableAdapter
        Dim intTest As Integer = -1
        Dim intApndxLetter As Integer = 0
        Dim TheCustomPasses As String()
        '   get loop of all appendix defs
        Dim dtApnxDefs As New QTT_LookupsData.tblAppendixDefsDataTable
        Dim taApndxDefs As New QTT_LookupsDataTableAdapters.tblAppendixDefsTableAdapter
        taApndxDefs.FillBy_InspectionType(dtApnxDefs, InspectionType)
        '       Loop through all items returned
        For Each oApndxDef As QTT_LookupsData.tblAppendixDefsRow In dtApnxDefs
            If oApndxDef.IsMultiPass Then
                Dim intPass As Integer
                Dim strPassDescription As String
                For intPass = 1 To NumberOfPasses
                    'adds based on unique pass, appendix def id, inspection id
                    Try
                        TheCustomPasses = strCustomPasses.Split(",")
                    Catch ex As Exception
                        'Otherwise it will just be blank, but that's okay.
                    End Try
                    intTest = taAppendixes.SetupAppendix_InspectionID_MultiPass(InspectionID, intPass, PassesInAlpha, oApndxDef.ID, strPassDescription)
                    If intTest <> 0 Then
                        MsgBox("Unexpected result encountered setting up appendix for new inspection for an unknown reason.", MsgBoxStyle.Information)
                    End If
                Next intPass
                intApndxLetter += intPass  ' increment for subsequent appendix defs
                intApndxLetter -= 1
            Else
                ' non multi-pass appendix type - adds on unique inspection id and type alone
                intApndxLetter += 1
                intTest = taAppendixes.sprocSetupAppendix_SingleItem(InspectionID, oApndxDef.ID, intApndxLetter)
                If intTest <> 0 Then
                    MsgBox("Un-expected result encountered setting up appendix for new inspection for an unknown reason.", MsgBoxStyle.Information)
                End If

            End If
        Next

        Return True
    End Function

    Public Function ResetAppendixSectionItems(ByVal pAppendixSectionID As Integer, ByVal pDeleteExistingItems As Boolean) As Boolean  ', ByVal NumberOfPasses As Integer, ByVal PassesInAlpha As Boolean) As Boolean
        ' setup inspection appendix(es) and default appendsix items by calling sprocs on db server

        Dim taAppendixes As New QTT_InspectionsDataSetsTableAdapters.AppendixesTableAdapter
        Dim taAppendixItems As New QTT_InspectionsDataSetsTableAdapters.vwAppendixItem_DetailsTableAdapter
        Dim intTest As Integer = -1

        ' add missing items
        intTest = taAppendixItems.sprocAddMissingAppendixSectionItems(pAppendixSectionID)
        If intTest <> 0 Then
            MsgBox("Unexpected result encountered setting up appendix for new inspection for an unknown reason.", MsgBoxStyle.Information)
            Return False
        End If

        Return True

    End Function

    ' Private Structure PassInfo
    '    Dim ExpectedFolder As String
    '    Dim ActualPassIdentifier As String
    '    Dim PassNumber As String
    '    Dim PassAlpha As String
    '    Dim PassCustom As String
    '    Dim Location As String
    '    Dim Directory As String
    'End Structure

End Module

