
Public Class cPipeTubeReportDataItems
    ' simple collection class for cPipeTubeReportData objects
    Inherits CollectionBase

    Default Public Property Item(ByVal Index As Integer) As cPipeTubeReportData
        Get
            Return CType(List.Item(Index), cPipeTubeReportData)
        End Get
        Set(ByVal Value As cPipeTubeReportData)
            List.Item(Index) = Value
        End Set
    End Property
    Public Function Add(ByVal Item As cPipeTubeReportData) As Integer
        Return List.Add(Item)
    End Function
End Class

