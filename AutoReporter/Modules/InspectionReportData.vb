
Public Structure InspectionAppendixData
    Public AppendixTitle As String
    Public PassDescription As String
    Public AppendixLetter As String
    Public MS_Word_TemplateName As String
    Public AppendixDescription As String
    Public LinkedAppendixFiles As cReportLinkedFiles
End Structure

Public Structure InspectionReportData
    Public Client_PO As String
    Public ClientContacts As String
    Public CustomerName As String
    Public ToolDescription As String
    Public InspectionDate As Date
    Public LocationDescription As String
    Public Address As String
    Public City As String
    Public State As String
    Public PostalCode As String
    Public Country As String
    Public FooterID As String
    Public QTT_InspectorsOther As String
    Public PrimaryInspectorName As String
    Public PrimaryInspectorPhone As String
    Public PrimaryInspectorTitle As String
    Public InspectionDescription As String
    Public InspectionSummary As String
    Public ExecutiveSummary As String
    Public DetailsSummary As String
    Public DiaGrowthSummary As String
    Public FFSSummary As String
    Public TubeHarvestingSummary As String
    Public PriorHistory As String
    Public QTT_OfficeInfo As String
    Public QTT_ProjectNum As String
    Public QTT_ReportDate As Date
    Public QTT_ReportNumber As String
    Public QTT_ReportVersion As Integer
    Public QTT_ReportRevision As Integer
    Public UnitName As String
    Public UnitNumber As String
    Public UnitService As String
    Public UnitManufacturer As String
    Public PlantProcessType As String
    Public ProcessFlowDirection As String
    Public SpareUninstalled As String
    Public BurnerConfiguration As String
    Public InspectionLayout As String
    Public LinkedReportFiles As cReportLinkedFiles
    Public AppendixDatas As cAppendixReportDatas
    Public MSWord_ReportTemplate As String
    Public NumberOfPasses As Integer
    Public Imperial As Boolean
    Public ClientNum As Boolean
    Public CustomPasses As Boolean
    Public NumberingSystem As Integer
    Public SampleLevel3 As Boolean
    Public JMC As Boolean
End Structure

Public Structure PipeTubeReportData
    Public Allowance As String
    Public Description As String
    Public InnerDims As String
    Public Lengths As String
    Public Material As String
    Public OuterDims As String
    Public SectionsInspected As String
    Public B As String
    Public Surface As String
    Public Thickness As String
    Public Manufacturer As String
    Public OrientationOrDirection As String
    Public Pressure As String
    Public TargetTemperature As String
    Public InspectionLocations As String
    Public AgeAtInspection As String
    Public NumberThermalCycles As String
    Public OperatingHours As String
    Public NextInspectionDate As String
    Public PutIntoServiceDate As String
    Public InspectionThreshold As String
    Public NumberTubesInspected As Integer
    Public NumberTubesTotal As Integer
    Public DegreesMarker As Integer
    Public TolerancedProvidedBy As Integer
    Public SortOrder As Integer
    Public ReportSummaries As cPipeTubeSummaries

End Structure

Public Structure PipeTubeSummary
    Public PassNumber As Integer
    Public SummaryText As String
    Public PassDescription As String
End Structure

Public Structure ReportLinkedFile
    Public LinkedFileName As String
    Public BookMark As String
    Public ReportText As String
    Public FileNotFound As Boolean
    Public IsImage As Boolean
End Structure

Public Class cPipeTubeSummary
    Private mPTI_Sum As PipeTubeSummary

    Public Property SummaryText() As String
        Get
            SummaryText = mPTI_Sum.SummaryText
        End Get
        Set(ByVal value As String)
            mPTI_Sum.SummaryText = value
        End Set

    End Property
    Public Property PassDescription() As String
        Get
            PassDescription = mPTI_Sum.PassDescription
        End Get
        Set(ByVal value As String)
            mPTI_Sum.PassDescription = value
        End Set

    End Property
    Public Property PassNumber() As Integer
        Get
            PassNumber = mPTI_Sum.PassNumber
        End Get
        Set(ByVal value As Integer)
            mPTI_Sum.PassNumber = value
        End Set

    End Property

End Class

Public Class cPipeTubeSummaries
    ' simple collection class for cPipeTubeSummary objects
    Inherits CollectionBase

    Default Public Property Item(ByVal Index As Integer) As cPipeTubeSummary
        Get
            Return CType(List.Item(Index), cPipeTubeSummary)
        End Get
        Set(ByVal Value As cPipeTubeSummary)
            List.Item(Index) = Value
        End Set
    End Property
    Public Function Add(ByVal Item As cPipeTubeSummary) As Integer
        Return List.Add(Item)
    End Function
End Class

Public Class cAppendixReportData
    Private mIAD As InspectionAppendixData

    Public Property AppendixTitle() As String
        Get
            AppendixTitle = mIAD.AppendixTitle
        End Get
        Set(ByVal value As String)
            mIAD.AppendixTitle = value
        End Set

    End Property

    Public Property AppendixLetter() As String
        Get
            AppendixLetter = mIAD.AppendixLetter
        End Get
        Set(ByVal value As String)
            mIAD.AppendixLetter = value
        End Set

    End Property

    Public Property PassDescription() As String
        Get
            PassDescription = mIAD.PassDescription
        End Get
        Set(ByVal value As String)
            mIAD.PassDescription = value
        End Set

    End Property
    Public Property MSWord_TemplateName() As String
        Get
            MSWord_TemplateName = mIAD.MS_Word_TemplateName
        End Get
        Set(ByVal value As String)
            mIAD.MS_Word_TemplateName = value
        End Set

    End Property
    Public Property AppendixDescription() As String
        Get
            AppendixDescription = mIAD.AppendixDescription
        End Get
        Set(ByVal value As String)
            mIAD.AppendixDescription = value
        End Set

    End Property
    Public Property LinkedAppendixFiles() As cReportLinkedFiles
        Get
            If (mIAD.LinkedAppendixFiles Is Nothing) Then
                mIAD.LinkedAppendixFiles = New cReportLinkedFiles
            End If
            LinkedAppendixFiles = mIAD.LinkedAppendixFiles

        End Get
        Set(ByVal value As cReportLinkedFiles)
            mIAD.LinkedAppendixFiles = value
        End Set

    End Property

End Class

Public Class cAppendixReportDatas
    ' simple collection class for cAppendixReportData objects
    Inherits CollectionBase

    Default Public Property Item(ByVal Index As Integer) As cAppendixReportData
        Get
            Return CType(List.Item(Index), cAppendixReportData)
        End Get
        Set(ByVal Value As cAppendixReportData)
            List.Item(Index) = Value
        End Set
    End Property

    Public Function Add(ByVal Item As cAppendixReportData) As Integer
        Return List.Add(Item)
    End Function
End Class

Public Class cReportLinkedFile
    Private mRIF As ReportLinkedFile

    Public Property LinkedFileName() As String
        Get
            LinkedFileName = mRIF.LinkedFileName
        End Get
        Set(ByVal value As String)
            mRIF.LinkedFileName = value
        End Set

    End Property
    Public Property BookMark() As String
        Get
            BookMark = mRIF.BookMark
        End Get
        Set(ByVal value As String)
            mRIF.BookMark = value
        End Set

    End Property
    Public Property ReportText() As String
        Get
            ReportText = mRIF.ReportText
        End Get
        Set(ByVal value As String)
            mRIF.ReportText = value
        End Set

    End Property
    Public Property FileNotFound() As Boolean
        Get
            FileNotFound = mRIF.FileNotFound
        End Get
        Set(ByVal value As Boolean)
            mRIF.FileNotFound = value
        End Set

    End Property
    Public Property IsImageFile() As Boolean
        Get
            IsImageFile = mRIF.IsImage
        End Get
        Set(ByVal value As Boolean)
            mRIF.IsImage = value
        End Set

    End Property

    Public Overrides Function ToString() As String
        Return mRIF.LinkedFileName
    End Function
End Class

Public Class cReportLinkedFiles
    ' simple collection class for cReportLinkedFile objects
    Inherits CollectionBase

    Default Public Property Item(ByVal Index As Integer) As cReportLinkedFile
        Get
            Return CType(List.Item(Index), cReportLinkedFile)
        End Get
        Set(ByVal Value As cReportLinkedFile)
            List.Item(Index) = Value
        End Set
    End Property

    Public Function Add(ByVal Item As cReportLinkedFile) As Integer
        Return List.Add(Item)
    End Function
End Class