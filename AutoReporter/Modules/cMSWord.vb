Imports Microsoft.Office.Interop
Imports Microsoft.Office.Core
Imports Microsoft.Office.Interop.Word

Public Class cMSWord

    Public Shared wd As Microsoft.Office.Interop.Word.Application
    Public Shared wdoc As Microsoft.Office.Interop.Word.Document
    Public wdEvents As Microsoft.Office.Interop.Word.ApplicationEvents2_Event

    Public Function RunMSWord() As Boolean
        ' ensures that AutoReporter version instance of MSWord is running / starts new instance if not
        Dim WordAppReady As Boolean = False
        ' Start Word
        wd = New Microsoft.Office.Interop.Word.Application
        'wd.Visible = False
        ' Create a Quit event handler.
        wdEvents = CType(wd, Word.ApplicationEvents2_Event)
        AddHandler wdEvents.Quit, AddressOf Me.handlerQuit

    End Function

    Public Function CreateNewTemplateDoc(ByVal pTemplateFileName As String, ByVal pPaperSize_A4 As Boolean) As Boolean
        ' creates the report document from the specified template
        ' if pPaperSize_A4 = True, uses "A4 Mode", in which either a specific A4 version is used, or the current template is 'brute forced' into A4

        Dim strStartupPath As String = System.Windows.Forms.Application.StartupPath
        'KM 7/8/2011
        Dim strAppDataPath As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
        Const TEMPLATE_FOLDER = "\AutoReporter\Templates\"
        Const DEV_TEMPLATE_FOLDER = "\ReportTemplates\"

        'KM 4/9/13
        'Use startup location if it exists (for debugging during development), otherwise use template folder
        If System.IO.File.Exists(strStartupPath & DEV_TEMPLATE_FOLDER & pTemplateFileName) Then
            wdoc = CreateWordDocFromTemplate(strStartupPath & DEV_TEMPLATE_FOLDER & pTemplateFileName, pPaperSize_A4)
        Else
            wdoc = CreateWordDocFromTemplate(strAppDataPath & TEMPLATE_FOLDER & pTemplateFileName, pPaperSize_A4)
        End If
        CreateNewTemplateDoc = True

    End Function

    Function CreateWordDocFromTemplate(ByVal pFullTemplateName As String, ByVal pPaperSize_A4 As Boolean) As Word.Document

        ' added May 2007 to support 'A4' size option - when pPaperSize_A4 = True, either uses specific A4 template (if it exists), or call page setup if it does not
        Const A4_PAPER_SUFFIX = " A4"
        Dim oDoc As Word.Document
        Dim WordTemplateFullName As String = pFullTemplateName
        Dim A4_TemplateFullName As String = ""
        Dim Forced_A4 As Boolean

        ' May 2007 test for A4 version as needed
        If pPaperSize_A4 = True Then
            With pFullTemplateName
                A4_TemplateFullName = .Substring(0, .LastIndexOf(".")) & A4_PAPER_SUFFIX & .Substring(.LastIndexOf("."))
            End With
            If My.Computer.FileSystem.FileExists(A4_TemplateFullName) Then
                WordTemplateFullName = A4_TemplateFullName
            Else
                Forced_A4 = True
            End If
        End If

        ' create word doc
        Call RunMSWord()

        oDoc = wd.Documents.Add(WordTemplateFullName)
        If Forced_A4 Then
            With oDoc.PageSetup
                .PaperSize = Word.WdPaperSize.wdPaperA4
            End With
        End If

        Return oDoc

    End Function

    Public Function OpenDocument(ByVal strFullPath) As Boolean

        Call RunMSWord()

        wd.Documents.Open(strFullPath)
        OpenDocument = True

    End Function

    Public Sub DeleteBookmark(ByVal strBM As String)

        If wdoc.Bookmarks.Exists(strBM) Then
            wdoc.Bookmarks.Item(strBM).Range.Delete()
        End If

    End Sub

    Public Sub InsertAtBookMark(ByVal strBM As String, ByVal strText As String)
        ' generic function that inserts text at a bookmark
        ' ensure bookmark exists
        If wdoc.Bookmarks.Exists(strBM) Then
            wdoc.Bookmarks.Item(strBM).Range.Text = strText
        End If
    End Sub

    Public Sub InsertAtBookMark_Appendix(ByVal strBM As String, ByVal strText As String)
        If wdoc.Bookmarks.Exists(strBM) Then
            wdoc.Bookmarks.Item(strBM).Range.Text = strText
        End If
    End Sub

    Public Sub InsertEquation(ByVal strBM As String, ByVal bImperial As Boolean)

        'Added to insert equation in LOTIS/MANTIS report from AutoText entry stored in report
        Dim oAT As Word.AutoTextEntry
        Dim oTemplate As Word.Template
        Dim strAutoText As String

        If wdoc.Bookmarks.Exists(strBM) Then
            oTemplate = wdoc.AttachedTemplate

            If bImperial Then
                strAutoText = "ImperialEquation"
            Else
                strAutoText = "MetricEquation"
            End If

            wdoc.Bookmarks.Item(strBM).Range.Select()
            For Each oAT In oTemplate.AutoTextEntries
                If oAT.Name = strAutoText Then
                    oTemplate.AutoTextEntries(strAutoText).Insert(Where:=wdoc.ActiveWindow.Selection.Range, RichText:=True)
                End If
            Next

        End If

    End Sub

    Public Sub InsertJMCLogo(ByVal strBM As String)
        Dim oAT As Word.AutoTextEntry
        Dim oTemplate As Word.Template
        Dim strAutoText As String

        If wdoc.Bookmarks.Exists(strBM) Then
            oTemplate = wdoc.AttachedTemplate

            strAutoText = "JMC"

            wdoc.Bookmarks.Item(strBM).Range.Select()
            For Each oAT In oTemplate.AutoTextEntries
                If oAT.Name = strAutoText Then
                    oTemplate.AutoTextEntries(strAutoText).Insert(Where:=wdoc.ActiveWindow.Selection.Range, RichText:=True)
                End If
            Next

        End If


    End Sub


    Public Sub PasteAtBookMark(ByVal strBM As String, ByVal strText As String)
        ' ensure bookmark exists and text is not blank
        If wdoc.Bookmarks.Exists(strBM) And strText <> "" Then
            Dim oBMk As Word.Bookmark = wdoc.Bookmarks.Item(strBM)
            Dim oRng As Word.Range = wdoc.Bookmarks.Item(strBM).Range
            Clipboard.Clear()
            Clipboard.SetText(strText, System.Windows.Forms.TextDataFormat.Rtf)
            oRng.Select()
            oRng.PasteAndFormat(Word.WdRecoveryType.wdFormatSurroundingFormattingWithEmphasis)
        End If
    End Sub

    Public Sub PasteAtBookMarkTrim(ByVal strBM As String, ByVal strText As String)
        ' ensure bookmark exists and text is not blank
        If wdoc.Bookmarks.Exists(strBM) And strText <> "" Then
            Dim oBMk As Word.Bookmark = wdoc.Bookmarks.Item(strBM)
            Dim oRng As Word.Range = wdoc.Bookmarks.Item(strBM).Range
            Clipboard.Clear()
            Clipboard.SetText(strText, System.Windows.Forms.TextDataFormat.Rtf)
            oRng.Select()
            oRng.PasteAndFormat(Word.WdRecoveryType.wdFormatSurroundingFormattingWithEmphasis)
            'Replace the extra return character in table cell
            wdoc.ActiveWindow.Selection.SelectCell()
            oRng = wdoc.ActiveWindow.Selection.Range
            With oRng.Find
                .ClearFormatting()
                .Text = "^p"
                .Replacement.Text = ""
                .Replacement.ClearFormatting()
                .Forward = True
                .Wrap = Word.WdFindWrap.wdFindStop
                .Execute(Replace:=Word.WdReplace.wdReplaceAll)
            End With
        End If
    End Sub

    Public Sub PasteAtBookMarkBold(ByVal strBM As String, ByVal strText As String)
        ' ensure bookmark exists and text is not blank
        If wdoc.Bookmarks.Exists(strBM) And strText <> "" Then
            Dim oBMk As Word.Bookmark = wdoc.Bookmarks.Item(strBM)
            Dim oRng As Word.Range = wdoc.Bookmarks.Item(strBM).Range
            Clipboard.Clear()
            Clipboard.SetText(strText, System.Windows.Forms.TextDataFormat.Rtf)
            oRng.Select()
            oRng.PasteAndFormat(Word.WdRecoveryType.wdFormatSurroundingFormattingWithEmphasis)

            'Bold last sentence in each paragraph
            'Required for Tube Harvesting Summary
            oRng.Select()
            Dim oPara As Word.Range
            Dim p As Word.Paragraph
            Dim i As Integer
            Dim j As Integer

            For j = 1 To 3
                oPara = wdoc.ActiveWindow.Selection.Paragraphs(1).Range
                i = oPara.Sentences.Count
                oPara.Sentences(i).Font.Bold = True
                wdoc.ActiveWindow.Selection.MoveDown(Unit:=Word.WdUnits.wdParagraph, Count:=1)
            Next j

            oRng.Select()
            wdoc.ActiveWindow.Selection.MoveDown(Unit:=Word.WdUnits.wdParagraph, Count:=3, Extend:=Word.WdMovementType.wdExtend)
            oRng = wdoc.ActiveWindow.Selection.Range
            With oRng.Find
                .ClearFormatting()
                .Font.Bold = True
                .Text = "^p"
                .Replacement.Text = "^p"
                .Replacement.ClearFormatting()
                .Replacement.Font.Bold = False
                .Forward = True
                .Wrap = Word.WdFindWrap.wdFindStop
                .Format = True
                .Execute(Replace:=Word.WdReplace.wdReplaceAll)
            End With
        End If
    End Sub

    Public Sub InsertWordDocAtBookMark(ByVal strBM As String, ByVal strFullFilename As String)

        ' ensure bookmark exists
        If wdoc.Bookmarks.Exists(strBM) Then
            wdoc.Bookmarks(strBM).Range.InsertFile(strFullFilename)
        End If
    End Sub

    Public Sub InsertWordDocAtBookMark_Appendix(ByVal strBM As String, ByVal strFullFilename As String)

        If wdoc.Bookmarks.Exists(strBM) Then
            wdoc.Bookmarks(strBM).Range.InsertFile(strFullFilename)
            If strBM = "BM_TABLE_ROW_SORT"
                For Each d As Table In wdoc.Bookmarks(strBM).Range.Tables
                    d.AllowAutoFit = True
                    d.AutoFitBehavior(WdAutoFitBehavior.wdAutoFitWindow)
                Next
            End If
        End If
    End Sub

    Public Sub InsertImageAtBookMark(ByVal strBM As String, ByVal strFullFilename As String)

        If wdoc.Bookmarks.Exists(strBM) Then
            Try
                wdoc.Bookmarks.Item(strBM).Range.InlineShapes.AddPicture(FileName:=strFullFilename, LinkToFile:= _
                False, SaveWithDocument:=True)
            Catch
                'MsgBox(strFullFilename)
                'wdoc.Bookmarks.Item(strBM).Range.Text = strText
            End Try
        End If

    End Sub

    Public Sub InsertImageAtBookMark_Appendix(ByVal strBM As String, ByVal strFullFilename As String, ByVal fPrepForNextImage As Boolean, ByVal fSizing_2_PerPage As Boolean)

        Const PICTURE_HEIGHT_2UP_PORTRAIT = 325  '(approx 4.5 inches, in Points) 
        Dim oRng As Word.Range
        Dim oPict As Word.InlineShape

        If wdoc.Bookmarks.Exists(strBM) Then
            oRng = wdoc.Bookmarks(strBM).Range
            oPict = oRng.InlineShapes.AddPicture(FileName:=strFullFilename, LinkToFile:= _
                    False, SaveWithDocument:=True)
            If fSizing_2_PerPage Then
                oPict.LockAspectRatio = Microsoft.Office.Core.MsoTriState.msoTrue
                oPict.Height = PICTURE_HEIGHT_2UP_PORTRAIT
            End If

            If strBM = "BM_TUBE_IMAGE" AndAlso oPict.Width > 504 Then
                oPict.Width = 504
            End If
            If fPrepForNextImage Then
                ' add carriage return
                oRng.Start = oRng.Start + 1
                oRng.InsertAfter(vbCr)
                ' redefine bookmark to new line
                wdoc.Bookmarks(strBM).Start = oRng.Start + 1
            End If
            ' cleanup
            oRng = Nothing
        End If
    End Sub

    Public Sub SetDocProp(ByVal Prop As String, ByVal Value As Boolean)
        'Set document properties in Word (storing values to be used by fields within templates)
        wdoc.CustomDocumentProperties(Prop) = Value

    End Sub

    Public Function SaveReportFile(ByVal strFullName As String) As Boolean
        ' performs save of report file to disk
        If (wdoc Is Nothing) Then
            MsgBox("Can't save Report File - no current Report File is loaded", MsgBoxStyle.Information, "cMSWord.vb.SaveReportFile")
            Exit Function
        End If
        wdoc.SaveAs(strFullName)
        SaveReportFile = True

    End Function

    Public Sub ReleaseCurrentReportFile()
        ' releases object reference to current report file
        If Not (wdoc Is Nothing) Then
            wdoc = Nothing
        End If

    End Sub

    Private Sub handlerQuit()

        ' General clean-up when Word quits.
        wd = Nothing
        GC.Collect()
        GC.WaitForPendingFinalizers()

    End Sub

    Public Shared Sub CleanUpReport()
        'KM 12/5/2011
        Dim strChar As String
        Dim myRange As Word.Range

        myRange = wdoc.Content

        'Replace multiple spaces with one space 
        With myRange.Find
            .ClearFormatting()
            .Text = "  "
            .Replacement.Text = " "
            .Forward = True
            .Wrap = Word.WdFindWrap.wdFindContinue
            .Execute(Replace:=Word.WdReplace.wdReplaceAll)
            Do While .Found = True
                .Execute(Replace:=Word.WdReplace.wdReplaceAll)
            Loop
        End With

        'Go back to the top of the document
        wdoc.ActiveWindow.Selection.HomeKey(Unit:=Word.WdUnits.wdStory)




        'Replace Quest Integrity Group with Quest Integrity 
        With myRange.Find
            .ClearFormatting()
            .Text = "Quest Integrity Group"
            .Replacement.Text = "Quest Integrity"
            .Forward = True
            .Wrap = Word.WdFindWrap.wdFindContinue
            .Execute(Replace:=Word.WdReplace.wdReplaceAll)
            Do While .Found = True
                .Execute(Replace:=Word.WdReplace.wdReplaceAll)
            Loop
        End With

        'Go back to the top of the document
        wdoc.ActiveWindow.Selection.HomeKey(Unit:=Word.WdUnits.wdStory)



        'Change text to italics
        With wdoc.ActiveWindow.Selection.Find
            .ClearFormatting()
            .Replacement.ClearFormatting()
            .Text = "(unless otherwise noted)"
            .Replacement.Text = "(unless otherwise noted)"
            .Replacement.Font.Italic = True
            .Forward = True
            .Wrap = Word.WdFindWrap.wdFindContinue
            .Format = True
        End With
        wdoc.ActiveWindow.Selection.Find.Execute(Replace:=Word.WdReplace.wdReplaceAll)

                   'Change text to italics
        With wdoc.ActiveWindow.Selection.Find
            .ClearFormatting()
            .Replacement.ClearFormatting() 
            .Text = "Pass Level 1 Assessment (Green):"
            .Replacement.Text = "Pass Level 1 Assessment (Green):"
            .Replacement.Font.Underline = True
            .Replacement.Font.Color = Information.RGB(227, 114, 34)
            .Forward = True
            .Wrap = Word.WdFindWrap.wdFindContinue
            .Format = True
        End With
        wdoc.ActiveWindow.Selection.Find.Execute(Replace:=Word.WdReplace.wdReplaceAll)

                With wdoc.ActiveWindow.Selection.Find
            .ClearFormatting()
            .Replacement.ClearFormatting() 
            .Text = "Fail Level 1 Assessment, Consult Engineering (Yellow):"
            .Replacement.Text = "Fail Level 1 Assessment, Consult Engineering (Yellow):"
            .Replacement.Font.Underline = True
            .Replacement.Font.Color = Information.RGB(227, 114, 34)
            .Forward = True
            .Wrap = Word.WdFindWrap.wdFindContinue
            .Format = True
        End With
        wdoc.ActiveWindow.Selection.Find.Execute(Replace:=Word.WdReplace.wdReplaceAll)

                With wdoc.ActiveWindow.Selection.Find
            .ClearFormatting()
            .Replacement.ClearFormatting() 
            .Text = "Fail Level 1 Assessment, Retire Now (Red):"
            .Replacement.Text = "Fail Level 1 Assessment, Retire Now (Red):"
            .Replacement.Font.Underline = True
            .Replacement.Font.Color = Information.RGB(227, 114, 34)
            .Forward = True
            .Wrap = Word.WdFindWrap.wdFindContinue
            .Format = True
        End With
        wdoc.ActiveWindow.Selection.Find.Execute(Replace:=Word.WdReplace.wdReplaceAll)



        'Go back to the top of the document
        wdoc.ActiveWindow.Selection.HomeKey(Unit:=Word.WdUnits.wdStory)

        'Replace double paragraph marks with single
        'Repeat code because looping in find/replace is causing an infinite loop in Word
        For i = 1 To 2
            'Go back to the top of the document
            wdoc.ActiveWindow.Selection.HomeKey(Unit:=Word.WdUnits.wdStory)
            With myRange.Find
                .Text = "^p^p"
                .ClearFormatting()
                .Replacement.Text = "^p"
                .Replacement.ClearFormatting()
                .Forward = True
                .Wrap = Word.WdFindWrap.wdFindContinue
                'Do While .Found = True
                .Execute(Replace:=Word.WdReplace.wdReplaceAll)
                'Loop
            End With
        Next i

        'Remove the remaining extra return characters. Search and replace gets caught in loop and doesn't get all the doubles.
        wdoc.ActiveWindow.Selection.HomeKey(Unit:=Word.WdUnits.wdStory)
        wdoc.ActiveWindow.Selection.Find.ClearFormatting()
        wdoc.ActiveWindow.Selection.Find.Replacement.ClearFormatting()
        With wdoc.ActiveWindow.Selection.Find
            .Text = "^p^p"
            .Forward = True
            .Wrap = Word.WdFindWrap.wdFindContinue
            .Execute()
            Do While .Found = True
                With wdoc.ActiveWindow.Selection
                    .MoveRight(Unit:=Word.WdUnits.wdCharacter, Count:=1)
                    .MoveLeft(Unit:=Word.WdUnits.wdCharacter, Count:=1)
                    .Delete(Unit:=Word.WdUnits.wdCharacter, Count:=1)
                End With
                .Execute()
            Loop
        End With

        'Go back to the top of the document
        'wdoc.ActiveWindow.Selection.HomeKey(Unit:=Word.WdUnits.wdStory)

        'In FTIS, wall thickness table fix goes here
        'Add code if tables in LOTIS/MANTIS need to be formatted

        'Go back to the top of the document
        wdoc.ActiveWindow.Selection.HomeKey(Unit:=Word.WdUnits.wdStory)

        'Update all the fields codes (tricky because of the ASK fields)
        With wdoc.ActiveWindow
            'Show the field codes
            .View.ShowFieldCodes = True

            'Temporarily remove the ASK fields (so the user isn't prompted to update them)
            .Selection.GoTo(What:=Word.WdGoToItem.wdGoToBookmark, Name:="BM_ASK_FIELDS")
            .Selection.Cut()

            'Update field codes
            .Selection.WholeStory()
            .Selection.Fields.Update()
            'Go back to the top of the document
            .Selection.HomeKey(Unit:=Word.WdUnits.wdStory)

            'Paste back the ASK fields
            .Selection.PasteAndFormat(Word.WdPasteOptions.wdKeepSourceFormatting)
            .View.ShowFieldCodes = False
        End With

        'Repaginate so the TOC page numbers are okay
        wdoc.Repaginate()

        'update TOCs (should be does with update fields above, but without repaginate, sometimes doesn't work)
        Dim toc As Word.TableOfContents
        For Each toc In wdoc.TablesOfContents
            toc.Update()
            'toc.UpdatePageNumbers()
        Next

        'Go into PrintPreview (to force update the footers)
        wdoc.PrintPreview()
        wdoc.ClosePrintPreview()

    End Sub


    Private Shared Sub Selection()
        Throw New NotImplementedException
    End Sub

End Class
