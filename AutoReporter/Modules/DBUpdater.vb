﻿Public Class DBUpdater

#Region "Address Updating"

    Public Shared Sub UpdateAddresses()
        'Get the data that we're going to try to update.
        Dim adapter As QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblQTT_OfficesTableAdapter = New QTT_LookupsDataTableAdapters.tblQTT_OfficesTableAdapter
        Dim dataTable As QIG.AutoReporter.QTT_LookupsData.tblQTT_OfficesDataTable = New QTT_LookupsData.tblQTT_OfficesDataTable
        adapter.Fill(dataTable)

        'Generate a dictionary containing the current addresses that we will attempt to update. 
        'Also include a flag that will show whether it was found or not, indicating if it needs to be created.
        GenerateAddressList()
        Dim updatedAddresses As List(Of Address) = New List(Of Address)
        updatedAddresses.Add(Kent)
        updatedAddresses.Add(Webster)
        updatedAddresses.Add(Calgary)
        updatedAddresses.Add(Vianen)
        updatedAddresses.Add(AbuDhabi)

        ' Go through all the addresses in the database and update.
        Dim rowsToDelete As List(Of DataRow) = New List(Of DataRow)
        For rowNum = 0 To dataTable.Rows.Count - 1
            Dim descriptionSnippet As String = dataTable.Rows(rowNum)("Description").ToString().ToLower().Substring(0, 4)
            Dim shouldStay As Boolean = False
            ' go through each known address and try to match it using the snippet.
            For Each add In updatedAddresses
                If add.Description.ToLower().Contains(descriptionSnippet) Then
                    Call UpdateThisAddress(dataTable.Rows(rowNum), add)
                    add.Found = True
                    shouldStay = True
                End If
            Next
            If Not shouldStay Then
                rowsToDelete.Add(dataTable.Rows(rowNum))
            End If
        Next

        'delete rows we don't need anymore
        'My bad, can't delete these rows due to constraints.

        'Create any that weren't found
        For Each add In updatedAddresses
            If Not add.Found Then
                Call CreateNewAddress(dataTable, add)
            End If
        Next
        adapter.Update(dataTable)
    End Sub

    Private Shared Sub CreateNewAddress(dataTable As QTT_LookupsData.tblQTT_OfficesDataTable, address As Address)
        dataTable.AddtblQTT_OfficesRow(address.Description, address.Address, address.City, address.State, address.PostalCode, address.Country, address.Phone, address.Fax, Nothing)
    End Sub

    Private Shared Sub UpdateThisAddress(dataRow As DataRow, address As Address)
        dataRow("Description") = address.Description
        dataRow("Address") = address.Address
        dataRow("City") = address.City
        dataRow("State") = address.State
        dataRow("PostalCode") = address.PostalCode
        dataRow("Country") = address.Country
        dataRow("Phone") = address.Phone
        dataRow("Fax") = address.Fax
    End Sub

    Private Class Address
        Public ID As Int32
        Public Description As String
        Public Address As String
        Public City As String
        Public State As String
        Public PostalCode As String
        Public Country As String
        Public Phone As String
        Public Fax As String
        Public Found As Boolean
    End Class

    Private Shared Kent As Address
    Private Shared Webster As Address
    Private Shared Calgary As Address
    Private Shared Vianen As Address
    Private Shared AbuDhabi As Address

    Private Shared Sub GenerateAddressList()

        Kent = New Address()
        Kent.ID = 1
        Kent.Description = "Kent, Washington, USA"
        Kent.Address = "19823 58th Place South, Suite 100"
        Kent.City = "Kent"
        Kent.State = "Washington"
        Kent.PostalCode = "98032"
        Kent.Country = "United States"
        Kent.Phone = "+1.253.893.7070"
        Kent.Fax = "+1.253.893.7075"

        Webster = New Address()
        Webster.ID = 3
        Webster.Description = "Webster, Texas, USA"
        Webster.Address = "17146 Feathercraft Lane, #350"
        Webster.City = "Webster"
        Webster.State = "Texas"
        Webster.PostalCode = "77598"
        Webster.Country = "United States"
        Webster.Phone = "+1.281.786.4700"
        Webster.Fax = "+1.281.786.4719"

        Calgary = New Address()
        Calgary.ID = 4
        Calgary.Description = "Calgary, AB, CAN"
        Calgary.Address = "1339 40 Avenue NE, #27"
        Calgary.City = "Calgary"
        Calgary.State = "Alberta"
        Calgary.PostalCode = "T2E 8N6"
        Calgary.Country = "Canada"
        Calgary.Phone = "+1.403.273.0051"
        Calgary.Fax = "+1.403.273.0070"

        Vianen = New Address()
        Vianen.Description = "Vianen, Netherlands"
        Vianen.Address = "Kamerlingh Onnesweg 3"
        Vianen.City = "4131 PK Vianen"
        Vianen.State = Nothing
        Vianen.PostalCode = Nothing
        Vianen.Country = "Netherlands"
        Vianen.Phone = "+31.0.347.320.140"
        Vianen.Fax = Nothing

        AbuDhabi = New Address()
        AbuDhabi.ID = 14
        AbuDhabi.Description = "Saudi Arabia"
        AbuDhabi.Address = "PO Box 130403, Zayed 2nd Street (Electra Street) Ali Bin Khalfan Al Dhahry Bldg., (Ali & Sons Building)"
        AbuDhabi.City = "Abu Dhabi"
        AbuDhabi.State = Nothing
        AbuDhabi.PostalCode = Nothing
        AbuDhabi.Country = "UAE"
        AbuDhabi.Phone = "+011.971.02.671.8961"
        AbuDhabi.Fax = "+011.971.02.674.1476"
    End Sub

#End Region

#Region "Inspector Name Updating"

    Shared Sub UpdateInspectors()
        'Get the data that we're going to try to update.
        Dim adapter As QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblQTT_InspectorsTableAdapter = New QTT_LookupsDataTableAdapters.tblQTT_InspectorsTableAdapter
        Dim dataTable As QIG.AutoReporter.QTT_LookupsData.tblQTT_InspectorsDataTable = New QIG.AutoReporter.QTT_LookupsData.tblQTT_InspectorsDataTable
        adapter.Fill(dataTable)

        'Generate a dictionary containing the current addresses that we will attempt to update. 
        'Also include a flag that will show whether it was found or not, indicating if it needs to be created.
        Dim nameList As List(Of Inspector) = GenerateNameList()

        ' Go through all the addresses in the database and update.
        Dim rowsToDelete As List(Of DataRow) = New List(Of DataRow)
        For rowNum = 0 To dataTable.Rows.Count - 1
            'Console.WriteLine("Found " + dataTable.Rows(rowNum)("LastName").ToString())
            Dim lastName As String = dataTable.Rows(rowNum)("LastName").ToString().ToLower()
            Dim shouldStay As Boolean = False
            ' go through each known address and try to match it using the snippet.
            For Each name In nameList
                If name.LastName.ToLower().Contains(lastName) Then
                    Call UpdateThisInspector(dataTable.Rows(rowNum), name)
                    name.Found = True
                    shouldStay = True
                End If
            Next
            If Not shouldStay Then
                rowsToDelete.Add(dataTable.Rows(rowNum))
            End If
        Next

        'delete rows we don't need anymore
        ' Never mind, can't delete these due to DB constraints

        'Create any that weren't found
        Try
            For Each name In nameList
                If Not name.Found Then
                    Call CreateNewInspector(dataTable, name)
                End If
            Next

            adapter.Update(dataTable)
        Catch ex As Exception
            Dim s As String = "uhoh"
        End Try

    End Sub

    Private Shared Sub CreateNewInspector(dataTable As QTT_LookupsData.tblQTT_InspectorsDataTable, insp As Inspector)
        Dim newRow As QTT_LookupsData.tblQTT_InspectorsRow = dataTable.AddtblQTT_InspectorsRow(insp.LastName, insp.FirstName, insp.PhoneNumber, False, Nothing, insp.Salutation, Nothing, Nothing)
    End Sub

    Private Shared Sub UpdateThisInspector(dataRow As DataRow, insp As Inspector)
        dataRow("LastName") = insp.LastName
        dataRow("FirstName") = insp.FirstName
        dataRow("Salutation") = insp.Salutation
        dataRow("PhoneNumber") = insp.PhoneNumber
    End Sub

    Private Shared Function GenerateNameList()
        Dim inspectors As List(Of Inspector) = New List(Of Inspector)
        
        'alive
        inspectors.Add(New Inspector("Ahmed", "Mohammad", "Mr.", "+587.433.0786"))
        inspectors.Add(New Inspector("Cazares", "Adrian", "Mr.", "+1.281.508.5894"))
        inspectors.Add(New Inspector("Licina", "Stefan", "Mr.", "+1.403.903.4209"))
        inspectors.Add(New Inspector("Luong", "Bobby", "Mr.", "+1.281.898.2477"))
        inspectors.Add(New Inspector("Mandafero", "Abrarawe", "Mr.", "+1.832.985.3722"))
        inspectors.Add(New Inspector("Zinna", "Robert", "Mr.", "+1.281.508.5856"))
        inspectors.Add(New Inspector("Bell-Bacon", "Richard", "Mr.", "+1.206.708.9512"))
        inspectors.Add(New Inspector("Bistel", "Matthew", "Mr.", "+1.206.653.5077"))
        inspectors.Add(New Inspector("Bosman", "Dane", "Mr.", "+1.403.818.1859"))
        inspectors.Add(New Inspector("Crisostomo", "Jesse", "Mr.", "+1.206.999.2814"))
        inspectors.Add(New Inspector("Cross", "Graham", "Mr.", "+1.403.200.2940"))
        inspectors.Add(New Inspector("Gandhi", "Amit", "Mr.", "+1.403.671.4307"))
        inspectors.Add(New Inspector("Janardhanan", "Jithin", "Mr.", "+971.56.3951681"))
         inspectors.Add(New Inspector("Keijser", "Arno", "Mr.", "+31.6.533.560.51"))
         inspectors.Add(New Inspector("Kutty", "Liyad", "Mr.", "+971.12.6718961"))
         inspectors.Add(New Inspector("Reyes", "Ruel", "Mr.", "+971.56.3259035"))
        inspectors.Add(New Inspector("Gjersee", "Tyler", "Mr.", "+1.206.422.2372"))
        inspectors.Add(New Inspector("Jacob", "Nitin", "Mr.", "+1.206.653.5078"))
        inspectors.Add(New Inspector("Lutkenhouse", "William", "Mr.", "+1.206.240.0981"))
        inspectors.Add(New Inspector("Reduan", "Amir", "Mr.", "+1.206.519.4513"))
        inspectors.Add(New Inspector("Xavier", "Albinus", "Mr.", "+60.012.211.9970"))
        inspectors.Add(New Inspector("Jefferies", "Nathan", "Mr.", "+1.713.560.5808"))
        inspectors.Add(New Inspector("Perez", "Anmanual", "Mr.", "+1.281.785.3431"))
        inspectors.Add(New Inspector("Azad", "Purayath", "Mr.", "+011.971.563.230.933"))
        inspectors.Add(New Inspector("Durocher", "Raphael", "Mr.", "+1.403.461.1908"))
        inspectors.Add(New Inspector("Sergerie", "Philippe", "Mr.", "+1.587.891.9823"))
        inspectors.Add(New Inspector("Ferrer", "Ed", "Mr.", "+1.253.220.5811"))
        inspectors.Add(New Inspector("Hardy", "Joel", "Mr.", "+1.206.495.1140"))
        inspectors.Add(New Inspector("Nettles", "Dave", "Mr.", "+1.206.651.6303"))
        inspectors.Add(New Inspector("Crossley", "Lucas", "Mr.", "+1.713.471.8970"))
        inspectors.Add(New Inspector("Lee", "Charles", "Mr.", "+1.713.855.0374"))
        inspectors.Add(New Inspector("Lister", "Josh", "Mr.", "+1.832.330.3356"))
        inspectors.Add(New Inspector("Snow", "Daniel", "Mr.", "+3.161.271.0970"))
        inspectors.Add(New Inspector("Weaver", "Johnny", "Mr.", "+1.281.910.0617"))

       
        '''
        'Dead
        'inspectors.Add(New Inspector("Tamboong", "Ella", "Mr.", "+971.52.7988508"))
        'inspectors.Add(New Inspector("LeBlanc", "Will", "Mr.", "+1.403.612.1316"))
        'inspectors.Add(New Inspector("Arikan", "Musa", "Mr.", "+3.161.342.2990"))
        'inspectors.Add(New Inspector("Heijmans", "Marco", "Mr.", "+3.168.321.4908"))
        'inspectors.Add(New Inspector("Smeins", "Henk", "Mr.", "+11.31.652.632.603"))
        'inspectors.Add(New Inspector("Anderson", "Jeffery", "Mr.", "+1.713.855.0374"))
        'inspectors.Add(New Inspector("Meakin", "Chris", "Mr.", "+011.971.501.881.985"))
        'inspectors.Add(New Inspector("Flenniken", "Mike", "Mr.", "+1.206.554.1579"))
        'inspectors.Add(New Inspector("Babcock", "Tony", "Mr.", "+1.713.452.9366"))
        'inspectors.Add(New Inspector("Baros", "Matt", "Mr.", "+1.832.244.0641"))
        'inspectors.Add(New Inspector("Ellis", "Robert", "Mr.", "+1.832.691.0229"))
        'inspectors.Add(New Inspector("Haugen", "Tim", "Mr.", "+1.832.250.7753"))
        
        Return inspectors
    End Function
    Private Class Inspector
        Sub New(ByVal last As String, ByVal first As String, ByVal sal As String, ByVal phone As String)
            FirstName = first
            LastName = last
            Salutation = sal
            PhoneNumber = phone
        End Sub

        Public LastName As String
        Public FirstName As String
        Public Salutation As String
        Public PhoneNumber As String
        Public Found As Boolean
    End Class
#End Region
End Class
