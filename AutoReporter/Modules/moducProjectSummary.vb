﻿'Module moducProjectSummary
'#Region "SummaryData Class Definitions"
'    Public Class SummaryData
'        'This is the highest level class that contains all the data that is ever entered. Subsections of data are split into subClasses that are created when a new instance of this class is created.
'        Public PassNumber As String
'        Public Units As String
'        Public Appendix As String
'        Public NewUsedPipes As String
'        Public GeneralOrSectionSummary As String
'        Public MyPipeData As PipeData
'        Public MyBendData As BendData
'        Public MyDeformationData As DeformationData

'        Public Sub New()
'            'When a new SummaryData class is instantiated, create
'            MyPipeData = New PipeData
'            MyBendData = New BendData
'            MyDeformationData = New DeformationData
'        End Sub
'    End Class


'    Public Class PipeData
'        Private Const NumberOfTextboxesEach As Integer = 8
'        Public ID(NumberOfTextboxesEach - 1)
'        Public MinWall(NumberOfTextboxesEach - 1)
'        Public Location(NumberOfTextboxesEach - 1)
'        Public EstWallLoss(NumberOfTextboxesEach - 1)
'        Public EstRemWall(NumberOfTextboxesEach - 1)
'        Public AvgWall(NumberOfTextboxesEach - 1)
'        Public DesignedWall(NumberOfTextboxesEach - 1)
'        Public AvgDiameter(NumberOfTextboxesEach - 1)
'        Public GeneralOrLocal As String
'        Public InternalOrExternal As String
'        Public PipeSize As String
'        Public PipeSchedule As String
'    End Class

'    Public Class BendData
'        Private Const NumberOfTextboxesEach As Integer = 8
'        Public ID(NumberOfTextboxesEach - 1)
'        Public MinWall(NumberOfTextboxesEach - 1)
'        Public EstWallLoss(NumberOfTextboxesEach - 1)
'        Public EstRemWall(NumberOfTextboxesEach - 1)
'        Public GeneralOrLocal As String
'        Public InternalOrExternal As String
'        Public PipeSize As String
'        Public PipeSchedule As String
'    End Class


'    Public Function CreateDataSummary()
'        Dim NewSummary As New SummaryData()
'        Return NewSummary

'    End Function
'    Public Structure DeformationData
'        Public BulgingExceedingCount As Integer
'        Public BulgingNotExceedingCount As Integer
'        Public BulgingPipeIDs As String
'        Public BulgingPasses As String
'        Public BulgingThreshold As Double
'        Public BulgingWorst As Double
'        Public SwellingExceedingCount As Integer
'        Public SwellingNotExceedingCount As Integer
'        Public SwellingPipeIDs As String
'        Public SwellingPasses As String
'        Public SwellingThreshold As Double
'        Public SwellingWorst As Double
'        Public OvalityExceedingCount As Integer
'        Public OvalityNotExceedingCount As Integer
'        Public OvalityPipeIDs As String
'        Public OvalityPasses As String
'        Public OvalityThreshold As Double
'        Public OvalityWorst As Double
'        Public DentingExceedingCount As Integer
'        Public DentingNotExceedingCount As Integer
'        Public DentingPipeIDs As String
'        Public DentingPasses As String
'        Public DentingThreshold As Double
'        Public DentingWorst As Double
'        Public InternalFouling As Boolean
'        Public InternalFoulingExecutiveText As String
'    End Structure
'#End Region

'    Public Function WriteUpSummaryForMe(ByVal TheSummaryData As SummaryData, ByVal PipeSectionName As String, ByVal PipeOrBend As String)
'        'This is the main subroutine that is called by the form to get the desired write up. It determines if valid input has been provided, and then kicks off a procedure that will create a paragraph of text.

'        Dim TheFullWriteUp As String
'        Dim SourceData As Object
'        Dim strUnits As String = TheSummaryData.Units
'        Dim strPassNumber As String = TheSummaryData.PassNumber
'        Dim strNewUsedPipes As String = TheSummaryData.NewUsedPipes
'        Dim strGeneralOrSection As String = TheSummaryData.GeneralOrSectionSummary
'        Dim strAppendix As String = TheSummaryData.Appendix
'        Dim blnInvalidInput As Boolean = False

'        'Do some initial error checking. Don't bother trying if these aren't filled out with something.
'        If IsNothing(strUnits) Or IsNothing(strPassNumber) Or IsNothing(strNewUsedPipes) Or IsNothing(strGeneralOrSection) Or IsNothing(strAppendix) Then
'            blnInvalidInput = True
'            TheFullWriteUp = "Invalid Input in General Options. The type of summary, whether the pipes are new or used, the appendix, the units, and the pass number must be chosen before proceeding."
'        Else
'            'Figure out which source the data is going to come from inside of the summarydata object. It chooses based on strings passed during each button's click event.
'            If PipeOrBend = "Pipe" Then
'                SourceData = TheSummaryData.MyPipeData
'                TheFullWriteUp = ErrorCheckGeneralInput(SourceData)
'                If TheFullWriteUp = Nothing Then TheFullWriteUp = WritePipeDataSummary(SourceData, TheSummaryData.MyDeformationData, strUnits, strPassNumber, strNewUsedPipes, strGeneralOrSection, strAppendix, PipeSectionName)
'            ElseIf PipeOrBend = "Bend" Then
'                SourceData = TheSummaryData.MyBendData
'                TheFullWriteUp = ErrorCheckGeneralInput(SourceData)
'                If TheFullWriteUp = Nothing Then TheFullWriteUp = WriteBendDataSummary(SourceData, TheSummaryData.MyDeformationData, strUnits, strPassNumber, strNewUsedPipes, strGeneralOrSection, strAppendix, PipeSectionName)
'            End If
'        End If

'        Return TheFullWriteUp
'    End Function
'#Region "Paragraph Writing Functions"
'    Private Function WritePipeDataSummary(ByVal SourceDataList As PipeData, ByVal TheDeformationData As DeformationData, ByVal strUnits As String, ByVal strPassNumber As String, ByVal strNewUsedPipes As String, ByVal strGeneralOrSection As String, ByVal strAppendix As String, ByVal pipeSectionName As String)
'        'Figure out how many pipes there are based on the ID's that have been given.
'        Dim NumberOfPipes As Integer = FigureOutNumberOfPipes(SourceDataList.ID)
'        Dim ErrorText As String = ErrorCheckSpecificInput(SourceDataList, NumberOfPipes, "Pipe")
'        Dim ReturnText As String
'        Const _pipeOrBend As String = "Pipe"
'        'Only write the sentences if the data hasn't been shown as bad
'        If ErrorText = Nothing Then
'            Dim strGenOrLocal As String = SourceDataList.GeneralOrLocal
'            Dim strIntOrExt As String = SourceDataList.InternalOrExternal

'            'Determine whether the text warrants plural or singular "Deviation" for the first sentence of new pipes.
'            Dim strDeviation As String
'            Dim strDeviationWasWere As String
'            If strGenOrLocal.Contains(" and ") Or strIntOrExt.Contains(" and ") Then
'                strDeviation = "deviations"
'                strDeviationWasWere = "were"
'            Else
'                strDeviation = "deviation"
'                strDeviationWasWere = "was"
'            End If


'            'Determine if the section abbreviation will be Convection or an Unknown
'            Dim strSectionAbbreviation As String = pipeSectionName

'            'Predetermine certain variables that store capitalied and noncaps versions of Tubes or Pipes based on the schedule.
'            Dim strTubeOrPipeCaps As String
'            Dim strTubeOrPipeNonCaps As String
'            Dim strTubingOrPipingCaps As String
'            If SourceDataList.PipeSchedule = "Tube" Then
'                strTubeOrPipeCaps = " Tube"
'                strTubeOrPipeNonCaps = " tube"
'                strTubingOrPipingCaps = "Tubing"
'            Else
'                strTubeOrPipeCaps = " Pipe"
'                strTubeOrPipeNonCaps = " pipe"
'                strTubingOrPipingCaps = "Piping"
'            End If


'            Dim strSegmentOrSegments As String
'            If NumberOfPipes > 1 Then strSegmentOrSegments = "Segments" Else strSegmentOrSegments = "Segment"

'            'Determine how to write the passes based on what has been provided.
'            Select Case strPassNumber
'                Case "North", "East", "South", "West"
'                    strPassNumber = String.Format("the {0} Pass", strPassNumber)
'                Case "Other"
'                    strPassNumber = String.Format("{{Pass {0}}}", strPassNumber)
'                Case Else
'                    strPassNumber = String.Format("Pass {0}", strPassNumber)
'            End Select
'            Dim strAbbreviatedPipeIDs As String = GetAbbreviatedPipeIDs(SourceDataList.ID)

'            Dim strLocations As String = GenerateFlawLocations(SourceDataList.Location, strUnits, NumberOfPipes)
'            Dim strMinWallThickness As String = GenerateWallThickness(SourceDataList.MinWall(0), strUnits)
'            Dim strMinWallPercent As String = GenerateMinWallPercent(SourceDataList.EstRemWall)
'            Dim strPipeOrTubeText As String = GenerateDesignedPipeOrTubeText(SourceDataList.PipeSchedule, SourceDataList.PipeSize, SourceDataList.DesignedWall, strUnits)
'            Dim strEstimatedWallLoss As String = GenerateWallThickness(SourceDataList.EstWallLoss(0), strUnits)
'            Dim strAverageThicknessText As String = GenerateAverageThicknessText(NumberOfPipes, strTubeOrPipeNonCaps)
'            Dim strAverageThicknessNumbers As String = GenerateAverageThicknessNumbers(NumberOfPipes, SourceDataList.AvgWall, strUnits)
'            Dim blnAddAppendixSpiel As Boolean = False 'flag on whether to add a mention about appendixes in the internal fouling sentence, if applicable.


'            'This portion actually writes the sentences and combines the strings built above with flavor text to create the final paragraph.
'            If strNewUsedPipes = "In-Service Pipes" And strGeneralOrSection = "General" Then
'                Dim Sentence1 As String = String.Format("{0} {1} wall thinning was identified within the {2} Section. ", strGenOrLocal, strIntOrExt, strSectionAbbreviation)
'                Dim Sentence2 As String = String.Format("The lowest minimum wall reading was found in {0}{1} {2} {3} of {4}. ", strSectionAbbreviation, strTubeOrPipeCaps, strSegmentOrSegments, strAbbreviatedPipeIDs, strPassNumber)
'                Dim Sentence3 As String = String.Format("A minimum wall thickness of {0} was observed. ", strMinWallThickness)
'                Dim Sentence4 As String = String.Format("Based on {0}, this equates to an estimated wall loss of {1}%. ", strPipeOrTubeText, strMinWallPercent)
'                Dim Sentence5 As String = GenerateExecutiveSummaryDeformationText(TheDeformationData, strSectionAbbreviation, _pipeOrBend)
'                Dim Sentence6 As String = GenerateExecutiveSummaryFoulingText(TheDeformationData)
'                ReturnText = Sentence1 & Sentence2 & Sentence3 & Sentence4 & Sentence5 & Sentence6

'            ElseIf strNewUsedPipes = "New Pipes (Baseline)" And strGeneralOrSection = "General" Then
'                Dim Sentence1 As String = String.Format("{0} {1} {2} from the manufacturer's specified wall thickness {3} identified within the {4} Section. ", strGenOrLocal, strIntOrExt, strDeviation, strDeviationWasWere, strSectionAbbreviation)
'                Dim Sentence2 As String = String.Format("The lowest minimum wall reading was found in {0}{1} {2} {3} of {4}. ", strSectionAbbreviation, strTubeOrPipeCaps, strSegmentOrSegments, strAbbreviatedPipeIDs, strPassNumber)
'                Dim Sentence3 As String = String.Format("A minimum wall thickness of {0} was observed. ", strMinWallThickness)
'                Dim Sentence4 As String = String.Format("Based on {0}, this equates to an estimated wall deviation of {1}%. ", strPipeOrTubeText, strMinWallPercent)
'                Dim Sentence5 As String = GenerateExecutiveSummaryDeformationText(TheDeformationData, strSectionAbbreviation, _pipeOrBend)
'                Dim Sentence6 As String = GenerateExecutiveSummaryFoulingText(TheDeformationData)
'                ReturnText = Sentence1 & Sentence2 & Sentence3 & Sentence4 & Sentence5 & Sentence6

'            ElseIf strNewUsedPipes = "In-Service Pipes" And strGeneralOrSection = "Section" Then
'                Dim Sentence1 As String = String.Format("{0} {1} wall thinning was identified within the {2} Section of {3}. ", strGenOrLocal, strIntOrExt, strSectionAbbreviation, strPassNumber)
'                Dim Sentence2 As String = String.Format("The maximum percentage of wall loss was found in {0}{1} {2} {3} at {4} from the beginning of the{5} segment at the upstream return bend weld seam. ", strSectionAbbreviation, strTubeOrPipeCaps, strSegmentOrSegments, strAbbreviatedPipeIDs, strLocations, strTubeOrPipeNonCaps)
'                Dim Sentence3 As String = String.Format("A minimum wall thickness of {0}, equating to {1}% wall loss was found in {2} {3}. ", strMinWallThickness, strMinWallPercent, strSegmentOrSegments, strAbbreviatedPipeIDs)
'                Dim Sentence4 As String = String.Format("The estimated wall loss for {0} {1}, based on {2}, was {3}. ", strSegmentOrSegments, strAbbreviatedPipeIDs, strPipeOrTubeText, strEstimatedWallLoss)
'                Dim Sentence5 As String = String.Format("The average wall thickness {0} was {1}. ", strAverageThicknessText, strAverageThicknessNumbers)
'                Dim Sentence7 As String = GeneratePassSummaryFoulingText(TheDeformationData, strAppendix)
'                If Sentence7.Contains("Appendix") = False Then blnAddAppendixSpiel = True
'                Dim Sentence6 As String = GeneratePassDeformationText(strGeneralOrSection, TheDeformationData, strSectionAbbreviation, strAppendix, blnAddAppendixSpiel, _pipeOrBend)


'                ReturnText = Sentence1 & Sentence2 & Sentence3 & Sentence4 & Sentence5 & Sentence6 & Sentence7

'            ElseIf strNewUsedPipes = "New Pipes (Baseline)" And strGeneralOrSection = "Section" Then
'                Dim Sentence1 As String = String.Format("{0} {1} {2} from the manufacturer's specified wall thickness {3} identified within the {4} Section of {5}. ", strGenOrLocal, strIntOrExt, strDeviation, strDeviationWasWere, strSectionAbbreviation, strPassNumber)
'                Dim Sentence2 As String = String.Format("The maximum percentage of deviation was found in {0}{1} {2} {3} at {4} from the beginning of the{5} segment at the upstream return bend weld seam. ", strSectionAbbreviation, strTubeOrPipeCaps, strSegmentOrSegments, strAbbreviatedPipeIDs, strLocations, strTubeOrPipeNonCaps)
'                Dim Sentence3 As String = String.Format("A minimum reading of {0}, which equates to a {1}% deviation in wall thickness was found in {2} {3}. ", strMinWallThickness, strMinWallPercent, strSegmentOrSegments, strAbbreviatedPipeIDs)
'                Dim Sentence4 As String = String.Format("The estimated deviation for {0} {1}, based on {2}, was {3}. ", strSegmentOrSegments, strAbbreviatedPipeIDs, strPipeOrTubeText, strEstimatedWallLoss)
'                Dim Sentence5 As String = String.Format("The average wall thickness {0} was {1}. ", strAverageThicknessText, strAverageThicknessNumbers)
'                Dim Sentence7 As String = GeneratePassSummaryFoulingText(TheDeformationData, strAppendix)
'                If Sentence7.Contains("Appendix") = False Then blnAddAppendixSpiel = True
'                Dim Sentence6 As String = GeneratePassDeformationText(strGeneralOrSection, TheDeformationData, strSectionAbbreviation, strAppendix, blnAddAppendixSpiel, _pipeOrBend)
'                ReturnText = Sentence1 & Sentence2 & Sentence3 & Sentence4 & Sentence5 & Sentence6 & Sentence7
'            End If
'        Else
'            ReturnText = ErrorText
'        End If

'        Return ReturnText
'    End Function

'    Private Function WriteBendDataSummary(ByVal SourceDataList As BendData, ByVal TheDeformationData As DeformationData, ByVal strUnits As String, ByVal strPassNumber As String, ByVal strNewUsedPipes As String, ByVal strGeneralOrSection As String, ByVal strAppendix As String, ByVal pipeSectionName As String)
'        'Figure out how many pipes there are based on the ID's that have been given.
'        Dim NumberOfPipes As Integer = FigureOutNumberOfPipes(SourceDataList.ID)
'        Dim ErrorText As String = ErrorCheckSpecificInput(SourceDataList, NumberOfPipes, "Bend")
'        Dim ReturnText As String
'        Const _pipeOrBend As String = "Bend"

'        'Only write the sentences if the data hasn't been shown as bad
'        If ErrorText = Nothing Then
'            Dim strGenOrLocal As String = SourceDataList.GeneralOrLocal
'            Dim strIntOrExt As String = SourceDataList.InternalOrExternal
'            Dim strDeviation As String
'            Dim strDeviationWasWere As String
'            If strGenOrLocal.Contains(" and ") Or strIntOrExt.Contains(" and ") Then
'                strDeviation = "deviations"
'                strDeviationWasWere = "were"
'            Else
'                strDeviation = "deviation"
'                strDeviationWasWere = "was"
'            End If

'            'Determine if the section abbreviation will be Convection or an Unknown XXX 'deprecated, now this is obtained from the pipe section name.
'            Dim strSectionAbbreviation As String = pipeSectionName

'            Dim strBendOrBends As String
'            If NumberOfPipes > 1 Then strBendOrBends = "Bends" Else strBendOrBends = "Bend"

'            'Determine what the designed wall thickness should be based on the wall loss and measured wall
'            Dim arrDesignedWallThickness(NumberOfPipes - 1)
'            For i = 0 To NumberOfPipes - 1
'                arrDesignedWallThickness(i) = CType(SourceDataList.MinWall(i), Double) / CType(SourceDataList.EstRemWall(i).ToString().Replace("%", ""), Double) * 100
'            Next

'            'Determine how to write the passes based on what has been provided.
'            Select Case strPassNumber
'                Case "North", "East", "South", "West"
'                    strPassNumber = "the " & strPassNumber & " Pass"
'                Case "Other"
'                    strPassNumber = "{Pass " & strPassNumber & "}"
'                Case Else
'                    strPassNumber = "Pass " & strPassNumber
'            End Select
'            Dim strAbbreviatedPipeIDs As String = GetAbbreviatedPipeIDs(SourceDataList.ID)
'            Dim strPipeOrTubeText As String = GenerateDesignedPipeOrTubeText(SourceDataList.PipeSchedule, SourceDataList.PipeSize, arrDesignedWallThickness, strUnits)


'            Dim strMinWallThickness As String = GenerateWallThickness(SourceDataList.MinWall(0), strUnits)
'            Dim strMinWallPercent As String = GenerateMinWallPercent(SourceDataList.EstRemWall)
'            Dim strEstimatedWallLoss As String = GenerateWallThickness(SourceDataList.EstWallLoss(0), strUnits)
'            Dim blnAddAppendixSpiel As Boolean = False 'flag on whether to add a mention about appendixes in the internal fouling sentence, if applicable.

'            If strNewUsedPipes = "In-Service Pipes" And strGeneralOrSection = "General" Then
'                Dim Sentence1 As String = String.Format("{0} {1} wall thinning was identified within the {2} Bends. ", strGenOrLocal, strIntOrExt, strSectionAbbreviation)
'                Dim Sentence2 As String = String.Format("The lowest minimum wall reading was found in {0} {1} {2} of {3}. ", strSectionAbbreviation, strBendOrBends, strAbbreviatedPipeIDs, strPassNumber)
'                Dim Sentence3 As String = String.Format("A minimum wall thickness of {0} was observed. ", strMinWallThickness)
'                Dim Sentence4 As String = String.Format("Based on {0}, this equates to an estimated wall loss of {1}%. ", strPipeOrTubeText, strMinWallPercent)
'                Dim Sentence5 As String = GenerateExecutiveSummaryDeformationText(TheDeformationData, strSectionAbbreviation, _pipeOrBend)
'                Dim Sentence6 As String = GenerateExecutiveSummaryFoulingText(TheDeformationData)
'                ReturnText = Sentence1 & Sentence2 & Sentence3 & Sentence4 & Sentence5 & Sentence6

'            ElseIf strNewUsedPipes = "New Pipes (Baseline)" And strGeneralOrSection = "General" Then
'                Dim Sentence1 As String = String.Format("{0} {1} {2} from the manufacturer's specified wall thickness {3} identified within the {4} Bends. ", strGenOrLocal, strIntOrExt, strDeviation, strDeviationWasWere, strSectionAbbreviation)
'                Dim Sentence2 As String = String.Format("The lowest minimum wall reading was found in {0} {1} {2} of {3}. ", strSectionAbbreviation, strBendOrBends, strAbbreviatedPipeIDs, strPassNumber)
'                Dim Sentence3 As String = String.Format("A minimum wall thickness of {0} was observed. ", strMinWallThickness)
'                Dim Sentence4 As String = String.Format("Based on {0}, this equates to an estimated wall deviation of {1}%. ", strPipeOrTubeText, strMinWallPercent)
'                Dim Sentence5 As String = GenerateExecutiveSummaryDeformationText(TheDeformationData, strSectionAbbreviation, _pipeOrBend)
'                Dim Sentence6 As String = GenerateExecutiveSummaryFoulingText(TheDeformationData)
'                ReturnText = Sentence1 & Sentence2 & Sentence3 & Sentence4 & Sentence5 & Sentence6

'            ElseIf strNewUsedPipes = "In-Service Pipes" And strGeneralOrSection = "Section" Then
'                Dim Sentence1 As String = String.Format("{0} {1} wall thinning was identified within the {2} Bends of {3}. ", strGenOrLocal, strIntOrExt, strSectionAbbreviation, strPassNumber)
'                Dim Sentence2 As String = String.Format("The maximum percentage of wall loss was found in {0} {1} {2}. ", strSectionAbbreviation, strBendOrBends, strAbbreviatedPipeIDs)
'                Dim Sentence3 As String = String.Format("A minimum wall thickness of {0}, equating to {1}% wall loss was found in {2} {3}. ", strMinWallThickness, strMinWallPercent, strBendOrBends, strAbbreviatedPipeIDs)
'                Dim Sentence4 As String = String.Format("The estimated wall loss for {0} {1}, based on {2}, was {3}. ", strBendOrBends, strAbbreviatedPipeIDs, strPipeOrTubeText, strEstimatedWallLoss)
'                Dim Sentence6 As String = GeneratePassSummaryFoulingText(TheDeformationData, strAppendix)
'                If Sentence6.Contains("Appendix") = False Then blnAddAppendixSpiel = True
'                Dim Sentence5 As String = GeneratePassDeformationText(strGeneralOrSection, TheDeformationData, strSectionAbbreviation, strAppendix, blnAddAppendixSpiel, _pipeOrBend)
'                ReturnText = Sentence1 & Sentence2 & Sentence3 & Sentence4 & Sentence5 & Sentence6

'            ElseIf strNewUsedPipes = "New Pipes (Baseline)" And strGeneralOrSection = "Section" Then
'                Dim Sentence1 As String = String.Format("{0} {1} {2} from the manufacturer's specified wall thickness {3} identified within the {4} Bends of {5}. ", strGenOrLocal, strIntOrExt, strDeviation, strDeviationWasWere, strSectionAbbreviation, strPassNumber)
'                Dim Sentence2 As String = String.Format("The maximum percentage of deviation was found in {0} {1} {2}. ", strSectionAbbreviation, strBendOrBends, strAbbreviatedPipeIDs)
'                Dim Sentence3 As String = String.Format("A minimum reading of {0}, which equates to a {1}% deviation in wall thickness was found in {2} {3}. ", strMinWallThickness, strMinWallPercent, strBendOrBends, strAbbreviatedPipeIDs)
'                Dim Sentence4 As String = String.Format("The estimated deviation for {0} {1}, based on {2}, was {3}. ", strBendOrBends, strAbbreviatedPipeIDs, strPipeOrTubeText, strEstimatedWallLoss)
'                Dim Sentence6 As String = GeneratePassSummaryFoulingText(TheDeformationData, strAppendix)
'                If Sentence6.Contains("Appendix") = False Then blnAddAppendixSpiel = True
'                Dim Sentence5 As String = GeneratePassDeformationText(strGeneralOrSection, TheDeformationData, strSectionAbbreviation, strAppendix, blnAddAppendixSpiel, _pipeOrBend)
'                ReturnText = Sentence1 & Sentence2 & Sentence3 & Sentence4 & Sentence5 & Sentence6

'            End If
'        Else
'            ReturnText = ErrorText
'        End If


'        Return ReturnText
'    End Function
'#End Region
'#Region "Supporting Functions"
'    Public Function FigureOutNumberOfPipes(ByVal ArrayColumnToCheck As Array)
'        Dim NumberOfValidRows As Integer
'        Try
'            For i = 0 To ArrayColumnToCheck.Length - 1
'                If ArrayColumnToCheck(i).ToString.Length > 0 Then NumberOfValidRows = NumberOfValidRows + 1
'            Next
'        Catch
'        End Try
'        Return NumberOfValidRows
'    End Function
'    Public Function ErrorCheckGeneralInput(ByVal SourceData As Object)
'        Dim ReturnText As String
'        If SourceData.ID(0) Is Nothing Then ReturnText = String.Format("{0}There must be at least one Pipe or Bend ID Given, and they must start at the top row.{1}", ReturnText, vbCrLf)
'        If SourceData.GeneralOrLocal = Nothing Or SourceData.GeneralOrLocal = "" Then ReturnText = String.Format("{0}Please specify if there is general, local, or both types of metal loss.{1}", ReturnText, vbCrLf)
'        If SourceData.InternalOrExternal = Nothing Or SourceData.InternalOrExternal = "" Then ReturnText = String.Format("{0}Please specify whether the metal loss is Internal, External, or both.{1}", ReturnText, vbCrLf)
'        If SourceData.PipeSize = Nothing Or SourceData.PipeSize = "" Then ReturnText = String.Format("{0}Please specify a Pipe Size.{1}", ReturnText, vbCrLf)
'        If SourceData.PipeSchedule = Nothing Or SourceData.PipeSchedule = "" Then ReturnText = String.Format("{0}Please specify a Pipe Schedule.{1}", ReturnText, vbCrLf)

'        Return ReturnText
'    End Function
'    Public Function ErrorCheckSpecificInput(ByVal SourceData As Object, ByVal NumberOfPipes As Integer, ByVal PipeOrBend As String)
'        Dim ReturnText As String
'        'Now try casting all of the valid values to Doubles to see if there are any letters or innapropriate symbols
'        Dim DblText As Double
'        Dim TempRemWall As String
'        If NumberOfPipes = 0 Then ReturnText = String.Format("{0}There must be at least one {1} input.{2}", ReturnText, PipeOrBend, vbCrLf)
'        For i = 0 To NumberOfPipes - 1
'            'Check the values common to both Pipes or Bends
'            Try
'                DblText = CType(SourceData.MinWall(i), Double)
'            Catch ex As Exception
'                ReturnText = String.Format("{0}Minimum Wall value {1} in row {2} is invalid.{3}", ReturnText, SourceData.MinWall(i), i + 1, vbCrLf)
'            End Try
'            Try
'                DblText = CType(SourceData.EstWallLoss(i), Double)
'            Catch ex As Exception
'                ReturnText = String.Format("{0}Estimated Wall Loss value {1} in row {2} is invalid.{3}", ReturnText, SourceData.EstWallLoss(i), i + 1, vbCrLf)
'            End Try
'            Try
'                If SourceData.EstRemWall(i).ToString <> "" Then TempRemWall = SourceData.EstRemWall(i).ToString
'                If TempRemWall <> Nothing And TempRemWall.Contains("%") = True Then TempRemWall = TempRemWall.Replace("%", "")
'                DblText = CType(TempRemWall, Double)
'            Catch ex As Exception
'                ReturnText = String.Format("{0}Estimated Remaining Wall value {1} in row {2} is invalid.{3}", ReturnText, SourceData.EstRemWall(i), i + 1, vbCrLf)
'            End Try

'            'Now do the arrays that only exist in Pipes (and not in bends)
'            If PipeOrBend = "Pipe" Then
'                Try
'                    DblText = CType(SourceData.Location(i), Double)
'                Catch ex As Exception
'                    ReturnText = String.Format("{0}Location value {1} in row {2} is invalid.{3}", ReturnText, SourceData.Location(i), i + 1, vbCrLf)
'                End Try
'                Try
'                    DblText = CType(SourceData.AvgWall(i), Double)
'                Catch ex As Exception
'                    ReturnText = String.Format("{0}Average Wall value {1} in row {2} is invalid.{3}", ReturnText, SourceData.AvgWall(i), i + 1, vbCrLf)
'                End Try
'                Try
'                    DblText = CType(SourceData.DesignedWall(i), Double)
'                Catch ex As Exception
'                    ReturnText = String.Format("{0}Designed Wall value {1} in row {2} is invalid.{3}", ReturnText, SourceData.DesignedWall(i), i + 1, vbCrLf)
'                End Try
'                Try
'                    DblText = CType(SourceData.AvgDiameter(i), Double)
'                Catch ex As Exception
'                    ReturnText = String.Format("{0}Average Diameter value {1} in row {2} is invalid.{3}", ReturnText, SourceData.AvgDiameter(i), i + 1, vbCrLf)
'                End Try
'            End If
'        Next
'        Return ReturnText
'    End Function

'    Private Function GenerateAverageThicknessNumbers(ByVal Numberofpipes As Integer, ByVal arrAvgWallData As Array, ByVal Units As String)
'        Dim returnText As String
'        Dim strAvgWallToAnalyze As String
'        Dim strFormattedWall As String
'        If Numberofpipes > 0 Then
'            For intCounter = 0 To Numberofpipes - 1
'                strAvgWallToAnalyze = arrAvgWallData(intCounter)
'                strFormattedWall = GenerateWallThickness(strAvgWallToAnalyze, Units)

'                If intCounter = 0 Then returnText = strFormattedWall
'                If intCounter > 0 And intCounter < Numberofpipes - 1 Then
'                    returnText = String.Format("{0}, {1}", returnText, strFormattedWall)
'                ElseIf intCounter > 0 And intCounter = Numberofpipes - 1 Then
'                    returnText = String.Format("{0} and {1}, respectively", returnText, strFormattedWall)
'                End If
'            Next
'        End If
'        Return returnText
'    End Function

'    Private Function GenerateAverageThicknessText(ByVal NumberOfPipes As Integer, ByVal strTubeOrPipe As String)
'        Dim ReturnText As String
'        If NumberOfPipes > 1 Then
'            If strTubeOrPipe = " tube" Then
'                ReturnText = "for each of the tube segments listed above"
'            ElseIf strTubeOrPipe = " pipe" Then
'                ReturnText = "for each of the pipe segments listed above"
'            End If
'        Else
'            If strTubeOrPipe = " tube" Then
'                ReturnText = "throughout the entire tube segment"
'            ElseIf strTubeOrPipe = " pipe" Then
'                ReturnText = "throughout the entire pipe segment"
'            End If
'        End If

'        Return ReturnText
'    End Function
'    Private Function GetAbbreviatedPipeIDs(ByVal TheIDs As Array)
'        'This function is intended to return the abbreviated Pipe ID's by turning the array {Conv 4-2, Conv 5-3, SomethingElse 5:4, blank, blank} into a string "4-2, 5-3, and 5:4"
'        'There is supposed to be a space in the name, and anything after the last space is considered to be the identifier.
'        'This replaces strings that don't follow the normal convention with "XXX" like the spreadsheet this is based off of did.

'        Dim intNumberOfPipeID As Integer
'        Dim strAbbreviatedPipeID As String
'        Dim strCombinedPipeIDs As String

'        Try
'            For i = 0 To TheIDs.Length - 1
'                If TheIDs(i).ToString.Length > 0 Then intNumberOfPipeID = intNumberOfPipeID + 1
'            Next
'        Catch
'        End Try
'        Dim strPipeIDToAnalyze As String
'        If intNumberOfPipeID > 0 Then
'            For intIDCounter = 0 To intNumberOfPipeID - 1
'                strPipeIDToAnalyze = TheIDs(intIDCounter)
'                Dim intLastSpace As Integer = strPipeIDToAnalyze.LastIndexOf(" ")
'                If intLastSpace <> -1 Then
'                    strAbbreviatedPipeID = strPipeIDToAnalyze.Substring(intLastSpace + 1, strPipeIDToAnalyze.Length - intLastSpace - 1)
'                Else : strAbbreviatedPipeID = strPipeIDToAnalyze
'                End If
'                If intIDCounter = 0 Then strCombinedPipeIDs = strAbbreviatedPipeID
'                If intIDCounter > 0 And intIDCounter < intNumberOfPipeID - 1 Then
'                    strCombinedPipeIDs = String.Format("{0}, {1}", strCombinedPipeIDs, strAbbreviatedPipeID)
'                ElseIf intIDCounter > 0 And intIDCounter = intNumberOfPipeID - 1 Then
'                    strCombinedPipeIDs = String.Format("{0} and {1}", strCombinedPipeIDs, strAbbreviatedPipeID)
'                End If
'            Next
'        End If
'        Return strCombinedPipeIDs
'    End Function
'    Private Function GenerateFlawLocations(ByVal FlawLocations As Array, ByVal ImperialOrMetric As String, ByVal NumberOfPipes As Integer)
'        Dim strFlawToAnalyze As String
'        Dim strFlawText As String
'        Dim strCombinedPipeFlaws As String
'        Dim dblFlawToAnalyze As Double
'        If NumberOfPipes > 0 Then
'            For intFlawCounter = 0 To NumberOfPipes - 1
'                strFlawToAnalyze = FlawLocations(intFlawCounter)
'                Try
'                    dblFlawToAnalyze = CType(strFlawToAnalyze, Double)
'                    If ImperialOrMetric = "Imperial" Then
'                        strFlawText = String.Format("{0}ft. ({1}m)", dblFlawToAnalyze & Chr(160), Format(dblFlawToAnalyze / 3.28083989501312, "0.0") & Chr(160)) 'chr(160) prevent breaking the text
'                    ElseIf ImperialOrMetric = "Metric" Then
'                        strFlawText = String.Format("{0}m ({1}ft.)", dblFlawToAnalyze & Chr(160), Format(dblFlawToAnalyze * 3.28083989501312, "0.0") & Chr(160))
'                    End If

'                Catch ex As Exception
'                    strFlawText = "{XXX}"
'                End Try

'                If intFlawCounter = 0 Then strCombinedPipeFlaws = strFlawText
'                If intFlawCounter > 0 And intFlawCounter < NumberOfPipes - 1 Then
'                    strCombinedPipeFlaws = String.Format("{0}, {1}", strCombinedPipeFlaws, strFlawText)
'                ElseIf intFlawCounter > 0 And intFlawCounter = NumberOfPipes - 1 Then
'                    strCombinedPipeFlaws = String.Format("{0} and {1}, respectively,", strCombinedPipeFlaws, strFlawText)
'                End If
'            Next
'        End If

'        Return strCombinedPipeFlaws

'    End Function

'    Private Function GenerateWallThickness(ByVal strWallThickness As String, ByVal ImperialOrMetric As String)
'        'LIke the spreadsheet, this takes the first minimum wall value and formats it to look pretty. For example: 14.2 would turn to "14.2m (46.6 ft.)" if it were metric

'        Dim dblMinWallToAnalyze As Double = CType(strWallThickness, Double)
'        Dim strFormattedMinWall As String
'        If ImperialOrMetric = "Imperial" Then
'            strFormattedMinWall = String.Format("{0} in. ({1} mm)", Format(dblMinWallToAnalyze, "0.000"), Format(dblMinWallToAnalyze * 25.4, "0.0"))
'        ElseIf ImperialOrMetric = "Metric" Then
'            strFormattedMinWall = String.Format("{0} mm ({1} in.)", Format(dblMinWallToAnalyze, "0.0"), Format(dblMinWallToAnalyze / 25.4, "0.000"))
'        End If
'        'Replace spaces with no-break spaces.
'        strFormattedMinWall = strFormattedMinWall.Replace(" ", Chr(160))
'        Return strFormattedMinWall

'    End Function
'    Private Function GenerateMinWallPercent(ByVal arrMinWall As Array)
'        Dim strMinWallPercent As String
'        Dim strMinWallToAnalyze As String = arrMinWall(0)
'        If strMinWallToAnalyze.Contains("%") = True Then strMinWallToAnalyze = strMinWallToAnalyze.Replace("%", "")


'        Dim dblMinWallToAnalyze As Double = CType(strMinWallToAnalyze, Double)
'        If dblMinWallToAnalyze > 100 Then
'            strMinWallPercent = "0.0"
'        Else
'            strMinWallPercent = Format(100 - dblMinWallToAnalyze, "0.0")
'        End If
'        Return strMinWallPercent


'    End Function
'    Private Function GenerateDesignedPipeOrTubeText(ByVal ThePipeSchedule As String, ByVal ThePipeSize As String, ByVal DesignedWallThickness As Array, ByVal TheUnits As String)
'        'This recreates the pipe or tube text. If the schedule is a number, it should be the number followed by its schedule and some flavor text. If not, then it has to be either a special tube value or "X"
'        Dim ReturnText As String
'        If ThePipeSchedule = "Tube" Then
'            ReturnText = GenerateWallThickness(DesignedWallThickness(0), TheUnits) & " Designed Wall Tube"
'        ElseIf ThePipeSize = "Other" Then
'            ReturnText = "{X}"
'        Else : ReturnText = String.Format("{0}inch Schedule{1} Pipe Nominal Dimensions", ThePipeSize & Chr(160), Chr(160) & ThePipeSchedule) 'chr(160) is a no-break space.
'        End If

'        Return ReturnText
'    End Function
'#End Region

'#Region "Deformation Text Writing"

'#Region "Section Summary Deformation"

'    Private Function GeneratePassDeformationText(ByVal strSectionType As String, ByVal TheDeformationData As DeformationData, ByVal strSectionAbbreviation As String, ByVal strAppendix As String, ByVal blnAddAppendixSpiel As Boolean, ByVal pipeOrBend As String)
'        'This function will write a sentence of deformation text by combining the passed data. THis applies to pass summaries only.
'        Dim strResultingSentence As String 'The string that will later be passed back.

'        'Figure out how many deformations there are total
'        Dim intNumberOfDeformations As Integer = TheDeformationData.BulgingExceedingCount + TheDeformationData.DentingExceedingCount + TheDeformationData.OvalityExceedingCount + TheDeformationData.SwellingExceedingCount + TheDeformationData.BulgingNotExceedingCount + TheDeformationData.DentingNotExceedingCount + TheDeformationData.OvalityNotExceedingCount + TheDeformationData.SwellingNotExceedingCount
'        Dim strAppendixSpiel As String
'        'If there are no deformations, return the canned text.
'        If intNumberOfDeformations = 0 Then
'            If blnAddAppendixSpiel = True Then strAppendixSpiel = String.Format(" (see Appendix {0} for additional details on all pipe segments)", strAppendix)
'            strResultingSentence = String.Format("No deformations (denting, ovality, swelling, bulging) were detected in the {0} {1}s of this pass{2}. ", strSectionAbbreviation, pipeOrBend, strAppendixSpiel)
'            Return strResultingSentence
'        End If
'        Dim _putAppendixSpielInExceeding As Boolean

'        'If there are some deformations, now it gets complicated. We have a number of sentences to make from here, so let's get started.
'        Dim NonExceedingSentence As String = strResultingSentence & WritePassSummaryNotExceedingDeformationSentence(TheDeformationData, strSectionAbbreviation, strAppendix, blnAddAppendixSpiel)
'        If NonExceedingSentence.Contains("Appendix") Or blnAddAppendixSpiel = False Then _putAppendixSpielInExceeding = False Else _putAppendixSpielInExceeding = True
'        Dim ExceedingSentence As String = WritePassSummaryExceedingDeformationSentence(TheDeformationData, strSectionAbbreviation, strAppendix, _putAppendixSpielInExceeding)
'        strResultingSentence = ExceedingSentence & NonExceedingSentence
'        Return strResultingSentence
'    End Function

'    Private Function WritePassSummaryExceedingDeformationSentence(ByVal TheDeformationData As DeformationData, ByVal strSectionAbbreviation As String, ByVal strAppendix As String, ByVal blnAddAppendixSpiel As Boolean)
'        Dim strSentence As String
'        Dim _strAreaOrAreas As String
'        Dim DeformationNames As New ArrayList
'        Dim DeformationCounts As New ArrayList
'        Dim Thresholds As New ArrayList
'        Dim AreaOrAreas As New ArrayList

'        'First lets do deformations that only exceeded.
'        If TheDeformationData.BulgingExceedingCount > 0 Then
'            If TheDeformationData.BulgingExceedingCount = 1 Then _strAreaOrAreas = "area" Else _strAreaOrAreas = "areas"
'            DeformationNames.Add("Bulging")
'            DeformationCounts.Add(TheDeformationData.BulgingExceedingCount)
'            Thresholds.Add(TheDeformationData.BulgingThreshold)
'            AreaOrAreas.Add(_strAreaOrAreas)
'        End If
'        If TheDeformationData.DentingExceedingCount > 0 Then
'            If TheDeformationData.DentingExceedingCount = 1 Then _strAreaOrAreas = "area" Else _strAreaOrAreas = "areas"
'            DeformationNames.Add("Denting")
'            DeformationCounts.Add(TheDeformationData.DentingExceedingCount)
'            Thresholds.Add(TheDeformationData.DentingThreshold)
'            AreaOrAreas.Add(_strAreaOrAreas)
'        End If
'        If TheDeformationData.OvalityExceedingCount > 0 Then
'            If TheDeformationData.OvalityExceedingCount = 1 Then _strAreaOrAreas = "area" Else _strAreaOrAreas = "areas"
'            DeformationNames.Add("Ovality")
'            DeformationCounts.Add(TheDeformationData.OvalityExceedingCount)
'            Thresholds.Add(TheDeformationData.OvalityThreshold)
'            AreaOrAreas.Add(_strAreaOrAreas)
'        End If
'        If TheDeformationData.SwellingExceedingCount > 0 Then
'            If TheDeformationData.SwellingExceedingCount = 1 Then _strAreaOrAreas = "area" Else _strAreaOrAreas = "areas"
'            DeformationNames.Add("Swelling")
'            DeformationCounts.Add(TheDeformationData.SwellingExceedingCount)
'            Thresholds.Add(TheDeformationData.SwellingThreshold)
'            AreaOrAreas.Add(_strAreaOrAreas)
'        End If
'        If DeformationNames.Count > 0 Then strSentence = PassSummaryExceedingText(DeformationNames, DeformationCounts, Thresholds, AreaOrAreas, strAppendix, strSectionAbbreviation, blnAddAppendixSpiel)
'        Return strSentence
'    End Function
'    Private Function PassSummaryExceedingText(ByVal DeformationNames As ArrayList, ByVal DeformationCounts As ArrayList, ByVal Thresholds As ArrayList, ByVal AreaOrAreas As ArrayList, ByVal strAppendix As String, ByVal strSectionAbbreviation As String, ByVal blnAddAppendixSpiel As Boolean)
'        'The sentence will be broken up into pieces. The first piece is '#Areas of #deformationType and #Area of #DeformationType' for each deformation type.
'        Dim strSentence As String = ""
'        Dim strDeformationCountText As String = ""
'        Dim intDeformationTotalCount As Integer
'        For i = 0 To DeformationNames.Count - 1
'            intDeformationTotalCount = intDeformationTotalCount + DeformationCounts(i)
'            'Add the word and if this is the last of a series greater than 2
'            If i = DeformationNames.Count - 1 And DeformationNames.Count > 2 Then strSentence = strSentence & ", and "
'            'Add only the word and if this is only a series of 2
'            If i = DeformationNames.Count - 1 And DeformationNames.Count = 2 Then strSentence = strSentence & " and "
'            'Add only a comma if this is a series of more than 2 and this is the not the last one
'            If i < DeformationNames.Count - 1 And i <> 0 Then strSentence = strSentence & ", "
'            'Since a sentence can't start with a number, relate the number to the textual representation of it.
'            If DeformationCounts(i) = 1 Then
'                strDeformationCountText = "One"
'            ElseIf DeformationCounts(i) = 2 Then
'                strDeformationCountText = "Two"
'            ElseIf DeformationCounts(i) = 3 Then
'                strDeformationCountText = "Three"
'            ElseIf DeformationCounts(i) = 4 Then
'                strDeformationCountText = "Four"
'            ElseIf DeformationCounts(i) = 5 Then
'                strDeformationCountText = "Five"
'            ElseIf DeformationCounts(i) = 6 Then
'                strDeformationCountText = "Six"
'            Else : strDeformationCountText = DeformationCounts(i) 'Just give it the number past here. There's no way it'll get this far.
'            End If
'            Dim strNumberText As String
'            If i > 0 Then
'                strNumberText = strDeformationCountText.ToString.ToLower
'            Else : strNumberText = strDeformationCountText
'            End If

'            'Add the canned text to the string for this item
'            If i > 0 Then strDeformationCountText = strDeformationCountText.ToString.ToLower
'            strSentence = String.Format("{0}{1} {2} of {3}", strSentence, strDeformationCountText, AreaOrAreas(i), DeformationNames(i).ToString.ToLower)
'        Next
'        strSentence = strSentence & " exceeding the predetermined threshold of "

'        'This section is 'Exceeding the predetermined threshold of X.X% and X.X%, respectively
'        For i = 0 To DeformationNames.Count - 1
'            'Add the word and if this is the last of a series greater than 2
'            If i = DeformationNames.Count - 1 And DeformationNames.Count > 2 Then strSentence = strSentence & ", and "
'            'Add only the word and if this is only a series of 2
'            If i = DeformationNames.Count - 1 And DeformationNames.Count = 2 Then strSentence = strSentence & " and "
'            'Add only a comma if this is a series of more than 2 and this is the not the last one
'            If i < DeformationNames.Count - 1 And i <> 0 Then strSentence = strSentence & ", "

'            'Add the canned text to the string for this item
'            strSentence = strSentence & FormatNumber(Thresholds(i), 1) & "%"
'            'Add the word ', respectively' if listing more than 2 deformation thresholds
'            If i = DeformationNames.Count - 1 And DeformationNames.Count > 1 Then strSentence = strSentence & ", respectively,"
'        Next

'        'This chunk is the part 'was/were found in the Convection/radiant section of this pass
'        Dim strWasWere As String
'        If DeformationNames.Count > 1 Or intDeformationTotalCount > 1 Then
'            strWasWere = "were"
'        Else : strWasWere = "was"
'        End If
'        Dim strAppendixSpiel As String
'        If blnAddAppendixSpiel = True Then strAppendixSpiel = String.Format(" (see Appendix {0} for additional details on all pipe segments)", strAppendix)
'        strSentence = String.Format("{0} {1} found in the {2} Section of this pass{3}. ", strSentence, strWasWere, strSectionAbbreviation, strAppendixSpiel)
'        Return strSentence
'    End Function

'    Private Function WritePassSummaryNotExceedingDeformationSentence(ByVal TheDeformationData As DeformationData, ByVal strSectionAbbreviation As String, ByVal strAppendix As String, ByVal addAppendixSpiel As Boolean)
'        Dim strSentence As String
'        Dim _strAreaOrAreas As String
'        Dim DeformationNames As New ArrayList
'        Dim DeformationCounts As New ArrayList
'        Dim Thresholds As New ArrayList
'        Dim AreaOrAreas As New ArrayList

'        'Now let's get all the ones that didn't exceed.
'        If TheDeformationData.BulgingNotExceedingCount > 0 Then
'            If TheDeformationData.BulgingNotExceedingCount = 1 Then _strAreaOrAreas = "area" Else _strAreaOrAreas = "areas"
'            DeformationNames.Add("Bulging")
'            DeformationCounts.Add(TheDeformationData.BulgingNotExceedingCount)
'            Thresholds.Add(TheDeformationData.BulgingThreshold)
'            AreaOrAreas.Add(_strAreaOrAreas)
'        End If
'        If TheDeformationData.DentingNotExceedingCount > 0 Then
'            If TheDeformationData.DentingNotExceedingCount = 1 Then _strAreaOrAreas = "area" Else _strAreaOrAreas = "areas"
'            DeformationNames.Add("Denting")
'            DeformationCounts.Add(TheDeformationData.DentingNotExceedingCount)
'            Thresholds.Add(TheDeformationData.DentingThreshold)
'            AreaOrAreas.Add(_strAreaOrAreas)
'        End If
'        If TheDeformationData.OvalityNotExceedingCount > 0 Then
'            If TheDeformationData.OvalityNotExceedingCount = 1 Then _strAreaOrAreas = "area" Else _strAreaOrAreas = "areas"
'            DeformationNames.Add("Ovality")
'            DeformationCounts.Add(TheDeformationData.OvalityNotExceedingCount)
'            Thresholds.Add(TheDeformationData.OvalityThreshold)
'            AreaOrAreas.Add(_strAreaOrAreas)
'        End If
'        If TheDeformationData.SwellingNotExceedingCount > 0 Then
'            If TheDeformationData.SwellingNotExceedingCount = 1 Then _strAreaOrAreas = "area" Else _strAreaOrAreas = "areas"
'            DeformationNames.Add("Swelling")
'            DeformationCounts.Add(TheDeformationData.SwellingNotExceedingCount)
'            Thresholds.Add(TheDeformationData.SwellingThreshold)
'            AreaOrAreas.Add(_strAreaOrAreas)
'        End If
'        If DeformationNames.Count > 0 Then strSentence = PassSummaryNotExceedingText(DeformationNames, DeformationCounts, Thresholds, AreaOrAreas, strAppendix, strSectionAbbreviation, addAppendixSpiel)
'        Return strSentence
'    End Function
'    Private Function PassSummaryNotExceedingText(ByVal DeformationNames As ArrayList, ByVal DeformationCounts As ArrayList, ByVal Thresholds As ArrayList, ByVal AreaOrAreas As ArrayList, ByVal strAppendix As String, ByVal strSectionAbbreviation As String, addAppendixSpiel As String)
'        Dim strSentence As String
'        'We're trying to make a sentence like, "(One, Several, Numerous) area(s) of (denting, ovality, bulging, swelling) was/were found in the (convection/radiant/other) section of this pass but fell below the predetermined threshol of x.x% and X.X%, respectively
'        'So we have to figure out the first 2 words based on the input.
'        Dim strWasWere As String
'        Dim strDeformationCountText As String
'        Dim strFormattedThresholds As String
'        For i = 0 To DeformationNames.Count - 1
'            'Add the word and if this is the last of a series greater than 2
'            If i = DeformationNames.Count - 1 And DeformationNames.Count > 2 Then
'                strSentence = strSentence & ", and "
'                strFormattedThresholds = strFormattedThresholds & ", and "
'            End If
'            'Add only the word and if this is only a series of 2
'            If i = DeformationNames.Count - 1 And DeformationNames.Count = 2 Then
'                strSentence = strSentence & " and "
'                strFormattedThresholds = strFormattedThresholds & " and "
'            End If
'            'Add only a comma if this is a series of more than 2 and this is the not the last one
'            If i < DeformationNames.Count - 1 And i <> 0 Then
'                strSentence = strSentence & ", "
'                strFormattedThresholds = strFormattedThresholds & ", "
'            End If
'            'Since a sentence can't start with a number, relate the number to the textual representation of it.
'            If DeformationCounts(i) = 1 Then
'                strDeformationCountText = "One"
'            ElseIf DeformationCounts(i) > 1 And DeformationCounts(i) < 5 Then
'                strDeformationCountText = "Several"
'            ElseIf DeformationCounts(i) >= 5 Then
'                strDeformationCountText = "Numerous"
'            End If
'            Dim strNumberText As String
'            If i > 0 Then
'                strNumberText = strDeformationCountText.ToString.ToLower
'            Else : strNumberText = strDeformationCountText
'            End If

'            'Add the canned text to the string for this item
'            If i > 0 Then strDeformationCountText = strDeformationCountText.ToString.ToLower
'            strSentence = String.Format("{0}{1} {2} of {3}", strSentence, strDeformationCountText, AreaOrAreas(i), DeformationNames(i).ToString.ToLower)
'            strFormattedThresholds = String.Format("{0}{1}%", strFormattedThresholds, FormatNumber(Thresholds(i), 1))
'        Next
'        If DeformationNames.Count = 1 And DeformationCounts(0) = 1 Then 'this only has 1 deformation and can be singular
'            strWasWere = " was"
'        Else
'            strWasWere = " were"
'        End If
'        Dim _appendixSpiel As String
'        If addAppendixSpiel = True Then _appendixSpiel = String.Format(" (see Appendix {0} for additional details on all pipe segments)", strAppendix)
'        Dim strRespectivelyIfNeeded As String
'        If DeformationNames.Count > 1 Then
'            strRespectivelyIfNeeded = ", respectively"
'        Else
'            strRespectivelyIfNeeded = ""
'        End If
'        strSentence = String.Format("{0}{1} found in the {2} Section of this pass but fell below the predetermined threshold of {3}{4}{5}. ", strSentence, strWasWere, strSectionAbbreviation, strFormattedThresholds, strRespectivelyIfNeeded, _appendixSpiel)
'        Return strSentence
'    End Function

'#End Region

'#Region "Executive Summary Deformation Text"
'    Public Function GenerateExecutiveSummaryDeformationText(ByVal TheDeformationData As DeformationData, ByVal strConvectionOrRadiant As String, ByVal pipeOrBend As String)
'        'This function will write a sentence of deformation text by combining the passed data. THis applies to pass summaries only.
'        Dim strResultingSentence As String 'The string that will later be passed back.

'        'Figure out how many deformations there are total
'        Dim intNumberOfDeformations As Integer = TheDeformationData.BulgingExceedingCount + TheDeformationData.DentingExceedingCount + TheDeformationData.OvalityExceedingCount + TheDeformationData.SwellingExceedingCount + TheDeformationData.BulgingNotExceedingCount + TheDeformationData.DentingNotExceedingCount + TheDeformationData.OvalityNotExceedingCount + TheDeformationData.SwellingNotExceedingCount

'        'If there are no deformations, return nothing for the executive summary
'        If intNumberOfDeformations = 0 Then
'            strResultingSentence = String.Format("No deformations (denting, ovality, swelling, bulging) were detected in the {0} {1}s. ", strConvectionOrRadiant, pipeOrBend)
'            Return strResultingSentence
'        End If

'        'If there are some deformations, now it gets complicated. We have a number of sentences to make from here, so let's get started.
'        strResultingSentence = WriteExecutiveSummaryExceedingDeformationSentence(TheDeformationData, pipeOrBend)
'        strResultingSentence = strResultingSentence & WriteExecutiveSummaryAboveAndBelowDeformationSentence(TheDeformationData, pipeOrBend)
'        strResultingSentence = strResultingSentence & WriteExecutiveSummarynotExceedingDeformationSentence(TheDeformationData)

'        Return strResultingSentence
'    End Function
'    Private Function WriteExecutiveSummaryExceedingDeformationSentence(ByVal TheDeformationData As DeformationData, ByVal pipeOrBend As String)
'        Dim strSentencesCreated As String
'        Dim AreaOrAreas As New ArrayList
'        Dim DeformationNames As New ArrayList
'        Dim DeformationCounts As New ArrayList
'        Dim Thresholds As New ArrayList
'        Dim PipeSegments As New ArrayList
'        Dim Passes As New ArrayList
'        Dim WorstPercents As New ArrayList

'        'Fill the string arrays with useful values obtained from the data, discarding the zero values
'        If TheDeformationData.BulgingExceedingCount > 0 And TheDeformationData.BulgingNotExceedingCount = 0 Then
'            DeformationNames.Add("Bulging")
'            Thresholds.Add(TheDeformationData.BulgingThreshold)
'            DeformationCounts.Add(TheDeformationData.BulgingExceedingCount)
'            PipeSegments.Add(TheDeformationData.BulgingPipeIDs)
'            Passes.Add(TheDeformationData.BulgingPasses)
'            WorstPercents.Add(TheDeformationData.BulgingWorst)
'            If TheDeformationData.BulgingExceedingCount > 1 Then AreaOrAreas.Add("Areas") Else AreaOrAreas.Add("Area")
'        End If
'        If TheDeformationData.SwellingExceedingCount > 0 And TheDeformationData.SwellingNotExceedingCount = 0 Then
'            DeformationNames.Add("Swelling")
'            DeformationCounts.Add(TheDeformationData.SwellingExceedingCount)
'            Thresholds.Add(TheDeformationData.SwellingThreshold)
'            PipeSegments.Add(TheDeformationData.SwellingPipeIDs)
'            Passes.Add(TheDeformationData.SwellingPasses)
'            WorstPercents.Add(TheDeformationData.SwellingWorst)
'            If TheDeformationData.SwellingExceedingCount > 1 Then AreaOrAreas.Add("Areas") Else AreaOrAreas.Add("Area")
'        End If
'        If TheDeformationData.OvalityExceedingCount > 0 And TheDeformationData.OvalityNotExceedingCount = 0 Then
'            DeformationNames.Add("Ovality")
'            DeformationCounts.Add(TheDeformationData.OvalityExceedingCount)
'            Thresholds.Add(TheDeformationData.OvalityThreshold)
'            PipeSegments.Add(TheDeformationData.OvalityPipeIDs)
'            Passes.Add(TheDeformationData.OvalityPasses)
'            WorstPercents.Add(TheDeformationData.OvalityWorst)
'            If TheDeformationData.OvalityExceedingCount > 1 Then AreaOrAreas.Add("Areas") Else AreaOrAreas.Add("Area")
'        End If
'        If TheDeformationData.DentingExceedingCount > 0 And TheDeformationData.DentingNotExceedingCount = 0 Then
'            DeformationNames.Add("Denting")
'            DeformationCounts.Add(TheDeformationData.DentingExceedingCount)
'            Thresholds.Add(TheDeformationData.DentingThreshold)
'            PipeSegments.Add(TheDeformationData.DentingPipeIDs)
'            Passes.Add(TheDeformationData.DentingPasses)
'            WorstPercents.Add(TheDeformationData.DentingWorst)
'            If TheDeformationData.DentingExceedingCount > 1 Then AreaOrAreas.Add("Areas") Else AreaOrAreas.Add("Area")
'        End If
'        'Depending on if the threshold was exceeded or not, write the appropriate sentence.
'        If DeformationNames.Count > 0 Then strSentencesCreated = ExecutiveSummaryExceedingText(DeformationNames, DeformationCounts, Thresholds, PipeSegments, Passes, AreaOrAreas, WorstPercents, pipeOrBend)

'        Return strSentencesCreated
'    End Function
'    Private Function ExecutiveSummaryExceedingText(ByVal DeformationNames As ArrayList, ByVal DeformationCounts As ArrayList, ByVal Thresholds As ArrayList, ByVal PipeSegments As ArrayList, ByVal Passes As ArrayList, ByVal AreaOrAreas As ArrayList, ByVal WorstPercents As ArrayList, ByVal pipeOrBend As String)
'        'Generate the sentences to the tune of 'Several types of pipe deformations (ovality and bulging) were found during the examination'. Its style is based heaviy on how many different types of deformation there are
'        Dim strWasWere As String
'        Dim strSentencesCreated As String = ""
'        If DeformationNames.Count = 1 Then
'            'A special case sentence is when there is only one type of deformation and only one of them, IE: only one bulge was found.
'            If DeformationNames.Count = 1 And DeformationCounts(0) = 1 Then
'                strSentencesCreated = String.Format("One area of {0} was found during this examination in {1} of Pass {2} that exceeded the predetermined threshold of {3}%. This area equated to {4}%. ", DeformationNames(0).ToString.ToLower, GeneratePipeTubeName(PipeSegments(0), pipeOrBend), Passes(0), FormatNumber(Thresholds(0), 1), FormatNumber(WorstPercents(0), 1))
'            ElseIf DeformationCounts(0) > 1 Then
'                Dim StrPluralDeformation As String
'                If DeformationNames(0) = "Ovality" Or DeformationNames(0) = "Swelling" Then
'                    StrPluralDeformation = DeformationNames(0)
'                    strWasWere = "was"
'                ElseIf DeformationNames(0) = "Bulging" Then
'                    StrPluralDeformation = "Bulges"
'                    strWasWere = "were"
'                ElseIf DeformationNames(0) = "Denting" Then
'                    StrPluralDeformation = "Dents"
'                    strWasWere = "were"
'                End If
'                strSentencesCreated = String.Format("{0} {1} found during this examination that exceeded the predetermined threshold of {2}%. ", StrPluralDeformation, strWasWere, FormatNumber(Thresholds(0), 1))
'                strSentencesCreated = String.Format("{0}The maximum percentage of {1}, {2}%, was located in {3} of Pass {4}. ", strSentencesCreated, DeformationNames(0).ToString.ToLower, FormatNumber(WorstPercents(0), 1), GeneratePipeTubeName(PipeSegments(0), pipeOrBend), Passes(0))
'            End If
'        Else
'            'Make a comma separated list of the types of deformation names present (Bulging, Swelling, and SomethingElse)
'            Dim strCommaJoinedDeformationNames As String
'            Dim strFormattedThresholds As String

'            For i = 0 To DeformationNames.Count - 1
'                If i = 0 Then
'                    strCommaJoinedDeformationNames = DeformationNames(0)
'                ElseIf i > 0 And i < DeformationNames.Count - 1 Then
'                    strCommaJoinedDeformationNames = strCommaJoinedDeformationNames & ", " & DeformationNames(i)
'                    strFormattedThresholds = strFormattedThresholds & ", "
'                ElseIf i = DeformationNames.Count - 1 And DeformationNames.Count > 2 Then
'                    strCommaJoinedDeformationNames = strCommaJoinedDeformationNames & ", and " & DeformationNames(i)
'                    strFormattedThresholds = strFormattedThresholds & ", and "
'                ElseIf DeformationNames.Count = 2 And i = DeformationNames.Count - 1 Then
'                    strCommaJoinedDeformationNames = strCommaJoinedDeformationNames & " and " & DeformationNames(i)
'                    strFormattedThresholds = strFormattedThresholds & " and "
'                End If
'                strFormattedThresholds = strFormattedThresholds & FormatNumber(Thresholds(i), 1) & "%"
'            Next
'            Dim strRespectivelyIfNeeded As String
'            If DeformationNames.Count > 1 Then strRespectivelyIfNeeded = ", respectively"
'            'write the first sentence using the names we just put into that comma separated string
'            strSentencesCreated = String.Format("Several types of pipe deformations ({0}) were found during this examination that exceeded the predetermined threshold of {1}{2}. ", strCommaJoinedDeformationNames.ToLower, strFormattedThresholds, strRespectivelyIfNeeded)
'            For i = 0 To DeformationNames.Count - 1
'                'write an additional sentence pointing out the worst deformation of each type
'                strSentencesCreated = String.Format("{0}The maximum percentage of {1}, {2}%, was located in {3} of Pass {4}. ", strSentencesCreated, DeformationNames(i).tolower, FormatNumber(WorstPercents(i), 1), GeneratePipeTubeName(PipeSegments(i), pipeOrBend), Passes(i))

'            Next
'        End If
'        Return strSentencesCreated
'    End Function
'    Private Function WriteExecutiveSummaryAboveAndBelowDeformationSentence(ByVal TheDeformationData As DeformationData, pipeOrBend As String)
'        Dim strSentencesCreated As String
'        Dim AreaOrAreas As New ArrayList
'        Dim DeformationNames As New ArrayList
'        Dim DeformationAboveCounts As New ArrayList
'        Dim DeformationBelowCounts As New ArrayList
'        Dim Thresholds As New ArrayList
'        Dim PipeSegments As New ArrayList
'        Dim Passes As New ArrayList
'        Dim WorstPercents As New ArrayList

'        'Fill the string arrays with useful values obtained from the data, discarding the zero values
'        If TheDeformationData.BulgingExceedingCount > 0 And TheDeformationData.BulgingNotExceedingCount > 0 Then
'            DeformationNames.Add("Bulging")
'            Thresholds.Add(TheDeformationData.BulgingThreshold)
'            DeformationAboveCounts.Add(TheDeformationData.BulgingExceedingCount)
'            DeformationBelowCounts.Add(TheDeformationData.BulgingNotExceedingCount)
'            PipeSegments.Add(TheDeformationData.BulgingPipeIDs)
'            Passes.Add(TheDeformationData.BulgingPasses)
'            WorstPercents.Add(TheDeformationData.BulgingWorst)
'            If TheDeformationData.BulgingExceedingCount > 1 Then AreaOrAreas.Add("Areas") Else AreaOrAreas.Add("Area")
'        End If
'        If TheDeformationData.SwellingExceedingCount > 0 And TheDeformationData.SwellingNotExceedingCount > 0 Then
'            DeformationNames.Add("Swelling")
'            DeformationAboveCounts.Add(TheDeformationData.SwellingExceedingCount)
'            DeformationBelowCounts.Add(TheDeformationData.SwellingNotExceedingCount)
'            Thresholds.Add(TheDeformationData.SwellingThreshold)
'            PipeSegments.Add(TheDeformationData.SwellingPipeIDs)
'            Passes.Add(TheDeformationData.SwellingPasses)
'            WorstPercents.Add(TheDeformationData.SwellingWorst)
'            If TheDeformationData.SwellingExceedingCount > 1 Then AreaOrAreas.Add("Areas") Else AreaOrAreas.Add("Area")
'        End If
'        If TheDeformationData.OvalityExceedingCount > 0 And TheDeformationData.OvalityNotExceedingCount > 0 Then
'            DeformationNames.Add("Ovality")
'            DeformationAboveCounts.Add(TheDeformationData.OvalityExceedingCount)
'            DeformationBelowCounts.Add(TheDeformationData.OvalityNotExceedingCount)
'            Thresholds.Add(TheDeformationData.OvalityThreshold)
'            PipeSegments.Add(TheDeformationData.OvalityPipeIDs)
'            Passes.Add(TheDeformationData.OvalityPasses)
'            WorstPercents.Add(TheDeformationData.OvalityWorst)
'            If TheDeformationData.OvalityExceedingCount > 1 Then AreaOrAreas.Add("Areas") Else AreaOrAreas.Add("Area")
'        End If
'        If TheDeformationData.DentingExceedingCount > 0 And TheDeformationData.DentingNotExceedingCount > 0 Then
'            DeformationNames.Add("Denting")
'            DeformationAboveCounts.Add(TheDeformationData.DentingExceedingCount)
'            DeformationBelowCounts.Add(TheDeformationData.DentingNotExceedingCount)
'            Thresholds.Add(TheDeformationData.DentingThreshold)
'            PipeSegments.Add(TheDeformationData.DentingPipeIDs)
'            Passes.Add(TheDeformationData.DentingPasses)
'            WorstPercents.Add(TheDeformationData.DentingWorst)
'            If TheDeformationData.DentingExceedingCount > 1 Then AreaOrAreas.Add("Areas") Else AreaOrAreas.Add("Area")
'        End If
'        'Depending on if the threshold was exceeded or not, write the appropriate sentence.
'        If DeformationNames.Count > 0 Then strSentencesCreated = ExecutiveSummaryExceedingAndNotExceededText(DeformationNames, DeformationAboveCounts, DeformationBelowCounts, Thresholds, PipeSegments, Passes, AreaOrAreas, WorstPercents, pipeOrBend)

'        Return strSentencesCreated
'    End Function
'    Private Function ExecutiveSummaryExceedingAndNotExceededText(ByVal DeformationNames As ArrayList, ByVal DeformationAboveCounts As ArrayList, ByVal DeformationBelowCounts As ArrayList, ByVal Thresholds As ArrayList, ByVal PipeSegments As ArrayList, ByVal Passes As ArrayList, ByVal AreaOrAreas As ArrayList, ByVal WorstPercents As ArrayList, pipeOrBend As String)
'        'Generate the sentences to the tune of 'Several types of pipe deformations (ovality and bulging) were found during the examination'. Its style is based heaviy on how many different types of deformation there are
'        Dim strWasWere As String
'        Dim strSentencesCreated As String = ""
'        If DeformationNames.Count = 1 Then
'            Dim StrPluralDeformation As String
'            If DeformationNames(0) = "Ovality" Or DeformationNames(0) = "Swelling" Then
'                StrPluralDeformation = DeformationNames(0)
'                strWasWere = "was"
'            ElseIf DeformationNames(0) = "Bulging" Then
'                StrPluralDeformation = "Bulges"
'                strWasWere = "were"
'            ElseIf DeformationNames(0) = "Denting" Then
'                StrPluralDeformation = "Dents"
'                strWasWere = "were"
'            End If
'            strSentencesCreated = String.Format("{0} {1} found during this examination that {1} above and below the predetermined threshold of {2}%. ", StrPluralDeformation, strWasWere, FormatNumber(Thresholds(0), 1))
'            strSentencesCreated = String.Format("{0}The maximum percentage of {1}, {2}%, was located in {3} of Pass {4}. ", strSentencesCreated, DeformationNames(0).ToString.ToLower, FormatNumber(WorstPercents(0), 1), GeneratePipeTubeName(PipeSegments(0), pipeOrBend), Passes(0))

'        Else
'            'Make a comma separated list of the types of deformation names present (Bulging, Swelling, and SomethingElse)
'            Dim strCommaJoinedDeformationNames As String
'            For i = 0 To DeformationNames.Count - 1
'                If i = 0 Then
'                    strCommaJoinedDeformationNames = DeformationNames(0)
'                ElseIf i > 0 And i < DeformationNames.Count - 1 Then : strCommaJoinedDeformationNames = strCommaJoinedDeformationNames & ", " & DeformationNames(i)
'                ElseIf i = DeformationNames.Count - 1 And DeformationNames.Count > 2 Then : strCommaJoinedDeformationNames = strCommaJoinedDeformationNames & ", and " & DeformationNames(i)
'                ElseIf DeformationNames.Count = 2 And i = DeformationNames.Count - 1 Then : strCommaJoinedDeformationNames = strCommaJoinedDeformationNames & " and " & DeformationNames(i)
'                End If
'            Next
'            'write the first sentence using the names we just put into that comma separated string
'            strSentencesCreated = String.Format("Several types of pipe deformations ({0}) were found during this examination that were above and below the predetermined threshold. ", strCommaJoinedDeformationNames.ToLower)
'            For i = 0 To DeformationNames.Count - 1
'                'write an additional sentence pointing out the worst deformation of each type
'                strSentencesCreated = String.Format("{0}The maximum percentage of {1}, {2}%, was located in {3} of Pass {4}. ", strSentencesCreated, DeformationNames(i).tolower, FormatNumber(WorstPercents(0), 1), GeneratePipeTubeName(PipeSegments(i), pipeOrBend), Passes(i))
'            Next
'        End If
'        Return strSentencesCreated
'    End Function
'    Private Function WriteExecutiveSummarynotExceedingDeformationSentence(ByVal TheDeformationData As DeformationData)
'        Dim strSentencesCreated As String
'        Dim AreaOrAreas As New ArrayList
'        Dim DeformationNames As New ArrayList
'        Dim DeformationCounts As New ArrayList
'        Dim Thresholds As New ArrayList
'        Dim PipeSegments As New ArrayList
'        Dim Passes As New ArrayList

'        'Fill the string arrays with useful values obtained from the data, discarding the zero values
'        If TheDeformationData.BulgingNotExceedingCount > 0 And TheDeformationData.BulgingExceedingCount = 0 Then
'            DeformationNames.Add("Bulging")
'            Thresholds.Add(TheDeformationData.BulgingThreshold)
'            DeformationCounts.Add(TheDeformationData.BulgingNotExceedingCount)
'            PipeSegments.Add(TheDeformationData.BulgingPipeIDs)
'            Passes.Add(TheDeformationData.BulgingPasses)
'            If TheDeformationData.BulgingNotExceedingCount > 1 Then AreaOrAreas.Add("Areas") Else AreaOrAreas.Add("Area")
'        End If
'        If TheDeformationData.SwellingNotExceedingCount > 0 And TheDeformationData.SwellingExceedingCount = 0 Then
'            DeformationNames.Add("Swelling")
'            DeformationCounts.Add(TheDeformationData.SwellingNotExceedingCount)
'            Thresholds.Add(TheDeformationData.SwellingThreshold)
'            PipeSegments.Add(TheDeformationData.SwellingPipeIDs)
'            Passes.Add(TheDeformationData.SwellingPasses)
'            If TheDeformationData.SwellingNotExceedingCount > 1 Then AreaOrAreas.Add("Areas") Else AreaOrAreas.Add("Area")
'        End If
'        If TheDeformationData.OvalityNotExceedingCount > 0 And TheDeformationData.OvalityExceedingCount = 0 Then
'            DeformationNames.Add("Ovality")
'            DeformationCounts.Add(TheDeformationData.OvalityNotExceedingCount)
'            Thresholds.Add(TheDeformationData.OvalityThreshold)
'            PipeSegments.Add(TheDeformationData.OvalityPipeIDs)
'            Passes.Add(TheDeformationData.OvalityPasses)
'            If TheDeformationData.OvalityNotExceedingCount > 1 Then AreaOrAreas.Add("Areas") Else AreaOrAreas.Add("Area")
'        End If
'        If TheDeformationData.DentingNotExceedingCount > 0 And TheDeformationData.DentingExceedingCount = 0 Then
'            DeformationNames.Add("Denting")
'            DeformationCounts.Add(TheDeformationData.DentingNotExceedingCount)
'            Thresholds.Add(TheDeformationData.DentingThreshold)
'            PipeSegments.Add(TheDeformationData.DentingPipeIDs)
'            Passes.Add(TheDeformationData.DentingPasses)
'            If TheDeformationData.DentingNotExceedingCount > 1 Then AreaOrAreas.Add("Areas") Else AreaOrAreas.Add("Area")
'        End If
'        'Depending on if the threshold was exceeded or not, write the appropriate sentence.
'        If DeformationNames.Count > 0 Then strSentencesCreated = ExecutiveSummaryNotExceedingText(DeformationNames, DeformationCounts, Thresholds, PipeSegments, Passes, AreaOrAreas)

'        Return strSentencesCreated
'    End Function

'    Private Function ExecutiveSummaryNotExceedingText(ByVal DeformationNames As ArrayList, ByVal DeformationCounts As ArrayList, ByVal Thresholds As ArrayList, ByVal PipeSegments As ArrayList, ByVal Passes As ArrayList, ByVal AreaOrAreas As ArrayList)
'        'Generate the sentences to the tune of 'Several types of pipe deformations (ovality and bulging) were found during the examination'. Its style is based heaviy on how many different types of deformation there are
'        Dim strWasWere As String
'        Dim strSentencesCreated As String = ""
'        If DeformationNames.Count = 1 Then
'            'A special case sentence is when there is only one type of deformation and only one of them, IE: only one bulge was found.
'            If DeformationNames.Count = 1 And DeformationCounts(0) = 1 Then
'                strSentencesCreated = String.Format("An area of {0} was found during this examination but fell below the predetermined threshold of {1}%. ", DeformationNames(0), FormatNumber(Thresholds(0), 1))
'            ElseIf DeformationCounts(0) > 1 Then
'                Dim StrPluralDeformation As String
'                If DeformationNames(0) = "Ovality" Or DeformationNames(0) = "Swelling" Then
'                    StrPluralDeformation = DeformationNames(0)
'                    strWasWere = "was"
'                ElseIf DeformationNames(0) = "Bulging" Then
'                    StrPluralDeformation = "Bulges"
'                    strWasWere = "were"
'                ElseIf DeformationNames(0) = "Denting" Then
'                    StrPluralDeformation = "Dents"
'                    strWasWere = "were"
'                End If
'                'The case for only one deformation type but more than one instance of that defect
'                strSentencesCreated = String.Format("{0} {1} found during this examination but fell below the predetermined threshold of {2}%. ", StrPluralDeformation, strWasWere, FormatNumber(Thresholds(0), 1))

'            End If
'        Else
'            'Make a comma separated list of the types of deformation names present (Bulging, Swelling, and SomethingElse)
'            Dim strCommaJoinedDeformationNames As String
'            Dim strFormattedThresholds As String
'            For i = 0 To DeformationNames.Count - 1
'                If i = 0 Then
'                    strCommaJoinedDeformationNames = DeformationNames(0)
'                ElseIf i > 0 And i < DeformationNames.Count - 1 Then
'                    strCommaJoinedDeformationNames = strCommaJoinedDeformationNames & ", " & DeformationNames(i)
'                    strFormattedThresholds = strFormattedThresholds & ", "
'                ElseIf i = DeformationNames.Count - 1 And DeformationNames.Count > 2 Then
'                    strCommaJoinedDeformationNames = strCommaJoinedDeformationNames & ", and " & DeformationNames(i)
'                    strFormattedThresholds = strFormattedThresholds & ", and "
'                ElseIf DeformationNames.Count = 2 And i = DeformationNames.Count - 1 Then
'                    strCommaJoinedDeformationNames = strCommaJoinedDeformationNames & " and " & DeformationNames(i)
'                    strFormattedThresholds = strFormattedThresholds & " and "
'                End If
'                strFormattedThresholds = strFormattedThresholds & FormatNumber(Thresholds(i), 1) & "%"

'            Next
'            Dim strRespectivelyIfNeeded As String
'            If DeformationNames.Count > 1 Then strRespectivelyIfNeeded = ", respectively"
'            'This is the sentence for multiple types of pipe deformations
'            strSentencesCreated = String.Format("Several types of pipe deformations ({0}) were found during this examination but fell below the predetermined thresholds of {1}{2}. ", strCommaJoinedDeformationNames.ToString.ToLower, strFormattedThresholds, strRespectivelyIfNeeded)

'        End If
'        Return strSentencesCreated
'    End Function
'#End Region
'    Private Function GenerateExecutiveSummaryFoulingText(ByVal TheDeformationData As DeformationData)
'        'This write three sentences that explain internal fouling if it was present for general or the executive summary (same thing)
'        Dim sentencesToReturn As String = ""
'        If TheDeformationData.InternalFouling = True Then
'            sentencesToReturn = String.Format("Internal fouling was present {0} The presence of internal fouling prevents the UT signal from measuring wall thickness. This can mask wall thinning and/or pipe deformation and reduce our ability to detect and quantify those anomalies. ", TheDeformationData.InternalFoulingExecutiveText)
'        End If

'        Return sentencesToReturn
'    End Function
'    Private Function GeneratePassSummaryFoulingText(ByVal TheDeformationData As DeformationData, ByVal strAppendix As String)
'        'This writes a sentence about internal fouling if it's present for a pass or section summary.(Same thing)
'        Dim sentenceToReturn As String = ""
'        Dim strAppendixSpiel As String = ""
'        If TheDeformationData.InternalFouling = True Then
'            strAppendixSpiel = String.Format(" (see Appendix {0} for additional details on all pipe segments)", strAppendix)
'            sentenceToReturn = String.Format("Internal fouling was present in this pass{0}. ", strAppendixSpiel)
'        End If
'        Return sentenceToReturn
'    End Function
'    Private Function GeneratePipeTubeName(ByVal PipeName As String, ByVal pipeOrBend As String)
'        'Try to make something like 'Convection Pipe Segment 4B' if the input looks like 'conv 4B'. If it's not convection or Radiant, just put the first word first and the name of the pipe last.
'        If IsNothing(PipeName) Then
'            MessageBox.Show("Worst Pipe ID is missing from one of the deformations.", "Missing Pipe ID", MessageBoxButtons.OK)
'            Return ""
'        End If
'        Dim strReturnValue As String = ""
'        Try
'            If PipeName.ToLower.Contains("conv") Then
'                Dim _splitNames() As String = PipeName.Split(" ") 'split something like 'conv 4' at the space
'                If pipeOrBend = "Pipe" Then
'                    strReturnValue = "Convection Pipe Segment " & _splitNames(1)
'                ElseIf pipeOrBend = "Bend" Then
'                    strReturnValue = "Convection Bend " & _splitNames(1)
'                End If
'            ElseIf PipeName.ToLower.Contains("rad") Then
'                Dim _splitNames() As String = PipeName.Split(" ") 'split something like 'conv 4' at the space
'                If pipeOrBend = "Pipe" Then
'                    strReturnValue = "Radiant Pipe Segment " & _splitNames(1)
'                ElseIf pipeOrBend = "Bend" Then
'                    strReturnValue = "Radiant Bend " & _splitNames(1)
'                End If
'            Else
'                Dim _splitNames() As String = PipeName.Split(" ") 'split something like 'Economizer 4' at the space
'                If pipeOrBend = "Pipe" Then
'                    strReturnValue = _splitNames(0) & " Pipe Segment " & _splitNames(1)
'                ElseIf pipeOrBend = "Bend" Then
'                    strReturnValue = _splitNames(0) & " Bend " & _splitNames(1)
'                End If
'            End If
'        Catch
'            MessageBox.Show(String.Format("Could not analyze {0} from the Worst Pipe ID's listed in the deformations. Expected format is a section name and an identifier separated by a space (IE: Con 4, Rad 5, or Economizer 5", PipeName), "Improper Deformation Pipe ID Format.", MessageBoxButtons.OK)
'        End Try
'        Return strReturnValue
'    End Function
'#End Region
'End Module
