Imports Microsoft.Office.Interop


Public Class cReportData
    Const BM_CUSTOMER = "BM_CUSTOMER"
    Const BM_DAMAGE_DISTRIBUTION = "BM_DAMAGE_DISTRIBUTION"
    Const BM_DETAILS_SUMMARY = "BM_DETAILS_SUMMARY"
    Const BM_DIAGROWTH_SUMMARY = "BM_DIAGROWTH_SUMMARY"
    Const BM_EQUATION = "BM_EQUATION"
    Const BM_EXECUTIVE_SUMMARY = "BM_EXECUTIVE_SUMMARY"
    Const BM_FFS_SUMMARY = "BM_FFS_SUMMARY"
    Const BM_FOOTERID = "BM_FOOTERID"
    Const BM_HISTORY_LOCATION_DESC = "BM_HISTORY_LOCATION_DESC"
    Const BM_HISTORY_PRIOR_TEXT = "BM_HISTORY_PRIOR_TEXT"
    Const BM_INSPECTION_SUMMARY = "BM_INSPECTION_SUMMARY"
    Const BM_JMC_LOGO = "BM_JMC"
    Const BM_LEVEL3_PAGE = "BM_LEVEL3_PAGE"
    Const BM_OVERALLTUBEDESIGNLENGTH = "BM_OVERALLTUBEDESIGNLENGTH"
    Const BM_PIPE_LAYOUT_DESCRIPTION = "BM_PIPE_LAYOUT_DESCRIPTION"
    Const BM_PIPE_LAYOUT_UNITNAMEANDNUMBER = "BM_PIPE_LAYOUT_UNITNAMEANDNUMBER"
    Const BM_PIPETUBE_DETS_AGE_AT_INSPECTION = "BM_PIPETUBE_DETS_AGE_AT_INSPECTION"
    Const BM_PIPETUBE_DETS_ALLOWANCE = "BM_PIPETUBE_DETS_ALLOWANCE"
    Const BM_PIPETUBE_DETS_FLOWDIRECTION = "BM_PIPETUBE_DETS_FLOWDIRECTION"
    Const BM_PIPETUBE_DETS_INNERDIMS = "BM_PIPETUBE_DETS_INNERDIMS"
    Const BM_PIPETUBE_DETS_INSPECTION_LOCATION = "BM_PIPETUBE_DETS_INSPECTION_LOCATION"
    Const BM_PIPETUBE_DETS_LENGTHS = "BM_PIPETUBE_DETS_LENGTHS"
    Const BM_PIPETUBE_DETS_MATERIAL = "BM_PIPETUBE_DETS_MATERIAL"
    Const BM_PIPETUBE_DETS_NEXTINSPECTIONDATE = "BM_PIPETUBE_DETS_NEXTINSPECTIONDATE"
    Const BM_PIPETUBE_DETS_OPERATING_HOURS = "BM_PIPETUBE_DETS_OPERATING_HOURS"
    Const BM_PIPETUBE_DETS_OUTERDIMS = "BM_PIPETUBE_DETS_OUTERDIMS"
    Const BM_PIPETUBE_DETS_SECTIONS_INSP = "BM_PIPETUBE_DETS_SECTIONS_INSP"
    Const BM_PIPETUBE_DETS_SERVICE = "BM_PIPETUBE_DETS_SERVICE"
    Const BM_PIPETUBE_DETS_SERVICEDATE = "BM_PIPETUBE_DETS_SERVICEDATE"
    Const BM_PIPETUBE_DETS_SURFACE = "BM_PIPETUBE_DETS_SURFACE"
    Const BM_PIPETUBE_DETS_TEMPERATURE = "BM_PIPETUBE_DETS_TEMPERATURE"
    Const BM_PIPETUBE_DETS_THERMALCYCLES = "BM_PIPETUBE_DETS_THERMALCYCLES"
    Const BM_PIPETUBE_DETS_THICKNESS = "BM_PIPETUBE_DETS_THICKNESS"
    Const BM_PIPETUBE_DETS_TUBEMANUFACTURER = "BM_PIPETUBE_DETS_TUBEMANUFACTURER"
    Const BM_PIPETUBE_DETS_TUBEPRESSURE = "BM_PIPETUBE_DETS_TUBEPRESSURE"
    Const BM_PIPETUBE_DETS_UNIT_MANUFACTURER = "BM_PIPETUBE_DETS_UNIT_MANUFACTURER"
    Const BM_PIPETUBE_DETS_UNITNAMEANDNUMBER = "BM_PIPETUBE_DETS_UNITNAMEANDNUMBER"
    Const BM_PIPETUBE_SUMMARY = "BM_PIPETUBE_SUMMARY"
    Const BM_PROJECT_DESC = "BM_PROJECT_DESC"
    Const BM_PROJSUM_CLIENT_CONTACTS = "BM_PROJSUM_CLIENT_CONTACTS"
    Const BM_PROJSUM_CLIENT_PO = "BM_PROJSUM_CLIENT_PO"
    Const BM_PROJSUM_LOCATION_FULL = "BM_PROJSUM_LOCATION_FULL"
    Const BM_PROJSUM_QTT_INSPECTIONDATE = "BM_PROJSUM_QTT_INSPECTIONDATE"
    Const BM_PROJSUM_QTT_INSPECTORS = "BM_PROJSUM_QTT_INSPECTORS"
    Const BM_PROJSUM_QTT_PROJNUM = "BM_PROJSUM_QTT_PROJNUM"
    Const BM_PROJSUM_UNITNAMEANDNUMBER = "BM_PROJSUM_UNITNAMEANDNUMBER"
    Const BM_QTT_PREPBY_NAME = "BM_QTT_PREPBY_NAME"
    Const BM_QTT_PREPBY_TITLE = "BM_QTT_PREPBY_TITLE"
    Const BM_SMRY_LEADINSPECTOR_NAME = "BM_SMRY_LEADINSPECTOR_NAME"
    Const BM_SMRY_LEADINSPECTOR_PHONE = "BM_SMRY_LEADINSPECTOR_PHONE"
    Const BM_SMRY_LOCATION_DESC = "BM_SMRY_LOCATION_DESC"
    Const BM_SMRY_LOCATION2_DESC = "BM_SMRY_LOCATION2_DESC"
    Const BM_SMRY_QTT_OFFICE_INFO = "BM_SMRY_QTT_OFFICE_INFO"
    Const BM_TITLE_INSPECTIONDATE = "BM_TITLE_INSPECTIONDATE"
    Const BM_TITLE_LOCATION_CITYSTATE = "BM_TITLE_LOCATION_CITYSTATE"
    Const BM_TITLE_LOCATION_NAME = "BM_TITLE_LOCATION_NAME"
    Const BM_TITLE_PREPD_FOR = "BM_TITLE_PREPD_FOR"
    Const BM_TITLE_QTT_OFFICE_INFO = "BM_TITLE_QTT_OFFICE_INFO"
    Const BM_TITLE_QTT_RPT_NUMBER = "BM_TITLE_QTT_RPT_NUMBER"
    Const BM_TITLE_SPARE_UNINSTALLED = "BM_TITLE_SPARE_UNINSTALLED"
    Const BM_TITLE_UNITNAME = "BM_TITLE_UNITNAME"
    Const BM_TITLE_UNITNUMBER = "BM_TITLE_UNITNUMBER"
    Const BM_TOOLDESC = "BM_TOOLS_DESCRIPTION"
    Const BM_TUBE_LAYOUT_DESCRIPTION = "BM_TUBE_LAYOUT_DESCRIPTION"
    Const BM_TUBE_TOLERANCE = "BM_TUBE_TOLERANCE"
    Const BM_TUBEHARVESTING_SUMMARY = "BM_TUBEHARVESTING_SUMMARY"
    Const DOC_CLIENT_NUM = "ClientNum"
    Const DOC_IMPERIAL = "Imperial"

    'Holds properties
    Private mReportData As InspectionReportData
    'Var for pipetube items 
    Private mPTI_RptItems As cPipeTubeReportDataItems
    'Flag to hold custom pass info

    Public Property PipeTubeDataItems() As cPipeTubeReportDataItems
        Get
            ' instance as needed
            If (mPTI_RptItems Is Nothing) Then
                mPTI_RptItems = New cPipeTubeReportDataItems
            End If
            PipeTubeDataItems = mPTI_RptItems
        End Get
        Set(ByVal value As cPipeTubeReportDataItems)
            mPTI_RptItems = value
        End Set
    End Property

    Public Property ReportLinkedFiles() As cReportLinkedFiles
        'Holds collection of all linked files (items) in main report
        Get
            ' instance as needed
            If (mReportData.LinkedReportFiles Is Nothing) Then
                mReportData.LinkedReportFiles = New cReportLinkedFiles
            End If
            ReportLinkedFiles = mReportData.LinkedReportFiles
        End Get
        Set(ByVal value As cReportLinkedFiles)
            mReportData.LinkedReportFiles = value
        End Set
    End Property

    Public Property AppendixReportDatas() As cAppendixReportDatas
        'Holds collection of all appendix sections (metadata and linked items) for the report
        Get
            ' instance as needed
            If (mReportData.AppendixDatas Is Nothing) Then
                mReportData.AppendixDatas = New cAppendixReportDatas
            End If
            AppendixReportDatas = mReportData.AppendixDatas
        End Get
        Set(ByVal value As cAppendixReportDatas)
            mReportData.AppendixDatas = value
        End Set
    End Property

    Public Property Client_PO() As String
        Get
            Client_PO = mReportData.Client_PO
        End Get
        Set(ByVal value As String)
            mReportData.Client_PO = value
        End Set
    End Property

    Public Property MSWord_ReportTemplate() As String
        Get
            MSWord_ReportTemplate = mReportData.MSWord_ReportTemplate
        End Get
        Set(ByVal value As String)
            mReportData.MSWord_ReportTemplate = value
        End Set
    End Property

    Public Property ClientContacts() As String
        Get
            ClientContacts = mReportData.ClientContacts '= Value
        End Get
        Set(ByVal value As String)
            mReportData.ClientContacts = value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            CustomerName = mReportData.CustomerName
        End Get
        Set(ByVal value As String)
            mReportData.CustomerName = value
        End Set
    End Property

    Public Property FooterID() As String
        Get
            FooterID = mReportData.FooterID
        End Get
        Set(ByVal value As String)
            mReportData.FooterID = value
        End Set
    End Property

    Public Property InspectionDate() As Date
        Get
            InspectionDate = mReportData.InspectionDate
        End Get
        Set(ByVal value As Date)
            mReportData.InspectionDate = value
        End Set
    End Property

    Public Property LocationDescription() As String
        Get
            LocationDescription = mReportData.LocationDescription
        End Get
        Set(ByVal value As String)
            mReportData.LocationDescription = value
        End Set
    End Property

    Public Property QTT_OfficeInfo() As String
        Get
            QTT_OfficeInfo = mReportData.QTT_OfficeInfo
        End Get
        Set(ByVal value As String)
            mReportData.QTT_OfficeInfo = value
        End Set
    End Property

    Public Property QTT_InspectorsOther() As String
        Get
            QTT_InspectorsOther = mReportData.QTT_InspectorsOther
        End Get
        Set(ByVal value As String)
            mReportData.QTT_InspectorsOther = value
        End Set
    End Property

    Public Property PriorHistory() As String
        Get
            PriorHistory = mReportData.PriorHistory
        End Get
        Set(ByVal value As String)
            mReportData.PriorHistory = value
        End Set
    End Property

    Public Property PrimaryInspectorName() As String
        Get
            PrimaryInspectorName = mReportData.PrimaryInspectorName
        End Get
        Set(ByVal value As String)
            mReportData.PrimaryInspectorName = value
        End Set
    End Property

    Public Property PrimaryInspectorPhone() As String
        Get
            PrimaryInspectorPhone = mReportData.PrimaryInspectorPhone
        End Get
        Set(ByVal value As String)
            mReportData.PrimaryInspectorPhone = value
        End Set
    End Property

    Public Property PrimaryInspectorTitle() As String
        Get
            PrimaryInspectorTitle = mReportData.PrimaryInspectorTitle
        End Get
        Set(ByVal value As String)
            mReportData.PrimaryInspectorTitle = value
        End Set
    End Property

    Public Property InspectionDescription() As String
        Get
            InspectionDescription = mReportData.InspectionDescription
        End Get
        Set(ByVal value As String)
            mReportData.InspectionDescription = value
        End Set
    End Property

    Public Property InspectionSummary() As String
        Get
            InspectionSummary = mReportData.InspectionSummary
        End Get
        Set(ByVal value As String)
            mReportData.InspectionSummary = value
        End Set
    End Property

    Public Property ExecutiveSummary() As String
        Get
            ExecutiveSummary = mReportData.ExecutiveSummary
        End Get
        Set(ByVal value As String)
            mReportData.ExecutiveSummary = value
        End Set
    End Property

    Public Property DetailsSummary() As String
        Get
            DetailsSummary = mReportData.DetailsSummary
        End Get
        Set(ByVal value As String)
            mReportData.DetailsSummary = value
        End Set
    End Property

    Public Property DiaGrowthSummary() As String
        Get
            DiaGrowthSummary = mReportData.DiaGrowthSummary
        End Get
        Set(ByVal value As String)
            mReportData.DiaGrowthSummary = value
        End Set
    End Property

    Public Property FFSSummary() As String
        Get
            FFSSummary = mReportData.FFSSummary
        End Get
        Set(ByVal value As String)
            mReportData.FFSSummary = value
        End Set
    End Property

    Public Property TubeHarvestingSummary() As String
        Get
            TubeHarvestingSummary = mReportData.TubeHarvestingSummary
        End Get
        Set(ByVal value As String)
            mReportData.TubeHarvestingSummary = value
        End Set
    End Property
    Public Property NumberOfPasses() As Integer
        Get
            NumberOfPasses = mReportData.NumberOfPasses
        End Get
        Set(ByVal value As Integer)
            mReportData.NumberOfPasses = value
        End Set
    End Property

    Public Property ToolDescription() As String
        Get
            ToolDescription = mReportData.ToolDescription
        End Get
        Set(ByVal value As String)
            mReportData.ToolDescription = value
        End Set
    End Property

    Public Property QTT_ProjectNum() As String
        Get
            QTT_ProjectNum = mReportData.QTT_ProjectNum
        End Get
        Set(ByVal value As String)
            mReportData.QTT_ProjectNum = value
        End Set
    End Property

    Public Property QTT_ReportDate() As Date
        Get
            QTT_ReportDate = mReportData.QTT_ReportDate
        End Get
        Set(ByVal value As Date)
            mReportData.QTT_ReportDate = value
        End Set
    End Property

    Public Property QTT_ReportNumber() As String
        Get
            QTT_ReportNumber = mReportData.QTT_ReportNumber
        End Get
        Set(ByVal value As String)
            mReportData.QTT_ReportNumber = value
        End Set
    End Property

    Public Property UnitName() As String
        Get
            UnitName = mReportData.UnitName
        End Get
        Set(ByVal value As String)
            mReportData.UnitName = value
        End Set
    End Property

    Public Property UnitNumber() As String
        Get
            UnitNumber = mReportData.UnitNumber
        End Get
        Set(ByVal value As String)
            mReportData.UnitNumber = value
        End Set
    End Property

    Public Property UnitService() As String
        Get
            UnitService = mReportData.UnitService
        End Get
        Set(ByVal value As String)
            mReportData.UnitService = value
        End Set
    End Property

    Public Property UnitManufacturer() As String
        Get
            UnitManufacturer = mReportData.UnitManufacturer
        End Get
        Set(ByVal value As String)
            mReportData.UnitManufacturer = value
        End Set
    End Property

    Public Property SpareUninstalled() As String
        Get
            SpareUninstalled = mReportData.SpareUninstalled
        End Get
        Set(ByVal value As String)
            mReportData.SpareUninstalled = value
        End Set
    End Property
    Public Property BurnerConfiguration() As String
        Get
            BurnerConfiguration = mReportData.BurnerConfiguration
        End Get
        Set(ByVal value As String)
            mReportData.BurnerConfiguration = value
        End Set
    End Property

    Public Property PlantProcessType() As String
        Get
            PlantProcessType = mReportData.PlantProcessType
        End Get
        Set(ByVal value As String)
            mReportData.PlantProcessType = value
        End Set
    End Property

    Public Property ProcessFlowDirection() As String
        Get
            ProcessFlowDirection = mReportData.ProcessFlowDirection
        End Get
        Set(ByVal value As String)
            mReportData.ProcessFlowDirection = value
        End Set
    End Property

    Public Property NumberingSystem() As Integer
        Get
            NumberingSystem = mReportData.NumberingSystem
        End Get
        Set(ByVal value As Integer)
            mReportData.NumberingSystem = value
        End Set
    End Property

    Public Property SampleLevel3() As Boolean
        Get
            SampleLevel3 = mReportData.SampleLevel3
        End Get
        Set(ByVal value As Boolean)
            mReportData.SampleLevel3 = value
        End Set
    End Property

    Public Property JMC() As Boolean
        Get
            JMC = mReportData.JMC
        End Get
        Set(ByVal value As Boolean)
            mReportData.JMC = value
        End Set
    End Property

    Public Property Address() As String
        Get
            Address = mReportData.Address
        End Get
        Set(ByVal value As String)
            mReportData.Address = value
        End Set
    End Property

    Public Property City() As String
        Get
            City = mReportData.City
        End Get
        Set(ByVal value As String)
            mReportData.City = value
        End Set
    End Property

    Public Property State() As String
        Get
            State = mReportData.State
        End Get
        Set(ByVal value As String)
            mReportData.State = value
        End Set
    End Property

    Public Property PostalCode() As String
        Get
            PostalCode = mReportData.PostalCode
        End Get
        Set(ByVal value As String)
            mReportData.PostalCode = value
        End Set
    End Property

    Public Property Country() As String
        Get
            Country = mReportData.Country
        End Get
        Set(ByVal value As String)
            mReportData.Country = value
        End Set
    End Property

    Public Property InspectionLayout() As String
        Get
            InspectionLayout = mReportData.InspectionLayout
        End Get
        Set(ByVal value As String)
            mReportData.InspectionLayout = value
        End Set
    End Property

    Public Property QTT_ReportVersion() As Integer
        Get
            QTT_ReportVersion = mReportData.QTT_ReportVersion
        End Get
        Set(ByVal value As Integer)
            mReportData.QTT_ReportVersion = value
        End Set
    End Property

    Public Property Imperial() As Boolean
        Get
            Imperial = mReportData.Imperial
        End Get
        Set(ByVal value As Boolean)
            mReportData.Imperial = value
        End Set
    End Property

    Public Property CustomPasses() As Boolean
        Get
            CustomPasses = mReportData.CustomPasses
        End Get
        Set(ByVal value As Boolean)
            mReportData.CustomPasses = value
        End Set
    End Property

    Public Property ClientNum() As Boolean
        Get
            ClientNum = mReportData.ClientNum
        End Get
        Set(ByVal value As Boolean)
            mReportData.ClientNum = value
        End Set
    End Property

    Public Property QTT_ReportRevision() As Integer
        Get
            QTT_ReportRevision = mReportData.QTT_ReportRevision
        End Get
        Set(ByVal value As Integer)
            mReportData.QTT_ReportRevision = value
        End Set
    End Property

    Shared Property mPassesCustom As Boolean

    Public Sub WriteToInspectionReport(ByVal oWordRpt As cMSWord, ByVal pPageSize_A4 As Boolean)
        'Generic print routine designed to handle all Inspection Types

        Const DATE_FORMAT_LONG = "MMMM d, yyyy"   'i.e. format as "January 5, 2006" No leading zero
        Dim strDesc As String = ""
        Dim intItem As Integer
        Dim intSmry As Integer
        Dim intImg As Integer
        Dim oPTIdata As cPipeTubeReportData
        Dim strTemp As String = ""
        Dim intPassNum As Integer

        With oWordRpt

            ' Set document properties
            ' KM 11/1/2011
            .SetDocProp("Imperial", Me.Imperial)
            .SetDocProp("ClientNum", Me.ClientNum)

            'footer ID
            .InsertAtBookMark(BM_FOOTERID, Me.FooterID)

            'Title Page
            .InsertAtBookMark(BM_TITLE_UNITNAME, Me.UnitName)
            .InsertAtBookMark(BM_TITLE_UNITNUMBER, Me.UnitNumber)
            .InsertAtBookMark(BM_TITLE_LOCATION_NAME, Me.LocationDescription)
            .InsertAtBookMark(BM_TITLE_SPARE_UNINSTALLED, Me.SpareUninstalled)

            'Assemble city state and country string
            strTemp = Me.City
            If Len(Me.State) > 0 Then
                strTemp = strTemp & ", " & Me.State
            End If
            If Len(Me.Country) > 0 Then
                strTemp = strTemp & vbVerticalTab & Me.Country
            End If
            .InsertAtBookMark(BM_TITLE_LOCATION_CITYSTATE, strTemp)
            .InsertAtBookMark(BM_TITLE_INSPECTIONDATE, Format(Me.InspectionDate, DATE_FORMAT_LONG))
            .InsertAtBookMark(BM_TITLE_PREPD_FOR, Me.CustomerName)
            .InsertAtBookMark(BM_TITLE_QTT_RPT_NUMBER, Me.QTT_ReportNumber)
            .InsertAtBookMark(BM_TITLE_QTT_OFFICE_INFO, Me.QTT_OfficeInfo)

            'Executive Summary
            .PasteAtBookMark(BM_EXECUTIVE_SUMMARY, Me.ExecutiveSummary)
            .InsertAtBookMark(BM_SMRY_LOCATION_DESC, Me.LocationDescription)
            .InsertAtBookMark(BM_SMRY_LEADINSPECTOR_NAME, Me.PrimaryInspectorName)
            .InsertAtBookMark(BM_SMRY_LEADINSPECTOR_PHONE, Me.PrimaryInspectorPhone)

            'Equation
            .InsertEquation(BM_EQUATION, Me.Imperial)

            'JMC Logo
            If Me.JMC Then .InsertJMCLogo(BM_JMC_LOGO)

            'Project Summary
            'Location full description and address uses several fields
            strDesc = Me.LocationDescription
            If Len(strDesc) > 0 Then strDesc = strDesc & vbVerticalTab
            strDesc = strDesc & Me.Address
            If Len(Me.Address) Then strDesc = strDesc & vbVerticalTab
            strDesc = strDesc & Me.City
            If Len(Me.City) > 0 And Len(Me.State) > 0 Then
                strDesc = strDesc & ", " & Me.State
            End If
            strDesc = strDesc & " " & Me.PostalCode & vbVerticalTab & Me.Country
            .InsertAtBookMark(BM_PROJSUM_LOCATION_FULL, strDesc)
            .InsertAtBookMark(BM_SMRY_QTT_OFFICE_INFO, Me.QTT_OfficeInfo)
            .InsertAtBookMark(BM_PROJSUM_UNITNAMEANDNUMBER, (Me.UnitName & ", " & Me.UnitNumber))

            'Inspector(s) uses several fields
            If Me.PrimaryInspectorTitle <> "" Then strDesc = (Me.PrimaryInspectorName & " (" & Me.PrimaryInspectorTitle & ")")
            If Len(Me.QTT_InspectorsOther) > 0 Then
                strDesc = strDesc & vbVerticalTab & Me.QTT_InspectorsOther
            End If
            .InsertAtBookMark(BM_PROJSUM_QTT_INSPECTORS, strDesc)
            .InsertAtBookMark(BM_PROJSUM_QTT_INSPECTIONDATE, Me.InspectionDate.ToString(DATE_FORMAT_LONG))
            .InsertAtBookMark(BM_PROJSUM_QTT_PROJNUM, Me.QTT_ProjectNum)
            .InsertAtBookMark(BM_PROJSUM_CLIENT_PO, Me.Client_PO)
            .InsertAtBookMark(BM_PROJSUM_CLIENT_CONTACTS, Me.ClientContacts)
            .InsertAtBookMark(BM_QTT_PREPBY_NAME, Me.PrimaryInspectorName)
            .InsertAtBookMark(BM_QTT_PREPBY_TITLE, Me.PrimaryInspectorTitle)

            'Project Description
            .PasteAtBookMark(BM_PROJECT_DESC, Me.InspectionDescription)

            'Prior History
            .PasteAtBookMark(BM_HISTORY_PRIOR_TEXT, (Me.PriorHistory))

            'PipeTube Details
            oPTIdata = Me.PipeTubeDataItems(0)
            .InsertAtBookMark(BM_PIPETUBE_DETS_UNITNAMEANDNUMBER, (Me.UnitName & ", " & Me.UnitNumber))
            .InsertAtBookMark(BM_PIPETUBE_DETS_SERVICE, Me.PlantProcessType)
            .InsertAtBookMark(BM_PIPETUBE_DETS_UNIT_MANUFACTURER, Me.UnitManufacturer)
            .InsertAtBookMark(BM_PIPETUBE_DETS_SURFACE, Me.BurnerConfiguration)
            .InsertAtBookMark(BM_PIPETUBE_DETS_FLOWDIRECTION, Me.ProcessFlowDirection)
            .InsertAtBookMark(BM_PIPETUBE_DETS_TUBEMANUFACTURER, oPTIdata.Manufacturer)
            .InsertAtBookMark(BM_PIPETUBE_DETS_SERVICEDATE, oPTIdata.PutIntoServiceDate)
            .InsertAtBookMark(BM_PIPETUBE_DETS_AGE_AT_INSPECTION, oPTIdata.AgeAtInspection)
            .InsertAtBookMark(BM_PIPETUBE_DETS_MATERIAL, oPTIdata.Material)
            .InsertAtBookMark(BM_PIPETUBE_DETS_INNERDIMS, oPTIdata.InnerDims)
            .InsertAtBookMark(BM_PIPETUBE_DETS_OUTERDIMS, oPTIdata.OuterDims)
            .InsertAtBookMark(BM_PIPETUBE_DETS_THICKNESS, oPTIdata.Thickness)
            .InsertAtBookMark(BM_PIPETUBE_DETS_ALLOWANCE, oPTIdata.InspectionThreshold)
            .InsertAtBookMark(BM_PIPETUBE_DETS_LENGTHS, oPTIdata.Lengths)
            If oPTIdata.NumberTubesTotal = 0 Then
                .InsertAtBookMark(BM_PIPETUBE_DETS_SECTIONS_INSP, oPTIdata.NumberTubesInspected)
            Else
                .InsertAtBookMark(BM_PIPETUBE_DETS_SECTIONS_INSP, String.Format("{0} of {1} Tubes", oPTIdata.NumberTubesInspected, oPTIdata.NumberTubesTotal))
            End If
            .PasteAtBookMarkTrim(BM_PIPETUBE_DETS_INSPECTION_LOCATION, oPTIdata.InspectionLocations)
            .InsertAtBookMark(BM_PIPETUBE_DETS_TUBEPRESSURE, oPTIdata.Pressure)
            'Temperature
            Dim strMarker As String = "F"
            If oPTIdata.DegreesMarker = 1 Then strMarker = "C"
            If oPTIdata.TargetTemperature = "Not Supplied" Then
                .InsertAtBookMark(BM_PIPETUBE_DETS_TEMPERATURE, String.Format("{0}", oPTIdata.TargetTemperature))
            Else
                .InsertAtBookMark(BM_PIPETUBE_DETS_TEMPERATURE, String.Format("{0} Deg. {1}", oPTIdata.TargetTemperature, strMarker))
            End If
            .InsertAtBookMark(BM_PIPETUBE_DETS_THERMALCYCLES, oPTIdata.NumberThermalCycles)
            .InsertAtBookMark(BM_PIPETUBE_DETS_OPERATING_HOURS, oPTIdata.OperatingHours)
            .InsertAtBookMark(BM_PIPETUBE_DETS_NEXTINSPECTIONDATE, oPTIdata.NextInspectionDate)

            'Summaries
            .PasteAtBookMark(BM_INSPECTION_SUMMARY, Me.InspectionSummary)
            .PasteAtBookMark(BM_DETAILS_SUMMARY, Me.DetailsSummary)
            .PasteAtBookMark(BM_DIAGROWTH_SUMMARY, Me.DiaGrowthSummary)
            .PasteAtBookMark(BM_FFS_SUMMARY, Me.FFSSummary)
            .PasteAtBookMarkBold(BM_TUBEHARVESTING_SUMMARY, Me.TubeHarvestingSummary)
            .InsertAtBookMark(BM_CUSTOMER, Me.CustomerName)
            .PasteAtBookMark(BM_TUBE_LAYOUT_DESCRIPTION, Me.InspectionLayout)
            .InsertAtBookMark(BM_OVERALLTUBEDESIGNLENGTH, oPTIdata.Lengths)
            Dim strText As String = ""
            Dim strToolTolNumbers As String
            Dim strToolName As String
            
            Select Case oPTIdata.TolerancesProvidedBy
                Case 0
                    strText += String.Format("Quest Integrity utilized an industry standard tube tolerance for this inspection in the absence of a specified tolerance from {0}.", Me.CustomerName)
                Case 1
                    strText += String.Format("The tube tolerance for this inspection was provided by {0}.", Me.CustomerName)
            End Select
            .InsertAtBookMark(BM_TUBE_TOLERANCE, strText)
            If Not Me.SampleLevel3 Then .DeleteBookmark(BM_LEVEL3_PAGE)
        End With

        'TODO Code could be cleaner
        ' linked files - main report
        For intItem = 0 To (Me.ReportLinkedFiles.Count - 1)
            With Me.ReportLinkedFiles.Item(intItem)
                If Len(.BookMark) > 0 Then
                    If .FileNotFound Then
                        ' if file not found , insert message text
                        oWordRpt.InsertAtBookMark(.BookMark, .ReportText)
                    ElseIf Right(.LinkedFileName, 3) = "skf" Or Right(.LinkedFileName, 3) = "xls" Or Right(.LinkedFileName, 4) = "xlsx" Then
                        'If it is a skf or xls file
                        oWordRpt.InsertAtBookMark(.BookMark, "Copy and paste file " & .LinkedFileName & " file here.")
                    ElseIf .IsImageFile Then
                        ' insert image file
                        oWordRpt.InsertImageAtBookMark(.BookMark, .LinkedFileName)
                    Else
                        ' assume it's an ms word document
                        If Not (.FileNotFound) Then
                            ' calls insert file command
                            oWordRpt.InsertWordDocAtBookMark(.BookMark, .LinkedFileName)
                        Else
                            ' insert error text if file not found
                            oWordRpt.InsertAtBookMark(.BookMark, .ReportText)
                        End If
                    End If
                End If
            End With
        Next

        Const BM_EXPANSION_MAP = "BM_EXPANSION_MAP"
        Const BM_ASSESSMENT = "BM_ASSESSMENT"
        Const BM_3D_REFORMER = "BM_3D_REFORMER"
        Const BM_TUBE_IMAGE = "BM_TUBE_IMAGE"
        Const BM_DIAMETER_GRAPH = "BM_DIAMETER_GRAPH"
        Const BM_TABLE_MAX_SORT = "BM_TABLE_MAX_SORT"
        Const BM_TABLE_ROW_SORT = "BM_TABLE_ROW_SORT"
        Const BM_CALIBRATION_RESULTS = "BM_CALIBRATION_RESULTS"

        Dim fPadForNext As Boolean
        Dim fSize_2PerPage As Boolean

        ' Report Appendixes
        For intItem = 0 To (Me.AppendixReportDatas.Count - 1)
            'For intItem = 0 To (Me.NumberOfPasses - 1)

            With Me.AppendixReportDatas.Item(intItem)

                'TODO CLEAN UP THIS CODE!!! It was convoluted to start with and has been edited since. Needs to be cleaner.

                ' add appendix images / linked files
                For intImg = 0 To (.LinkedAppendixFiles.Count - 1)
                    With .LinkedAppendixFiles.Item(intImg)
                        If .FileNotFound Then
                            'if file not found , insert message text
                            oWordRpt.InsertAtBookMark(.BookMark, .ReportText)
                        ElseIf Right(.LinkedFileName, 3) = "skf" Or Right(.LinkedFileName, 3) = "xls" Or Right(.LinkedFileName, 4) = "xlsx" Then
                            oWordRpt.InsertAtBookMark(.BookMark, "Copy and paste file " & .LinkedFileName & " file here.")
                        ElseIf .IsImageFile Then
                            ' determine Image file attributes
                            fPadForNext = False  'reset each pass
                            Select Case .BookMark
                                ' for 'pad for next'
                                Case BM_DIAMETER_GRAPH, BM_TUBE_IMAGE, BM_3D_REFORMER
                                    fPadForNext = True ' used to flag that image should have new line added after, and bookmark re-positioned
                                Case Else
                                    ' no code needed
                            End Select
                            Select Case .BookMark
                                ' for '2-up sizing'
                                Case BM_DIAMETER_GRAPH, BM_TUBE_IMAGE
                                    fSize_2PerPage = True ' used to flag that image should have new line added after, and bookmark re-positioned
                                Case Else
                                    fSize_2PerPage = False
                            End Select
                            ' calls insert image
                            If Not (.FileNotFound) And Not (.BookMark = "BM_EXPANSION_MAP" Or .BookMark = "BM_ASSESSMENT") Then
                                If .BookMark = "BM_DIAMETER_GRAPH" Then .BookMark = "BM_TUBE_IMAGE"
                                oWordRpt.InsertImageAtBookMark_Appendix(.BookMark, .LinkedFileName, fPadForNext, fSize_2PerPage)
                            Else
                                oWordRpt.InsertAtBookMark_Appendix(.BookMark, "Copy and paste file " & .LinkedFileName & " file here.")
                            End If
                        Else
                            'insert the file
                            If Not (.FileNotFound) Then
                                'Just in case this type of file can't be inserted where it's trying to go, wrap this portion in a try-catch statement
                                Try
                                    ' calls insert file command
                                    oWordRpt.InsertWordDocAtBookMark_Appendix(.BookMark, .LinkedFileName)
                                Catch
                                    ' insert error text if file not found
                                    MessageBox.Show(String.Format("The File: {0} could not be added automatically. It will have to be inserted manually after AutoReporter generates the report.", .LinkedFileName), "Unable to Insert File", MessageBoxButtons.OK)
                                    oWordRpt.InsertAtBookMark_Appendix(.BookMark, .ReportText)
                                End Try
                            Else
                                ' insert error text if file not found
                                oWordRpt.InsertAtBookMark_Appendix(.BookMark, .ReportText)
                            End If
                        End If
                    End With
                Next intImg
            End With
        Next

    End Sub

End Class
