﻿Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraGrid.Views.Base

Public Class DataEditor

    Dim WithEvents cmbToolType As RepositoryItemComboBox = New RepositoryItemComboBox
    Dim g_CurrentFieldID As Integer = 0

    Private Sub pUpdateDatabase()
        Try
            Dim oTable As DataTable = dsQTT_DatabaseEditor.Tables(bsTables.DataMember)
            Select Case oTable.TableName
                Case dsQTT_DatabaseEditor.tblQTT_Offices.TableName
                    tatblQTT_Offices.Update(oTable)
                Case dsQTT_DatabaseEditor.tblQTT_Roles.TableName
                    taRoles.Update(oTable)
                Case dsQTT_DatabaseEditor.tblQTT_Inspectors.TableName
                    taInspectors.Update(oTable)
                Case dsQTT_DatabaseEditor.tblInspectionTools.TableName
                    tatblInspectionTools.Update(oTable)
                Case dsQTT_DatabaseEditor.tblDefaultTextEntries.TableName
                    taTblDefaultTextEntries.Update(oTable)
                Case dsQTT_DatabaseEditor.tblDefaultText_ListItems.TableName
                    taTblDefaultText_ListItems.Update(oTable)
            End Select
        Catch ex As Exception
            MessageBox.Show(Me, "There was a problem updating the database, data has not been saved." & Environment.NewLine & Environment.NewLine & ex.Message, "Database Update Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub DataEditor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'This line of code loads data into the 'dsQTT_DatabaseEditor.tblInspectionTypes' table. You can move, or remove it, as needed.
        Me.taTblInspectionTypes.Fill(Me.dsQTT_DatabaseEditor.tblInspectionTypes)

        Me.taTblDefaultText_ListItems.Fill(Me.dsQTT_DatabaseEditor.tblDefaultText_ListItems)
        Me.taTblDefaultTextEntries.Fill(Me.dsQTT_DatabaseEditor.tblDefaultTextEntries)
        Me.tatblInspectionTools.Fill(Me.dsQTT_DatabaseEditor.tblInspectionTools)
        Me.taInspectors.Fill(Me.dsQTT_DatabaseEditor.tblQTT_Inspectors)
        Me.taRoles.Fill(Me.dsQTT_DatabaseEditor.tblQTT_Roles)
        Me.tatblQTT_Offices.Fill(Me.dsQTT_DatabaseEditor.tblQTT_Offices)

        GridControl1.ForceInitialize()
        uiTableNames.EditValue = "Offices"

    End Sub

    Private Sub GridView1_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles GridView1.InitNewRow
        'Put in an initial FieldID if this is one of the defaultText_ListItems
        If GridView1.Columns.Count > 1 AndAlso GridView1.Columns(2).FieldName = "FieldID" Then
            GridView1.SetRowCellValue(e.RowHandle, GridView1.Columns("FieldID"), g_CurrentFieldID)
        End If
    End Sub

    Private Sub GridView1_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
        pUpdateDatabase()
    End Sub

    Public Sub ApplyDataFilter()
        'This subroutine filters certain sections of the 'default text list' so that only a portion is displayed at a time. It also displays a dropdown selection box for people to choose from on the tool type column.

        GridView1.Columns("FieldID").Visible = False
        'Select Case ComboBox1.Text
        Select Case uiTableNames.Text
            Case "Tube Materials"
                g_CurrentFieldID = 1
                GridView1.Columns("FieldID").FilterInfo = New ColumnFilterInfo("[FieldID] = '1'")
            Case "Tube Manufacturers"
                g_CurrentFieldID = 3
                GridView1.Columns("FieldID").FilterInfo = New ColumnFilterInfo("[FieldID] = '3'")
            Case "Process Flow Directions"
                g_CurrentFieldID = 5
                GridView1.Columns("FieldID").FilterInfo = New ColumnFilterInfo("[FieldID] = '5'")
            Case "Plant Process Types"
                g_CurrentFieldID = 6
                GridView1.Columns("FieldID").FilterInfo = New ColumnFilterInfo("[FieldID] = '6'")
            Case "Reformer OEMs"
                g_CurrentFieldID = 7
                GridView1.Columns("FieldID").FilterInfo = New ColumnFilterInfo("[FieldID] = '7'")
            Case "Burner Configurations"
                g_CurrentFieldID = 11
                GridView1.Columns("FieldID").FilterInfo = New ColumnFilterInfo("[FieldID] = '11'")
        End Select

    End Sub

    Private Sub Grid_DataNavigator1_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If MessageBox.Show("Do you want to delete the current row?", "Confirm deletion", _
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) <> DialogResult.Yes Then
                e.Handled = True
            Else
                e.Handled = True
                GridView1.CloseEditor()
                GridView1.UpdateCurrentRow()
                bsTables.RemoveCurrent()
                bsTables.EndEdit()
                pUpdateDatabase()
            End If
        End If
    End Sub

    Private Sub GridView1_CustomColumnDisplayText(ByVal sender As Object, ByVal e As CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        'This subroutine will give custom input options to the user so that field keys can be set without having to know the number associated with it. IE: 3 - FTIS will let the user just click 'FTIS" and the program will know to automatically put in 3 instead.
        If e.Column.FieldName = "FK_InspectionType" Then
            If Not IsDBNull(e.Value) AndAlso Not e.Value = Nothing Then
                Dim InspectionTypeRow() As DataRow = dsQTT_DatabaseEditor.tblInspectionTypes.Select("ID = " & e.Value)
                If InspectionTypeRow.Length > 0 Then e.DisplayText = InspectionTypeRow(0).Item("Description")
            End If
        ElseIf e.Column.FieldName = "FK_DefaultRole" Then
            If Not IsDBNull(e.Value) AndAlso Not e.Value = Nothing Then
                Dim RoleRow() As DataRow = dsQTT_DatabaseEditor.tblQTT_Roles.Select("ID = " & e.Value)
                If RoleRow.Length > 0 Then e.DisplayText = RoleRow(0).Item("Description")
            End If

        ElseIf e.Column.FieldName = "FK_MainOffice" Then
            If Not IsDBNull(e.Value) AndAlso Not e.Value = Nothing Then
                Dim OfficeRow() As DataRow = dsQTT_DatabaseEditor.tblQTT_Offices.Select("ID = " & e.Value)
                If OfficeRow.Length > 0 Then e.DisplayText = OfficeRow(0).Item("Description")
            End If
        End If

    End Sub

    Private Sub uiCloseButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiCloseButton.Click
        Me.Close()
    End Sub

    Private Sub uiTableNames_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiTableNames.SelectedIndexChanged
        Dim blnFilterRequired As Boolean = False
        'Reset any filters that may have been set.
        GridView1.ClearColumnsFilter()
        bsTables.DataSource = dsQTT_DatabaseEditor
        'lblNumberToolCorrelation.Visible = False


        Select Case uiTableNames.Text
            Case "Offices"
                bsTables.DataMember = dsQTT_DatabaseEditor.tblQTT_Offices.TableName
            Case "Roles"
                bsTables.DataMember = dsQTT_DatabaseEditor.tblQTT_Roles.TableName
            Case "Inspectors"
                bsTables.DataMember = dsQTT_DatabaseEditor.tblQTT_Inspectors.TableName
            Case "Project Tools"
                bsTables.DataMember = dsQTT_DatabaseEditor.tblInspectionTools.TableName
            Case "Default Text Entries"
                bsTables.DataMember = dsQTT_DatabaseEditor.tblDefaultTextEntries.TableName
            Case Else
                bsTables.DataMember = dsQTT_DatabaseEditor.tblDefaultText_ListItems.TableName
                blnFilterRequired = True
        End Select
        GridView1.PopulateColumns()

        'Hide the database ID and FieldID since it is useless to display
        GridView1.Columns("ID").Visible = False

        Dim col As GridColumn
        For Each col In GridView1.Columns
            col.AppearanceCell.Options.UseTextOptions = True
            col.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            col.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            col.AppearanceHeader.Options.UseTextOptions = True
            col.AppearanceHeader.ForeColor = Color.DarkSlateBlue
            col.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            col.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Next

        'Add an internal repository item stored in the grid itself to manage offices and role ID's
        If uiTableNames.Text = "Inspectors" Then GridView1.Columns("FK_MainOffice").ColumnEdit = RepItemOffices
        If uiTableNames.Text = "Inspectors" Then GridView1.Columns("FK_DefaultRole").ColumnEdit = RepItemRoles

        'Add the Inspection Types column edit from the grid's repositories if there's a column named FK_InspectionType
        If Not GridView1.Columns.ColumnByFieldName("FK_InspectionType") Is Nothing Then GridView1.Columns("FK_InspectionType").ColumnEdit = RepItemInspectionTypes

        'Format the default text entries column to be an exmemo type that allows expansion of the box
        If uiTableNames.Text = "Default Text Entries" Then
            Dim SomeTextBox As RepositoryItemRichTextEdit = New RepositoryItemRichTextEdit  'KM 9/7/2011 Changed to RTF for bullets and other formatting
            SomeTextBox.AutoHeight = True
            GridControl1.RepositoryItems.Add(SomeTextBox)
            GridView1.Columns("DefaultText").ColumnEdit = SomeTextBox
        End If

        If blnFilterRequired = True Then ApplyDataFilter()

        'KM Make best fit columns so tables with few values won't spread clear across the screen
        GridView1.BestFitColumns()
    End Sub
End Class
