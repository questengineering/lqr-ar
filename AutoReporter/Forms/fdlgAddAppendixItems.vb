Imports System.Windows.Forms

Public Class fdlgAddAppendixItems
    Private mAppendixSectionID As Integer
    Private mInspectionDetails2Form As InspectionDetails
    Private mNewReportItemType As QTT_LookupsData.sprocGetAppendixItemTypesToAddRow
    Private mNumberOfItemsToAdd As Integer


    Public Property AppendixSectionID() As Integer
        ' passed by calling form to specify which appendix section is currently selected
        Get
            Return mAppendixSectionID
        End Get
        Set(ByVal value As Integer)
            mAppendixSectionID = value
            ' call sub to populate list of Item Types with relevant choices
            Call ListAppendixItemTypes()
        End Set
    End Property

    Public Property ParentForm_InspectionDetails2() As InspectionDetails
        ' strongly typed reference to calling form 
        Get
            Return mInspectionDetails2Form
        End Get
        Set(ByVal value As InspectionDetails)
            mInspectionDetails2Form = value
        End Set
    End Property
    Public ReadOnly Property ReportItemTypeToAdd() As QTT_LookupsData.sprocGetAppendixItemTypesToAddRow
        ' used to return user's selection
        Get
            Return mNewReportItemType
        End Get

    End Property

    Public ReadOnly Property NumberOfItemsToAdd() As Integer
        ' used to return user's selection
        Get
            Return mNumberOfItemsToAdd
        End Get

    End Property

    Public ReadOnly Property RelatedPipeTubeItem() As QTT_InspectionsDataSets.tblPipeTubeItemsRow
        ' used to return user's selection
        Get
            Return cboRelatedPipeTube.SelectedItem.row
        End Get

    End Property

    Private Function SetupUI_AppendixItemSelected()
        ' Sets up UI as per settings
        Dim oPTI As QTT_InspectionsDataSets.tblPipeTubeItemsRow

        Dim fShowPTI As Boolean = False
        ' is selected item Pipe/Tube linked
        Dim oTmp As QTT_LookupsData.sprocGetAppendixItemTypesToAddRow = cboImageFileTypes.SelectedItem

        If oTmp Is Nothing Then
            GoTo Setup_UI
        End If

        mNewReportItemType = oTmp

        ' verify selection - currently selected report item type
        Dim drvTemp As DataRowView '= cboImageFileTypes.SelectedItem
        'If drvTemp Is Nothing Then
        '    GoTo Setup_UI
        'End If
        ''Dim oRow As QTT_LookupsData.sprocGetAppendixItemTypesToAddRow = drvTemp.Row
        'mNewReportItemType = drvTemp.Row
        If Not (mNewReportItemType.IsFK_PipeTubeTypeNull) Then
            fShowPTI = True  ' display Piping / Tubing interface
            drvTemp = cboRelatedPipeTube.SelectedItem
            If Not (drvTemp Is Nothing) Then
                oPTI = drvTemp.Row
                If oPTI.FK_PipeItemType = mNewReportItemType.FK_PipeTubeType Then
                    ' current selected item is of correct type
                    GoTo Setup_UI
                End If
            End If
            ' if to here, we need to select an item of the same type if possible
            For Each drvTemp In cboRelatedPipeTube.Items
                oPTI = drvTemp.Row
                If oPTI.FK_PipeItemType = mNewReportItemType.FK_PipeTubeType Then
                    cboRelatedPipeTube.SelectedItem = oPTI
                    Exit For
                End If
            Next
        End If

Setup_UI:
        cboRelatedPipeTube.Visible = fShowPTI
        lblRelatedPTI.Visible = fShowPTI

    End Function

    Public Function SetupUI_PipeTubeItems(ByVal PipeTubeItemsList As BindingSource, ByVal SelectPipeTubeID As Integer) As Boolean

        Const PIPETUBE_DESC_FIELD = "Description"

        ' called to populate listing of pipe tube items, and specify default selection
        With cboRelatedPipeTube
            .DataSource = PipeTubeItemsList
            .DisplayMember = PIPETUBE_DESC_FIELD
            ' select items as specified
            Dim oDRV As DataRowView
            Dim oRow As QTT_InspectionsDataSets.tblPipeTubeItemsRow
            For Each oDRV In .Items
                oRow = oDRV.Row
                If oRow.ID = SelectPipeTubeID Then
                    .SelectedItem = oRow
                    Exit For
                End If
            Next
        End With

        Return True

    End Function

    Private Sub ListAppendixItemTypes()
        ' populates list with Appendix Item types

        ' Define field name constant and needed dataset objects
        Const ITEM_TYPE_DESC_FIELD = "Description"
        Dim taTypes As New QTT_LookupsDataTableAdapters.sprocGetAppendixItemTypesToAddTableAdapter
        Dim ItemTypesListing As New QTT_LookupsData.sprocGetAppendixItemTypesToAddDataTable
        Dim oCustomList As List(Of QTT_LookupsData.sprocGetAppendixItemTypesToAddRow) = New List(Of QTT_LookupsData.sprocGetAppendixItemTypesToAddRow)
        Dim SkipRowFlag As Boolean = False
        Const REFORMER_APDX_B_TEXT = "PAIR: 3D Tube Image / Diameter Graph"
        Const FTIS_ADD_SET_TEXT = "SET: 2D/3D Inner Wall Graph / 2D/3D Radius Graph"
        Const REFORMER_3D_REFORMER_SET = "SET: 3D Reformer Models - NE,NW,SE,SW"

        ' fill dataset
        taTypes.Fill(ItemTypesListing, mAppendixSectionID)

        ' linking a pair of items and also removes duplicates from list 
        For Each tr As QTT_LookupsData.sprocGetAppendixItemTypesToAddRow In ItemTypesListing
            ' is item type already listed?
            For Each t2 As QTT_LookupsData.sprocGetAppendixItemTypesToAddRow In oCustomList
                If t2.ReportItemTypeID = tr.ReportItemTypeID Then
                    SkipRowFlag = True
                    Exit For
                End If
            Next
            Select Case tr.ReportItemTypeID
                Case 6
                    If Not SkipRowFlag Then
                        tr.Description = FTIS_ADD_SET_TEXT
                        oCustomList.Add(tr)
                    End If
                Case 13
                    If Not SkipRowFlag Then
                        tr.Description = REFORMER_APDX_B_TEXT
                        oCustomList.Add(tr)
                    End If
                Case 30
                    If Not SkipRowFlag Then
                        tr.Description = REFORMER_3D_REFORMER_SET
                        oCustomList.Add(tr)
                    End If
                Case 7, 8, 9, 27, 31, 32, 33
                    SkipRowFlag = True
                Case Else
                    If Not SkipRowFlag Then
                        tr.Description = tr.Description_ReportItemType
                        oCustomList.Add(tr)
                    End If
            End Select
            SkipRowFlag = False
        Next

        ' bind to control
        cboImageFileTypes.DataSource = oCustomList
        cboImageFileTypes.DisplayMember = ITEM_TYPE_DESC_FIELD
        cboImageFileTypes.SelectedIndex = -1

        ' UI Setup
        Call SetupUI_AppendixItemSelected()


    End Sub

    Function VerifyUserSelections() As Boolean

        ' verify selection - currently selected report item type
        Dim drvTemp As DataRowView
        Dim oTemp As QTT_LookupsData.sprocGetAppendixItemTypesToAddRow = cboImageFileTypes.SelectedItem

        If oTemp Is Nothing Then
            MsgBox("Please select the Type of Linked File that you want to add to the appendix.")
            cboImageFileTypes.Select()
            GoTo EXIT_AND_FAIL
        End If
        mNewReportItemType = oTemp  'drvTemp.Row
        ' does item require Pipe/Tube link?
        If Not mNewReportItemType.IsID_PipeTubeItemNull Then
            ' verify PipeTubeItem is selected
            drvTemp = cboRelatedPipeTube.SelectedItem
            If drvTemp Is Nothing Then
                MsgBox("Please select a related Pipe / Tube for the Item(s) you are adding.")
                cboRelatedPipeTube.Select()
                GoTo EXIT_AND_FAIL
            End If
            ' verify proper type selected
            ' not needed in this version / implementation
        End If

        ' number of items to add
        mNumberOfItemsToAdd = uiNumberOfItems.Value
        If mNumberOfItemsToAdd < 1 Then
            MsgBox("Number of Items to add cannot be less than 1.", MsgBoxStyle.Information)
            uiNumberOfItems.Select()
            GoTo EXIT_AND_FAIL
        End If

        ' if to here, things should be ok
        Return True
        Exit Function

EXIT_AND_FAIL:
        Return False
        Exit Function

    End Function

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click

        ' Verify / record user's selection
        If Not VerifyUserSelections() Then
            Exit Sub
        End If

        ' standard dialog OK button code:
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cboImageFileTypes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboImageFileTypes.SelectedIndexChanged
        Call SetupUI_AppendixItemSelected()
    End Sub

End Class
