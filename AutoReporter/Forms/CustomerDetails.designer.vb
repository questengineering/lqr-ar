<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CustomerDetails
   Inherits System.Windows.Forms.Form

   'Form overrides dispose to clean up the component list.
   <System.Diagnostics.DebuggerNonUserCode()> _
   Protected Overrides Sub Dispose(ByVal disposing As Boolean)
      If disposing AndAlso components IsNot Nothing Then
         components.Dispose()
      End If
      MyBase.Dispose(disposing)
   End Sub

   'Required by the Windows Form Designer
   Private components As System.ComponentModel.IContainer

   'NOTE: The following procedure is required by the Windows Form Designer
   'It can be modified using the Windows Form Designer.  
   'Do not modify it using the code editor.
   <System.Diagnostics.DebuggerStepThrough()> _
   Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim NameLabel As System.Windows.Forms.Label
        Dim AddressLabel As System.Windows.Forms.Label
        Dim CityLabel As System.Windows.Forms.Label
        Dim StateLabel As System.Windows.Forms.Label
        Dim PostalCodeLabel As System.Windows.Forms.Label
        Dim CountryLabel As System.Windows.Forms.Label
        Dim ContactLabel As System.Windows.Forms.Label
        Dim PhoneLabel As System.Windows.Forms.Label
        Dim FaxLabel As System.Windows.Forms.Label
        Dim EmailLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CustomerDetails))
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject2 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule1 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Me.lblHeading = New System.Windows.Forms.Label()
        Me.bsCustomers = New System.Windows.Forms.BindingSource(Me.components)
        Me.QTT_InspectionsDataSets = New QIG.AutoReporter.QTT_InspectionsDataSets()
        Me.AddressTextBox = New System.Windows.Forms.TextBox()
        Me.CityTextBox = New System.Windows.Forms.TextBox()
        Me.PostalCodeTextBox = New System.Windows.Forms.TextBox()
        Me.ContactTextBox = New System.Windows.Forms.TextBox()
        Me.PhoneTextBox = New System.Windows.Forms.TextBox()
        Me.FaxTextBox = New System.Windows.Forms.TextBox()
        Me.EmailTextBox = New System.Windows.Forms.TextBox()
        Me.taTblCustomers = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblCustomersTableAdapter()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.uiSaveButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiDeleteButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiCancelCloseButton = New DevExpress.XtraEditors.SimpleButton()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.uiState = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiCountry = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.DxValidationProvider = New DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(Me.components)
        Me.uiCompanyName = New DevExpress.XtraEditors.TextEdit()
        NameLabel = New System.Windows.Forms.Label()
        AddressLabel = New System.Windows.Forms.Label()
        CityLabel = New System.Windows.Forms.Label()
        StateLabel = New System.Windows.Forms.Label()
        PostalCodeLabel = New System.Windows.Forms.Label()
        CountryLabel = New System.Windows.Forms.Label()
        ContactLabel = New System.Windows.Forms.Label()
        PhoneLabel = New System.Windows.Forms.Label()
        FaxLabel = New System.Windows.Forms.Label()
        EmailLabel = New System.Windows.Forms.Label()
        CType(Me.bsCustomers,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.QTT_InspectionsDataSets,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.PictureBox1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiState.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCountry.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DxValidationProvider,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCompanyName.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'NameLabel
        '
        NameLabel.AutoSize = true
        NameLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        NameLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        NameLabel.Location = New System.Drawing.Point(7, 51)
        NameLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        NameLabel.Name = "NameLabel"
        NameLabel.Size = New System.Drawing.Size(90, 13)
        NameLabel.TabIndex = 1
        NameLabel.Text = "Company Name:"
        '
        'AddressLabel
        '
        AddressLabel.AutoSize = true
        AddressLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        AddressLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        AddressLabel.Location = New System.Drawing.Point(7, 132)
        AddressLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        AddressLabel.Name = "AddressLabel"
        AddressLabel.Size = New System.Drawing.Size(80, 13)
        AddressLabel.TabIndex = 4
        AddressLabel.Text = "Main Address:"
        '
        'CityLabel
        '
        CityLabel.AutoSize = true
        CityLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        CityLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        CityLabel.Location = New System.Drawing.Point(7, 245)
        CityLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        CityLabel.Name = "CityLabel"
        CityLabel.Size = New System.Drawing.Size(29, 13)
        CityLabel.TabIndex = 12
        CityLabel.Text = "City:"
        '
        'StateLabel
        '
        StateLabel.AutoSize = true
        StateLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        StateLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        StateLabel.Location = New System.Drawing.Point(161, 245)
        StateLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        StateLabel.Name = "StateLabel"
        StateLabel.Size = New System.Drawing.Size(83, 13)
        StateLabel.TabIndex = 14
        StateLabel.Text = "State/Province:"
        StateLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'PostalCodeLabel
        '
        PostalCodeLabel.AutoSize = true
        PostalCodeLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        PostalCodeLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        PostalCodeLabel.Location = New System.Drawing.Point(304, 245)
        PostalCodeLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        PostalCodeLabel.Name = "PostalCodeLabel"
        PostalCodeLabel.Size = New System.Drawing.Size(71, 13)
        PostalCodeLabel.TabIndex = 16
        PostalCodeLabel.Text = "Postal Code:"
        PostalCodeLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'CountryLabel
        '
        CountryLabel.AutoSize = true
        CountryLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        CountryLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        CountryLabel.Location = New System.Drawing.Point(7, 198)
        CountryLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        CountryLabel.Name = "CountryLabel"
        CountryLabel.Size = New System.Drawing.Size(51, 13)
        CountryLabel.TabIndex = 6
        CountryLabel.Text = "Country:"
        '
        'ContactLabel
        '
        ContactLabel.AutoSize = true
        ContactLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        ContactLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        ContactLabel.Location = New System.Drawing.Point(7, 338)
        ContactLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        ContactLabel.Name = "ContactLabel"
        ContactLabel.Size = New System.Drawing.Size(128, 13)
        ContactLabel.TabIndex = 19
        ContactLabel.Text = "Primary Contact Person:"
        '
        'PhoneLabel
        '
        PhoneLabel.AutoSize = true
        PhoneLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        PhoneLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        PhoneLabel.Location = New System.Drawing.Point(161, 198)
        PhoneLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        PhoneLabel.Name = "PhoneLabel"
        PhoneLabel.Size = New System.Drawing.Size(43, 13)
        PhoneLabel.TabIndex = 8
        PhoneLabel.Text = "Phone:"
        '
        'FaxLabel
        '
        FaxLabel.AutoSize = true
        FaxLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        FaxLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        FaxLabel.Location = New System.Drawing.Point(266, 198)
        FaxLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        FaxLabel.Name = "FaxLabel"
        FaxLabel.Size = New System.Drawing.Size(27, 13)
        FaxLabel.TabIndex = 10
        FaxLabel.Text = "Fax:"
        FaxLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'EmailLabel
        '
        EmailLabel.AutoSize = true
        EmailLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        EmailLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        EmailLabel.Location = New System.Drawing.Point(7, 382)
        EmailLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        EmailLabel.Name = "EmailLabel"
        EmailLabel.Size = New System.Drawing.Size(37, 13)
        EmailLabel.TabIndex = 21
        EmailLabel.Text = "Email:"
        '
        'lblHeading
        '
        Me.lblHeading.AutoSize = true
        Me.lblHeading.Font = New System.Drawing.Font("Segoe UI", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic),System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblHeading.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblHeading.Location = New System.Drawing.Point(7, 3)
        Me.lblHeading.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.Size = New System.Drawing.Size(302, 40)
        Me.lblHeading.TabIndex = 0
        Me.lblHeading.Text = "Add a New Customer"
        '
        'bsCustomers
        '
        Me.bsCustomers.DataMember = "tblCustomers"
        Me.bsCustomers.DataSource = Me.QTT_InspectionsDataSets
        '
        'QTT_InspectionsDataSets
        '
        Me.QTT_InspectionsDataSets.DataSetName = "QTT_InspectionsDataSets"
        Me.QTT_InspectionsDataSets.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'AddressTextBox
        '
        Me.AddressTextBox.AcceptsReturn = true
        Me.AddressTextBox.BackColor = System.Drawing.Color.OldLace
        Me.AddressTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCustomers, "Address", true))
        Me.AddressTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.AddressTextBox.Location = New System.Drawing.Point(7, 149)
        Me.AddressTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.AddressTextBox.MaxLength = 255
        Me.AddressTextBox.Multiline = true
        Me.AddressTextBox.Name = "AddressTextBox"
        Me.AddressTextBox.Size = New System.Drawing.Size(366, 45)
        Me.AddressTextBox.TabIndex = 5
        '
        'CityTextBox
        '
        Me.CityTextBox.BackColor = System.Drawing.Color.OldLace
        Me.CityTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCustomers, "City", true))
        Me.CityTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CityTextBox.Location = New System.Drawing.Point(7, 261)
        Me.CityTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.CityTextBox.MaxLength = 50
        Me.CityTextBox.Name = "CityTextBox"
        Me.CityTextBox.Size = New System.Drawing.Size(149, 25)
        Me.CityTextBox.TabIndex = 13
        '
        'PostalCodeTextBox
        '
        Me.PostalCodeTextBox.BackColor = System.Drawing.Color.OldLace
        Me.PostalCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCustomers, "PostalCode", true))
        Me.PostalCodeTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PostalCodeTextBox.Location = New System.Drawing.Point(304, 261)
        Me.PostalCodeTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.PostalCodeTextBox.MaxLength = 20
        Me.PostalCodeTextBox.Name = "PostalCodeTextBox"
        Me.PostalCodeTextBox.Size = New System.Drawing.Size(69, 25)
        Me.PostalCodeTextBox.TabIndex = 17
        '
        'ContactTextBox
        '
        Me.ContactTextBox.BackColor = System.Drawing.Color.OldLace
        Me.ContactTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCustomers, "Contact", true))
        Me.ContactTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ContactTextBox.Location = New System.Drawing.Point(7, 354)
        Me.ContactTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.ContactTextBox.MaxLength = 255
        Me.ContactTextBox.Name = "ContactTextBox"
        Me.ContactTextBox.Size = New System.Drawing.Size(366, 25)
        Me.ContactTextBox.TabIndex = 20
        '
        'PhoneTextBox
        '
        Me.PhoneTextBox.BackColor = System.Drawing.Color.OldLace
        Me.PhoneTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCustomers, "Phone", true))
        Me.PhoneTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PhoneTextBox.Location = New System.Drawing.Point(161, 214)
        Me.PhoneTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.PhoneTextBox.MaxLength = 64
        Me.PhoneTextBox.Name = "PhoneTextBox"
        Me.PhoneTextBox.Size = New System.Drawing.Size(104, 25)
        Me.PhoneTextBox.TabIndex = 9
        '
        'FaxTextBox
        '
        Me.FaxTextBox.BackColor = System.Drawing.Color.OldLace
        Me.FaxTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCustomers, "Fax", true))
        Me.FaxTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.FaxTextBox.Location = New System.Drawing.Point(269, 214)
        Me.FaxTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.FaxTextBox.MaxLength = 64
        Me.FaxTextBox.Name = "FaxTextBox"
        Me.FaxTextBox.Size = New System.Drawing.Size(104, 25)
        Me.FaxTextBox.TabIndex = 11
        '
        'EmailTextBox
        '
        Me.EmailTextBox.BackColor = System.Drawing.Color.OldLace
        Me.EmailTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCustomers, "Email", true))
        Me.EmailTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.EmailTextBox.Location = New System.Drawing.Point(7, 398)
        Me.EmailTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.EmailTextBox.MaxLength = 128
        Me.EmailTextBox.Name = "EmailTextBox"
        Me.EmailTextBox.Size = New System.Drawing.Size(366, 25)
        Me.EmailTextBox.TabIndex = 22
        '
        'taTblCustomers
        '
        Me.taTblCustomers.ClearBeforeFill = true
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label1.Location = New System.Drawing.Point(7, 106)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(325, 24)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Customer's Corporate Address (optional)"
        '
        'uiSaveButton
        '
        Me.uiSaveButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiSaveButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiSaveButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiSaveButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiSaveButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiSaveButton.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(32,Byte),Integer), CType(CType(31,Byte),Integer), CType(CType(53,Byte),Integer))
        Me.uiSaveButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiSaveButton.Appearance.Options.UseBackColor = true
        Me.uiSaveButton.Appearance.Options.UseBorderColor = true
        Me.uiSaveButton.Appearance.Options.UseFont = true
        Me.uiSaveButton.Appearance.Options.UseForeColor = true
        Me.uiSaveButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiSaveButton.Location = New System.Drawing.Point(398, 336)
        Me.uiSaveButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiSaveButton.Name = "uiSaveButton"
        Me.uiSaveButton.Size = New System.Drawing.Size(169, 24)
        Me.uiSaveButton.TabIndex = 23
        Me.uiSaveButton.Text = "&Save Customer"
        '
        'uiDeleteButton
        '
        Me.uiDeleteButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiDeleteButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiDeleteButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiDeleteButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiDeleteButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDeleteButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiDeleteButton.Appearance.Options.UseBackColor = true
        Me.uiDeleteButton.Appearance.Options.UseBorderColor = true
        Me.uiDeleteButton.Appearance.Options.UseFont = true
        Me.uiDeleteButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiDeleteButton.Location = New System.Drawing.Point(398, 367)
        Me.uiDeleteButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiDeleteButton.Name = "uiDeleteButton"
        Me.uiDeleteButton.Size = New System.Drawing.Size(169, 24)
        Me.uiDeleteButton.TabIndex = 24
        Me.uiDeleteButton.Text = "&Delete Customer"
        '
        'uiCancelCloseButton
        '
        Me.uiCancelCloseButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiCancelCloseButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiCancelCloseButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiCancelCloseButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiCancelCloseButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCancelCloseButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiCancelCloseButton.Appearance.Options.UseBackColor = true
        Me.uiCancelCloseButton.Appearance.Options.UseBorderColor = true
        Me.uiCancelCloseButton.Appearance.Options.UseFont = true
        Me.uiCancelCloseButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiCancelCloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uiCancelCloseButton.Location = New System.Drawing.Point(398, 399)
        Me.uiCancelCloseButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiCancelCloseButton.Name = "uiCancelCloseButton"
        Me.uiCancelCloseButton.Size = New System.Drawing.Size(169, 24)
        Me.uiCancelCloseButton.TabIndex = 25
        Me.uiCancelCloseButton.Text = "&Cancel"
        '
        'PictureBox1
        '
        Me.PictureBox1.ErrorImage = Nothing
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"),System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(400, 9)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(167, 69)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 62
        Me.PictureBox1.TabStop = false
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label2.Location = New System.Drawing.Point(7, 308)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(301, 21)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Contact Information (optional)"
        '
        'uiState
        '
        Me.uiState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsCustomers, "State", true))
        Me.uiState.EditValue = ""
        Me.uiState.Location = New System.Drawing.Point(161, 261)
        Me.uiState.Name = "uiState"
        Me.uiState.Properties.Appearance.BackColor = System.Drawing.Color.OldLace
        Me.uiState.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiState.Properties.Appearance.Options.UseBackColor = true
        Me.uiState.Properties.Appearance.Options.UseFont = true
        Me.uiState.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiState.Properties.AppearanceDropDown.Options.UseFont = true
        SerializableAppearanceObject1.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject1.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject1.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject1.Options.UseBackColor = true
        SerializableAppearanceObject1.Options.UseBorderColor = true
        Me.uiState.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "", Nothing, Nothing, true)})
        Me.uiState.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiState.Properties.MaxLength = 10
        Me.uiState.Size = New System.Drawing.Size(138, 24)
        Me.uiState.TabIndex = 15
        '
        'uiCountry
        '
        Me.uiCountry.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsCustomers, "Country", true))
        Me.uiCountry.EditValue = ""
        Me.uiCountry.Location = New System.Drawing.Point(7, 214)
        Me.uiCountry.Name = "uiCountry"
        Me.uiCountry.Properties.Appearance.BackColor = System.Drawing.Color.OldLace
        Me.uiCountry.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCountry.Properties.Appearance.Options.UseBackColor = true
        Me.uiCountry.Properties.Appearance.Options.UseFont = true
        Me.uiCountry.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCountry.Properties.AppearanceDropDown.Options.UseFont = true
        SerializableAppearanceObject2.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject2.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject2.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject2.Options.UseBackColor = true
        SerializableAppearanceObject2.Options.UseBorderColor = true
        Me.uiCountry.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject2, "", Nothing, Nothing, true)})
        Me.uiCountry.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiCountry.Properties.MaxLength = 40
        Me.uiCountry.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiCountry.Size = New System.Drawing.Size(149, 24)
        Me.uiCountry.TabIndex = 7
        '
        'DxValidationProvider
        '
        Me.DxValidationProvider.ValidationMode = DevExpress.XtraEditors.DXErrorProvider.ValidationMode.Manual
        '
        'uiCompanyName
        '
        Me.uiCompanyName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCustomers, "Name", true))
        Me.uiCompanyName.Location = New System.Drawing.Point(7, 68)
        Me.uiCompanyName.Name = "uiCompanyName"
        Me.uiCompanyName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCompanyName.Properties.Appearance.Options.UseFont = true
        Me.uiCompanyName.Properties.MaxLength = 255
        Me.uiCompanyName.Size = New System.Drawing.Size(366, 24)
        Me.uiCompanyName.TabIndex = 2
        ConditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule1.ErrorText = "Company Name cannot be blank."
        ConditionValidationRule1.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical
        Me.DxValidationProvider.SetValidationRule(Me.uiCompanyName, ConditionValidationRule1)
        '
        'CustomerDetails
        '
        Me.AcceptButton = Me.uiSaveButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96!, 96!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = true
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.uiCancelCloseButton
        Me.ClientSize = New System.Drawing.Size(579, 433)
        Me.Controls.Add(Me.uiCompanyName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.uiCancelCloseButton)
        Me.Controls.Add(Me.uiSaveButton)
        Me.Controls.Add(Me.uiDeleteButton)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(EmailLabel)
        Me.Controls.Add(Me.EmailTextBox)
        Me.Controls.Add(FaxLabel)
        Me.Controls.Add(Me.FaxTextBox)
        Me.Controls.Add(PhoneLabel)
        Me.Controls.Add(Me.PhoneTextBox)
        Me.Controls.Add(ContactLabel)
        Me.Controls.Add(Me.ContactTextBox)
        Me.Controls.Add(CountryLabel)
        Me.Controls.Add(PostalCodeLabel)
        Me.Controls.Add(Me.PostalCodeTextBox)
        Me.Controls.Add(StateLabel)
        Me.Controls.Add(CityLabel)
        Me.Controls.Add(Me.CityTextBox)
        Me.Controls.Add(AddressLabel)
        Me.Controls.Add(Me.AddressTextBox)
        Me.Controls.Add(NameLabel)
        Me.Controls.Add(Me.lblHeading)
        Me.Controls.Add(Me.uiState)
        Me.Controls.Add(Me.uiCountry)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = false
        Me.MinimizeBox = false
        Me.Name = "CustomerDetails"
        Me.ShowInTaskbar = false
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Customer Information"
        Me.TopMost = true
        CType(Me.bsCustomers,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.QTT_InspectionsDataSets,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.PictureBox1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiState.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCountry.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DxValidationProvider,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCompanyName.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
   Friend WithEvents lblHeading As System.Windows.Forms.Label
   Friend WithEvents bsCustomers As System.Windows.Forms.BindingSource
   Friend WithEvents QTT_InspectionsDataSets As AutoReporter.QTT_InspectionsDataSets
   Friend WithEvents taTblCustomers As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblCustomersTableAdapter
   Friend WithEvents AddressTextBox As System.Windows.Forms.TextBox
   Friend WithEvents CityTextBox As System.Windows.Forms.TextBox
   Friend WithEvents PostalCodeTextBox As System.Windows.Forms.TextBox
   Friend WithEvents ContactTextBox As System.Windows.Forms.TextBox
   Friend WithEvents PhoneTextBox As System.Windows.Forms.TextBox
   Friend WithEvents FaxTextBox As System.Windows.Forms.TextBox
   Friend WithEvents EmailTextBox As System.Windows.Forms.TextBox
   Friend WithEvents Label1 As System.Windows.Forms.Label
   Friend WithEvents uiSaveButton As DevExpress.XtraEditors.SimpleButton
   Friend WithEvents uiDeleteButton As DevExpress.XtraEditors.SimpleButton
   Friend WithEvents uiCancelCloseButton As DevExpress.XtraEditors.SimpleButton
   Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
   Friend WithEvents Label2 As System.Windows.Forms.Label
   Friend WithEvents uiState As DevExpress.XtraEditors.ComboBoxEdit
   Friend WithEvents uiCountry As DevExpress.XtraEditors.ComboBoxEdit
   Friend WithEvents DxValidationProvider As DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider
   Friend WithEvents uiCompanyName As DevExpress.XtraEditors.TextEdit

End Class
