﻿Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Columns
Imports System.Text.RegularExpressions

Public Class ucExecutiveSummary
    Dim MySummaryData As SummaryData
    Private g_intExecutiveSummaryID As Integer
    Private g_intPipeOrBendDataID As Integer
    Const g_ExecutiveSummaryConvectionEnum As Integer = 0
    Const g_ExecutiveSummaryRadiantEnum As Integer = 1
    Const g_ExecutiveSummaryEconomizerRadiantEnum As Integer = 2
    Const g_ExecutiveSummaryCrossoverEnum As Integer = 3
    Const g_ExecutiveSummaryUnknownEnum As Integer = -1
    Const g_ExecutiveSummaryPipeName As String = "ExecutiveSummaryPipe"
    Private _PipeSegmentName As String
    Private _PipeOrBend As String
    Public lstPasses As New ArrayList

#Region "Form Load Subroutines"

    Public Sub LoadExecutiveSummaryData(ByVal intInspectionNumber As Integer)
        
        'Check if there's an executive summary already in the database
        g_intExecutiveSummaryID = GetExecutiveSummaryID(intInspectionNumber)
        If g_intExecutiveSummaryID = -1 Then
            CreateEmptyExecutiveSummaryInDB(intInspectionNumber)
        End If
        g_intExecutiveSummaryID = GetExecutiveSummaryID(intInspectionNumber)
        'Make sure the binding source is set to the proper Executive Summary Item
        taTblSummary.Fill(Me.QTT_SummaryData.tblSummary)
        bsExecutiveSummary.Filter = "ID = " & g_intExecutiveSummaryID
        taTblSummaryBendData.Fill(Me.QTT_SummaryData.tblSummaryBendData)
        taTblSummaryPipeData.Fill(Me.QTT_SummaryData.tblSummaryPipeData)
        taTblSummaryBendDataInfo.Fill(Me.QTT_SummaryData.tblSummaryBendDataInfo)
        taTblSummaryPipeDataInfo.Fill(Me.QTT_SummaryData.tblSummaryPipeDataInfo)
        taTblPipeTubeItems.Fill(Me.QTT_SummaryData.tblPipeTubeItems)
        Dim FirstPipeTubeItemID As Integer = Me.QTT_SummaryData.tblPipeTubeItems.Select("FK_Inspection = " & intInspectionNumber)(0).Item("ID")
        bsSummaryPipeOrBendData.Filter = String.Format("FK_Summary = {0} and FK_PipeTubeItemID = {1}", g_intExecutiveSummaryID, g_ExecutiveSummaryConvectionEnum)
        bsSummaryPipeDataInfo.Filter = String.Format("FK_SummaryPipeData = {0} and FK_PipeTubeItemID = {1}", bsSummaryPipeOrBendData.Item(0)("ID"), g_ExecutiveSummaryConvectionEnum)
        bsSummaryBendDataInfo.Filter = String.Format("FK_SummaryBendData = {0} and FK_PipeTubeItemID = {1}", bsSummaryPipeOrBendData.Item(0)("ID"), g_ExecutiveSummaryConvectionEnum)
        'Tell the whole user control which summarypipeOrBendData is currently being shown.
        UpdateCurrentPipeOrBendDataID()
        'setup the grid view that contains the PIpeDataInfo, and make it show the convection pipe by default.
        bsSummaryPipeOrBendData.DataMember = "tblSummaryPipeData"
        uiPipeOrBendDataInfo.DataSource = bsSummaryPipeDataInfo
        UpdateGridViewColumns(uiPipeOrBendDataInfoView, "Pipe")
        _PipeOrBend = "Pipe" 'default to pipe data.
        'Analyze if the custom slot has already been used and set the 5th item in the dropdown with the proper name.
        AnalyzeForPreviousCustomSlotUsage()
        ToggleColumns() 'set Read-Only on certain columns depending on values changing.
    End Sub

#End Region
    Private Function GetExecutiveSummaryID(ByVal intInspectionID)
        Dim SomeDataTable() As DataRow = taTblSummary.GetData.Select(String.Format("FK_Inspection = {0} and ExecutiveOrSection = 'Executive'", intInspectionID))
        If SomeDataTable.Length > 0 Then
            g_intExecutiveSummaryID = SomeDataTable(0).Item("ID")
        Else : g_intExecutiveSummaryID = -1
        End If
        Return g_intExecutiveSummaryID
    End Function

    Private Sub CreateEmptyExecutiveSummaryInDB(ByVal intInspectionNumber)
        'Create the Executive Summary Object itself then figure out its ID
        taTblSummary.Insert(intInspectionNumber, "In-Service Pipes", "Imperial", Nothing, "Executive", Nothing)
        g_intExecutiveSummaryID = GetExecutiveSummaryID(intInspectionNumber)
        'Now create the 10 variations of executive summaries in the BendData and PipeData tables, Convection-bend, COnvection-pipe, radiant-bend, and radiant-pipe. These use SQL statements stored in the dataset.
        taTblSummaryPipeData.CreateEmptyRecord(g_intExecutiveSummaryID, g_ExecutiveSummaryConvectionEnum, g_ExecutiveSummaryPipeName)
        taTblSummaryPipeData.CreateEmptyRecord(g_intExecutiveSummaryID, g_ExecutiveSummaryRadiantEnum, g_ExecutiveSummaryPipeName)
        taTblSummaryPipeData.CreateEmptyRecord(g_intExecutiveSummaryID, g_ExecutiveSummaryEconomizerRadiantEnum, g_ExecutiveSummaryPipeName)
        taTblSummaryPipeData.CreateEmptyRecord(g_intExecutiveSummaryID, g_ExecutiveSummaryCrossoverEnum, g_ExecutiveSummaryPipeName)
        taTblSummaryPipeData.CreateEmptyRecord(g_intExecutiveSummaryID, g_ExecutiveSummaryUnknownEnum, g_ExecutiveSummaryPipeName)

        taTblSummaryBendData.CreateEmptyRecord(g_intExecutiveSummaryID, g_ExecutiveSummaryConvectionEnum, g_ExecutiveSummaryPipeName)
        taTblSummaryBendData.CreateEmptyRecord(g_intExecutiveSummaryID, g_ExecutiveSummaryRadiantEnum, g_ExecutiveSummaryPipeName)
        taTblSummaryBendData.CreateEmptyRecord(g_intExecutiveSummaryID, g_ExecutiveSummaryEconomizerRadiantEnum, g_ExecutiveSummaryPipeName)
        taTblSummaryBendData.CreateEmptyRecord(g_intExecutiveSummaryID, g_ExecutiveSummaryCrossoverEnum, g_ExecutiveSummaryPipeName)
        taTblSummaryBendData.CreateEmptyRecord(g_intExecutiveSummaryID, g_ExecutiveSummaryUnknownEnum, g_ExecutiveSummaryPipeName)

        'Update the database so it knows these have been created.
        taTblSummary.Update(QTT_SummaryData)
        taTblSummaryPipeData.Update(QTT_SummaryData)
        taTblSummaryBendData.Update(QTT_SummaryData)
    End Sub
    Private Sub AnalyzeForPreviousCustomSlotUsage()
        Dim CustomSlotExecutiveWriteup As String
        If _PipeOrBend = "Pipe" Then
            Dim ThePipeDataRow As QTT_SummaryData.tblSummaryPipeDataRow
            ThePipeDataRow = QTT_SummaryData.tblSummaryPipeData.Select(String.Format("FK_Summary = {0} and FK_PipeTubeItemID = {1}", g_intExecutiveSummaryID, g_ExecutiveSummaryUnknownEnum))(0)
            If Not (ThePipeDataRow.IsParagraphNull) Then CustomSlotExecutiveWriteup = ThePipeDataRow.Paragraph
        ElseIf _PipeOrBend = "Bend" Then
            Dim TheBendDataRow As QTT_SummaryData.tblSummaryBendDataRow
            TheBendDataRow = QTT_SummaryData.tblSummaryBendData.Select(String.Format("FK_Summary = {0} and FK_PipeTubeItemID = {1}", g_intExecutiveSummaryID, g_ExecutiveSummaryUnknownEnum))(0)
            If Not (TheBendDataRow.IsParagraphNull) Then CustomSlotExecutiveWriteup = TheBendDataRow.Paragraph
        End If
        'ANalye the text to see if it contains a section name.It has to be between 'The' and 'Section', such as 'The CustomNamed Section'. Allow everything between besides periods.
        'Dim _RegExPattern As String = "the ([ a-zA-Z0-9,\.\<\(\)]+) Section"
        Dim _RegExPattern As String = "the ([^\.]+) Section"
        Dim _RegEx As Regex = New Regex(_RegExPattern)
        If Not (IsNothing(CustomSlotExecutiveWriteup)) AndAlso _RegEx.Match(CustomSlotExecutiveWriteup).Success Then
            Dim _CustomName As String = _RegEx.Match(CustomSlotExecutiveWriteup).Value
            _CustomName = _CustomName.Remove(0, 4) ' Remove 'The '
            _CustomName = _CustomName.Remove(_CustomName.LastIndexOf(" Section"), 8) 'Now remove 'Section'
            If _CustomName.Contains("Piping") Then cmbPipeSectionName.Properties.Items(4) = _CustomName Else cmbPipeSectionName.Properties.Items(4) = _CustomName & " Piping" 'add Piping if needed
        End If
    End Sub
    Public Sub UpdateSummaryDBTables()
        bsSummaryPipeOrBendData.EndEdit()
        bsSummaryPipeDataInfo.EndEdit()
        bsSummaryBendDataInfo.EndEdit()
        bsExecutiveSummary.EndEdit()
        taTblSummaryPipeData.Update(QTT_SummaryData.tblSummaryPipeData)
        taTblSummaryBendData.Update(QTT_SummaryData.tblSummaryBendData)
        taTblSummaryPipeDataInfo.Update(QTT_SummaryData.tblSummaryPipeDataInfo)
        taTblSummaryBendDataInfo.Update(QTT_SummaryData.tblSummaryBendDataInfo)
        taTblSummary.Update(QTT_SummaryData.tblSummary)

    End Sub
    Private Sub UpdateCurrentPipeOrBendDataID()
        g_intPipeOrBendDataID = bsSummaryPipeOrBendData.Item(0)("ID")
    End Sub
#Region "UI Stuff"
    Private Sub btnWriteExecutiveSummary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWriteExecutiveSummary.Click
        MySummaryData = CreateDataSummary()
        GatherRequiredInput(MySummaryData)
        _PipeSegmentName = cmbPipeSectionName.Text
        If _PipeSegmentName.Contains(" Piping") Then _PipeSegmentName = _PipeSegmentName.Remove(_PipeSegmentName.LastIndexOf(" Piping"))

        Dim PipeOrBend As String = radPipeOrBend.Properties.Items(radPipeOrBend.SelectedIndex).Value
        Dim TheWrittenSummary As String = WriteUpSummaryForMe(MySummaryData, _PipeSegmentName, PipeOrBend)
        uiExecutiveSummaryText.Text = TheWrittenSummary
        If cmbPipeSectionName.SelectedIndex = -1 Then cmbPipeSectionName.Properties.Items(4) = cmbPipeSectionName.Text
    End Sub

    Private Sub cmbPipeSectionName_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPipeSectionName.EditValueChanged
        UpdateSummaryDBTables()
        If Not String.IsNullOrEmpty(cmbPipeSectionName.Text) Then
            Dim _alignedPipeSection As String
            If cmbPipeSectionName.SelectedIndex = 4 Then _alignedPipeSection = -1 Else _alignedPipeSection = cmbPipeSectionName.SelectedIndex
            ' radconvectionOrRadiant is set so that when the user selects Convection the value is -1 and Radiant is -2, these need to line up with the global evalue equivalents.
            bsSummaryPipeOrBendData.Filter = String.Format("FK_Summary = {0} and FK_PipeTubeItemID = '{1}'", g_intExecutiveSummaryID, _alignedPipeSection)
            bsSummaryBendDataInfo.Filter = String.Format("FK_SummaryBendData = {0} and FK_PipeTubeItemID = {1}", g_intPipeOrBendDataID, _alignedPipeSection)
            bsSummaryPipeDataInfo.Filter = String.Format("FK_SummaryPipeData = {0} and FK_PipeTubeItemID = {1}", g_intPipeOrBendDataID, _alignedPipeSection)
        Else
            MessageBox.Show("Please Select or Write In a Pipe Segment")
            Const _alignedPipeSection As String = "0"
            cmbPipeSectionName.Text = "Convection Piping" 'give it a default value
            bsSummaryPipeOrBendData.Filter = String.Format("FK_Summary = {0} and FK_PipeTubeItemID = '{1}'", g_intExecutiveSummaryID, _alignedPipeSection)
            bsSummaryBendDataInfo.Filter = String.Format("FK_SummaryBendData = {0} and FK_PipeTubeItemID = {1}", g_intPipeOrBendDataID, _alignedPipeSection)
            bsSummaryPipeDataInfo.Filter = String.Format("FK_SummaryPipeData = {0} and FK_PipeTubeItemID = {1}", g_intPipeOrBendDataID, _alignedPipeSection)
        End If

    End Sub

    Private Sub radPipeOrBend_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radPipeOrBend.SelectedIndexChanged
        UpdateSummaryDBTables()
        _PipeOrBend = radPipeOrBend.Properties.Items(radPipeOrBend.SelectedIndex).Value
        If _PipeOrBend = "Pipe" Then
            bsSummaryPipeOrBendData.DataMember = "tblSummaryPipeData"
            uiPipeOrBendDataInfo.DataSource = bsSummaryPipeDataInfo
        Else
            bsSummaryPipeOrBendData.DataMember = "tblSummaryBendData"
            uiPipeOrBendDataInfo.DataSource = bsSummaryBendDataInfo
        End If
        UpdateGridViewColumns(uiPipeOrBendDataInfoView, _PipeOrBend)
    End Sub
    Private Function UpdateGridViewColumns(ByVal ThisGrid As DevExpress.XtraGrid.Views.Grid.GridView, ByVal PipeOrBend As String)
        'Grab all the columns that are in the table
        ThisGrid.PopulateColumns()
        'Now make unwanted cells invisible like the ID, Field Keys, and designators (Convection or Radiant)
        ThisGrid.Columns.Item("ID").Visible = False
        ThisGrid.Columns.Item("FK_PipeTubeItemID").Visible = False
        ThisGrid.Columns.Item("PipeName").Visible = False
        If PipeOrBend = "Pipe" Then
            ThisGrid.Columns.Item("FK_SummaryPipeData").Visible = False
            ThisGrid.Columns.Item("PipeID").Caption = "Pipe ID"
            ThisGrid.Columns.Item("PipeID").Width = 54
            ThisGrid.Columns.Item("AvgWall").Caption = "Average Wall"
            ThisGrid.Columns.Item("AvgWall").Width = 56
            ThisGrid.Columns.Item("DesWall").Caption = "Designed Wall"
            ThisGrid.Columns.Item("DesWall").Width = 62
            ThisGrid.Columns.Item("AvgDiam").Caption = "Average Diameter"
            ThisGrid.Columns.Item("AvgDiam").Width = 63
            ThisGrid.Columns.Item("Location").Width = 53
        End If
        If PipeOrBend = "Bend" Then
            ThisGrid.Columns.Item("FK_SummaryBendData").Visible = False
            ThisGrid.Columns.Item("BendID").Caption = "Bend ID"
            ThisGrid.Columns.Item("BendID").Width = 54
        End If
        ThisGrid.Columns.Item("MinWall").Caption = "Minimum Wall"
        ThisGrid.Columns.Item("MinWall").Width = 57
        ThisGrid.Columns.Item("EstWallLoss").Caption = "Estimated Wall Loss"
        ThisGrid.Columns.Item("EstWallLoss").Width = 64
        ThisGrid.Columns.Item("EstRemWall").Caption = "Estimated Remaining Wall"
        ThisGrid.Columns.Item("EstRemWall").Width = 89

        For i = 0 To ThisGrid.Columns.Count - 1
            With ThisGrid.Columns.Item(i).AppearanceHeader
                .TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
                .TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom
                .TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
                .Font = New Font("Segoe UI", 8.25)
                .ForeColor = Color.DarkSlateBlue
            End With
            With ThisGrid.Columns.Item(i)
                .OptionsColumn.AllowMove = False
                .OptionsColumn.AllowSort = False
                .OptionsColumn.AllowSize = False
                .OptionsFilter.AllowFilter = False
                .AppearanceCell.Font = New Font("Segoe UI", 8.25)
                .AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            End With
        Next i

    End Function
    Private Sub uiPipeOrBendDataInfoView_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles uiPipeOrBendDataInfoView.InitNewRow
        'This sets default values for new rows in the gridview that cannot be null and cannot be set by the user. FK_Summar(pipeorbend)data and whether it's convection or radiant.
        Dim TheView As ColumnView = sender
        'Make sure they don't try to make more than 8 rows.
        If TheView.RowCount < 8 Then
            Dim strPipeOrBend As String = radPipeOrBend.Properties.Items(radPipeOrBend.SelectedIndex).Value
            Dim strFieldKeyName As String
            If strPipeOrBend = "Pipe" Then strFieldKeyName = "FK_SummaryPipeData"
            If strPipeOrBend = "Bend" Then strFieldKeyName = "FK_SummaryBendData"
            TheView.SetRowCellValue(e.RowHandle, strFieldKeyName, g_intPipeOrBendDataID)
            Dim pipeSectionIndex As Integer = cmbPipeSectionName.SelectedIndex
            If pipeSectionIndex = 4 Then pipeSectionIndex = -1 'change the custom slot back to -1

            TheView.SetRowCellValue(e.RowHandle, "FK_PipeTubeItemID", pipeSectionIndex)
        Else
            TheView.DeleteRow(e.RowHandle)
        End If
    End Sub
#End Region
#Region "Data Gathering for Report Writing"
    Private Sub GatherRequiredInput(ByVal SummaryDataIAmFilling As SummaryData)
        'Fill the SummaryData object with data so it can be passed to the paragraph writing module.

        'First get the easy stuff, the stuff that can be filled right from the form.
        SummaryDataIAmFilling.Appendix = "Appendix Not Used in General/Executive Summary" 'Get the appendix item
        SummaryDataIAmFilling.GeneralOrSectionSummary = "General" 'General because this is the executive summary, not a pass-specific summary.
        SummaryDataIAmFilling.NewUsedPipes = uiNewUsedPipes.Text
        SummaryDataIAmFilling.PassNumber = uiPassNumber.Text
        SummaryDataIAmFilling.Units = uiUnits.Text

        'Now fill the more complicated items. Deformation Data First
        GetDeformationDataFromUI(SummaryDataIAmFilling.MyDeformationData)

        'Second, get the data in the gridview.
        Dim ConvectionOrRadiant As String = cmbPipeSectionName.Text
        Dim PipeOrBend As String = radPipeOrBend.Properties.Items(radPipeOrBend.SelectedIndex).Value
        If PipeOrBend = "Pipe" Then
            GetPipeDataFromUI(SummaryDataIAmFilling.MyPipeData)
        ElseIf PipeOrBend = "Bend" Then
            GetBendDataFromUI(SummaryDataIAmFilling.MyBendData)
        End If

    End Sub
    Private Function GetPipeDataFromUI(ByVal PipeDataToFill As PipeData)
        'This gets all the data out of the rows visible in uiPipeOrBendDataGridView so it can be dumped into the summary data.
        For i = 0 To uiPipeOrBendDataInfoView.RowCount - 1
            Dim ThisRow As DataRow = uiPipeOrBendDataInfoView.GetDataRow(i)
            If Not ThisRow Is Nothing Then
                PipeDataToFill.AvgDiameter(i) = ThisRow("AvgDiam")
                PipeDataToFill.AvgWall(i) = ThisRow("AvgWall")
                PipeDataToFill.DesignedWall(i) = ThisRow("DesWall")
                PipeDataToFill.EstRemWall(i) = ThisRow("EstRemWall")
                PipeDataToFill.EstWallLoss(i) = ThisRow("EstWallLoss")
                PipeDataToFill.Location(i) = ThisRow("Location")
                PipeDataToFill.ID(i) = ThisRow("PipeID")
                PipeDataToFill.MinWall(i) = ThisRow("MinWall")
            End If
        Next
        PipeDataToFill.PipeSize = uiPipeSize.Text
        PipeDataToFill.PipeSchedule = uiScheduleSize.Text
        PipeDataToFill.InternalOrExternal = uiInternalOrExternal.Text
        PipeDataToFill.GeneralOrLocal = uiGeneralOrLocal.Text
    End Function
    Private Function GetBendDataFromUI(ByVal PipeDataToFill As BendData)
        'This gets all the data out of the rows visible in uiPipeOrBendDataGridView so it can be dumped into the summary data.
        For i = 0 To uiPipeOrBendDataInfoView.RowCount - 1
            Dim ThisRow As DataRow = uiPipeOrBendDataInfoView.GetDataRow(i)
            If Not ThisRow Is Nothing Then
                PipeDataToFill.EstRemWall(i) = ThisRow("EstRemWall")
                PipeDataToFill.EstWallLoss(i) = ThisRow("EstWallLoss")
                PipeDataToFill.ID(i) = ThisRow("BendID")
                PipeDataToFill.MinWall(i) = ThisRow("MinWall")
            End If
        Next
        PipeDataToFill.PipeSize = uiPipeSize.Text
        PipeDataToFill.PipeSchedule = uiScheduleSize.Text
        PipeDataToFill.InternalOrExternal = uiInternalOrExternal.Text
        PipeDataToFill.GeneralOrLocal = uiGeneralOrLocal.Text
    End Function

    Private Sub GetDeformationDataFromUI(ByRef TheDeformationData As DeformationData)
        'Store the rowhandle that's currently visible.
        If uiDeformationDataView.RowCount < 1 Then Return 'just in case there's nothing here. THis should never happen, though.
        Dim _TheOnlyRow As Integer = uiDeformationDataView.GetVisibleRowHandle(0)

        With uiDeformationDataView
            If .GetRowCellValue(_TheOnlyRow, colBulgingExceedingCount).ToString <> "" Then TheDeformationData.BulgingExceedingCount = .GetRowCellValue(_TheOnlyRow, colBulgingExceedingCount)
            If .GetRowCellValue(_TheOnlyRow, colBulgingNotExceedingCount).ToString <> "" Then TheDeformationData.BulgingNotExceedingCount = .GetRowCellValue(_TheOnlyRow, colBulgingNotExceedingCount)
            If .GetRowCellValue(_TheOnlyRow, colBulgingPass).ToString <> "" Then TheDeformationData.BulgingPasses = .GetRowCellValue(_TheOnlyRow, colBulgingPass)
            If .GetRowCellValue(_TheOnlyRow, colBulgingThreshold).ToString <> "" Then TheDeformationData.BulgingThreshold = .GetRowCellValue(_TheOnlyRow, colBulgingThreshold)
            If .GetRowCellValue(_TheOnlyRow, colBulgingPipeID).ToString <> "" Then TheDeformationData.BulgingPipeIDs = .GetRowCellValue(_TheOnlyRow, colBulgingPipeID)
            If .GetRowCellValue(_TheOnlyRow, colBulgingWorstDefo).ToString <> "" Then TheDeformationData.BulgingWorst = .GetRowCellValue(_TheOnlyRow, colBulgingWorstDefo)
            If .GetRowCellValue(_TheOnlyRow, colDentingExceedingCount).ToString <> "" Then TheDeformationData.DentingExceedingCount = .GetRowCellValue(_TheOnlyRow, colDentingExceedingCount)
            If .GetRowCellValue(_TheOnlyRow, colDentingNotExceedingCount).ToString <> "" Then TheDeformationData.DentingNotExceedingCount = .GetRowCellValue(_TheOnlyRow, colDentingNotExceedingCount)
            If .GetRowCellValue(_TheOnlyRow, colDentingPass).ToString <> "" Then TheDeformationData.DentingPasses = .GetRowCellValue(_TheOnlyRow, colDentingPass)
            If .GetRowCellValue(_TheOnlyRow, colDentingThreshold).ToString <> "" Then TheDeformationData.DentingThreshold = .GetRowCellValue(_TheOnlyRow, colDentingThreshold)
            If .GetRowCellValue(_TheOnlyRow, colDentingPipeID).ToString <> "" Then TheDeformationData.DentingPipeIDs = .GetRowCellValue(_TheOnlyRow, colDentingPipeID)
            If .GetRowCellValue(_TheOnlyRow, colDentingWorstDefo).ToString <> "" Then TheDeformationData.DentingWorst = .GetRowCellValue(_TheOnlyRow, colDentingWorstDefo)
            If .GetRowCellValue(_TheOnlyRow, colOvalityExceedingCount).ToString <> "" Then TheDeformationData.OvalityExceedingCount = .GetRowCellValue(_TheOnlyRow, colOvalityExceedingCount)
            If .GetRowCellValue(_TheOnlyRow, colOvalityNotExceedingCount).ToString <> "" Then TheDeformationData.OvalityNotExceedingCount = .GetRowCellValue(_TheOnlyRow, colOvalityNotExceedingCount)
            If .GetRowCellValue(_TheOnlyRow, colOvalityPass).ToString <> "" Then TheDeformationData.OvalityPasses = .GetRowCellValue(_TheOnlyRow, colOvalityPass)
            If .GetRowCellValue(_TheOnlyRow, colOvalityThreshold).ToString <> "" Then TheDeformationData.OvalityThreshold = .GetRowCellValue(_TheOnlyRow, colOvalityThreshold)
            If .GetRowCellValue(_TheOnlyRow, colOvalityPipeID).ToString.ToString <> "" Then TheDeformationData.OvalityPipeIDs = .GetRowCellValue(_TheOnlyRow, colOvalityPipeID)
            If .GetRowCellValue(_TheOnlyRow, colOvalityWorstDefo).ToString <> "" Then TheDeformationData.OvalityWorst = .GetRowCellValue(_TheOnlyRow, colOvalityWorstDefo)
            If .GetRowCellValue(_TheOnlyRow, colSwellingExceedingCount).ToString <> "" Then TheDeformationData.SwellingExceedingCount = .GetRowCellValue(_TheOnlyRow, colSwellingExceedingCount)
            If .GetRowCellValue(_TheOnlyRow, colSwellingNotExceedingCount).ToString <> "" Then TheDeformationData.SwellingNotExceedingCount = .GetRowCellValue(_TheOnlyRow, colSwellingNotExceedingCount)
            If .GetRowCellValue(_TheOnlyRow, colSwellingPass).ToString <> "" Then TheDeformationData.SwellingPasses = .GetRowCellValue(_TheOnlyRow, colSwellingPass)
            If .GetRowCellValue(_TheOnlyRow, colSwellingThreshold).ToString <> "" Then TheDeformationData.SwellingThreshold = .GetRowCellValue(_TheOnlyRow, colSwellingThreshold)
            If .GetRowCellValue(_TheOnlyRow, colSwellingPipeID).ToString <> "" Then TheDeformationData.SwellingPipeIDs = .GetRowCellValue(_TheOnlyRow, colSwellingPipeID)
            If .GetRowCellValue(_TheOnlyRow, colSwellingWorstDefo).ToString <> "" Then TheDeformationData.SwellingWorst = .GetRowCellValue(_TheOnlyRow, colSwellingWorstDefo)
        End With
        If cbInternalFoulingText.Text <> "" Then TheDeformationData.InternalFouling = True
        TheDeformationData.InternalFoulingExecutiveText = cbInternalFoulingText.Text
    End Sub
#End Region
#Region "Copy Paste Routines"
    Private Property ClipboardData() As String
        Get
            Dim iData As IDataObject = Clipboard.GetDataObject()
            If iData Is Nothing Then
                Return ""
            End If

            If iData.GetDataPresent(DataFormats.Text) Then
                Return CStr(iData.GetData(DataFormats.Text))
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            Clipboard.SetDataObject(value)
        End Set
    End Property


    Private Sub AddRow(ByVal data As String)
        If data = String.Empty Then
            Return
        End If
        'Split up one line of data separated by the special character.
        Dim rowData() As String = data.Split(New Char() {ControlChars.Cr, ChrW(&H9)})
        'Create a new row, unfortunately this does not return the handle to the new row.
        uiPipeOrBendDataInfoView.AddNewRow()
        'Get the new data row that was just created. This is an obnoxious looking line, I know!
        Dim TheNewRowHandle As Integer = uiPipeOrBendDataInfoView.GetDataRowHandleByGroupRowHandle(uiPipeOrBendDataInfoView.FocusedRowHandle)
        For i As Integer = 0 To rowData.Length - 1
            'if this is the first column of data, it should start with either 'Conv' or 'Rad' if it's a full row. Check to see if it has that at the start, and give a warning if not.
            If i = 0 And Not rowData(i).Contains("Rad") And Not rowData(i).Contains("Conv") Then
                Dim msgResult As MsgBoxResult = MessageBox.Show("The first item in this row to be pasted does not contain Rad or Conv. " & Environment.NewLine & Environment.NewLine & "Are you sure you have what you expect in the clipboard?", "Invalid Input", MessageBoxButtons.YesNo)
                If msgResult = MsgBoxResult.No Then
                    uiPipeOrBendDataInfoView.DeleteRow(TheNewRowHandle)
                    Return
                End If
            End If
            'Don't add data if there are no columns left to add to
            If i >= uiPipeOrBendDataInfoView.Columns.Count Then
                Exit For
            End If
            uiPipeOrBendDataInfoView.SetRowCellValue(TheNewRowHandle, uiPipeOrBendDataInfoView.VisibleColumns(i), rowData(i))
        Next i
    End Sub

#End Region


    Private Sub btnPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPaste.Click
        Dim data() As String = ClipboardData.Split(ControlChars.Lf)
        If data.Length < 1 Then
            Return
        End If
        'Do a preliminary check for Conv or Rad in the data so they can quickly say 'no' to even starting the process of pasting if they have unexpected data.
        If Not data(0).Contains("Rad") And Not data(0).Contains("Conv") And data.Length > 1 Then
            Dim msgResult As MsgBoxResult = MessageBox.Show("The first row to be pasted does not contain Rad or Conv. Each row will be checked individually after this step if you continue." & Environment.NewLine & Environment.NewLine & "Are you sure you have what you expect in the clipboard?", "Probably Invalid Input", MessageBoxButtons.YesNo)
            If msgResult = MsgBoxResult.No Then Return
        End If
        For Each row As String In data
            'Only allow up to 8 rows to be created.
            If uiPipeOrBendDataInfoView.RowCount < 7 Then AddRow(row)
        Next row
        uiPipeOrBendDataInfoView.Focus()
    End Sub

    Public Sub ToggleColumns(Optional ByVal eventArgs As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs = Nothing)
        With uiDeformationDataView
            If .RowCount < 1 Then Return 'just in case there's nothing here. THis should never happen, though.
            Dim _TheOnlyRow As Integer = .GetVisibleRowHandle(0)
            Dim _OverBulgeCount As Integer = CType(.GetRowCellValue(_TheOnlyRow, colBulgingExceedingCount), Integer)
            Dim _OverDentingCount As Integer = .GetRowCellValue(_TheOnlyRow, colDentingExceedingCount)
            Dim _OverOvalityCount As Integer = .GetRowCellValue(_TheOnlyRow, colOvalityExceedingCount)
            Dim _OverSwellingCount As Integer = .GetRowCellValue(_TheOnlyRow, colSwellingExceedingCount)

            If _OverBulgeCount < 1 Then
                colBulgingPipeID.OptionsColumn.ReadOnly = True
                colBulgingPass.OptionsColumn.ReadOnly = True
                colBulgingWorstDefo.OptionsColumn.ReadOnly = True
                colBulgingPipeID.AppearanceCell.BackColor = Color.LightGray
                colBulgingPass.AppearanceCell.BackColor = Color.LightGray
                colBulgingWorstDefo.AppearanceCell.BackColor = Color.LightGray
            Else
                colBulgingPipeID.OptionsColumn.ReadOnly = False
                colBulgingPass.OptionsColumn.ReadOnly = False
                colBulgingWorstDefo.OptionsColumn.ReadOnly = False
                colBulgingPipeID.AppearanceCell.BackColor = Color.White
                colBulgingPass.AppearanceCell.BackColor = Color.White
                colBulgingWorstDefo.AppearanceCell.BackColor = Color.White
            End If
            If _OverDentingCount < 1 Then
                colDentingPipeID.OptionsColumn.ReadOnly = True
                colDentingPass.OptionsColumn.ReadOnly = True
                colDentingWorstDefo.OptionsColumn.ReadOnly = True
                colDentingPipeID.AppearanceCell.BackColor = Color.LightGray
                colDentingPass.AppearanceCell.BackColor = Color.LightGray
                colDentingWorstDefo.AppearanceCell.BackColor = Color.LightGray
            Else
                colDentingPipeID.OptionsColumn.ReadOnly = False
                colDentingPass.OptionsColumn.ReadOnly = False
                colDentingWorstDefo.OptionsColumn.ReadOnly = False
                colDentingPipeID.AppearanceCell.BackColor = Color.White
                colDentingPass.AppearanceCell.BackColor = Color.White
                colDentingWorstDefo.AppearanceCell.BackColor = Color.White
            End If
            If _OverOvalityCount < 1 Then
                colOvalityPipeID.OptionsColumn.ReadOnly = True
                colOvalityPass.OptionsColumn.ReadOnly = True
                colOvalityWorstDefo.OptionsColumn.ReadOnly = True
                colOvalityPipeID.AppearanceCell.BackColor = Color.LightGray
                colOvalityPass.AppearanceCell.BackColor = Color.LightGray
                colOvalityWorstDefo.AppearanceCell.BackColor = Color.LightGray
            Else
                colOvalityPipeID.OptionsColumn.ReadOnly = False
                colOvalityPass.OptionsColumn.ReadOnly = False
                colOvalityWorstDefo.OptionsColumn.ReadOnly = False
                colOvalityPipeID.AppearanceCell.BackColor = Color.White
                colOvalityPass.AppearanceCell.BackColor = Color.White
                colOvalityWorstDefo.AppearanceCell.BackColor = Color.White
            End If
            If _OverSwellingCount < 1 Then
                colSwellingPipeID.OptionsColumn.ReadOnly = True
                colSwellingPass.OptionsColumn.ReadOnly = True
                colSwellingWorstDefo.OptionsColumn.ReadOnly = True
                colSwellingPipeID.AppearanceCell.BackColor = Color.LightGray
                colSwellingPass.AppearanceCell.BackColor = Color.LightGray
                colSwellingWorstDefo.AppearanceCell.BackColor = Color.LightGray
            Else
                colSwellingPipeID.OptionsColumn.ReadOnly = False
                colSwellingPass.OptionsColumn.ReadOnly = False
                colSwellingWorstDefo.OptionsColumn.ReadOnly = False
                colSwellingPipeID.AppearanceCell.BackColor = Color.White
                colSwellingPass.AppearanceCell.BackColor = Color.White
                colSwellingWorstDefo.AppearanceCell.BackColor = Color.White
            End If
        End With
    End Sub
    Public Sub ClearAndFillInPasses()
        'This subroutine will clear and then add the correct pass names to the comboboxes on the user control based on what has been provided by the main form.
        uiPassNumber.Properties.Items.Clear()
        PassSelector.Items.Clear()

        uiPassNumber.Properties.Items.AddRange(lstPasses)
        PassSelector.Items.AddRange(lstPasses)
       
    End Sub


    
    Private Sub uiDeformationDataView_CellValueChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles uiDeformationDataView.CellValueChanged
        ToggleColumns(e) 'Hide certain columns depending on values changing.
    End Sub

    
End Class
