Imports DevExpress.Data.Filtering
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraEditors.DXErrorProvider

Public Class NewInspection
    Private frmCustomer As CustomerDetails
    Private frmInspectDetails2 As InspectionDetails
    Private oCurInspectionType As QTT_InspectionsDataSets.tblInspectionTypesRow
    Private oCurCustomer As QTT_InspectionsDataSets.tblCustomersRow
    Private FolderName As String

    Public Sub New()

        InitializeComponent()

    End Sub

    Private Sub NewInspection_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.taTblInspectionTypes.FillBy_ActiveTypes(Me.QTT_InspectionsDataSets1.tblInspectionTypes)

        Call SetupUI_CustomersList(0)      'Fill customers lookup editor
        Call SetupUI_InspectionType()      'Set up UI - FTIS or not

        'Setup dates UI - default to single day inspection using current date
        uiInspectionStartDate.EditValue = DateTime.Now
        uiInspectionEndDate.EditValue = DateTime.Now

        'KM 7/1/2011  Setup UI for custom filters on grid
        AddHandler uiGridViewInspections.CustomFilterDialog, AddressOf GridView_CustomFilterDialog
        AddHandler uiGridViewInspections.ShowFilterPopupDate, AddressOf GridView_ShowFilterPopupDate

        'KM 8/1/2011  Set up customer validation rule (default in control doesn't work)
        Dim NotBlankValidationRule As New NotBlankValidationRule()
        NotBlankValidationRule.ErrorText = "Customer cannot be blank."
        DxValidationProvider.SetValidationRule(uiCustomers, NotBlankValidationRule)

        Call ShowPriorInspections()  'Load table with prior inspections

    End Sub

    Public Class NotBlankValidationRule
        'KM 8/1/2011  'Custom validation rule for "not blank" 
        Inherits ValidationRule

        Public Overrides Function Validate(control As Control, value As Object) As Boolean

            Dim result As Boolean = True
            If control.Text = String.Empty Then result = False

            Return result

        End Function

    End Class

    Public Function SetupUI_CustomersList(ByVal intSelectID As Integer) As Boolean
        'Fill customers lookup editor

        oCurCustomer = Nothing  'reset
        Me.taTblCustomers.Fill(Me.QTT_InspectionsDataSets1.tblCustomers)

        'KM 7/2/2011  'Set the dropdown length to fit the number of customers
        uiCustomers.Properties.DropDownRows = Math.Min(Me.QTT_InspectionsDataSets1.tblCustomers.Count, 15)

        If intSelectID = 0 Then
            uiCustomers.EditValue = System.DBNull.Value
        Else
            uiCustomers.EditValue = intSelectID
            Call UpdateFolderName()
        End If

    End Function

    Public Sub SetupUI_InspectionType()
        'Get currently selected inspection type
        Dim drvTemp As DataRowView = uiInspectionTypes.Properties.GetDataSourceRowByKeyValue(uiInspectionTypes.EditValue)

        If Not (drvTemp Is Nothing) Then
            oCurInspectionType = drvTemp.Row
        Else
            oCurInspectionType = Nothing
        End If

    End Sub

    Public Sub SetupUI_PriorInspections()
        'Setup UI - copy or not
        Dim fShowPriors As Boolean = uiCopyDataCheckbox.Checked

        uiViewInspectionButton.Enabled = fShowPriors
        uiGridControlInspections.Enabled = fShowPriors
        uiGridLabel.Enabled = fShowPriors

        'copy options
        uiCopyOptionsLabel.Enabled = fShowPriors
        uiCopyCustomerCheckbox.Enabled = fShowPriors
        uiCopyOtherInspectorsCheckbox.Enabled = fShowPriors
        uiCopyDescriptionCheckbox.Enabled = fShowPriors
        uiCopyPlantLocationCheckbox.Enabled = fShowPriors
        uiCopyPrimaryInspectorCheckbox.Enabled = fShowPriors

        'If the types match, enable and copy tools, and unit info
        If (uiInspectionTypes.Text = uiGridViewInspections.GetFocusedRowCellValue(colInspectionTypeDescription)) And fShowPriors Then
            uiCopyToolsCheckbox.Enabled = True
            uiCopyUnitInfoCheckbox.Enabled = True

            uiCopyToolsCheckbox.Checked = True
            uiCopyUnitInfoCheckbox.Checked = True
            'Otherwise, don't
        Else
            uiCopyToolsCheckbox.Enabled = False
            uiCopyUnitInfoCheckbox.Enabled = False

            uiCopyToolsCheckbox.Checked = False
            'uiCopyPassInfoCheckbox.Checked = False
            uiCopyUnitInfoCheckbox.Checked = False
        End If

        'KM 8/1/2011
        'If they uncheck the copy option, empty out the text boxes and uncheck the boxes
        If Not fShowPriors Then
            uiCustomers.EditValue = -1
            uiLocation.EditValue = String.Empty
            uiUnitName.EditValue = String.Empty
            uiUnitNumber.EditValue = String.Empty
            uiCopyPrimaryInspectorCheckbox.Checked = fShowPriors
            uiCopyOtherInspectorsCheckbox.Checked = fShowPriors
            uiCopyDescriptionCheckbox.Checked = fShowPriors
        End If

        Call UpdateReportMessage() 'Update the "New Inspection Parameters" label on the form

    End Sub

    Private Sub UpdateCopyOptions(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiCopyCustomerCheckbox.CheckedChanged, uiCopyPlantLocationCheckbox.CheckedChanged, uiCopyUnitInfoCheckbox.CheckedChanged
        'KM 7/8/2011
        'Update whenever a copy checkbox option is changed
        Call UpdatePriorInspectionInfo() 'Update data fields
        Call UpdateFolderName()          'Update folder name

    End Sub

    Private Sub ShowPriorInspections()
        'Load table with prior inspections
        Dim strUnitName As String = uiUnitName.Text
        Dim strUnitNumber As String = uiUnitNumber.Text
        Const IGNORE_DATE = "1/1/1900"

        Dim intInspectionType As Integer
        Dim intCustomerID As Integer = 0
        Dim datRangeStart As Date = IGNORE_DATE
        Dim datRangeEnd As Date = IGNORE_DATE
        Dim strLocText As String = ""
        taTblInspections.FillBy_PriorInspections(Me.QTT_InspectionsDataSets1.tblInspections, intCustomerID, intInspectionType, datRangeStart, datRangeEnd, strLocText)

    End Sub

    Private Sub CreateNewInspection()
        'Create a new inspection
        Const CONF_DATE_FORMAT = "MMMM d, yyyy"
        Dim intCustomerID As Integer = uiCustomers.EditValue
        Dim intInspectionType As Integer = uiInspectionTypes.EditValue
        Dim intQTT_OfficeID As Integer = modProgram.CurrentQTTOffice
        Dim strUnitName As String = uiUnitName.Text
        Dim strUnitNumber As String = uiUnitNumber.Text
        Dim strRootFolder As String = modProgram.CleanInvalidFilenameChars(FolderName)
        Dim strLocation As String = uiLocation.Text
        Dim datStart As Date = uiInspectionStartDate.EditValue
        Dim datEnd As Date = uiInspectionEndDate.EditValue
        Dim intTest As Integer = -1
        'FTIS Related
        Dim intNumPasses As Integer = 0         'Need to keep to pass to pre-existing function
        Dim fPassesInAlpha As Boolean = False   'Need to keep to pass to pre-existing function
        'KM 7/7/2011 
        'Setup array for custom passes, other default values
        Dim arrCustomPasses As Array = Nothing
        Dim strCustomPasses As String = Nothing
        Dim fPassesCustom As Boolean = False
        Dim fRowsInAlpha As Boolean = False
        Dim intNumberOfRows As Integer = 0
        Dim fTubesInAlpha As Boolean = False
        Dim intNumberOfTubes As Integer = 0
        Dim fCanCircular As Boolean = False
        Dim fSampleLevel3 As Boolean = False
        'Prior Inspection related
        Dim oCurRow As DataRow
        Dim oPriorInspection As QTT_InspectionsDataSets.tblInspectionsRow
        Dim sbPriorDesc As New System.Text.StringBuilder(1000)
        Dim intCopyFromID As Integer = 0

        'If copying from a previous inspection, get the values from the selected item in the grid
        If uiCopyDataCheckbox.Checked Then
            oCurRow = uiGridViewInspections.GetDataRow(uiGridViewInspections.FocusedRowHandle)
            oPriorInspection = TryCast(oCurRow, QTT_InspectionsDataSets.tblInspectionsRow)
            intCopyFromID = oPriorInspection.ID
        End If

        ' check folder duplication 
        Dim taInspections As New QTT_InspectionsDataSetsTableAdapters.tblInspectionsTableAdapter
        Dim dtInsp As New QTT_InspectionsDataSets.tblInspectionsDataTable
        Dim MsgText As String = ""
        Dim strX As String
        Dim iDuplicates As Integer = 0
        Call taInspections.Fill(dtInsp)
        For Each drInsp As QTT_InspectionsDataSets.tblInspectionsRow In dtInsp
            If Not drInsp.IsRootFolderNameNull Then
                If drInsp.RootFolderName = strRootFolder Then
                    iDuplicates += 1
                    strX = "#" & iDuplicates & ") Customer: " & drInsp.CustomerName & vbNewLine & "  Date: " & drInsp.InspectionDateStart & vbNewLine & "  Location: " & drInsp.LocationDesc
                    If MsgText.Length > 0 Then
                        MsgText.Insert(MsgText.Length, vbNewLine & strX)
                    Else
                        MsgText = strX
                    End If
                End If
            End If
        Next
        If iDuplicates Then
            MessageBox.Show(Me, "A folder already exists for this inspection. Please open the existing report or, if this is a new inspection, modify your dates or details.", "Duplicate Report", MessageBoxButtons.OK)
            Exit Sub
        End If

        'Create the inspection
        'Added false for new tube/row/can/circular bit values, not needed until next screen, but can't be null
        Dim intNewInspectionID As Integer = taTblInspections.CreateNewInspection(intInspectionType, intCustomerID, intQTT_OfficeID, datStart, datEnd, strLocation, strUnitName, strUnitNumber, intNumPasses, fPassesInAlpha, strCustomPasses, fTubesInAlpha, intNumberOfTubes, fRowsInAlpha, intNumberOfRows, fCanCircular, fSampleLevel3, strRootFolder)
        If intNewInspectionID = 0 Then
            MsgBox("Failed to add new inspection for an unknown reason", MsgBoxStyle.Information)
            Exit Sub
        End If

        'setup folder structure
        'KM 7/7/2011 - Modified for custom passes
        Call modProgram.SetupInspectionFolders(strRootFolder, intNumPasses, fPassesInAlpha, fPassesCustom, arrCustomPasses, intInspectionType)

        'Copy data from prior Inspection as needed
        If uiCopyDataCheckbox.Checked Then
            'KM 8/2/2011 Updated to support new stored procedure
            taTblInspections.CopyInspectionData(intCopyFromID, intNewInspectionID, uiCopyPlantLocationCheckbox.Checked, False,
                                                uiCopyDescriptionCheckbox.Checked, uiCopyUnitInfoCheckbox.Checked, uiCopyPrimaryInspectorCheckbox.Checked,
                                                uiCopyOtherInspectorsCheckbox.Checked, uiCopyToolsCheckbox.Checked, False)
        End If

        'Setup pipe tube items. This procedure will only create the records and give them a pass number, not fill them with data.
        Dim intSourceID As Integer = 0
        If uiCopyDataCheckbox.Checked And uiCopyUnitInfoCheckbox.Checked Then
            intSourceID = intCopyFromID
        End If
        If taTblPipeTubeItems.Setup_PipeTubeItemsForInspectionID(intNewInspectionID, intNumPasses, intSourceID) <> 0 Then
            MsgBox("Failed to add new pipe/tube Items for new inspection for an unknown reason.", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Setup report items - main report items table
        If taReportItems.Setup_ReportItems_Main_ForInspection(intNewInspectionID).Value <> 0 Then
            MsgBox("Failed to add report items for new inspection for an unknown reason.", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Setup inspection appendix(es).
        Call modProgram.SetupInspectionAppendixes(intNewInspectionID, oCurInspectionType.ID, intNumPasses, fPassesInAlpha, fPassesCustom, strCustomPasses)

        'Add summary records
        taTblReformerAgedSummary.CreateEmptyRecord(intNewInspectionID, False, False, False, False, False, False, False)
        taTblReformerNewSummary.CreateEmptyRecord(intNewInspectionID)

        'Show InspectionDetails form
        frmInspectDetails2 = New InspectionDetails
        With frmInspectDetails2
            .Show()
            .OpenInspection(intNewInspectionID, True, True)
        End With

        'Close this form
        Me.Close()

    End Sub

    Private Sub UpdateFolderName()
        'Process update to folder name using combination of customer, unit, and current date

        'Get currently selected customer
        Dim drvTemp As DataRowView = uiCustomers.Properties.GetDataSourceRowByKeyValue(uiCustomers.EditValue)
        Dim strCurDesc As String
        If drvTemp Is Nothing Then
            oCurCustomer = Nothing
            strCurDesc = String.Empty
        Else
            oCurCustomer = drvTemp.Row
            strCurDesc = Trim(Mid(oCurCustomer.Name, 1, 20))
            'Location
            If Len(uiLocation.Text) > 0 Then
                strCurDesc = strCurDesc & "-" & Trim(Mid(uiLocation.Text, 1, 20))
            End If
            'Unit Name
            If Len(uiUnitName.Text) > 0 Then
                strCurDesc = strCurDesc & "_" & Trim(uiUnitName.Text)
            Else
                strCurDesc = strCurDesc & "_Unit-NA"
            End If
            'Unit Number
            If Len(uiUnitNumber.Text) > 0 Then
                strCurDesc = strCurDesc & "_" & Trim(uiUnitNumber.Text) & ""
            End If
            'LOTIS/MANTIS Type
            Select Case uiInspectionTypes.EditValue
                Case 6, 9   'Aged
                    strCurDesc = strCurDesc & "_" & "Aged"
                Case 7, 10   'NewInstalled
                    strCurDesc = strCurDesc & "_" & "NewInstalled"
                Case 8, 11   'Spare
                    strCurDesc = strCurDesc & "_" & "Uninstalled"
                Case Else
                    'Don't do anything
            End Select
            'Month day and year  KM 6/30/2011
            strCurDesc = strCurDesc & "-" & Format(uiInspectionStartDate.EditValue, "dMMMyyyy").ToUpper
            'Strip invalid/illegal characters and limit to 84 characters
            'Changed limit from 64 to 84 characters
            'May need to revisit if folder names are too long
            strCurDesc = modProgram.CleanInvalidFilenameChars(strCurDesc)
            If Len(strCurDesc) > 84 Then strCurDesc = Mid(strCurDesc, 1, 84)
        End If
        ' update
        FolderName = strCurDesc
        Call UpdateReportMessage() 'Update the "New Inspection Parameters" label on the form

    End Sub

    Private Sub UpdateReportMessage()
        'Update the "New Inspection Parameters" label on the form
        Const CONF_DATE_FORMAT = "MMMM d, yyyy"
        'JF 8/2/2011
        'Changed this form to show red data and blue labels. Also split up one large label into a bunch of smaller ones.
        uiDataInspectionType.Text = uiInspectionTypes.Text
        uiDataCustomers.Text = uiCustomers.Text
        uiDataPlantName.Text = uiLocation.Text
        uiDataHeaterName.Text = uiUnitName.Text
        uiDataHeaterID.Text = uiUnitNumber.Text
        uiDataInspectionDate.Text = Format(uiInspectionStartDate.EditValue, CONF_DATE_FORMAT) & " - " & Format(uiInspectionEndDate.EditValue, CONF_DATE_FORMAT)
        uiDataFolder.Text = FolderName

    End Sub

    Sub ViewSelectedPriorInspection(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiViewInspectionButton.Click

        'Display the selected inspection (from the prior inspections grid)
        Dim oCurRow As DataRow = uiGridViewInspections.GetDataRow(uiGridViewInspections.FocusedRowHandle)
        Dim oInspRow As QTT_InspectionsDataSets.tblInspectionsRow = TryCast(oCurRow, QTT_InspectionsDataSets.tblInspectionsRow)
        Try
            OpenQTTInspection(oInspRow.ID, oInspRow.FK_InspectionType, False)
        Catch
            MsgBox("There are no inspections available to view.", vbOKOnly, "Message")
        End Try

    End Sub

    Private Sub UpdatePriorInspectionInfo()

        'Called when a prior inspection is selected, updates data on screen as needed.

        'Exit early if not needed
        If Not uiCopyDataCheckbox.Checked Then Exit Sub

        'Read current row
        Dim oCurRow As DataRow = uiGridViewInspections.GetDataRow(uiGridViewInspections.FocusedRowHandle)
        Dim oInspRow As QTT_InspectionsDataSets.tblInspectionsRow = TryCast(oCurRow, QTT_InspectionsDataSets.tblInspectionsRow)

        'Update Location if needed
        If uiCopyPlantLocationCheckbox.Checked Then
            If Not oInspRow.IsLocationDescNull Then
                uiLocation.Text = oInspRow.LocationDesc
            End If
        Else
            uiLocation.Text = String.Empty
        End If

        'Update Unit data if needed
        If uiCopyUnitInfoCheckbox.Checked Then
            If Not oInspRow.IsUnitNameNull Then
                uiUnitName.Text = oInspRow.UnitName
            End If
            If Not oInspRow.IsUnitNumberNull Then
                uiUnitNumber.Text = oInspRow.UnitNumber
            End If
        Else
            uiUnitName.Text = String.Empty
            uiUnitNumber.Text = String.Empty
        End If

        'Update Customer if needed
        If uiCopyCustomerCheckbox.Checked Then
            If Not oInspRow.IsCustomerNameNull Then
                uiCustomers.EditValue = uiCustomers.Properties.GetKeyValueByDisplayText(oInspRow.CustomerName)
            End If
        Else
            uiCustomers.EditValue = -1
        End If

    End Sub

    Private Sub GridView_CustomFilterDialog(ByVal sender As Object, ByVal e As CustomFilterDialogEventArgs)
        'KM 7/1/2011 Add filter to grid
        e.Handled = True
        DirectCast(sender, GridView).ShowFilterEditor(e.Column)
    End Sub

    Private Sub GridView_ShowFilterPopupDate(ByVal sender As System.Object, ByVal e As FilterPopupDateEventArgs)
        'Add custom filter option for dates
        Select Case e.Column.FieldName
            'Customize filter for date columns
            Case "InspectionDateStart"
                e.List.Clear()
                Dim Today As DateTime = DateTime.Today

                Dim PastWeek As Date = Today.Subtract(TimeSpan.FromDays(7))
                Dim crit As CriteriaOperator = New BinaryOperator(e.Column.FieldName, PastWeek, BinaryOperatorType.GreaterOrEqual)
                e.List.Add(New DevExpress.XtraEditors.FilterDateElement("Past Week", "", crit))

                Dim PastMonth As DateTime = New DateTime(DateTime.Today.Year, DateTime.Today.Month - 1, DateTime.Today.Day)
                crit = New BinaryOperator(e.Column.FieldName, PastMonth, BinaryOperatorType.GreaterOrEqual)
                e.List.Add(New DevExpress.XtraEditors.FilterDateElement("Past Month", "", crit))

                Dim PastYear As DateTime = New DateTime(DateTime.Today.Year - 1, DateTime.Today.Month, DateTime.Today.Day)
                crit = New BinaryOperator(e.Column.FieldName, PastYear, BinaryOperatorType.GreaterOrEqual)
                e.List.Add(New DevExpress.XtraEditors.FilterDateElement("Past Year", "", crit))

            Case Else
                'As needed, add other custom filters for other columns
        End Select
    End Sub

    Private Sub uiCopyDataCheckbox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles uiCopyDataCheckbox.CheckedChanged
        'When the user checks the copy from a previous inspection checkbox:
        'Don't copy if there are no previous inspections
        If uiGridViewInspections.RowCount = 0 Then
            uiCopyDataCheckbox.Checked = False
            Exit Sub
        End If
        'Otherwise
        Call SetupUI_PriorInspections()  'Setup UI based on checkbox (copy or not)
        Call UpdatePriorInspectionInfo() 'Update data fields
        Call UpdateFolderName()          'Update folder name
    End Sub

    Private Sub uiCreateInspectionButton_Click(sender As System.Object, e As System.EventArgs) Handles uiCreateInspectionButton.Click

        Me.Cursor = Cursors.WaitCursor

        'Validate the data
        DxValidationProvider.Validate()

        'If the data is invalid, stop
        If (DxValidationProvider.GetInvalidControls.Count > 0) Then
            MessageBox.Show(Me, "Please check for missing or incorrect data. The new inspection cannot be created with the values you have provided.", "Missing or Incorrect Data", vbOKOnly, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Otherwise, continue on and create a new inspection
        Call CreateNewInspection()
        Main.UpdateInspections()

        Me.Cursor = Cursors.Default

    End Sub

    Private Sub uiCancelButton_Click(sender As System.Object, e As System.EventArgs) Handles uiCancelButton.Click

        Me.Close()

    End Sub

    Private Sub uiInspectionStartDate_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles uiInspectionStartDate.EditValueChanged

        'Update 'end date' as needed.
        If uiInspectionEndDate.EditValue < uiInspectionStartDate.EditValue Then
            uiInspectionEndDate.EditValue = uiInspectionStartDate.EditValue
        End If

        DxValidationProvider.Validate()

        Call UpdateFolderName() 'Update folder name

    End Sub

    Private Sub uiCustomers_ButtonClick(sender As Object, e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles uiCustomers.ButtonClick

        'KM 6/30/2011  Handle the NEW and VIEW customer events to add or edit customers
        If IsNothing(e.Button.Tag) Then Exit Sub

        Select Case e.Button.Tag
            Case "ADD"
                If uiCustomers.Text.Length = 0 Then Exit Sub 'Don't try to add a blank customer
                Dim intNewCustomerID As Integer
                Dim CustomerName As String = uiCustomers.Text 'Save the customer name because the data source will need to be updated if the new customer is created, and that will clear the text.
                'Check if there is a customer of the same name in the database already.
                Dim dtSimilarCustomers As New QTT_InspectionsDataSets.tblCustomersDataTable
                taTblCustomers.Fill(dtSimilarCustomers)
                'Just in case the name contains a single quote, replace it with two single quotes so the SQL will work.
                Dim FixedCustomerName As String
                If CustomerName.Contains("'") Then
                    FixedCustomerName = CustomerName.Replace("'", "''")
                Else
                    FixedCustomerName = CustomerName
                End If
                Dim drSimilarCustomers() As DataRow = dtSimilarCustomers.Select("Name = '" & FixedCustomerName & "'")
                If drSimilarCustomers.Length > 0 AndAlso drSimilarCustomers(0).Item("Name") = CustomerName Then
                    MessageBox.Show("A Customer named " & CustomerName & " already exists and cannot be added to the database.", "Customer Already Exists.", MessageBoxButtons.OK)
                    Exit Sub
                End If
                intNewCustomerID = taTblCustomers.CreateNewCustomer(uiCustomers.Text, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing).Value
                If intNewCustomerID = 0 Then
                    MsgBox("Failed to add new customer for an unknown reason", MsgBoxStyle.Information)
                    Exit Sub
                End If
                Me.taTblCustomers.Fill(Me.QTT_InspectionsDataSets1.tblCustomers)
                uiCustomers.Text = CustomerName
                If Me.QTT_InspectionsDataSets1.tblCustomers.Count > 9 Then
                    uiCustomers.Properties.DropDownRows = 9
                Else
                    uiCustomers.Properties.DropDownRows = Me.QTT_InspectionsDataSets1.tblCustomers.Count
                End If
        End Select

    End Sub

    Private Sub uiCustomers_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles uiCustomers.EditValueChanged

        'Update when customer is changed
        If Not IsDBNull(uiCustomers.EditValue) Then
            If (DxValidationProvider.GetInvalidControls().Contains(uiCustomers)) Then DxValidationProvider.Validate(uiCustomers)
        End If

        Call UpdateFolderName()    'Update folder name

    End Sub

    Private Sub uiLocation_TextChanged(sender As System.Object, e As System.EventArgs) Handles uiLocation.TextChanged

        'Update and validate if location changes
        Call UpdateFolderName()
        If (DxValidationProvider.GetInvalidControls().Contains(uiLocation)) Then DxValidationProvider.Validate(uiLocation)

    End Sub

    Private Sub uiInspectionEndDate_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles uiInspectionEndDate.EditValueChanged

        'Validate the dates whenever the end date is changed
        'KM 7/11/2011
        If (DxValidationProvider.GetInvalidControls().Contains(uiInspectionStartDate)) Then DxValidationProvider.Validate(uiInspectionEndDate)
        If (DxValidationProvider.GetInvalidControls().Contains(uiInspectionEndDate)) Then DxValidationProvider.Validate(uiInspectionStartDate)

    End Sub

    Private Sub uiInspectionTypes_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles uiInspectionTypes.EditValueChanged

        'Update if inspection type changed
        'JHF 8/9/2011 Added a visibility check to make sure the form is loaded and visible before trying to set up the prior inspections. Otherwise an error will be thrown.
        If Me.Visible = True Then Call SetupUI_PriorInspections() 'Setup UI based on checkbox (copy or not)
        Call SetupUI_InspectionType()    'Setup UI based on inspection type
        Call UpdatePriorInspectionInfo() 'Update data fields
        Call UpdateFolderName()          'Update folder name

    End Sub

    Private Sub uiGridViewInspections_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles uiGridViewInspections.FocusedRowChanged

        'Update if prior inspection changed
        Call SetupUI_PriorInspections()  'Setup UI based on checkbox (copy or not)
        Call UpdatePriorInspectionInfo() 'Update data fields
        Call UpdateFolderName()          'Update folder name

    End Sub

    Private Sub UnitNameOrNumChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiUnitName.TextChanged, uiUnitNumber.TextChanged

        'Update folder name (and report parameters label) if unit name or number is changed) 
        If (DxValidationProvider.GetInvalidControls().Contains(sender)) Then DxValidationProvider.Validate(sender)
        Call UpdateFolderName()

    End Sub

End Class