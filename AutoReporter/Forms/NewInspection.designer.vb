<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NewInspection
   Inherits System.Windows.Forms.Form

   'Form overrides dispose to clean up the component list.
   <System.Diagnostics.DebuggerNonUserCode()> _
   Protected Overrides Sub Dispose(ByVal disposing As Boolean)
      If disposing AndAlso components IsNot Nothing Then
         components.Dispose()
      End If
      MyBase.Dispose(disposing)
   End Sub

   'Required by the Windows Form Designer
   Private components As System.ComponentModel.IContainer

   'NOTE: The following procedure is required by the Windows Form Designer
   'It can be modified using the Windows Form Designer.  
   'Do not modify it using the code editor.
   <System.Diagnostics.DebuggerStepThrough()> _
   Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim CompareAgainstControlValidationRule1 As DevExpress.XtraEditors.DXErrorProvider.CompareAgainstControlValidationRule = New DevExpress.XtraEditors.DXErrorProvider.CompareAgainstControlValidationRule()
        Dim SerializableAppearanceObject2 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim CompareAgainstControlValidationRule2 As DevExpress.XtraEditors.DXErrorProvider.CompareAgainstControlValidationRule = New DevExpress.XtraEditors.DXErrorProvider.CompareAgainstControlValidationRule()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(NewInspection))
        Dim SerializableAppearanceObject3 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject4 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject5 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule1 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim ConditionValidationRule2 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim ConditionValidationRule3 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim ConditionValidationRule4 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Me.uiInspectionEndDate = New DevExpress.XtraEditors.DateEdit()
        Me.uiInspectionStartDate = New DevExpress.XtraEditors.DateEdit()
        Me.uiInspectionTypeLabel = New System.Windows.Forms.Label()
        Me.uiCustomerLabel = New System.Windows.Forms.Label()
        Me.TblInspectionTypesBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsNewInspectionTypes = New System.Windows.Forms.BindingSource(Me.components)
        Me.QTT_InspectionsDataSets1 = New QIG.AutoReporter.QTT_InspectionsDataSets()
        Me.TblInspectionTypesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.bstblCustomers = New System.Windows.Forms.BindingSource(Me.components)
        Me.uiUnitNameLabel = New System.Windows.Forms.Label()
        Me.uiFormTitleLabel = New System.Windows.Forms.Label()
        Me.bsQTTLookupsData = New System.Windows.Forms.BindingSource(Me.components)
        Me.QTT_LookupsDataSets = New QIG.AutoReporter.QTT_LookupsData()
        Me.uiUnitNumberLabel = New System.Windows.Forms.Label()
        Me.taTblCustomers = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblCustomersTableAdapter()
        Me.taTblInspectionTypes = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionTypesTableAdapter()
        Me.uiPlantNameLabel = New System.Windows.Forms.Label()
        Me.uiEndDateLabel = New System.Windows.Forms.Label()
        Me.uiStartDateLabel = New System.Windows.Forms.Label()
        Me.taTblPipeTubeItems = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblPipeTubeItemsTableAdapter()
        Me.taTblInspections = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionsTableAdapter()
        Me.taReportItems = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblReportItemsTableAdapter()
        Me.taAppendixes = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.AppendixesTableAdapter()
        Me.bsInspections = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsCustomerFilterList = New System.Windows.Forms.BindingSource(Me.components)
        Me.toolTipNew = New System.Windows.Forms.ToolTip(Me.components)
        Me.uiGridControlInspections = New DevExpress.XtraGrid.GridControl()
        Me.uiGridViewInspections = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colInspectionTypeDescription = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCustomerName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colInspectionDateStart = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLocationDesc = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCity = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCountry = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUnitName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUnitNumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colProjectNumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.uiQuestImage = New System.Windows.Forms.PictureBox()
        Me.uiInspectionTypes = New DevExpress.XtraEditors.LookUpEdit()
        Me.uiCustomers = New DevExpress.XtraEditors.LookUpEdit()
        Me.uiCopyDataCheckbox = New DevExpress.XtraEditors.CheckEdit()
        Me.uiCopyCustomerCheckbox = New DevExpress.XtraEditors.CheckEdit()
        Me.uiCopyPlantLocationCheckbox = New DevExpress.XtraEditors.CheckEdit()
        Me.uiCopyUnitInfoCheckbox = New DevExpress.XtraEditors.CheckEdit()
        Me.uiCopyToolsCheckbox = New DevExpress.XtraEditors.CheckEdit()
        Me.uiCopyPrimaryInspectorCheckbox = New DevExpress.XtraEditors.CheckEdit()
        Me.uiCopyOtherInspectorsCheckbox = New DevExpress.XtraEditors.CheckEdit()
        Me.uiCopyDescriptionCheckbox = New DevExpress.XtraEditors.CheckEdit()
        Me.uiCopyOptionsLabel = New System.Windows.Forms.Label()
        Me.uiGridLabel = New System.Windows.Forms.Label()
        Me.uiCancelButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiCreateInspectionButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiViewInspectionButton = New DevExpress.XtraEditors.SimpleButton()
        Me.DxValidationProvider = New DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(Me.components)
        Me.uiLocation = New DevExpress.XtraEditors.TextEdit()
        Me.uiUnitName = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiUnitNumber = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiDetailsLine1 = New DevExpress.XtraEditors.LabelControl()
        Me.uiDetailsPlantName = New DevExpress.XtraEditors.LabelControl()
        Me.uiDetailsHeaterName = New DevExpress.XtraEditors.LabelControl()
        Me.uiDetailsHeaterID = New DevExpress.XtraEditors.LabelControl()
        Me.uiDetailsInspectionDate = New DevExpress.XtraEditors.LabelControl()
        Me.uiDetailsFolder = New DevExpress.XtraEditors.LabelControl()
        Me.uiDataPlantName = New DevExpress.XtraEditors.LabelControl()
        Me.uiDataHeaterName = New DevExpress.XtraEditors.LabelControl()
        Me.uiDataHeaterID = New DevExpress.XtraEditors.LabelControl()
        Me.uiDataInspectionDate = New DevExpress.XtraEditors.LabelControl()
        Me.uiDataFolder = New DevExpress.XtraEditors.LabelControl()
        Me.uiDataInspectionType = New DevExpress.XtraEditors.LabelControl()
        Me.uiDetailsInspectionType = New DevExpress.XtraEditors.LabelControl()
        Me.uiDataCustomers = New DevExpress.XtraEditors.LabelControl()
        Me.uiDetailsCustomer = New DevExpress.XtraEditors.LabelControl()
        Me.taTblReformerAgedSummary = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblReformerAgedSummaryTableAdapter()
        Me.taTblReformerNewSummary = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblReformerNewSummaryTableAdapter()
        CType(Me.uiInspectionEndDate.Properties.VistaTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiInspectionEndDate.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiInspectionStartDate.Properties.VistaTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiInspectionStartDate.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TblInspectionTypesBindingSource1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsNewInspectionTypes,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.QTT_InspectionsDataSets1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TblInspectionTypesBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bstblCustomers,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsQTTLookupsData,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.QTT_LookupsDataSets,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsInspections,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsCustomerFilterList,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiGridControlInspections,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiGridViewInspections,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiQuestImage,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiInspectionTypes.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCustomers.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCopyDataCheckbox.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCopyCustomerCheckbox.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCopyPlantLocationCheckbox.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCopyUnitInfoCheckbox.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCopyToolsCheckbox.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCopyPrimaryInspectorCheckbox.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCopyOtherInspectorsCheckbox.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCopyDescriptionCheckbox.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DxValidationProvider,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiLocation.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiUnitName.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiUnitNumber.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'uiInspectionEndDate
        '
        Me.uiInspectionEndDate.EditValue = Nothing
        Me.uiInspectionEndDate.Location = New System.Drawing.Point(739, 97)
        Me.uiInspectionEndDate.Name = "uiInspectionEndDate"
        Me.uiInspectionEndDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionEndDate.Properties.Appearance.Options.UseFont = true
        SerializableAppearanceObject1.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject1.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject1.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject1.Options.UseBackColor = true
        SerializableAppearanceObject1.Options.UseBorderColor = true
        Me.uiInspectionEndDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "", Nothing, Nothing, true)})
        Me.uiInspectionEndDate.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiInspectionEndDate.Properties.DisplayFormat.FormatString = "MMMM d, yyyy"
        Me.uiInspectionEndDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.uiInspectionEndDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.uiInspectionEndDate.Size = New System.Drawing.Size(170, 24)
        Me.uiInspectionEndDate.TabIndex = 2
        CompareAgainstControlValidationRule1.CompareControlOperator = DevExpress.XtraEditors.DXErrorProvider.CompareControlOperator.GreaterOrEqual
        CompareAgainstControlValidationRule1.Control = Me.uiInspectionStartDate
        CompareAgainstControlValidationRule1.ErrorText = "End date must be equal to or greater than start date."
        Me.DxValidationProvider.SetValidationRule(Me.uiInspectionEndDate, CompareAgainstControlValidationRule1)
        '
        'uiInspectionStartDate
        '
        Me.uiInspectionStartDate.EditValue = Nothing
        Me.uiInspectionStartDate.Location = New System.Drawing.Point(478, 97)
        Me.uiInspectionStartDate.Name = "uiInspectionStartDate"
        Me.uiInspectionStartDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionStartDate.Properties.Appearance.Options.UseFont = true
        SerializableAppearanceObject2.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject2.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject2.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject2.Options.UseBackColor = true
        SerializableAppearanceObject2.Options.UseBorderColor = true
        Me.uiInspectionStartDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject2, "", Nothing, Nothing, true)})
        Me.uiInspectionStartDate.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiInspectionStartDate.Properties.DisplayFormat.FormatString = "MMMM d, yyyy"
        Me.uiInspectionStartDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.uiInspectionStartDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.uiInspectionStartDate.Size = New System.Drawing.Size(170, 24)
        Me.uiInspectionStartDate.TabIndex = 1
        CompareAgainstControlValidationRule2.CompareControlOperator = DevExpress.XtraEditors.DXErrorProvider.CompareControlOperator.LessOrEqual
        CompareAgainstControlValidationRule2.Control = Me.uiInspectionEndDate
        CompareAgainstControlValidationRule2.ErrorText = "Start Date must be equal to or less than the end date."
        Me.DxValidationProvider.SetValidationRule(Me.uiInspectionStartDate, CompareAgainstControlValidationRule2)
        '
        'uiInspectionTypeLabel
        '
        Me.uiInspectionTypeLabel.AutoSize = true
        Me.uiInspectionTypeLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionTypeLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiInspectionTypeLabel.Location = New System.Drawing.Point(17, 97)
        Me.uiInspectionTypeLabel.Name = "uiInspectionTypeLabel"
        Me.uiInspectionTypeLabel.Size = New System.Drawing.Size(102, 17)
        Me.uiInspectionTypeLabel.TabIndex = 1
        Me.uiInspectionTypeLabel.Text = "Inspection Type:"
        Me.uiInspectionTypeLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiCustomerLabel
        '
        Me.uiCustomerLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiCustomerLabel.AutoSize = true
        Me.uiCustomerLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomerLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCustomerLabel.Location = New System.Drawing.Point(55, 346)
        Me.uiCustomerLabel.Name = "uiCustomerLabel"
        Me.uiCustomerLabel.Size = New System.Drawing.Size(67, 17)
        Me.uiCustomerLabel.TabIndex = 3
        Me.uiCustomerLabel.Text = "Customer:"
        Me.uiCustomerLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TblInspectionTypesBindingSource1
        '
        Me.TblInspectionTypesBindingSource1.DataMember = "tblInspectionTypes"
        Me.TblInspectionTypesBindingSource1.DataSource = Me.bsNewInspectionTypes
        '
        'bsNewInspectionTypes
        '
        Me.bsNewInspectionTypes.DataSource = Me.QTT_InspectionsDataSets1
        Me.bsNewInspectionTypes.Position = 0
        '
        'QTT_InspectionsDataSets1
        '
        Me.QTT_InspectionsDataSets1.DataSetName = "QTT_InspectionsDataSets"
        Me.QTT_InspectionsDataSets1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblInspectionTypesBindingSource
        '
        Me.TblInspectionTypesBindingSource.DataMember = "tblInspectionTypes"
        Me.TblInspectionTypesBindingSource.DataSource = Me.bsNewInspectionTypes
        '
        'bstblCustomers
        '
        Me.bstblCustomers.DataMember = "tblCustomers"
        Me.bstblCustomers.DataSource = Me.QTT_InspectionsDataSets1
        '
        'uiUnitNameLabel
        '
        Me.uiUnitNameLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiUnitNameLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiUnitNameLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiUnitNameLabel.Location = New System.Drawing.Point(4, 406)
        Me.uiUnitNameLabel.Name = "uiUnitNameLabel"
        Me.uiUnitNameLabel.Size = New System.Drawing.Size(118, 17)
        Me.uiUnitNameLabel.TabIndex = 9
        Me.uiUnitNameLabel.Text = "Reformer Name:"
        Me.uiUnitNameLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiFormTitleLabel
        '
        Me.uiFormTitleLabel.Font = New System.Drawing.Font("Segoe UI", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic),System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiFormTitleLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiFormTitleLabel.Location = New System.Drawing.Point(9, 9)
        Me.uiFormTitleLabel.Name = "uiFormTitleLabel"
        Me.uiFormTitleLabel.Size = New System.Drawing.Size(391, 51)
        Me.uiFormTitleLabel.TabIndex = 0
        Me.uiFormTitleLabel.Text = "Create a New Inspection"
        '
        'bsQTTLookupsData
        '
        Me.bsQTTLookupsData.DataMember = "vwPriorInspections"
        Me.bsQTTLookupsData.DataSource = Me.QTT_LookupsDataSets
        '
        'QTT_LookupsDataSets
        '
        Me.QTT_LookupsDataSets.DataSetName = "QTT_LookupsData"
        Me.QTT_LookupsDataSets.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'uiUnitNumberLabel
        '
        Me.uiUnitNumberLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiUnitNumberLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiUnitNumberLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiUnitNumberLabel.Location = New System.Drawing.Point(39, 437)
        Me.uiUnitNumberLabel.Name = "uiUnitNumberLabel"
        Me.uiUnitNumberLabel.Size = New System.Drawing.Size(83, 17)
        Me.uiUnitNumberLabel.TabIndex = 11
        Me.uiUnitNumberLabel.Text = "Reformer ID:"
        Me.uiUnitNumberLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'taTblCustomers
        '
        Me.taTblCustomers.ClearBeforeFill = true
        '
        'taTblInspectionTypes
        '
        Me.taTblInspectionTypes.ClearBeforeFill = true
        '
        'uiPlantNameLabel
        '
        Me.uiPlantNameLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiPlantNameLabel.AutoSize = true
        Me.uiPlantNameLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPlantNameLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiPlantNameLabel.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiPlantNameLabel.Location = New System.Drawing.Point(44, 376)
        Me.uiPlantNameLabel.Name = "uiPlantNameLabel"
        Me.uiPlantNameLabel.Size = New System.Drawing.Size(78, 17)
        Me.uiPlantNameLabel.TabIndex = 7
        Me.uiPlantNameLabel.Text = "Plant Name:"
        Me.uiPlantNameLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiEndDateLabel
        '
        Me.uiEndDateLabel.AutoSize = true
        Me.uiEndDateLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiEndDateLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiEndDateLabel.Location = New System.Drawing.Point(669, 97)
        Me.uiEndDateLabel.Name = "uiEndDateLabel"
        Me.uiEndDateLabel.Size = New System.Drawing.Size(64, 17)
        Me.uiEndDateLabel.TabIndex = 15
        Me.uiEndDateLabel.Text = "End Date:"
        Me.uiEndDateLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiStartDateLabel
        '
        Me.uiStartDateLabel.AutoSize = true
        Me.uiStartDateLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiStartDateLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiStartDateLabel.Location = New System.Drawing.Point(404, 97)
        Me.uiStartDateLabel.Name = "uiStartDateLabel"
        Me.uiStartDateLabel.Size = New System.Drawing.Size(69, 17)
        Me.uiStartDateLabel.TabIndex = 13
        Me.uiStartDateLabel.Text = "Start Date:"
        Me.uiStartDateLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'taTblPipeTubeItems
        '
        Me.taTblPipeTubeItems.ClearBeforeFill = true
        '
        'taTblInspections
        '
        Me.taTblInspections.ClearBeforeFill = true
        '
        'taReportItems
        '
        Me.taReportItems.ClearBeforeFill = true
        '
        'bsInspections
        '
        Me.bsInspections.DataMember = "tblInspections"
        Me.bsInspections.DataSource = Me.QTT_InspectionsDataSets1
        '
        'bsCustomerFilterList
        '
        Me.bsCustomerFilterList.DataMember = "tblCustomers"
        Me.bsCustomerFilterList.DataSource = Me.QTT_InspectionsDataSets1
        '
        'toolTipNew
        '
        Me.toolTipNew.AutomaticDelay = 1000
        Me.toolTipNew.AutoPopDelay = 10000
        Me.toolTipNew.InitialDelay = 1000
        Me.toolTipNew.IsBalloon = true
        Me.toolTipNew.ReshowDelay = 1000
        Me.toolTipNew.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.toolTipNew.ToolTipTitle = "More Information"
        '
        'uiGridControlInspections
        '
        Me.uiGridControlInspections.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiGridControlInspections.DataSource = Me.bsInspections
        Me.uiGridControlInspections.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiGridControlInspections.Location = New System.Drawing.Point(16, 181)
        Me.uiGridControlInspections.LookAndFeel.SkinName = "Blue"
        Me.uiGridControlInspections.LookAndFeel.UseDefaultLookAndFeel = false
        Me.uiGridControlInspections.MainView = Me.uiGridViewInspections
        Me.uiGridControlInspections.Name = "uiGridControlInspections"
        Me.uiGridControlInspections.Size = New System.Drawing.Size(1197, 149)
        Me.uiGridControlInspections.TabIndex = 4
        Me.uiGridControlInspections.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uiGridViewInspections})
        '
        'uiGridViewInspections
        '
        Me.uiGridViewInspections.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewInspections.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewInspections.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewInspections.Appearance.ColumnFilterButton.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.ColumnFilterButton.Options.UseBorderColor = true
        Me.uiGridViewInspections.Appearance.ColumnFilterButton.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewInspections.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewInspections.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewInspections.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true
        Me.uiGridViewInspections.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewInspections.Appearance.Empty.BackColor2 = System.Drawing.Color.White
        Me.uiGridViewInspections.Appearance.Empty.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(229,Byte),Integer), CType(CType(234,Byte),Integer), CType(CType(237,Byte),Integer))
        Me.uiGridViewInspections.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.uiGridViewInspections.Appearance.EvenRow.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.EvenRow.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewInspections.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewInspections.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewInspections.Appearance.FilterCloseButton.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.FilterCloseButton.Options.UseBorderColor = true
        Me.uiGridViewInspections.Appearance.FilterCloseButton.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewInspections.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
        Me.uiGridViewInspections.Appearance.FilterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewInspections.Appearance.FilterPanel.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.FilterPanel.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167,Byte),Integer), CType(CType(178,Byte),Integer), CType(CType(185,Byte),Integer))
        Me.uiGridViewInspections.Appearance.FixedLine.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
        Me.uiGridViewInspections.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.uiGridViewInspections.Appearance.FocusedCell.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.FocusedCell.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewInspections.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.uiGridViewInspections.Appearance.FocusedRow.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.FocusedRow.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewInspections.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewInspections.Appearance.FooterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewInspections.Appearance.FooterPanel.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.FooterPanel.Options.UseBorderColor = true
        Me.uiGridViewInspections.Appearance.FooterPanel.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewInspections.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewInspections.Appearance.GroupButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewInspections.Appearance.GroupButton.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.GroupButton.Options.UseBorderColor = true
        Me.uiGridViewInspections.Appearance.GroupButton.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewInspections.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewInspections.Appearance.GroupFooter.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewInspections.Appearance.GroupFooter.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.GroupFooter.Options.UseBorderColor = true
        Me.uiGridViewInspections.Appearance.GroupFooter.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewInspections.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
        Me.uiGridViewInspections.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
        Me.uiGridViewInspections.Appearance.GroupPanel.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.GroupPanel.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewInspections.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewInspections.Appearance.GroupRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewInspections.Appearance.GroupRow.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.GroupRow.Options.UseBorderColor = true
        Me.uiGridViewInspections.Appearance.GroupRow.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewInspections.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewInspections.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewInspections.Appearance.HeaderPanel.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.HeaderPanel.Options.UseBorderColor = true
        Me.uiGridViewInspections.Appearance.HeaderPanel.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(128,Byte),Integer), CType(CType(153,Byte),Integer), CType(CType(195,Byte),Integer))
        Me.uiGridViewInspections.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewInspections.Appearance.HideSelectionRow.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.HideSelectionRow.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167,Byte),Integer), CType(CType(178,Byte),Integer), CType(CType(185,Byte),Integer))
        Me.uiGridViewInspections.Appearance.HorzLine.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(249,Byte),Integer), CType(CType(251,Byte),Integer), CType(CType(252,Byte),Integer))
        Me.uiGridViewInspections.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.uiGridViewInspections.Appearance.OddRow.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.OddRow.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(251,Byte),Integer), CType(CType(250,Byte),Integer), CType(CType(248,Byte),Integer))
        Me.uiGridViewInspections.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
        Me.uiGridViewInspections.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewInspections.Appearance.Preview.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.Preview.Options.UseFont = true
        Me.uiGridViewInspections.Appearance.Preview.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(249,Byte),Integer), CType(CType(251,Byte),Integer), CType(CType(252,Byte),Integer))
        Me.uiGridViewInspections.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.uiGridViewInspections.Appearance.Row.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.Row.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewInspections.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
        Me.uiGridViewInspections.Appearance.RowSeparator.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(107,Byte),Integer), CType(CType(133,Byte),Integer), CType(CType(179,Byte),Integer))
        Me.uiGridViewInspections.Appearance.SelectedRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewInspections.Appearance.SelectedRow.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.SelectedRow.Options.UseForeColor = true
        Me.uiGridViewInspections.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
        Me.uiGridViewInspections.Appearance.TopNewRow.Options.UseBackColor = true
        Me.uiGridViewInspections.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167,Byte),Integer), CType(CType(178,Byte),Integer), CType(CType(185,Byte),Integer))
        Me.uiGridViewInspections.Appearance.VertLine.Options.UseBackColor = true
        Me.uiGridViewInspections.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID, Me.colInspectionTypeDescription, Me.colCustomerName, Me.colInspectionDateStart, Me.colLocationDesc, Me.colCity, Me.colCountry, Me.colUnitName, Me.colUnitNumber, Me.colProjectNumber})
        Me.uiGridViewInspections.GridControl = Me.uiGridControlInspections
        Me.uiGridViewInspections.Name = "uiGridViewInspections"
        Me.uiGridViewInspections.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.uiGridViewInspections.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.uiGridViewInspections.OptionsBehavior.AutoSelectAllInEditor = false
        Me.uiGridViewInspections.OptionsBehavior.AutoUpdateTotalSummary = false
        Me.uiGridViewInspections.OptionsBehavior.Editable = false
        Me.uiGridViewInspections.OptionsBehavior.ReadOnly = true
        Me.uiGridViewInspections.OptionsCustomization.AllowColumnMoving = false
        Me.uiGridViewInspections.OptionsCustomization.AllowGroup = false
        Me.uiGridViewInspections.OptionsCustomization.AllowQuickHideColumns = false
        Me.uiGridViewInspections.OptionsDetail.AllowZoomDetail = false
        Me.uiGridViewInspections.OptionsDetail.EnableMasterViewMode = false
        Me.uiGridViewInspections.OptionsDetail.ShowDetailTabs = false
        Me.uiGridViewInspections.OptionsDetail.SmartDetailExpand = false
        Me.uiGridViewInspections.OptionsSelection.EnableAppearanceFocusedCell = false
        Me.uiGridViewInspections.OptionsView.EnableAppearanceEvenRow = true
        Me.uiGridViewInspections.OptionsView.EnableAppearanceOddRow = true
        Me.uiGridViewInspections.OptionsView.ShowGroupPanel = false
        '
        'colID
        '
        Me.colID.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colID.AppearanceCell.Options.UseFont = true
        Me.colID.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colID.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colID.AppearanceHeader.Options.UseFont = true
        Me.colID.AppearanceHeader.Options.UseForeColor = true
        Me.colID.Caption = "ID"
        Me.colID.Name = "colID"
        '
        'colInspectionTypeDescription
        '
        Me.colInspectionTypeDescription.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colInspectionTypeDescription.AppearanceCell.Options.UseFont = true
        Me.colInspectionTypeDescription.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colInspectionTypeDescription.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colInspectionTypeDescription.AppearanceHeader.Options.UseFont = true
        Me.colInspectionTypeDescription.AppearanceHeader.Options.UseForeColor = true
        Me.colInspectionTypeDescription.AppearanceHeader.Options.UseTextOptions = true
        Me.colInspectionTypeDescription.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colInspectionTypeDescription.Caption = "Inspection Type"
        Me.colInspectionTypeDescription.FieldName = "InspectionTypeDesciption"
        Me.colInspectionTypeDescription.Name = "colInspectionTypeDescription"
        Me.colInspectionTypeDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colInspectionTypeDescription.Visible = true
        Me.colInspectionTypeDescription.VisibleIndex = 0
        Me.colInspectionTypeDescription.Width = 109
        '
        'colCustomerName
        '
        Me.colCustomerName.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colCustomerName.AppearanceCell.Options.UseFont = true
        Me.colCustomerName.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colCustomerName.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colCustomerName.AppearanceHeader.Options.UseFont = true
        Me.colCustomerName.AppearanceHeader.Options.UseForeColor = true
        Me.colCustomerName.AppearanceHeader.Options.UseTextOptions = true
        Me.colCustomerName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colCustomerName.Caption = "Customer"
        Me.colCustomerName.FieldName = "CustomerName"
        Me.colCustomerName.Name = "colCustomerName"
        Me.colCustomerName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colCustomerName.Visible = true
        Me.colCustomerName.VisibleIndex = 1
        Me.colCustomerName.Width = 144
        '
        'colInspectionDateStart
        '
        Me.colInspectionDateStart.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colInspectionDateStart.AppearanceCell.Options.UseFont = true
        Me.colInspectionDateStart.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colInspectionDateStart.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colInspectionDateStart.AppearanceHeader.Options.UseFont = true
        Me.colInspectionDateStart.AppearanceHeader.Options.UseForeColor = true
        Me.colInspectionDateStart.AppearanceHeader.Options.UseTextOptions = true
        Me.colInspectionDateStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colInspectionDateStart.Caption = "Inspection Date"
        Me.colInspectionDateStart.FieldName = "InspectionDateStart"
        Me.colInspectionDateStart.Name = "colInspectionDateStart"
        Me.colInspectionDateStart.Visible = true
        Me.colInspectionDateStart.VisibleIndex = 2
        Me.colInspectionDateStart.Width = 115
        '
        'colLocationDesc
        '
        Me.colLocationDesc.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colLocationDesc.AppearanceCell.Options.UseFont = true
        Me.colLocationDesc.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colLocationDesc.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colLocationDesc.AppearanceHeader.Options.UseFont = true
        Me.colLocationDesc.AppearanceHeader.Options.UseForeColor = true
        Me.colLocationDesc.AppearanceHeader.Options.UseTextOptions = true
        Me.colLocationDesc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colLocationDesc.Caption = "Plant Name"
        Me.colLocationDesc.FieldName = "LocationDesc"
        Me.colLocationDesc.Name = "colLocationDesc"
        Me.colLocationDesc.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colLocationDesc.Visible = true
        Me.colLocationDesc.VisibleIndex = 3
        Me.colLocationDesc.Width = 109
        '
        'colCity
        '
        Me.colCity.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colCity.AppearanceCell.Options.UseFont = true
        Me.colCity.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colCity.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colCity.AppearanceHeader.Options.UseFont = true
        Me.colCity.AppearanceHeader.Options.UseForeColor = true
        Me.colCity.AppearanceHeader.Options.UseTextOptions = true
        Me.colCity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colCity.Caption = "City"
        Me.colCity.FieldName = "City"
        Me.colCity.Name = "colCity"
        Me.colCity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colCity.Visible = true
        Me.colCity.VisibleIndex = 4
        Me.colCity.Width = 87
        '
        'colCountry
        '
        Me.colCountry.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colCountry.AppearanceCell.Options.UseFont = true
        Me.colCountry.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colCountry.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colCountry.AppearanceHeader.Options.UseFont = true
        Me.colCountry.AppearanceHeader.Options.UseForeColor = true
        Me.colCountry.AppearanceHeader.Options.UseTextOptions = true
        Me.colCountry.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colCountry.Caption = "Country"
        Me.colCountry.FieldName = "Country"
        Me.colCountry.Name = "colCountry"
        Me.colCountry.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colCountry.Visible = true
        Me.colCountry.VisibleIndex = 5
        Me.colCountry.Width = 87
        '
        'colUnitName
        '
        Me.colUnitName.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colUnitName.AppearanceCell.Options.UseFont = true
        Me.colUnitName.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colUnitName.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colUnitName.AppearanceHeader.Options.UseFont = true
        Me.colUnitName.AppearanceHeader.Options.UseForeColor = true
        Me.colUnitName.AppearanceHeader.Options.UseTextOptions = true
        Me.colUnitName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colUnitName.Caption = "Reformer Name"
        Me.colUnitName.FieldName = "UnitName"
        Me.colUnitName.Name = "colUnitName"
        Me.colUnitName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colUnitName.Visible = true
        Me.colUnitName.VisibleIndex = 6
        Me.colUnitName.Width = 87
        '
        'colUnitNumber
        '
        Me.colUnitNumber.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colUnitNumber.AppearanceCell.Options.UseFont = true
        Me.colUnitNumber.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colUnitNumber.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colUnitNumber.AppearanceHeader.Options.UseFont = true
        Me.colUnitNumber.AppearanceHeader.Options.UseForeColor = true
        Me.colUnitNumber.AppearanceHeader.Options.UseTextOptions = true
        Me.colUnitNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colUnitNumber.Caption = "Reformer ID"
        Me.colUnitNumber.FieldName = "UnitNumber"
        Me.colUnitNumber.Name = "colUnitNumber"
        Me.colUnitNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colUnitNumber.Visible = true
        Me.colUnitNumber.VisibleIndex = 7
        Me.colUnitNumber.Width = 87
        '
        'colProjectNumber
        '
        Me.colProjectNumber.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colProjectNumber.AppearanceCell.Options.UseFont = true
        Me.colProjectNumber.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colProjectNumber.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colProjectNumber.AppearanceHeader.Options.UseFont = true
        Me.colProjectNumber.AppearanceHeader.Options.UseForeColor = true
        Me.colProjectNumber.AppearanceHeader.Options.UseTextOptions = true
        Me.colProjectNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colProjectNumber.Caption = "QIG Project #"
        Me.colProjectNumber.FieldName = "ProjectNumber"
        Me.colProjectNumber.Name = "colProjectNumber"
        Me.colProjectNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colProjectNumber.Visible = true
        Me.colProjectNumber.VisibleIndex = 8
        Me.colProjectNumber.Width = 117
        '
        'uiQuestImage
        '
        Me.uiQuestImage.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiQuestImage.ErrorImage = Nothing
        Me.uiQuestImage.Image = CType(resources.GetObject("uiQuestImage.Image"),System.Drawing.Image)
        Me.uiQuestImage.Location = New System.Drawing.Point(978, 5)
        Me.uiQuestImage.Name = "uiQuestImage"
        Me.uiQuestImage.Size = New System.Drawing.Size(241, 89)
        Me.uiQuestImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.uiQuestImage.TabIndex = 46
        Me.uiQuestImage.TabStop = false
        '
        'uiInspectionTypes
        '
        Me.uiInspectionTypes.EditValue = 4
        Me.uiInspectionTypes.Location = New System.Drawing.Point(125, 97)
        Me.uiInspectionTypes.Name = "uiInspectionTypes"
        Me.uiInspectionTypes.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionTypes.Properties.Appearance.Options.UseFont = true
        Me.uiInspectionTypes.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionTypes.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiInspectionTypes.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup
        SerializableAppearanceObject3.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject3.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject3.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject3.Options.UseBackColor = true
        SerializableAppearanceObject3.Options.UseBorderColor = true
        Me.uiInspectionTypes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject3, "", Nothing, Nothing, true)})
        Me.uiInspectionTypes.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiInspectionTypes.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 35, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 35, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ToolType", "Tool Type", 68, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("MultiplePass", "Multiple Pass", 87, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ReportTemplateFile", "Report Template File", 131, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("AppendixNumberingType", "Appendix Numbering Type", 167, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ActiveType", "Active Type", 76, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("HasUnitManufacturer", "Has Unit Manufacturer", 141, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)})
        Me.uiInspectionTypes.Properties.DataSource = Me.TblInspectionTypesBindingSource1
        Me.uiInspectionTypes.Properties.DisplayMember = "Description"
        Me.uiInspectionTypes.Properties.DropDownRows = 6
        Me.uiInspectionTypes.Properties.NullText = ""
        Me.uiInspectionTypes.Properties.PopupFormMinSize = New System.Drawing.Size(90, 0)
        Me.uiInspectionTypes.Properties.PopupSizeable = false
        Me.uiInspectionTypes.Properties.ShowFooter = false
        Me.uiInspectionTypes.Properties.ShowHeader = false
        Me.uiInspectionTypes.Properties.ValueMember = "ID"
        Me.uiInspectionTypes.Size = New System.Drawing.Size(258, 24)
        Me.uiInspectionTypes.TabIndex = 0
        '
        'uiCustomers
        '
        Me.uiCustomers.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiCustomers.Location = New System.Drawing.Point(125, 343)
        Me.uiCustomers.Name = "uiCustomers"
        Me.uiCustomers.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomers.Properties.Appearance.Options.UseFont = true
        Me.uiCustomers.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomers.Properties.AppearanceDropDown.Options.UseFont = true
        SerializableAppearanceObject4.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject4.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject4.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        SerializableAppearanceObject4.Options.UseBackColor = true
        SerializableAppearanceObject4.Options.UseBorderColor = true
        SerializableAppearanceObject4.Options.UseFont = true
        SerializableAppearanceObject5.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject5.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject5.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        SerializableAppearanceObject5.Options.UseBackColor = true
        SerializableAppearanceObject5.Options.UseBorderColor = true
        SerializableAppearanceObject5.Options.UseFont = true
        Me.uiCustomers.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject4, "", Nothing, Nothing, true), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Add", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject5, "Add a new customer", "ADD", Nothing, true)})
        Me.uiCustomers.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiCustomers.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 35, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 45, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Address", "Address", 58, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("City", "City", 31, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("State", "State", 39, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PostalCode", "Postal Code", 80, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Country", "Country", 55, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Contact", "Contact", 54, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Phone", "Phone", 46, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Fax", "Fax", 29, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Email", "Email", 41, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)})
        Me.uiCustomers.Properties.DataSource = Me.bstblCustomers
        Me.uiCustomers.Properties.DisplayMember = "Name"
        Me.uiCustomers.Properties.NullText = ""
        Me.uiCustomers.Properties.PopupFormMinSize = New System.Drawing.Size(120, 0)
        Me.uiCustomers.Properties.ShowFooter = false
        Me.uiCustomers.Properties.ShowHeader = false
        Me.uiCustomers.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.uiCustomers.Properties.ValueMember = "ID"
        Me.uiCustomers.Size = New System.Drawing.Size(298, 24)
        Me.uiCustomers.TabIndex = 5
        ConditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule1.ErrorText = "Customer cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiCustomers, ConditionValidationRule1)
        '
        'uiCopyDataCheckbox
        '
        Me.uiCopyDataCheckbox.Location = New System.Drawing.Point(123, 133)
        Me.uiCopyDataCheckbox.Name = "uiCopyDataCheckbox"
        Me.uiCopyDataCheckbox.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyDataCheckbox.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCopyDataCheckbox.Properties.Appearance.Options.UseFont = true
        Me.uiCopyDataCheckbox.Properties.Appearance.Options.UseForeColor = true
        Me.uiCopyDataCheckbox.Properties.Caption = "Copy data from a prior inspection"
        Me.uiCopyDataCheckbox.Size = New System.Drawing.Size(260, 22)
        Me.uiCopyDataCheckbox.TabIndex = 3
        '
        'uiCopyCustomerCheckbox
        '
        Me.uiCopyCustomerCheckbox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiCopyCustomerCheckbox.EditValue = true
        Me.uiCopyCustomerCheckbox.Enabled = false
        Me.uiCopyCustomerCheckbox.Location = New System.Drawing.Point(487, 371)
        Me.uiCopyCustomerCheckbox.Name = "uiCopyCustomerCheckbox"
        Me.uiCopyCustomerCheckbox.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyCustomerCheckbox.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCopyCustomerCheckbox.Properties.Appearance.Options.UseFont = true
        Me.uiCopyCustomerCheckbox.Properties.Appearance.Options.UseForeColor = true
        Me.uiCopyCustomerCheckbox.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyCustomerCheckbox.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.uiCopyCustomerCheckbox.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiCopyCustomerCheckbox.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.uiCopyCustomerCheckbox.Properties.Caption = "Customer Name"
        Me.uiCopyCustomerCheckbox.Size = New System.Drawing.Size(254, 22)
        Me.uiCopyCustomerCheckbox.TabIndex = 9
        '
        'uiCopyPlantLocationCheckbox
        '
        Me.uiCopyPlantLocationCheckbox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiCopyPlantLocationCheckbox.EditValue = true
        Me.uiCopyPlantLocationCheckbox.Enabled = false
        Me.uiCopyPlantLocationCheckbox.Location = New System.Drawing.Point(487, 395)
        Me.uiCopyPlantLocationCheckbox.Name = "uiCopyPlantLocationCheckbox"
        Me.uiCopyPlantLocationCheckbox.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyPlantLocationCheckbox.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCopyPlantLocationCheckbox.Properties.Appearance.Options.UseFont = true
        Me.uiCopyPlantLocationCheckbox.Properties.Appearance.Options.UseForeColor = true
        Me.uiCopyPlantLocationCheckbox.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyPlantLocationCheckbox.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.uiCopyPlantLocationCheckbox.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiCopyPlantLocationCheckbox.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.uiCopyPlantLocationCheckbox.Properties.Caption = "Plant Name and Location"
        Me.uiCopyPlantLocationCheckbox.Size = New System.Drawing.Size(254, 22)
        Me.uiCopyPlantLocationCheckbox.TabIndex = 10
        '
        'uiCopyUnitInfoCheckbox
        '
        Me.uiCopyUnitInfoCheckbox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiCopyUnitInfoCheckbox.Enabled = false
        Me.uiCopyUnitInfoCheckbox.Location = New System.Drawing.Point(487, 419)
        Me.uiCopyUnitInfoCheckbox.Name = "uiCopyUnitInfoCheckbox"
        Me.uiCopyUnitInfoCheckbox.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyUnitInfoCheckbox.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCopyUnitInfoCheckbox.Properties.Appearance.Options.UseFont = true
        Me.uiCopyUnitInfoCheckbox.Properties.Appearance.Options.UseForeColor = true
        Me.uiCopyUnitInfoCheckbox.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyUnitInfoCheckbox.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.uiCopyUnitInfoCheckbox.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiCopyUnitInfoCheckbox.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.uiCopyUnitInfoCheckbox.Properties.Caption = "Reformer and Tubing Details"
        Me.uiCopyUnitInfoCheckbox.Size = New System.Drawing.Size(254, 22)
        Me.uiCopyUnitInfoCheckbox.TabIndex = 11
        '
        'uiCopyToolsCheckbox
        '
        Me.uiCopyToolsCheckbox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiCopyToolsCheckbox.Enabled = false
        Me.uiCopyToolsCheckbox.Location = New System.Drawing.Point(487, 443)
        Me.uiCopyToolsCheckbox.Name = "uiCopyToolsCheckbox"
        Me.uiCopyToolsCheckbox.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyToolsCheckbox.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCopyToolsCheckbox.Properties.Appearance.Options.UseFont = true
        Me.uiCopyToolsCheckbox.Properties.Appearance.Options.UseForeColor = true
        Me.uiCopyToolsCheckbox.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyToolsCheckbox.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.uiCopyToolsCheckbox.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiCopyToolsCheckbox.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.uiCopyToolsCheckbox.Properties.Caption = "Inspection Tools Used"
        Me.uiCopyToolsCheckbox.Size = New System.Drawing.Size(254, 22)
        Me.uiCopyToolsCheckbox.TabIndex = 12
        '
        'uiCopyPrimaryInspectorCheckbox
        '
        Me.uiCopyPrimaryInspectorCheckbox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiCopyPrimaryInspectorCheckbox.Enabled = false
        Me.uiCopyPrimaryInspectorCheckbox.Location = New System.Drawing.Point(487, 467)
        Me.uiCopyPrimaryInspectorCheckbox.Name = "uiCopyPrimaryInspectorCheckbox"
        Me.uiCopyPrimaryInspectorCheckbox.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyPrimaryInspectorCheckbox.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCopyPrimaryInspectorCheckbox.Properties.Appearance.Options.UseFont = true
        Me.uiCopyPrimaryInspectorCheckbox.Properties.Appearance.Options.UseForeColor = true
        Me.uiCopyPrimaryInspectorCheckbox.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyPrimaryInspectorCheckbox.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.uiCopyPrimaryInspectorCheckbox.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiCopyPrimaryInspectorCheckbox.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.uiCopyPrimaryInspectorCheckbox.Properties.Caption = "Primary Inspector"
        Me.uiCopyPrimaryInspectorCheckbox.Size = New System.Drawing.Size(254, 22)
        Me.uiCopyPrimaryInspectorCheckbox.TabIndex = 13
        '
        'uiCopyOtherInspectorsCheckbox
        '
        Me.uiCopyOtherInspectorsCheckbox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiCopyOtherInspectorsCheckbox.Enabled = false
        Me.uiCopyOtherInspectorsCheckbox.Location = New System.Drawing.Point(487, 491)
        Me.uiCopyOtherInspectorsCheckbox.Name = "uiCopyOtherInspectorsCheckbox"
        Me.uiCopyOtherInspectorsCheckbox.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyOtherInspectorsCheckbox.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCopyOtherInspectorsCheckbox.Properties.Appearance.Options.UseFont = true
        Me.uiCopyOtherInspectorsCheckbox.Properties.Appearance.Options.UseForeColor = true
        Me.uiCopyOtherInspectorsCheckbox.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyOtherInspectorsCheckbox.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.uiCopyOtherInspectorsCheckbox.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiCopyOtherInspectorsCheckbox.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.uiCopyOtherInspectorsCheckbox.Properties.Caption = "Other Inspectors"
        Me.uiCopyOtherInspectorsCheckbox.Size = New System.Drawing.Size(254, 22)
        Me.uiCopyOtherInspectorsCheckbox.TabIndex = 14
        '
        'uiCopyDescriptionCheckbox
        '
        Me.uiCopyDescriptionCheckbox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiCopyDescriptionCheckbox.Enabled = false
        Me.uiCopyDescriptionCheckbox.Location = New System.Drawing.Point(487, 515)
        Me.uiCopyDescriptionCheckbox.Name = "uiCopyDescriptionCheckbox"
        Me.uiCopyDescriptionCheckbox.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyDescriptionCheckbox.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCopyDescriptionCheckbox.Properties.Appearance.Options.UseFont = true
        Me.uiCopyDescriptionCheckbox.Properties.Appearance.Options.UseForeColor = true
        Me.uiCopyDescriptionCheckbox.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyDescriptionCheckbox.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.uiCopyDescriptionCheckbox.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiCopyDescriptionCheckbox.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.uiCopyDescriptionCheckbox.Properties.Caption = "Project Description"
        Me.uiCopyDescriptionCheckbox.Size = New System.Drawing.Size(254, 22)
        Me.uiCopyDescriptionCheckbox.TabIndex = 15
        Me.uiCopyDescriptionCheckbox.Visible = false
        '
        'uiCopyOptionsLabel
        '
        Me.uiCopyOptionsLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiCopyOptionsLabel.Font = New System.Drawing.Font("Segoe UI", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCopyOptionsLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCopyOptionsLabel.Location = New System.Drawing.Point(460, 344)
        Me.uiCopyOptionsLabel.Name = "uiCopyOptionsLabel"
        Me.uiCopyOptionsLabel.Size = New System.Drawing.Size(281, 25)
        Me.uiCopyOptionsLabel.TabIndex = 26
        Me.uiCopyOptionsLabel.Text = "Copy Options for New Inspection"
        '
        'uiGridLabel
        '
        Me.uiGridLabel.AutoSize = true
        Me.uiGridLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiGridLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiGridLabel.Location = New System.Drawing.Point(16, 161)
        Me.uiGridLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.uiGridLabel.Name = "uiGridLabel"
        Me.uiGridLabel.Size = New System.Drawing.Size(217, 17)
        Me.uiGridLabel.TabIndex = 44
        Me.uiGridLabel.Text = "Use column headers to filter and sort."
        '
        'uiCancelButton
        '
        Me.uiCancelButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiCancelButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiCancelButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiCancelButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiCancelButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCancelButton.Appearance.Options.UseBackColor = true
        Me.uiCancelButton.Appearance.Options.UseBorderColor = true
        Me.uiCancelButton.Appearance.Options.UseFont = true
        Me.uiCancelButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiCancelButton.Location = New System.Drawing.Point(1015, 583)
        Me.uiCancelButton.Name = "uiCancelButton"
        Me.uiCancelButton.Size = New System.Drawing.Size(197, 32)
        Me.uiCancelButton.TabIndex = 19
        Me.uiCancelButton.Text = "&Cancel"
        '
        'uiCreateInspectionButton
        '
        Me.uiCreateInspectionButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiCreateInspectionButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiCreateInspectionButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiCreateInspectionButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiCreateInspectionButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCreateInspectionButton.Appearance.Options.UseBackColor = true
        Me.uiCreateInspectionButton.Appearance.Options.UseBorderColor = true
        Me.uiCreateInspectionButton.Appearance.Options.UseFont = true
        Me.uiCreateInspectionButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiCreateInspectionButton.Location = New System.Drawing.Point(1015, 541)
        Me.uiCreateInspectionButton.Name = "uiCreateInspectionButton"
        Me.uiCreateInspectionButton.Size = New System.Drawing.Size(197, 32)
        Me.uiCreateInspectionButton.TabIndex = 18
        Me.uiCreateInspectionButton.Text = "Create &New Inspection"
        '
        'uiViewInspectionButton
        '
        Me.uiViewInspectionButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiViewInspectionButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiViewInspectionButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiViewInspectionButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiViewInspectionButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiViewInspectionButton.Appearance.Options.UseBackColor = true
        Me.uiViewInspectionButton.Appearance.Options.UseBorderColor = true
        Me.uiViewInspectionButton.Appearance.Options.UseFont = true
        Me.uiViewInspectionButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiViewInspectionButton.Location = New System.Drawing.Point(1015, 135)
        Me.uiViewInspectionButton.Name = "uiViewInspectionButton"
        Me.uiViewInspectionButton.Size = New System.Drawing.Size(197, 32)
        Me.uiViewInspectionButton.TabIndex = 20
        Me.uiViewInspectionButton.Text = "&View Selected Inspection"
        '
        'DxValidationProvider
        '
        Me.DxValidationProvider.ValidationMode = DevExpress.XtraEditors.DXErrorProvider.ValidationMode.Manual
        '
        'uiLocation
        '
        Me.uiLocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiLocation.Location = New System.Drawing.Point(125, 373)
        Me.uiLocation.Name = "uiLocation"
        Me.uiLocation.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLocation.Properties.Appearance.Options.UseFont = true
        Me.uiLocation.Properties.MaxLength = 255
        Me.uiLocation.Size = New System.Drawing.Size(298, 24)
        Me.uiLocation.TabIndex = 6
        ConditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule2.ErrorText = "Plant Name cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiLocation, ConditionValidationRule2)
        '
        'uiUnitName
        '
        Me.uiUnitName.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiUnitName.Location = New System.Drawing.Point(125, 403)
        Me.uiUnitName.Name = "uiUnitName"
        Me.uiUnitName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiUnitName.Properties.Appearance.Options.UseFont = true
        Me.uiUnitName.Properties.Items.AddRange(New Object() {"Not Supplied"})
        Me.uiUnitName.Properties.MaxLength = 50
        Me.uiUnitName.Size = New System.Drawing.Size(298, 24)
        Me.uiUnitName.TabIndex = 7
        ConditionValidationRule3.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule3.ErrorText = "Reformer Name cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiUnitName, ConditionValidationRule3)
        '
        'uiUnitNumber
        '
        Me.uiUnitNumber.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiUnitNumber.Location = New System.Drawing.Point(125, 433)
        Me.uiUnitNumber.Name = "uiUnitNumber"
        Me.uiUnitNumber.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiUnitNumber.Properties.Appearance.Options.UseFont = true
        Me.uiUnitNumber.Properties.Items.AddRange(New Object() {"Not Supplied"})
        Me.uiUnitNumber.Properties.MaxLength = 15
        Me.uiUnitNumber.Size = New System.Drawing.Size(134, 24)
        Me.uiUnitNumber.TabIndex = 8
        ConditionValidationRule4.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule4.ErrorText = "Reformer ID cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiUnitNumber, ConditionValidationRule4)
        '
        'uiDetailsLine1
        '
        Me.uiDetailsLine1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiDetailsLine1.Appearance.Font = New System.Drawing.Font("Segoe UI", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDetailsLine1.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDetailsLine1.Location = New System.Drawing.Point(761, 344)
        Me.uiDetailsLine1.Name = "uiDetailsLine1"
        Me.uiDetailsLine1.Size = New System.Drawing.Size(209, 21)
        Me.uiDetailsLine1.TabIndex = 58
        Me.uiDetailsLine1.Text = "New Inspection Parameters"
        '
        'uiDetailsPlantName
        '
        Me.uiDetailsPlantName.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiDetailsPlantName.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDetailsPlantName.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDetailsPlantName.Location = New System.Drawing.Point(779, 407)
        Me.uiDetailsPlantName.Name = "uiDetailsPlantName"
        Me.uiDetailsPlantName.Size = New System.Drawing.Size(61, 13)
        Me.uiDetailsPlantName.TabIndex = 60
        Me.uiDetailsPlantName.Text = "Plant Name:"
        '
        'uiDetailsHeaterName
        '
        Me.uiDetailsHeaterName.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiDetailsHeaterName.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDetailsHeaterName.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDetailsHeaterName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.uiDetailsHeaterName.Location = New System.Drawing.Point(779, 426)
        Me.uiDetailsHeaterName.Name = "uiDetailsHeaterName"
        Me.uiDetailsHeaterName.Size = New System.Drawing.Size(83, 13)
        Me.uiDetailsHeaterName.TabIndex = 61
        Me.uiDetailsHeaterName.Text = "Reformer Name:"
        '
        'uiDetailsHeaterID
        '
        Me.uiDetailsHeaterID.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiDetailsHeaterID.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDetailsHeaterID.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDetailsHeaterID.Location = New System.Drawing.Point(779, 445)
        Me.uiDetailsHeaterID.Name = "uiDetailsHeaterID"
        Me.uiDetailsHeaterID.Size = New System.Drawing.Size(64, 13)
        Me.uiDetailsHeaterID.TabIndex = 62
        Me.uiDetailsHeaterID.Text = "Reformer ID:"
        '
        'uiDetailsInspectionDate
        '
        Me.uiDetailsInspectionDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiDetailsInspectionDate.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDetailsInspectionDate.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDetailsInspectionDate.Location = New System.Drawing.Point(779, 464)
        Me.uiDetailsInspectionDate.Name = "uiDetailsInspectionDate"
        Me.uiDetailsInspectionDate.Size = New System.Drawing.Size(95, 13)
        Me.uiDetailsInspectionDate.TabIndex = 63
        Me.uiDetailsInspectionDate.Text = "Inspection Date(s):"
        '
        'uiDetailsFolder
        '
        Me.uiDetailsFolder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiDetailsFolder.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDetailsFolder.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDetailsFolder.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
        Me.uiDetailsFolder.Location = New System.Drawing.Point(779, 483)
        Me.uiDetailsFolder.Name = "uiDetailsFolder"
        Me.uiDetailsFolder.Size = New System.Drawing.Size(36, 13)
        Me.uiDetailsFolder.TabIndex = 64
        Me.uiDetailsFolder.Text = "Folder:"
        '
        'uiDataPlantName
        '
        Me.uiDataPlantName.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiDataPlantName.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDataPlantName.Appearance.ForeColor = System.Drawing.Color.Crimson
        Me.uiDataPlantName.Location = New System.Drawing.Point(846, 407)
        Me.uiDataPlantName.Name = "uiDataPlantName"
        Me.uiDataPlantName.Size = New System.Drawing.Size(6, 13)
        Me.uiDataPlantName.TabIndex = 66
        Me.uiDataPlantName.Text = "  "
        '
        'uiDataHeaterName
        '
        Me.uiDataHeaterName.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiDataHeaterName.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDataHeaterName.Appearance.ForeColor = System.Drawing.Color.Crimson
        Me.uiDataHeaterName.Location = New System.Drawing.Point(868, 426)
        Me.uiDataHeaterName.Name = "uiDataHeaterName"
        Me.uiDataHeaterName.Size = New System.Drawing.Size(6, 13)
        Me.uiDataHeaterName.TabIndex = 67
        Me.uiDataHeaterName.Text = "  "
        '
        'uiDataHeaterID
        '
        Me.uiDataHeaterID.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiDataHeaterID.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDataHeaterID.Appearance.ForeColor = System.Drawing.Color.Crimson
        Me.uiDataHeaterID.Location = New System.Drawing.Point(849, 445)
        Me.uiDataHeaterID.Name = "uiDataHeaterID"
        Me.uiDataHeaterID.Size = New System.Drawing.Size(6, 13)
        Me.uiDataHeaterID.TabIndex = 68
        Me.uiDataHeaterID.Text = "  "
        '
        'uiDataInspectionDate
        '
        Me.uiDataInspectionDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiDataInspectionDate.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDataInspectionDate.Appearance.ForeColor = System.Drawing.Color.Crimson
        Me.uiDataInspectionDate.Location = New System.Drawing.Point(880, 464)
        Me.uiDataInspectionDate.Name = "uiDataInspectionDate"
        Me.uiDataInspectionDate.Size = New System.Drawing.Size(6, 13)
        Me.uiDataInspectionDate.TabIndex = 69
        Me.uiDataInspectionDate.Text = "  "
        '
        'uiDataFolder
        '
        Me.uiDataFolder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiDataFolder.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDataFolder.Appearance.ForeColor = System.Drawing.Color.Crimson
        Me.uiDataFolder.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.uiDataFolder.Location = New System.Drawing.Point(820, 483)
        Me.uiDataFolder.Name = "uiDataFolder"
        Me.uiDataFolder.Size = New System.Drawing.Size(392, 13)
        Me.uiDataFolder.TabIndex = 70
        Me.uiDataFolder.Text = "  "
        '
        'uiDataInspectionType
        '
        Me.uiDataInspectionType.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiDataInspectionType.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDataInspectionType.Appearance.ForeColor = System.Drawing.Color.Crimson
        Me.uiDataInspectionType.Location = New System.Drawing.Point(868, 371)
        Me.uiDataInspectionType.Name = "uiDataInspectionType"
        Me.uiDataInspectionType.Size = New System.Drawing.Size(6, 13)
        Me.uiDataInspectionType.TabIndex = 73
        Me.uiDataInspectionType.Text = "  "
        '
        'uiDetailsInspectionType
        '
        Me.uiDetailsInspectionType.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiDetailsInspectionType.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDetailsInspectionType.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDetailsInspectionType.Location = New System.Drawing.Point(779, 371)
        Me.uiDetailsInspectionType.Name = "uiDetailsInspectionType"
        Me.uiDetailsInspectionType.Size = New System.Drawing.Size(83, 13)
        Me.uiDetailsInspectionType.TabIndex = 72
        Me.uiDetailsInspectionType.Text = "Inspection Type:"
        '
        'uiDataCustomers
        '
        Me.uiDataCustomers.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiDataCustomers.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDataCustomers.Appearance.ForeColor = System.Drawing.Color.Crimson
        Me.uiDataCustomers.Location = New System.Drawing.Point(836, 389)
        Me.uiDataCustomers.Name = "uiDataCustomers"
        Me.uiDataCustomers.Size = New System.Drawing.Size(6, 13)
        Me.uiDataCustomers.TabIndex = 75
        Me.uiDataCustomers.Text = "  "
        '
        'uiDetailsCustomer
        '
        Me.uiDetailsCustomer.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiDetailsCustomer.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDetailsCustomer.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDetailsCustomer.Location = New System.Drawing.Point(779, 389)
        Me.uiDetailsCustomer.Name = "uiDetailsCustomer"
        Me.uiDetailsCustomer.Size = New System.Drawing.Size(52, 13)
        Me.uiDetailsCustomer.TabIndex = 74
        Me.uiDetailsCustomer.Text = "Customer:"
        '
        'taTblReformerAgedSummary
        '
        Me.taTblReformerAgedSummary.ClearBeforeFill = true
        '
        'taTblReformerNewSummary
        '
        Me.taTblReformerNewSummary.ClearBeforeFill = true
        '
        'NewInspection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96!, 96!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1230, 629)
        Me.Controls.Add(Me.uiDataCustomers)
        Me.Controls.Add(Me.uiDetailsCustomer)
        Me.Controls.Add(Me.uiDataInspectionType)
        Me.Controls.Add(Me.uiDetailsInspectionType)
        Me.Controls.Add(Me.uiDataFolder)
        Me.Controls.Add(Me.uiDataInspectionDate)
        Me.Controls.Add(Me.uiDataHeaterID)
        Me.Controls.Add(Me.uiDataHeaterName)
        Me.Controls.Add(Me.uiDataPlantName)
        Me.Controls.Add(Me.uiDetailsFolder)
        Me.Controls.Add(Me.uiDetailsInspectionDate)
        Me.Controls.Add(Me.uiDetailsHeaterID)
        Me.Controls.Add(Me.uiDetailsHeaterName)
        Me.Controls.Add(Me.uiDetailsPlantName)
        Me.Controls.Add(Me.uiDetailsLine1)
        Me.Controls.Add(Me.uiLocation)
        Me.Controls.Add(Me.uiViewInspectionButton)
        Me.Controls.Add(Me.uiCreateInspectionButton)
        Me.Controls.Add(Me.uiCancelButton)
        Me.Controls.Add(Me.uiCopyDescriptionCheckbox)
        Me.Controls.Add(Me.uiCopyOtherInspectorsCheckbox)
        Me.Controls.Add(Me.uiCopyPrimaryInspectorCheckbox)
        Me.Controls.Add(Me.uiCopyToolsCheckbox)
        Me.Controls.Add(Me.uiCopyUnitInfoCheckbox)
        Me.Controls.Add(Me.uiCopyPlantLocationCheckbox)
        Me.Controls.Add(Me.uiCopyCustomerCheckbox)
        Me.Controls.Add(Me.uiCopyDataCheckbox)
        Me.Controls.Add(Me.uiCustomers)
        Me.Controls.Add(Me.uiInspectionTypes)
        Me.Controls.Add(Me.uiInspectionEndDate)
        Me.Controls.Add(Me.uiInspectionStartDate)
        Me.Controls.Add(Me.uiQuestImage)
        Me.Controls.Add(Me.uiGridControlInspections)
        Me.Controls.Add(Me.uiGridLabel)
        Me.Controls.Add(Me.uiEndDateLabel)
        Me.Controls.Add(Me.uiStartDateLabel)
        Me.Controls.Add(Me.uiPlantNameLabel)
        Me.Controls.Add(Me.uiUnitNumberLabel)
        Me.Controls.Add(Me.uiFormTitleLabel)
        Me.Controls.Add(Me.uiCopyOptionsLabel)
        Me.Controls.Add(Me.uiInspectionTypeLabel)
        Me.Controls.Add(Me.uiUnitNameLabel)
        Me.Controls.Add(Me.uiCustomerLabel)
        Me.Controls.Add(Me.uiUnitName)
        Me.Controls.Add(Me.uiUnitNumber)
        Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(1000, 563)
        Me.Name = "NewInspection"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "New Inspection"
        CType(Me.uiInspectionEndDate.Properties.VistaTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiInspectionEndDate.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiInspectionStartDate.Properties.VistaTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiInspectionStartDate.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TblInspectionTypesBindingSource1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsNewInspectionTypes,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.QTT_InspectionsDataSets1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TblInspectionTypesBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bstblCustomers,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsQTTLookupsData,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.QTT_LookupsDataSets,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsInspections,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsCustomerFilterList,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiGridControlInspections,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiGridViewInspections,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiQuestImage,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiInspectionTypes.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCustomers.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCopyDataCheckbox.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCopyCustomerCheckbox.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCopyPlantLocationCheckbox.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCopyUnitInfoCheckbox.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCopyToolsCheckbox.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCopyPrimaryInspectorCheckbox.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCopyOtherInspectorsCheckbox.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCopyDescriptionCheckbox.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DxValidationProvider,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiLocation.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiUnitName.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiUnitNumber.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
   Friend WithEvents uiInspectionTypeLabel As System.Windows.Forms.Label
   Friend WithEvents uiCustomerLabel As System.Windows.Forms.Label
   Friend WithEvents uiUnitNameLabel As System.Windows.Forms.Label
   Friend WithEvents uiFormTitleLabel As System.Windows.Forms.Label
   Friend WithEvents bsInspectionTypes As System.Windows.Forms.BindingSource
   Friend WithEvents QTT_InspectionsDataSets As AutoReporter.QTT_InspectionsDataSets
   Friend WithEvents bsCustomers As System.Windows.Forms.BindingSource
   Friend WithEvents bstblCustomers As System.Windows.Forms.BindingSource
   Friend WithEvents bsQTTLookupsData As System.Windows.Forms.BindingSource
   Friend WithEvents QTT_LookupsDataSets As AutoReporter.QTT_LookupsData
   Friend WithEvents uiUnitNumberLabel As System.Windows.Forms.Label
    Friend WithEvents taTblInspectionTypes As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionTypesTableAdapter
   Friend WithEvents taTblCustomers As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblCustomersTableAdapter
   Friend WithEvents bsNewInspectionTypes As System.Windows.Forms.BindingSource
   Friend WithEvents uiPlantNameLabel As System.Windows.Forms.Label
   Friend WithEvents uiEndDateLabel As System.Windows.Forms.Label
   Friend WithEvents uiStartDateLabel As System.Windows.Forms.Label
   Friend WithEvents TblInspectionTypesBindingSource As System.Windows.Forms.BindingSource
   Friend WithEvents QTT_InspectionsDataSets1 As AutoReporter.QTT_InspectionsDataSets
   Friend WithEvents taTblPipeTubeItems As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblPipeTubeItemsTableAdapter
   Friend WithEvents taTblInspections As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionsTableAdapter
   Friend WithEvents taAppendixes As AutoReporter.QTT_InspectionsDataSetsTableAdapters.AppendixesTableAdapter
   Friend WithEvents taReportItems As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblReportItemsTableAdapter
   Friend WithEvents TblInspectionTypesBindingSource1 As System.Windows.Forms.BindingSource
   Friend WithEvents bsInspections As System.Windows.Forms.BindingSource
   Friend WithEvents bsCustomerFilterList As System.Windows.Forms.BindingSource
   Friend WithEvents toolTipNew As System.Windows.Forms.ToolTip
   Friend WithEvents uiGridControlInspections As DevExpress.XtraGrid.GridControl
   Friend WithEvents uiGridViewInspections As DevExpress.XtraGrid.Views.Grid.GridView
   Friend WithEvents colInspectionTypeDescription As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colCustomerName As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colInspectionDateStart As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colLocationDesc As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colCity As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colCountry As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colUnitName As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colUnitNumber As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colProjectNumber As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents uiQuestImage As System.Windows.Forms.PictureBox
    Friend WithEvents uiInspectionStartDate As DevExpress.XtraEditors.DateEdit
   Friend WithEvents uiInspectionEndDate As DevExpress.XtraEditors.DateEdit
   Friend WithEvents uiInspectionTypes As DevExpress.XtraEditors.LookUpEdit
   Friend WithEvents uiCustomers As DevExpress.XtraEditors.LookUpEdit
   Friend WithEvents uiCopyDataCheckbox As DevExpress.XtraEditors.CheckEdit
   Friend WithEvents uiCopyCustomerCheckbox As DevExpress.XtraEditors.CheckEdit
   Friend WithEvents uiCopyPlantLocationCheckbox As DevExpress.XtraEditors.CheckEdit
   Friend WithEvents uiCopyUnitInfoCheckbox As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uiCopyToolsCheckbox As DevExpress.XtraEditors.CheckEdit
   Friend WithEvents uiCopyPrimaryInspectorCheckbox As DevExpress.XtraEditors.CheckEdit
   Friend WithEvents uiCopyOtherInspectorsCheckbox As DevExpress.XtraEditors.CheckEdit
   Friend WithEvents uiCopyDescriptionCheckbox As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uiCopyOptionsLabel As System.Windows.Forms.Label
    Friend WithEvents uiGridLabel As System.Windows.Forms.Label
    Friend WithEvents uiCancelButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiCreateInspectionButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiViewInspectionButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DxValidationProvider As DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider
    Friend WithEvents uiLocation As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiDetailsLine1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uiDetailsPlantName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uiDetailsHeaterName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uiDetailsHeaterID As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uiDetailsInspectionDate As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uiDetailsFolder As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uiDataPlantName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uiDataHeaterName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uiDataHeaterID As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uiDataInspectionDate As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uiDataFolder As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uiDataInspectionType As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uiDetailsInspectionType As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uiDataCustomers As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uiDetailsCustomer As DevExpress.XtraEditors.LabelControl
    Friend WithEvents taTblReformerAgedSummary As QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblReformerAgedSummaryTableAdapter
    Friend WithEvents taTblReformerNewSummary As QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblReformerNewSummaryTableAdapter
    Friend WithEvents uiUnitName As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiUnitNumber As DevExpress.XtraEditors.ComboBoxEdit
End Class
