﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DataEditor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DataEditor))
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Me.LayoutViewCard1 = New DevExpress.XtraGrid.Views.Layout.LayoutViewCard()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.bsTables = New System.Windows.Forms.BindingSource(Me.components)
        Me.dsQTT_DatabaseEditor = New QIG.AutoReporter.QTT_DatabaseEditor()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFK_InspectionType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colListSortOrder = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDescription_Default = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIsPipeType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFileIDString = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHasSummary_Default = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHasAppendix_Default = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHasMaterial = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHasOrientation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHasPipeSchedule = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHasPressure = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHasTargetTemp = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHasFlowDirection = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHasAgeAtInspection = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHasInspectionLocations = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHasNumberThermalCycles = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHasOperatingHours = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepItemOffices = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.bsTblQTTOffices = New System.Windows.Forms.BindingSource(Me.components)
        Me.RepItemRoles = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.bsTblQTTRoles = New System.Windows.Forms.BindingSource(Me.components)
        Me.RepItemInspectionTypes = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.bsTblInspectionTypes = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.uiCloseButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiTableNames = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.tatblQTT_Offices = New QIG.AutoReporter.QTT_DatabaseEditorTableAdapters.Lookup_tblQTT_OfficesTableAdapter()
        Me.taRoles = New QIG.AutoReporter.QTT_DatabaseEditorTableAdapters.Lookup_RolesTableAdapter()
        Me.taInspectors = New QIG.AutoReporter.QTT_DatabaseEditorTableAdapters.Lookup_InspectorsTableAdapter()
        Me.tatblInspectionTools = New QIG.AutoReporter.QTT_DatabaseEditorTableAdapters.Lookup_tblInspectionToolsTableAdapter()
        Me.taTblDefaultTextEntries = New QIG.AutoReporter.QTT_DatabaseEditorTableAdapters.tblDefaultTextEntriesTableAdapter()
        Me.taTblDefaultText_ListItems = New QIG.AutoReporter.QTT_DatabaseEditorTableAdapters.tblDefaultText_ListItemsTableAdapter()
        Me.taTblInspectionTypes = New QIG.AutoReporter.QTT_DatabaseEditorTableAdapters.tblInspectionTypesTableAdapter()
        CType(Me.LayoutViewCard1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsTables, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dsQTT_DatabaseEditor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepItemOffices, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsTblQTTOffices, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepItemRoles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsTblQTTRoles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepItemInspectionTypes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsTblInspectionTypes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiTableNames.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutViewCard1
        '
        Me.LayoutViewCard1.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText
        Me.LayoutViewCard1.Name = "layoutViewTemplateCard"
        Me.LayoutViewCard1.OptionsItemText.TextToControlDistance = 5
        '
        'Label13
        '
        Me.Label13.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Red
        Me.Label13.Location = New System.Drawing.Point(6, 50)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(904, 79)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = resources.GetString("Label13.Text")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.Location = New System.Drawing.Point(6, 9)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(790, 32)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Quest Integrity Group Inspection Reporting System - Database Editor"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.Label2.Location = New System.Drawing.Point(6, 140)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(131, 17)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Select a table to edit:"
        '
        'GridView4
        '
        Me.GridView4.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView4.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView4.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView4.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Gray
        Me.GridView4.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.GridView4.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.GridView4.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.GridView4.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView4.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.GridView4.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView4.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.GridView4.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.GridView4.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.GridView4.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.GridView4.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView4.Appearance.Empty.Options.UseBackColor = True
        Me.GridView4.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView4.Appearance.EvenRow.BackColor2 = System.Drawing.Color.GhostWhite
        Me.GridView4.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.GridView4.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.GridView4.Appearance.EvenRow.Options.UseBackColor = True
        Me.GridView4.Appearance.EvenRow.Options.UseForeColor = True
        Me.GridView4.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView4.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(125, Byte), Integer))
        Me.GridView4.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView4.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black
        Me.GridView4.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.GridView4.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.GridView4.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.GridView4.Appearance.FilterCloseButton.Options.UseForeColor = True
        Me.GridView4.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.GridView4.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView4.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Red
        Me.GridView4.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.GridView4.Appearance.FilterPanel.Options.UseBackColor = True
        Me.GridView4.Appearance.FilterPanel.Options.UseForeColor = True
        Me.GridView4.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(81, Byte), Integer))
        Me.GridView4.Appearance.FixedLine.Options.UseBackColor = True
        Me.GridView4.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(151, Byte), Integer))
        Me.GridView4.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(135, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(201, Byte), Integer))
        Me.GridView4.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.GridView4.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView4.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView4.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView4.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView4.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
        Me.GridView4.Appearance.FooterPanel.Options.UseBackColor = True
        Me.GridView4.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.GridView4.Appearance.FooterPanel.Options.UseForeColor = True
        Me.GridView4.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView4.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView4.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black
        Me.GridView4.Appearance.GroupButton.Options.UseBackColor = True
        Me.GridView4.Appearance.GroupButton.Options.UseBorderColor = True
        Me.GridView4.Appearance.GroupButton.Options.UseForeColor = True
        Me.GridView4.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.GridView4.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.GridView4.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
        Me.GridView4.Appearance.GroupFooter.Options.UseBackColor = True
        Me.GridView4.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.GridView4.Appearance.GroupFooter.Options.UseForeColor = True
        Me.GridView4.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(65, Byte), Integer), CType(CType(65, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.GridView4.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
        Me.GridView4.Appearance.GroupPanel.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.GridView4.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White
        Me.GridView4.Appearance.GroupPanel.Options.UseBackColor = True
        Me.GridView4.Appearance.GroupPanel.Options.UseFont = True
        Me.GridView4.Appearance.GroupPanel.Options.UseForeColor = True
        Me.GridView4.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(151, Byte), Integer))
        Me.GridView4.Appearance.GroupRow.ForeColor = System.Drawing.Color.Silver
        Me.GridView4.Appearance.GroupRow.Options.UseBackColor = True
        Me.GridView4.Appearance.GroupRow.Options.UseForeColor = True
        Me.GridView4.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView4.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView4.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.GridView4.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
        Me.GridView4.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.GridView4.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.GridView4.Appearance.HeaderPanel.Options.UseFont = True
        Me.GridView4.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.GridView4.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gray
        Me.GridView4.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView4.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView4.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView4.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView4.Appearance.HorzLine.Options.UseBackColor = True
        Me.GridView4.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(206, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.GridView4.Appearance.OddRow.BackColor2 = System.Drawing.Color.White
        Me.GridView4.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.GridView4.Appearance.OddRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal
        Me.GridView4.Appearance.OddRow.Options.UseBackColor = True
        Me.GridView4.Appearance.OddRow.Options.UseForeColor = True
        Me.GridView4.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(217, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GridView4.Appearance.Preview.BackColor2 = System.Drawing.Color.White
        Me.GridView4.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(151, Byte), Integer))
        Me.GridView4.Appearance.Preview.Options.UseBackColor = True
        Me.GridView4.Appearance.Preview.Options.UseForeColor = True
        Me.GridView4.Appearance.Row.BackColor = System.Drawing.Color.White
        Me.GridView4.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.GridView4.Appearance.Row.Options.UseBackColor = True
        Me.GridView4.Appearance.Row.Options.UseForeColor = True
        Me.GridView4.Appearance.RowSeparator.BackColor = System.Drawing.Color.White
        Me.GridView4.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView4.Appearance.RowSeparator.Options.UseBackColor = True
        Me.GridView4.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.GridView4.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
        Me.GridView4.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView4.Appearance.SelectedRow.Options.UseForeColor = True
        Me.GridView4.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView4.Appearance.VertLine.Options.UseBackColor = True
        Me.GridView4.GridControl = Me.GridControl1
        Me.GridView4.Name = "GridView4"
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.DataSource = Me.bsTables
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridControl1.Location = New System.Drawing.Point(6, 170)
        Me.GridControl1.LookAndFeel.SkinName = "Blue"
        Me.GridControl1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepItemOffices, Me.RepItemRoles, Me.RepItemInspectionTypes})
        Me.GridControl1.ShowOnlyPredefinedDetails = True
        Me.GridControl1.Size = New System.Drawing.Size(901, 391)
        Me.GridControl1.TabIndex = 16
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1, Me.GridView2, Me.GridView3, Me.GridView4})
        '
        'bsTables
        '
        Me.bsTables.DataMember = "tblPipeTubeItemTypes"
        Me.bsTables.DataSource = Me.dsQTT_DatabaseEditor
        '
        'dsQTT_DatabaseEditor
        '
        Me.dsQTT_DatabaseEditor.DataSetName = "QTT_DatabaseEditor"
        Me.dsQTT_DatabaseEditor.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
        Me.GridView1.Appearance.Empty.Options.UseBackColor = True
        Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
        Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
        Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
        Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
        Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
        Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
        Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
        Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
        Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
        Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
        Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.GridView1.Appearance.GroupButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
        Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
        Me.GridView1.Appearance.GroupButton.Options.UseForeColor = True
        Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
        Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
        Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
        Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
        Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
        Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
        Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
        Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
        Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(195, Byte), Integer))
        Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
        Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
        Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
        Me.GridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
        Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.GridView1.Appearance.Preview.Options.UseBackColor = True
        Me.GridView1.Appearance.Preview.Options.UseFont = True
        Me.GridView1.Appearance.Preview.Options.UseForeColor = True
        Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.Row.Options.UseBackColor = True
        Me.GridView1.Appearance.Row.Options.UseForeColor = True
        Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
        Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
        Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(179, Byte), Integer))
        Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
        Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
        Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
        Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID, Me.colFK_InspectionType, Me.colListSortOrder, Me.colDescription_Default, Me.colIsPipeType, Me.colFileIDString, Me.colHasSummary_Default, Me.colHasAppendix_Default, Me.colHasMaterial, Me.colHasOrientation, Me.colHasPipeSchedule, Me.colHasPressure, Me.colHasTargetTemp, Me.colHasFlowDirection, Me.colHasAgeAtInspection, Me.colHasInspectionLocations, Me.colHasNumberThermalCycles, Me.colHasOperatingHours})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsCustomization.AllowColumnMoving = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsCustomization.AllowQuickHideColumns = False
        Me.GridView1.OptionsCustomization.AllowRowSizing = True
        Me.GridView1.OptionsMenu.EnableColumnMenu = False
        Me.GridView1.OptionsMenu.EnableFooterMenu = False
        Me.GridView1.OptionsMenu.EnableGroupPanelMenu = False
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
        Me.GridView1.OptionsView.EnableAppearanceOddRow = True
        Me.GridView1.OptionsView.ShowDetailButtons = False
        Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colID
        '
        Me.colID.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colID.AppearanceCell.Options.UseFont = True
        Me.colID.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colID.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colID.AppearanceHeader.Options.UseFont = True
        Me.colID.AppearanceHeader.Options.UseForeColor = True
        Me.colID.FieldName = "ID"
        Me.colID.Name = "colID"
        Me.colID.OptionsColumn.ReadOnly = True
        Me.colID.Visible = True
        Me.colID.VisibleIndex = 0
        '
        'colFK_InspectionType
        '
        Me.colFK_InspectionType.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colFK_InspectionType.AppearanceCell.Options.UseFont = True
        Me.colFK_InspectionType.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colFK_InspectionType.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colFK_InspectionType.AppearanceHeader.Options.UseFont = True
        Me.colFK_InspectionType.AppearanceHeader.Options.UseForeColor = True
        Me.colFK_InspectionType.FieldName = "FK_InspectionType"
        Me.colFK_InspectionType.Name = "colFK_InspectionType"
        Me.colFK_InspectionType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colFK_InspectionType.Visible = True
        Me.colFK_InspectionType.VisibleIndex = 1
        '
        'colListSortOrder
        '
        Me.colListSortOrder.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colListSortOrder.AppearanceCell.Options.UseFont = True
        Me.colListSortOrder.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colListSortOrder.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colListSortOrder.AppearanceHeader.Options.UseFont = True
        Me.colListSortOrder.AppearanceHeader.Options.UseForeColor = True
        Me.colListSortOrder.FieldName = "ListSortOrder"
        Me.colListSortOrder.Name = "colListSortOrder"
        Me.colListSortOrder.Visible = True
        Me.colListSortOrder.VisibleIndex = 2
        '
        'colDescription_Default
        '
        Me.colDescription_Default.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDescription_Default.AppearanceCell.Options.UseFont = True
        Me.colDescription_Default.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDescription_Default.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colDescription_Default.AppearanceHeader.Options.UseFont = True
        Me.colDescription_Default.AppearanceHeader.Options.UseForeColor = True
        Me.colDescription_Default.FieldName = "Description_Default"
        Me.colDescription_Default.Name = "colDescription_Default"
        Me.colDescription_Default.Visible = True
        Me.colDescription_Default.VisibleIndex = 3
        '
        'colIsPipeType
        '
        Me.colIsPipeType.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colIsPipeType.AppearanceCell.Options.UseFont = True
        Me.colIsPipeType.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colIsPipeType.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colIsPipeType.AppearanceHeader.Options.UseFont = True
        Me.colIsPipeType.AppearanceHeader.Options.UseForeColor = True
        Me.colIsPipeType.FieldName = "IsPipeType"
        Me.colIsPipeType.Name = "colIsPipeType"
        Me.colIsPipeType.Visible = True
        Me.colIsPipeType.VisibleIndex = 4
        '
        'colFileIDString
        '
        Me.colFileIDString.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colFileIDString.AppearanceCell.Options.UseFont = True
        Me.colFileIDString.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colFileIDString.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colFileIDString.AppearanceHeader.Options.UseFont = True
        Me.colFileIDString.AppearanceHeader.Options.UseForeColor = True
        Me.colFileIDString.FieldName = "FileIDString"
        Me.colFileIDString.Name = "colFileIDString"
        Me.colFileIDString.Visible = True
        Me.colFileIDString.VisibleIndex = 5
        '
        'colHasSummary_Default
        '
        Me.colHasSummary_Default.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasSummary_Default.AppearanceCell.Options.UseFont = True
        Me.colHasSummary_Default.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasSummary_Default.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colHasSummary_Default.AppearanceHeader.Options.UseFont = True
        Me.colHasSummary_Default.AppearanceHeader.Options.UseForeColor = True
        Me.colHasSummary_Default.FieldName = "HasSummary_Default"
        Me.colHasSummary_Default.Name = "colHasSummary_Default"
        Me.colHasSummary_Default.Visible = True
        Me.colHasSummary_Default.VisibleIndex = 6
        '
        'colHasAppendix_Default
        '
        Me.colHasAppendix_Default.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasAppendix_Default.AppearanceCell.Options.UseFont = True
        Me.colHasAppendix_Default.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasAppendix_Default.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colHasAppendix_Default.AppearanceHeader.Options.UseFont = True
        Me.colHasAppendix_Default.AppearanceHeader.Options.UseForeColor = True
        Me.colHasAppendix_Default.FieldName = "HasAppendix_Default"
        Me.colHasAppendix_Default.Name = "colHasAppendix_Default"
        Me.colHasAppendix_Default.Visible = True
        Me.colHasAppendix_Default.VisibleIndex = 7
        '
        'colHasMaterial
        '
        Me.colHasMaterial.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasMaterial.AppearanceCell.Options.UseFont = True
        Me.colHasMaterial.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasMaterial.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colHasMaterial.AppearanceHeader.Options.UseFont = True
        Me.colHasMaterial.AppearanceHeader.Options.UseForeColor = True
        Me.colHasMaterial.FieldName = "HasMaterial"
        Me.colHasMaterial.Name = "colHasMaterial"
        Me.colHasMaterial.Visible = True
        Me.colHasMaterial.VisibleIndex = 8
        '
        'colHasOrientation
        '
        Me.colHasOrientation.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasOrientation.AppearanceCell.Options.UseFont = True
        Me.colHasOrientation.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasOrientation.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colHasOrientation.AppearanceHeader.Options.UseFont = True
        Me.colHasOrientation.AppearanceHeader.Options.UseForeColor = True
        Me.colHasOrientation.FieldName = "HasOrientation"
        Me.colHasOrientation.Name = "colHasOrientation"
        Me.colHasOrientation.Visible = True
        Me.colHasOrientation.VisibleIndex = 9
        '
        'colHasPipeSchedule
        '
        Me.colHasPipeSchedule.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasPipeSchedule.AppearanceCell.Options.UseFont = True
        Me.colHasPipeSchedule.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasPipeSchedule.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colHasPipeSchedule.AppearanceHeader.Options.UseFont = True
        Me.colHasPipeSchedule.AppearanceHeader.Options.UseForeColor = True
        Me.colHasPipeSchedule.FieldName = "HasPipeSchedule"
        Me.colHasPipeSchedule.Name = "colHasPipeSchedule"
        Me.colHasPipeSchedule.Visible = True
        Me.colHasPipeSchedule.VisibleIndex = 10
        '
        'colHasPressure
        '
        Me.colHasPressure.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasPressure.AppearanceCell.Options.UseFont = True
        Me.colHasPressure.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasPressure.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colHasPressure.AppearanceHeader.Options.UseFont = True
        Me.colHasPressure.AppearanceHeader.Options.UseForeColor = True
        Me.colHasPressure.FieldName = "HasPressure"
        Me.colHasPressure.Name = "colHasPressure"
        Me.colHasPressure.Visible = True
        Me.colHasPressure.VisibleIndex = 11
        '
        'colHasTargetTemp
        '
        Me.colHasTargetTemp.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasTargetTemp.AppearanceCell.Options.UseFont = True
        Me.colHasTargetTemp.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasTargetTemp.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colHasTargetTemp.AppearanceHeader.Options.UseFont = True
        Me.colHasTargetTemp.AppearanceHeader.Options.UseForeColor = True
        Me.colHasTargetTemp.FieldName = "HasTargetTemp"
        Me.colHasTargetTemp.Name = "colHasTargetTemp"
        Me.colHasTargetTemp.Visible = True
        Me.colHasTargetTemp.VisibleIndex = 12
        '
        'colHasFlowDirection
        '
        Me.colHasFlowDirection.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasFlowDirection.AppearanceCell.Options.UseFont = True
        Me.colHasFlowDirection.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasFlowDirection.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colHasFlowDirection.AppearanceHeader.Options.UseFont = True
        Me.colHasFlowDirection.AppearanceHeader.Options.UseForeColor = True
        Me.colHasFlowDirection.FieldName = "HasFlowDirection"
        Me.colHasFlowDirection.Name = "colHasFlowDirection"
        Me.colHasFlowDirection.Visible = True
        Me.colHasFlowDirection.VisibleIndex = 13
        '
        'colHasAgeAtInspection
        '
        Me.colHasAgeAtInspection.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasAgeAtInspection.AppearanceCell.Options.UseFont = True
        Me.colHasAgeAtInspection.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasAgeAtInspection.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colHasAgeAtInspection.AppearanceHeader.Options.UseFont = True
        Me.colHasAgeAtInspection.AppearanceHeader.Options.UseForeColor = True
        Me.colHasAgeAtInspection.FieldName = "HasAgeAtInspection"
        Me.colHasAgeAtInspection.Name = "colHasAgeAtInspection"
        Me.colHasAgeAtInspection.Visible = True
        Me.colHasAgeAtInspection.VisibleIndex = 14
        '
        'colHasInspectionLocations
        '
        Me.colHasInspectionLocations.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasInspectionLocations.AppearanceCell.Options.UseFont = True
        Me.colHasInspectionLocations.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasInspectionLocations.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colHasInspectionLocations.AppearanceHeader.Options.UseFont = True
        Me.colHasInspectionLocations.AppearanceHeader.Options.UseForeColor = True
        Me.colHasInspectionLocations.FieldName = "HasInspectionLocations"
        Me.colHasInspectionLocations.Name = "colHasInspectionLocations"
        Me.colHasInspectionLocations.Visible = True
        Me.colHasInspectionLocations.VisibleIndex = 15
        '
        'colHasNumberThermalCycles
        '
        Me.colHasNumberThermalCycles.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasNumberThermalCycles.AppearanceCell.Options.UseFont = True
        Me.colHasNumberThermalCycles.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasNumberThermalCycles.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colHasNumberThermalCycles.AppearanceHeader.Options.UseFont = True
        Me.colHasNumberThermalCycles.AppearanceHeader.Options.UseForeColor = True
        Me.colHasNumberThermalCycles.FieldName = "HasNumberThermalCycles"
        Me.colHasNumberThermalCycles.Name = "colHasNumberThermalCycles"
        Me.colHasNumberThermalCycles.Visible = True
        Me.colHasNumberThermalCycles.VisibleIndex = 16
        '
        'colHasOperatingHours
        '
        Me.colHasOperatingHours.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasOperatingHours.AppearanceCell.Options.UseFont = True
        Me.colHasOperatingHours.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHasOperatingHours.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colHasOperatingHours.AppearanceHeader.Options.UseFont = True
        Me.colHasOperatingHours.AppearanceHeader.Options.UseForeColor = True
        Me.colHasOperatingHours.FieldName = "HasOperatingHours"
        Me.colHasOperatingHours.Name = "colHasOperatingHours"
        Me.colHasOperatingHours.Visible = True
        Me.colHasOperatingHours.VisibleIndex = 17
        '
        'RepItemOffices
        '
        Me.RepItemOffices.AutoHeight = False
        Me.RepItemOffices.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepItemOffices.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 34, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 63, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near)})
        Me.RepItemOffices.DataSource = Me.bsTblQTTOffices
        Me.RepItemOffices.DisplayMember = "Description"
        Me.RepItemOffices.Name = "RepItemOffices"
        Me.RepItemOffices.NullText = ""
        Me.RepItemOffices.ValueMember = "ID"
        '
        'bsTblQTTOffices
        '
        Me.bsTblQTTOffices.DataMember = "tblQTT_Offices"
        Me.bsTblQTTOffices.DataSource = Me.dsQTT_DatabaseEditor
        '
        'RepItemRoles
        '
        Me.RepItemRoles.AutoHeight = False
        Me.RepItemRoles.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepItemRoles.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 34, DevExpress.Utils.FormatType.Numeric, "", True, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 63, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("IsNotActive", "Is Not Active", 72, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.RepItemRoles.DataSource = Me.bsTblQTTRoles
        Me.RepItemRoles.DisplayMember = "Description"
        Me.RepItemRoles.Name = "RepItemRoles"
        Me.RepItemRoles.NullText = ""
        Me.RepItemRoles.ValueMember = "ID"
        '
        'bsTblQTTRoles
        '
        Me.bsTblQTTRoles.DataMember = "tblQTT_Roles"
        Me.bsTblQTTRoles.DataSource = Me.dsQTT_DatabaseEditor
        '
        'RepItemInspectionTypes
        '
        Me.RepItemInspectionTypes.AutoHeight = False
        Me.RepItemInspectionTypes.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepItemInspectionTypes.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 63, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near)})
        Me.RepItemInspectionTypes.DataSource = Me.bsTblInspectionTypes
        Me.RepItemInspectionTypes.DisplayMember = "Description"
        Me.RepItemInspectionTypes.Name = "RepItemInspectionTypes"
        Me.RepItemInspectionTypes.NullText = ""
        Me.RepItemInspectionTypes.ValueMember = "ID"
        '
        'bsTblInspectionTypes
        '
        Me.bsTblInspectionTypes.DataMember = "tblInspectionTypes"
        Me.bsTblInspectionTypes.DataSource = Me.dsQTT_DatabaseEditor
        '
        'GridView2
        '
        Me.GridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView2.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Gray
        Me.GridView2.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.GridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.GridView2.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.GridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView2.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.GridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.GridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.GridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.GridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.GridView2.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView2.Appearance.Empty.Options.UseBackColor = True
        Me.GridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView2.Appearance.EvenRow.BackColor2 = System.Drawing.Color.GhostWhite
        Me.GridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.GridView2.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.GridView2.Appearance.EvenRow.Options.UseBackColor = True
        Me.GridView2.Appearance.EvenRow.Options.UseForeColor = True
        Me.GridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView2.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(125, Byte), Integer))
        Me.GridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView2.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black
        Me.GridView2.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.GridView2.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.GridView2.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.GridView2.Appearance.FilterCloseButton.Options.UseForeColor = True
        Me.GridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.GridView2.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Red
        Me.GridView2.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.GridView2.Appearance.FilterPanel.Options.UseBackColor = True
        Me.GridView2.Appearance.FilterPanel.Options.UseForeColor = True
        Me.GridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(81, Byte), Integer))
        Me.GridView2.Appearance.FixedLine.Options.UseBackColor = True
        Me.GridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(151, Byte), Integer))
        Me.GridView2.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(135, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(201, Byte), Integer))
        Me.GridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.GridView2.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView2.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
        Me.GridView2.Appearance.FooterPanel.Options.UseBackColor = True
        Me.GridView2.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.GridView2.Appearance.FooterPanel.Options.UseForeColor = True
        Me.GridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView2.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black
        Me.GridView2.Appearance.GroupButton.Options.UseBackColor = True
        Me.GridView2.Appearance.GroupButton.Options.UseBorderColor = True
        Me.GridView2.Appearance.GroupButton.Options.UseForeColor = True
        Me.GridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.GridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.GridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
        Me.GridView2.Appearance.GroupFooter.Options.UseBackColor = True
        Me.GridView2.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.GridView2.Appearance.GroupFooter.Options.UseForeColor = True
        Me.GridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(65, Byte), Integer), CType(CType(65, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.GridView2.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
        Me.GridView2.Appearance.GroupPanel.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.GridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White
        Me.GridView2.Appearance.GroupPanel.Options.UseBackColor = True
        Me.GridView2.Appearance.GroupPanel.Options.UseFont = True
        Me.GridView2.Appearance.GroupPanel.Options.UseForeColor = True
        Me.GridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(151, Byte), Integer))
        Me.GridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Silver
        Me.GridView2.Appearance.GroupRow.Options.UseBackColor = True
        Me.GridView2.Appearance.GroupRow.Options.UseForeColor = True
        Me.GridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView2.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.GridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
        Me.GridView2.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.GridView2.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.GridView2.Appearance.HeaderPanel.Options.UseFont = True
        Me.GridView2.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.GridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gray
        Me.GridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView2.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView2.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView2.Appearance.HorzLine.Options.UseBackColor = True
        Me.GridView2.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(206, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.GridView2.Appearance.OddRow.BackColor2 = System.Drawing.Color.White
        Me.GridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.GridView2.Appearance.OddRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal
        Me.GridView2.Appearance.OddRow.Options.UseBackColor = True
        Me.GridView2.Appearance.OddRow.Options.UseForeColor = True
        Me.GridView2.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(217, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GridView2.Appearance.Preview.BackColor2 = System.Drawing.Color.White
        Me.GridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(151, Byte), Integer))
        Me.GridView2.Appearance.Preview.Options.UseBackColor = True
        Me.GridView2.Appearance.Preview.Options.UseForeColor = True
        Me.GridView2.Appearance.Row.BackColor = System.Drawing.Color.White
        Me.GridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.GridView2.Appearance.Row.Options.UseBackColor = True
        Me.GridView2.Appearance.Row.Options.UseForeColor = True
        Me.GridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.White
        Me.GridView2.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView2.Appearance.RowSeparator.Options.UseBackColor = True
        Me.GridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.GridView2.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
        Me.GridView2.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView2.Appearance.SelectedRow.Options.UseForeColor = True
        Me.GridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView2.Appearance.VertLine.Options.UseBackColor = True
        Me.GridView2.GridControl = Me.GridControl1
        Me.GridView2.Name = "GridView2"
        '
        'GridView3
        '
        Me.GridView3.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView3.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView3.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView3.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Gray
        Me.GridView3.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.GridView3.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.GridView3.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.GridView3.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView3.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.GridView3.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView3.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.GridView3.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.GridView3.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.GridView3.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.GridView3.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView3.Appearance.Empty.Options.UseBackColor = True
        Me.GridView3.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView3.Appearance.EvenRow.BackColor2 = System.Drawing.Color.GhostWhite
        Me.GridView3.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.GridView3.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.GridView3.Appearance.EvenRow.Options.UseBackColor = True
        Me.GridView3.Appearance.EvenRow.Options.UseForeColor = True
        Me.GridView3.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView3.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(125, Byte), Integer))
        Me.GridView3.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView3.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black
        Me.GridView3.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.GridView3.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.GridView3.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.GridView3.Appearance.FilterCloseButton.Options.UseForeColor = True
        Me.GridView3.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.GridView3.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView3.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Red
        Me.GridView3.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.GridView3.Appearance.FilterPanel.Options.UseBackColor = True
        Me.GridView3.Appearance.FilterPanel.Options.UseForeColor = True
        Me.GridView3.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(81, Byte), Integer))
        Me.GridView3.Appearance.FixedLine.Options.UseBackColor = True
        Me.GridView3.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(151, Byte), Integer))
        Me.GridView3.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(135, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(201, Byte), Integer))
        Me.GridView3.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.GridView3.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView3.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView3.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView3.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView3.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
        Me.GridView3.Appearance.FooterPanel.Options.UseBackColor = True
        Me.GridView3.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.GridView3.Appearance.FooterPanel.Options.UseForeColor = True
        Me.GridView3.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView3.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView3.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black
        Me.GridView3.Appearance.GroupButton.Options.UseBackColor = True
        Me.GridView3.Appearance.GroupButton.Options.UseBorderColor = True
        Me.GridView3.Appearance.GroupButton.Options.UseForeColor = True
        Me.GridView3.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.GridView3.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.GridView3.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
        Me.GridView3.Appearance.GroupFooter.Options.UseBackColor = True
        Me.GridView3.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.GridView3.Appearance.GroupFooter.Options.UseForeColor = True
        Me.GridView3.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(65, Byte), Integer), CType(CType(65, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.GridView3.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
        Me.GridView3.Appearance.GroupPanel.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.GridView3.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White
        Me.GridView3.Appearance.GroupPanel.Options.UseBackColor = True
        Me.GridView3.Appearance.GroupPanel.Options.UseFont = True
        Me.GridView3.Appearance.GroupPanel.Options.UseForeColor = True
        Me.GridView3.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(151, Byte), Integer))
        Me.GridView3.Appearance.GroupRow.ForeColor = System.Drawing.Color.Silver
        Me.GridView3.Appearance.GroupRow.Options.UseBackColor = True
        Me.GridView3.Appearance.GroupRow.Options.UseForeColor = True
        Me.GridView3.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView3.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView3.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.GridView3.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
        Me.GridView3.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.GridView3.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.GridView3.Appearance.HeaderPanel.Options.UseFont = True
        Me.GridView3.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.GridView3.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gray
        Me.GridView3.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView3.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView3.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView3.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView3.Appearance.HorzLine.Options.UseBackColor = True
        Me.GridView3.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(206, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.GridView3.Appearance.OddRow.BackColor2 = System.Drawing.Color.White
        Me.GridView3.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.GridView3.Appearance.OddRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal
        Me.GridView3.Appearance.OddRow.Options.UseBackColor = True
        Me.GridView3.Appearance.OddRow.Options.UseForeColor = True
        Me.GridView3.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(217, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GridView3.Appearance.Preview.BackColor2 = System.Drawing.Color.White
        Me.GridView3.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(151, Byte), Integer))
        Me.GridView3.Appearance.Preview.Options.UseBackColor = True
        Me.GridView3.Appearance.Preview.Options.UseForeColor = True
        Me.GridView3.Appearance.Row.BackColor = System.Drawing.Color.White
        Me.GridView3.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.GridView3.Appearance.Row.Options.UseBackColor = True
        Me.GridView3.Appearance.Row.Options.UseForeColor = True
        Me.GridView3.Appearance.RowSeparator.BackColor = System.Drawing.Color.White
        Me.GridView3.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.GridView3.Appearance.RowSeparator.Options.UseBackColor = True
        Me.GridView3.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.GridView3.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
        Me.GridView3.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView3.Appearance.SelectedRow.Options.UseForeColor = True
        Me.GridView3.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.GridView3.Appearance.VertLine.Options.UseBackColor = True
        Me.GridView3.GridControl = Me.GridControl1
        Me.GridView3.Name = "GridView3"
        '
        'uiCloseButton
        '
        Me.uiCloseButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uiCloseButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiCloseButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiCloseButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiCloseButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiCloseButton.Appearance.Options.UseBackColor = True
        Me.uiCloseButton.Appearance.Options.UseBorderColor = True
        Me.uiCloseButton.Appearance.Options.UseFont = True
        Me.uiCloseButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiCloseButton.Location = New System.Drawing.Point(759, 132)
        Me.uiCloseButton.Name = "uiCloseButton"
        Me.uiCloseButton.Size = New System.Drawing.Size(148, 32)
        Me.uiCloseButton.TabIndex = 22
        Me.uiCloseButton.Text = "&Close"
        '
        'uiTableNames
        '
        Me.uiTableNames.Location = New System.Drawing.Point(143, 140)
        Me.uiTableNames.Name = "uiTableNames"
        Me.uiTableNames.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiTableNames.Properties.Appearance.Options.UseFont = True
        Me.uiTableNames.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiTableNames.Properties.AppearanceDropDown.Options.UseFont = True
        SerializableAppearanceObject1.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject1.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject1.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject1.Options.UseBackColor = True
        SerializableAppearanceObject1.Options.UseBorderColor = True
        Me.uiTableNames.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "", Nothing, Nothing, True)})
        Me.uiTableNames.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiTableNames.Properties.DropDownRows = 12
        Me.uiTableNames.Properties.Items.AddRange(New Object() {"Burner Configurations", "Default Text Entries", "Inspectors", "Offices", "Plant Process Types", "Process Flow Directions", "Project Tools", "Reformer OEMs", "Roles", "Tube Manufacturers", "Tube Materials"})
        Me.uiTableNames.Properties.PopupFormMinSize = New System.Drawing.Size(180, 0)
        Me.uiTableNames.Properties.Sorted = True
        Me.uiTableNames.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiTableNames.Size = New System.Drawing.Size(180, 24)
        Me.uiTableNames.TabIndex = 21
        '
        'tatblQTT_Offices
        '
        Me.tatblQTT_Offices.ClearBeforeFill = True
        '
        'taRoles
        '
        Me.taRoles.ClearBeforeFill = True
        '
        'taInspectors
        '
        Me.taInspectors.ClearBeforeFill = True
        '
        'tatblInspectionTools
        '
        Me.tatblInspectionTools.ClearBeforeFill = True
        '
        'taTblDefaultTextEntries
        '
        Me.taTblDefaultTextEntries.ClearBeforeFill = True
        '
        'taTblDefaultText_ListItems
        '
        Me.taTblDefaultText_ListItems.ClearBeforeFill = True
        '
        'taTblInspectionTypes
        '
        Me.taTblInspectionTypes.ClearBeforeFill = True
        '
        'DataEditor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(925, 573)
        Me.Controls.Add(Me.uiCloseButton)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.uiTableNames)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "DataEditor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "System Maintenance"
        CType(Me.LayoutViewCard1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsTables, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dsQTT_DatabaseEditor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepItemOffices, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsTblQTTOffices, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepItemRoles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsTblQTTRoles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepItemInspectionTypes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsTblInspectionTypes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiTableNames.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents LayoutViewCard1 As DevExpress.XtraGrid.Views.Layout.LayoutViewCard
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents uiCloseButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiTableNames As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents RepItemOffices As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents RepItemRoles As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents dsQTT_DatabaseEditor As QIG.AutoReporter.QTT_DatabaseEditor
    Friend WithEvents bsTables As System.Windows.Forms.BindingSource
    Friend WithEvents tatblQTT_Offices As QIG.AutoReporter.QTT_DatabaseEditorTableAdapters.Lookup_tblQTT_OfficesTableAdapter
    Friend WithEvents taRoles As QIG.AutoReporter.QTT_DatabaseEditorTableAdapters.Lookup_RolesTableAdapter
    Friend WithEvents taInspectors As QIG.AutoReporter.QTT_DatabaseEditorTableAdapters.Lookup_InspectorsTableAdapter
    Friend WithEvents tatblInspectionTools As QIG.AutoReporter.QTT_DatabaseEditorTableAdapters.Lookup_tblInspectionToolsTableAdapter
    Friend WithEvents taTblDefaultTextEntries As QIG.AutoReporter.QTT_DatabaseEditorTableAdapters.tblDefaultTextEntriesTableAdapter
    Friend WithEvents taTblDefaultText_ListItems As QIG.AutoReporter.QTT_DatabaseEditorTableAdapters.tblDefaultText_ListItemsTableAdapter
    Friend WithEvents colID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFK_InspectionType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colListSortOrder As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDescription_Default As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIsPipeType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFileIDString As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHasSummary_Default As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHasAppendix_Default As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHasMaterial As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHasOrientation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHasPipeSchedule As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHasPressure As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHasTargetTemp As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHasFlowDirection As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHasAgeAtInspection As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHasInspectionLocations As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHasNumberThermalCycles As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHasOperatingHours As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents bsTblQTTOffices As System.Windows.Forms.BindingSource
    Friend WithEvents bsTblQTTRoles As System.Windows.Forms.BindingSource
    Friend WithEvents RepItemInspectionTypes As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents bsTblInspectionTypes As System.Windows.Forms.BindingSource
    Friend WithEvents taTblInspectionTypes As QIG.AutoReporter.QTT_DatabaseEditorTableAdapters.tblInspectionTypesTableAdapter
End Class
