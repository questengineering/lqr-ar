﻿Public Class PWEntry

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim STRPass As String = txtPassword.Text

        If STRPass = "Pa55word!" Then
            Me.Hide()
            txtPassword.Clear()
            Using oForm As New DataEditor
                oForm.ShowDialog(Me)
            End Using
        End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Hide()
    End Sub
End Class