﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucExecutiveSummary
   Inherits System.Windows.Forms.UserControl

   'UserControl overrides dispose to clean up the component list.
   <System.Diagnostics.DebuggerNonUserCode()> _
   Protected Overrides Sub Dispose(ByVal disposing As Boolean)
      Try
         If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
         End If
      Finally
         MyBase.Dispose(disposing)
      End Try
   End Sub

   'Required by the Windows Form Designer
   Private components As System.ComponentModel.IContainer

   'NOTE: The following procedure is required by the Windows Form Designer
   'It can be modified using the Windows Form Designer.  
   'Do not modify it using the code editor.
   <System.Diagnostics.DebuggerStepThrough()> _
   Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject2 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject3 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject4 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject5 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject6 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject7 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject8 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject9 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Me.uilblNewUsedPipes = New System.Windows.Forms.Label()
        Me.uilblPassNumber = New System.Windows.Forms.Label()
        Me.uilblUnits = New System.Windows.Forms.Label()
        Me.uiPipeOrBendDataInfo = New DevExpress.XtraGrid.GridControl()
        Me.uiPipeOrBendDataInfoView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.bsSummaryPipeDataInfo = New System.Windows.Forms.BindingSource(Me.components)
        Me.QTT_SummaryData = New QIG.AutoReporter.QTT_SummaryData()
        Me.bsSummaryPipeOrBendData = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsExecutiveSummary = New System.Windows.Forms.BindingSource(Me.components)
        Me.uiScheduleSize = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiPipeSize = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiInternalOrExternal = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uilblGeneralOrLocal = New System.Windows.Forms.Label()
        Me.uiGeneralOrLocal = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uilblInternalOrExternal = New System.Windows.Forms.Label()
        Me.uilblPipeSize = New System.Windows.Forms.Label()
        Me.uilblPipeSchedule = New System.Windows.Forms.Label()
        Me.btnWriteExecutiveSummary = New DevExpress.XtraEditors.SimpleButton()
        Me.uiExecutiveSummaryText = New DevExpress.XtraRichEdit.RichEditControl()
        Me.uiNewUsedPipes = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiUnits = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiPassNumber = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.lblDeformations = New System.Windows.Forms.Label()
        Me.lblDent = New DevExpress.XtraEditors.LabelControl()
        Me.lblOvality = New DevExpress.XtraEditors.LabelControl()
        Me.lblSwell = New DevExpress.XtraEditors.LabelControl()
        Me.lblInternalFouling = New DevExpress.XtraEditors.LabelControl()
        Me.cbInternalFoulingText = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.radPipeOrBend = New DevExpress.XtraEditors.RadioGroup()
        Me.taTblSummary = New QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryTableAdapter()
        Me.taTblSummaryPipeData = New QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryPipeDataTableAdapter()
        Me.taTblSummaryBendData = New QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryBendDataTableAdapter()
        Me.taTblSummaryBendDataInfo = New QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryBendDataInfoTableAdapter()
        Me.taTblSummaryPipeDataInfo = New QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryPipeDataInfoTableAdapter()
        Me.bsSummaryBendDataInfo = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnPaste = New DevExpress.XtraEditors.SimpleButton()
        Me.taTblPipeTubeItems = New QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblPipeTubeItemsTableAdapter()
        Me.cmbPipeSectionName = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiDeformationDataGrid = New DevExpress.XtraGrid.GridControl()
        Me.uiDeformationDataView = New DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView()
        Me.bndAbove = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.colBulgingExceedingCount = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.CountSelector = New DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit()
        Me.colSwellingExceedingCount = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colOvalityExceedingCount = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colDentingExceedingCount = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.bndBelow = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.colBulgingNotExceedingCount = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colSwellingNotExceedingCount = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colOvalityNotExceedingCount = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colDentingNotExceedingCount = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.bndThreshold = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.colBulgingThreshold = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.decimalSelector = New DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit()
        Me.colSwellingThreshold = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colOvalityThreshold = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colDentingThreshold = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.bndWorstPipeID = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.colBulgingPipeID = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colSwellingPipeID = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colOvalityPipeID = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colDentingPipeID = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.bndPass = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.colBulgingPass = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.PassSelector = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.colSwellingPass = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colOvalityPass = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colDentingPass = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.bndWorstDeformation = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.colBulgingWorstDefo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colSwellingWorstDefo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colOvalityWorstDefo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colDentingWorstDefo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.lblBulge = New DevExpress.XtraEditors.LabelControl()
        CType(Me.uiPipeOrBendDataInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiPipeOrBendDataInfoView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsSummaryPipeDataInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.QTT_SummaryData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsSummaryPipeOrBendData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsExecutiveSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiScheduleSize.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiPipeSize.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiInternalOrExternal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiGeneralOrLocal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiNewUsedPipes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiUnits.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiPassNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbInternalFoulingText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radPipeOrBend.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsSummaryBendDataInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbPipeSectionName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiDeformationDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiDeformationDataView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CountSelector, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.decimalSelector, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PassSelector, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uilblNewUsedPipes
        '
        Me.uilblNewUsedPipes.AutoSize = True
        Me.uilblNewUsedPipes.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uilblNewUsedPipes.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uilblNewUsedPipes.Location = New System.Drawing.Point(3, 26)
        Me.uilblNewUsedPipes.Name = "uilblNewUsedPipes"
        Me.uilblNewUsedPipes.Size = New System.Drawing.Size(107, 17)
        Me.uilblNewUsedPipes.TabIndex = 25
        Me.uilblNewUsedPipes.Text = "New/Used Pipes:"
        Me.uilblNewUsedPipes.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uilblPassNumber
        '
        Me.uilblPassNumber.AutoSize = True
        Me.uilblPassNumber.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uilblPassNumber.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uilblPassNumber.Location = New System.Drawing.Point(21, 88)
        Me.uilblPassNumber.Name = "uilblPassNumber"
        Me.uilblPassNumber.Size = New System.Drawing.Size(89, 17)
        Me.uilblPassNumber.TabIndex = 29
        Me.uilblPassNumber.Text = "Pass Number:"
        Me.uilblPassNumber.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uilblUnits
        '
        Me.uilblUnits.AutoSize = True
        Me.uilblUnits.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uilblUnits.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uilblUnits.Location = New System.Drawing.Point(20, 57)
        Me.uilblUnits.Name = "uilblUnits"
        Me.uilblUnits.Size = New System.Drawing.Size(90, 17)
        Me.uilblUnits.TabIndex = 27
        Me.uilblUnits.Text = "Leading Units:"
        Me.uilblUnits.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiPipeOrBendDataInfo
        '
        Me.uiPipeOrBendDataInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.CancelEdit.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Edit.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.EndEdit.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.First.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.First.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Last.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Last.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Next.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Next.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.NextPage.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.NextPage.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Prev.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Prev.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.PrevPage.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.PrevPage.Visible = False
        Me.uiPipeOrBendDataInfo.Location = New System.Drawing.Point(350, 102)
        Me.uiPipeOrBendDataInfo.LookAndFeel.SkinName = "Blue"
        Me.uiPipeOrBendDataInfo.MainView = Me.uiPipeOrBendDataInfoView
        Me.uiPipeOrBendDataInfo.Name = "uiPipeOrBendDataInfo"
        Me.uiPipeOrBendDataInfo.Size = New System.Drawing.Size(545, 104)
        Me.uiPipeOrBendDataInfo.TabIndex = 145
        Me.uiPipeOrBendDataInfo.UseEmbeddedNavigator = True
        Me.uiPipeOrBendDataInfo.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uiPipeOrBendDataInfoView})
        '
        'uiPipeOrBendDataInfoView
        '
        Me.uiPipeOrBendDataInfoView.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.uiPipeOrBendDataInfoView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiPipeOrBendDataInfoView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom
        Me.uiPipeOrBendDataInfoView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uiPipeOrBendDataInfoView.ColumnPanelRowHeight = 35
        Me.uiPipeOrBendDataInfoView.GridControl = Me.uiPipeOrBendDataInfo
        Me.uiPipeOrBendDataInfoView.Name = "uiPipeOrBendDataInfoView"
        Me.uiPipeOrBendDataInfoView.OptionsCustomization.AllowColumnMoving = False
        Me.uiPipeOrBendDataInfoView.OptionsMenu.EnableColumnMenu = False
        Me.uiPipeOrBendDataInfoView.OptionsView.ColumnAutoWidth = False
        Me.uiPipeOrBendDataInfoView.OptionsView.EnableAppearanceEvenRow = True
        Me.uiPipeOrBendDataInfoView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.uiPipeOrBendDataInfoView.OptionsView.ShowGroupPanel = False
        '
        'bsSummaryPipeDataInfo
        '
        Me.bsSummaryPipeDataInfo.DataMember = "tblSummaryPipeDataInfo"
        Me.bsSummaryPipeDataInfo.DataSource = Me.QTT_SummaryData
        '
        'QTT_SummaryData
        '
        Me.QTT_SummaryData.DataSetName = "QTT_SummaryData"
        Me.QTT_SummaryData.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'bsSummaryPipeOrBendData
        '
        Me.bsSummaryPipeOrBendData.DataMember = "tblSummaryPipeData"
        Me.bsSummaryPipeOrBendData.DataSource = Me.QTT_SummaryData
        '
        'bsExecutiveSummary
        '
        Me.bsExecutiveSummary.DataMember = "tblSummary"
        Me.bsExecutiveSummary.DataSource = Me.QTT_SummaryData
        '
        'uiScheduleSize
        '
        Me.uiScheduleSize.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "PipeSchedule", True))
        Me.uiScheduleSize.Location = New System.Drawing.Point(691, 72)
        Me.uiScheduleSize.Name = "uiScheduleSize"
        Me.uiScheduleSize.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiScheduleSize.Properties.Appearance.Options.UseFont = True
        Me.uiScheduleSize.Properties.Appearance.Options.UseTextOptions = True
        Me.uiScheduleSize.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiScheduleSize.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiScheduleSize.Properties.AppearanceDisabled.Options.UseFont = True
        Me.uiScheduleSize.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiScheduleSize.Properties.AppearanceDropDown.Options.UseFont = True
        Me.uiScheduleSize.Properties.AppearanceDropDown.Options.UseTextOptions = True
        Me.uiScheduleSize.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiScheduleSize.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiScheduleSize.Properties.AppearanceFocused.Options.UseFont = True
        Me.uiScheduleSize.Properties.AppearanceFocused.Options.UseTextOptions = True
        Me.uiScheduleSize.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiScheduleSize.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiScheduleSize.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.uiScheduleSize.Properties.AutoHeight = False
        SerializableAppearanceObject1.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject1.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject1.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject1.Options.UseBackColor = True
        SerializableAppearanceObject1.Options.UseBorderColor = True
        Me.uiScheduleSize.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "", Nothing, Nothing, True)})
        Me.uiScheduleSize.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiScheduleSize.Properties.DropDownRows = 13
        Me.uiScheduleSize.Properties.Items.AddRange(New Object() {"Tube", "5", "10", "20", "30", "40", "50", "60", "80", "120", "140", "160", "XXS"})
        Me.uiScheduleSize.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiScheduleSize.Size = New System.Drawing.Size(80, 24)
        Me.uiScheduleSize.TabIndex = 103
        '
        'uiPipeSize
        '
        Me.uiPipeSize.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "PipeSize", True))
        Me.uiPipeSize.Location = New System.Drawing.Point(622, 72)
        Me.uiPipeSize.Name = "uiPipeSize"
        Me.uiPipeSize.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPipeSize.Properties.Appearance.Options.UseFont = True
        Me.uiPipeSize.Properties.Appearance.Options.UseTextOptions = True
        Me.uiPipeSize.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiPipeSize.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPipeSize.Properties.AppearanceDisabled.Options.UseFont = True
        Me.uiPipeSize.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPipeSize.Properties.AppearanceDropDown.Options.UseFont = True
        Me.uiPipeSize.Properties.AppearanceDropDown.Options.UseTextOptions = True
        Me.uiPipeSize.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiPipeSize.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPipeSize.Properties.AppearanceFocused.Options.UseFont = True
        Me.uiPipeSize.Properties.AppearanceFocused.Options.UseTextOptions = True
        Me.uiPipeSize.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiPipeSize.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPipeSize.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.uiPipeSize.Properties.AutoHeight = False
        SerializableAppearanceObject2.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject2.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject2.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject2.Options.UseBackColor = True
        SerializableAppearanceObject2.Options.UseBorderColor = True
        Me.uiPipeSize.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject2, "", Nothing, Nothing, True)})
        Me.uiPipeSize.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiPipeSize.Properties.DropDownRows = 11
        Me.uiPipeSize.Properties.Items.AddRange(New Object() {"Other", "2", "2.5", "3", "3.5", "4", "5", "6", "8", "10", "12"})
        Me.uiPipeSize.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiPipeSize.Size = New System.Drawing.Size(63, 24)
        Me.uiPipeSize.TabIndex = 102
        '
        'uiInternalOrExternal
        '
        Me.uiInternalOrExternal.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "InternalOrExternal", True))
        Me.uiInternalOrExternal.Location = New System.Drawing.Point(478, 72)
        Me.uiInternalOrExternal.Name = "uiInternalOrExternal"
        Me.uiInternalOrExternal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiInternalOrExternal.Properties.Appearance.Options.UseFont = True
        Me.uiInternalOrExternal.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiInternalOrExternal.Properties.AppearanceDisabled.Options.UseFont = True
        Me.uiInternalOrExternal.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiInternalOrExternal.Properties.AppearanceDropDown.Options.UseFont = True
        Me.uiInternalOrExternal.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiInternalOrExternal.Properties.AppearanceFocused.Options.UseFont = True
        Me.uiInternalOrExternal.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiInternalOrExternal.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.uiInternalOrExternal.Properties.AutoHeight = False
        SerializableAppearanceObject3.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject3.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject3.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject3.Options.UseBackColor = True
        SerializableAppearanceObject3.Options.UseBorderColor = True
        Me.uiInternalOrExternal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject3, "", Nothing, Nothing, True)})
        Me.uiInternalOrExternal.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiInternalOrExternal.Properties.Items.AddRange(New Object() {"internal", "external", "internal and external"})
        Me.uiInternalOrExternal.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiInternalOrExternal.Size = New System.Drawing.Size(138, 24)
        Me.uiInternalOrExternal.TabIndex = 101
        '
        'uilblGeneralOrLocal
        '
        Me.uilblGeneralOrLocal.AutoSize = True
        Me.uilblGeneralOrLocal.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uilblGeneralOrLocal.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uilblGeneralOrLocal.Location = New System.Drawing.Point(350, 54)
        Me.uilblGeneralOrLocal.Name = "uilblGeneralOrLocal"
        Me.uilblGeneralOrLocal.Size = New System.Drawing.Size(95, 15)
        Me.uilblGeneralOrLocal.TabIndex = 57
        Me.uilblGeneralOrLocal.Text = "General or Local:"
        Me.uilblGeneralOrLocal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiGeneralOrLocal
        '
        Me.uiGeneralOrLocal.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "GenOrLocal", True))
        Me.uiGeneralOrLocal.Location = New System.Drawing.Point(350, 72)
        Me.uiGeneralOrLocal.Name = "uiGeneralOrLocal"
        Me.uiGeneralOrLocal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiGeneralOrLocal.Properties.Appearance.Options.UseFont = True
        Me.uiGeneralOrLocal.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiGeneralOrLocal.Properties.AppearanceDisabled.Options.UseFont = True
        Me.uiGeneralOrLocal.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiGeneralOrLocal.Properties.AppearanceDropDown.Options.UseFont = True
        Me.uiGeneralOrLocal.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiGeneralOrLocal.Properties.AppearanceFocused.Options.UseFont = True
        Me.uiGeneralOrLocal.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiGeneralOrLocal.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.uiGeneralOrLocal.Properties.AutoHeight = False
        SerializableAppearanceObject4.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject4.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject4.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject4.Options.UseBackColor = True
        SerializableAppearanceObject4.Options.UseBorderColor = True
        Me.uiGeneralOrLocal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject4, "", Nothing, Nothing, True)})
        Me.uiGeneralOrLocal.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiGeneralOrLocal.Properties.Items.AddRange(New Object() {"General", "Localized", "General and localized"})
        Me.uiGeneralOrLocal.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiGeneralOrLocal.Size = New System.Drawing.Size(122, 24)
        Me.uiGeneralOrLocal.TabIndex = 100
        '
        'uilblInternalOrExternal
        '
        Me.uilblInternalOrExternal.AutoSize = True
        Me.uilblInternalOrExternal.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uilblInternalOrExternal.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uilblInternalOrExternal.Location = New System.Drawing.Point(478, 54)
        Me.uilblInternalOrExternal.Name = "uilblInternalOrExternal"
        Me.uilblInternalOrExternal.Size = New System.Drawing.Size(108, 15)
        Me.uilblInternalOrExternal.TabIndex = 58
        Me.uilblInternalOrExternal.Text = "Internal or External:"
        Me.uilblInternalOrExternal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uilblPipeSize
        '
        Me.uilblPipeSize.AutoSize = True
        Me.uilblPipeSize.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uilblPipeSize.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uilblPipeSize.Location = New System.Drawing.Point(622, 54)
        Me.uilblPipeSize.Name = "uilblPipeSize"
        Me.uilblPipeSize.Size = New System.Drawing.Size(56, 15)
        Me.uilblPipeSize.TabIndex = 59
        Me.uilblPipeSize.Text = "Pipe Size:"
        Me.uilblPipeSize.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uilblPipeSchedule
        '
        Me.uilblPipeSchedule.AutoSize = True
        Me.uilblPipeSchedule.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uilblPipeSchedule.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uilblPipeSchedule.Location = New System.Drawing.Point(691, 54)
        Me.uilblPipeSchedule.Name = "uilblPipeSchedule"
        Me.uilblPipeSchedule.Size = New System.Drawing.Size(84, 15)
        Me.uilblPipeSchedule.TabIndex = 60
        Me.uilblPipeSchedule.Text = "Pipe Schedule:"
        Me.uilblPipeSchedule.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnWriteExecutiveSummary
        '
        Me.btnWriteExecutiveSummary.Appearance.BackColor = System.Drawing.Color.White
        Me.btnWriteExecutiveSummary.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.btnWriteExecutiveSummary.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.btnWriteExecutiveSummary.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnWriteExecutiveSummary.Appearance.Options.UseBackColor = True
        Me.btnWriteExecutiveSummary.Appearance.Options.UseBorderColor = True
        Me.btnWriteExecutiveSummary.Appearance.Options.UseFont = True
        Me.btnWriteExecutiveSummary.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.btnWriteExecutiveSummary.Location = New System.Drawing.Point(785, 63)
        Me.btnWriteExecutiveSummary.Name = "btnWriteExecutiveSummary"
        Me.btnWriteExecutiveSummary.Size = New System.Drawing.Size(110, 33)
        Me.btnWriteExecutiveSummary.TabIndex = 4
        Me.btnWriteExecutiveSummary.Text = "Write Summary"
        '
        'uiExecutiveSummaryText
        '
        Me.uiExecutiveSummaryText.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
        Me.uiExecutiveSummaryText.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uiExecutiveSummaryText.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiExecutiveSummaryText.Appearance.Text.Options.UseFont = True
        Me.uiExecutiveSummaryText.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.uiExecutiveSummaryText.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsSummaryPipeOrBendData, "Paragraph", True))
        Me.uiExecutiveSummaryText.Location = New System.Drawing.Point(350, 209)
        Me.uiExecutiveSummaryText.Margin = New System.Windows.Forms.Padding(0)
        Me.uiExecutiveSummaryText.Name = "uiExecutiveSummaryText"
        Me.uiExecutiveSummaryText.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden
        Me.uiExecutiveSummaryText.Options.HorizontalScrollbar.Visibility = DevExpress.XtraRichEdit.RichEditScrollbarVisibility.Hidden
        Me.uiExecutiveSummaryText.ReadOnly = True
        Me.uiExecutiveSummaryText.Size = New System.Drawing.Size(545, 131)
        Me.uiExecutiveSummaryText.TabIndex = 0
        '
        'uiNewUsedPipes
        '
        Me.uiNewUsedPipes.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsExecutiveSummary, "NewOrUsedPipes", True))
        Me.uiNewUsedPipes.Location = New System.Drawing.Point(113, 26)
        Me.uiNewUsedPipes.Name = "uiNewUsedPipes"
        Me.uiNewUsedPipes.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiNewUsedPipes.Properties.Appearance.Options.UseFont = True
        Me.uiNewUsedPipes.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiNewUsedPipes.Properties.AppearanceDisabled.Options.UseFont = True
        Me.uiNewUsedPipes.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiNewUsedPipes.Properties.AppearanceDropDown.Options.UseFont = True
        Me.uiNewUsedPipes.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiNewUsedPipes.Properties.AppearanceFocused.Options.UseFont = True
        Me.uiNewUsedPipes.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiNewUsedPipes.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.uiNewUsedPipes.Properties.AutoHeight = False
        SerializableAppearanceObject5.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject5.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject5.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject5.Options.UseBackColor = True
        SerializableAppearanceObject5.Options.UseBorderColor = True
        Me.uiNewUsedPipes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject5, "", Nothing, Nothing, True)})
        Me.uiNewUsedPipes.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiNewUsedPipes.Properties.Items.AddRange(New Object() {"New Pipes (Baseline)", "In-Service Pipes"})
        Me.uiNewUsedPipes.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiNewUsedPipes.Size = New System.Drawing.Size(150, 24)
        Me.uiNewUsedPipes.TabIndex = 0
        '
        'uiUnits
        '
        Me.uiUnits.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsExecutiveSummary, "LeadingUnits", True))
        Me.uiUnits.Location = New System.Drawing.Point(113, 57)
        Me.uiUnits.Name = "uiUnits"
        Me.uiUnits.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiUnits.Properties.Appearance.Options.UseFont = True
        Me.uiUnits.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiUnits.Properties.AppearanceDisabled.Options.UseFont = True
        Me.uiUnits.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiUnits.Properties.AppearanceDropDown.Options.UseFont = True
        Me.uiUnits.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiUnits.Properties.AppearanceFocused.Options.UseFont = True
        Me.uiUnits.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiUnits.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.uiUnits.Properties.AutoHeight = False
        SerializableAppearanceObject6.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject6.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject6.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject6.Options.UseBackColor = True
        SerializableAppearanceObject6.Options.UseBorderColor = True
        Me.uiUnits.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject6, "", Nothing, Nothing, True)})
        Me.uiUnits.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiUnits.Properties.Items.AddRange(New Object() {"Imperial", "Metric"})
        Me.uiUnits.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiUnits.Size = New System.Drawing.Size(150, 24)
        Me.uiUnits.TabIndex = 1
        '
        'uiPassNumber
        '
        Me.uiPassNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsExecutiveSummary, "Appendix", True))
        Me.uiPassNumber.Location = New System.Drawing.Point(113, 88)
        Me.uiPassNumber.Name = "uiPassNumber"
        Me.uiPassNumber.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPassNumber.Properties.Appearance.Options.UseFont = True
        Me.uiPassNumber.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPassNumber.Properties.AppearanceDisabled.Options.UseFont = True
        Me.uiPassNumber.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPassNumber.Properties.AppearanceDropDown.Options.UseFont = True
        Me.uiPassNumber.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPassNumber.Properties.AppearanceFocused.Options.UseFont = True
        Me.uiPassNumber.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPassNumber.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.uiPassNumber.Properties.AutoHeight = False
        SerializableAppearanceObject7.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject7.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject7.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject7.Options.UseBackColor = True
        SerializableAppearanceObject7.Options.UseBorderColor = True
        Me.uiPassNumber.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject7, "", Nothing, Nothing, True)})
        Me.uiPassNumber.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiPassNumber.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "A", "B", "C", "D", "E", "F", "G", "H", "North", "East", "South", "West", "Other"})
        Me.uiPassNumber.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiPassNumber.Size = New System.Drawing.Size(150, 24)
        Me.uiPassNumber.TabIndex = 2
        '
        'Label93
        '
        Me.Label93.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label93.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label93.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label93.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label93.Location = New System.Drawing.Point(0, 0)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(895, 15)
        Me.Label93.TabIndex = 41
        Me.Label93.Text = "EXECUTIVE SUMMARY WRITER"
        Me.Label93.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDeformations
        '
        Me.lblDeformations.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.lblDeformations.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeformations.ForeColor = System.Drawing.Color.GhostWhite
        Me.lblDeformations.Location = New System.Drawing.Point(0, 120)
        Me.lblDeformations.Name = "lblDeformations"
        Me.lblDeformations.Size = New System.Drawing.Size(342, 15)
        Me.lblDeformations.TabIndex = 83
        Me.lblDeformations.Text = "DEFORMATIONS"
        Me.lblDeformations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDent
        '
        Me.lblDent.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.lblDent.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblDent.Location = New System.Drawing.Point(6, 243)
        Me.lblDent.Name = "lblDent"
        Me.lblDent.Size = New System.Drawing.Size(48, 17)
        Me.lblDent.TabIndex = 73
        Me.lblDent.Text = "Denting:"
        '
        'lblOvality
        '
        Me.lblOvality.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.lblOvality.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblOvality.Location = New System.Drawing.Point(11, 225)
        Me.lblOvality.Name = "lblOvality"
        Me.lblOvality.Size = New System.Drawing.Size(42, 17)
        Me.lblOvality.TabIndex = 70
        Me.lblOvality.Text = "Ovality:"
        '
        'lblSwell
        '
        Me.lblSwell.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.lblSwell.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblSwell.Location = New System.Drawing.Point(3, 206)
        Me.lblSwell.Name = "lblSwell"
        Me.lblSwell.Size = New System.Drawing.Size(50, 17)
        Me.lblSwell.TabIndex = 67
        Me.lblSwell.Text = "Swelling:"
        '
        'lblInternalFouling
        '
        Me.lblInternalFouling.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.lblInternalFouling.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblInternalFouling.Location = New System.Drawing.Point(88, 268)
        Me.lblInternalFouling.Name = "lblInternalFouling"
        Me.lblInternalFouling.Size = New System.Drawing.Size(92, 17)
        Me.lblInternalFouling.TabIndex = 146
        Me.lblInternalFouling.Text = "Internal Fouling:"
        '
        'cbInternalFoulingText
        '
        Me.cbInternalFoulingText.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "InternalFoulingOrNot", True))
        Me.cbInternalFoulingText.Location = New System.Drawing.Point(186, 268)
        Me.cbInternalFoulingText.Name = "cbInternalFoulingText"
        Me.cbInternalFoulingText.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.cbInternalFoulingText.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(31, Byte), Integer), CType(CType(53, Byte), Integer))
        Me.cbInternalFoulingText.Properties.Appearance.Options.UseFont = True
        Me.cbInternalFoulingText.Properties.Appearance.Options.UseForeColor = True
        Me.cbInternalFoulingText.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbInternalFoulingText.Properties.AppearanceDisabled.Options.UseFont = True
        Me.cbInternalFoulingText.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbInternalFoulingText.Properties.AppearanceDropDown.Options.UseFont = True
        Me.cbInternalFoulingText.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbInternalFoulingText.Properties.AppearanceFocused.Options.UseFont = True
        Me.cbInternalFoulingText.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbInternalFoulingText.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.cbInternalFoulingText.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        SerializableAppearanceObject8.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject8.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject8.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject8.Options.UseBackColor = True
        SerializableAppearanceObject8.Options.UseBorderColor = True
        Me.cbInternalFoulingText.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject8, "", Nothing, Nothing, True)})
        Me.cbInternalFoulingText.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.cbInternalFoulingText.Properties.Items.AddRange(New Object() {"", "in one pass.", "in several passes.", "in a majority of the passes.", "in all of the passes."})
        Me.cbInternalFoulingText.Size = New System.Drawing.Size(156, 24)
        Me.cbInternalFoulingText.TabIndex = 220
        '
        'radPipeOrBend
        '
        Me.radPipeOrBend.EditValue = "Pipe"
        Me.radPipeOrBend.Location = New System.Drawing.Point(554, 24)
        Me.radPipeOrBend.Name = "radPipeOrBend"
        Me.radPipeOrBend.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.radPipeOrBend.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radPipeOrBend.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.radPipeOrBend.Properties.Appearance.Options.UseBackColor = True
        Me.radPipeOrBend.Properties.Appearance.Options.UseFont = True
        Me.radPipeOrBend.Properties.Appearance.Options.UseForeColor = True
        Me.radPipeOrBend.Properties.Columns = 2
        Me.radPipeOrBend.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem("Pipe", "Pipe"), New DevExpress.XtraEditors.Controls.RadioGroupItem("Bend", "Bend")})
        Me.radPipeOrBend.Size = New System.Drawing.Size(119, 27)
        Me.radPipeOrBend.TabIndex = 149
        '
        'taTblSummary
        '
        Me.taTblSummary.ClearBeforeFill = True
        '
        'taTblSummaryPipeData
        '
        Me.taTblSummaryPipeData.ClearBeforeFill = True
        '
        'taTblSummaryBendData
        '
        Me.taTblSummaryBendData.ClearBeforeFill = True
        '
        'taTblSummaryBendDataInfo
        '
        Me.taTblSummaryBendDataInfo.ClearBeforeFill = True
        '
        'taTblSummaryPipeDataInfo
        '
        Me.taTblSummaryPipeDataInfo.ClearBeforeFill = True
        '
        'bsSummaryBendDataInfo
        '
        Me.bsSummaryBendDataInfo.DataMember = "tblSummaryBendDataInfo"
        Me.bsSummaryBendDataInfo.DataSource = Me.QTT_SummaryData
        '
        'btnPaste
        '
        Me.btnPaste.Appearance.BackColor = System.Drawing.Color.White
        Me.btnPaste.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.btnPaste.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.btnPaste.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPaste.Appearance.Options.UseBackColor = True
        Me.btnPaste.Appearance.Options.UseBorderColor = True
        Me.btnPaste.Appearance.Options.UseFont = True
        Me.btnPaste.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.btnPaste.Location = New System.Drawing.Point(785, 26)
        Me.btnPaste.Name = "btnPaste"
        Me.btnPaste.Size = New System.Drawing.Size(110, 33)
        Me.btnPaste.TabIndex = 150
        Me.btnPaste.Text = "Paste Excel Data"
        '
        'taTblPipeTubeItems
        '
        Me.taTblPipeTubeItems.ClearBeforeFill = True
        '
        'cmbPipeSectionName
        '
        Me.cmbPipeSectionName.EditValue = "Convection Piping"
        Me.cmbPipeSectionName.Location = New System.Drawing.Point(353, 25)
        Me.cmbPipeSectionName.Name = "cmbPipeSectionName"
        Me.cmbPipeSectionName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPipeSectionName.Properties.Appearance.Options.UseFont = True
        Me.cmbPipeSectionName.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPipeSectionName.Properties.AppearanceDisabled.Options.UseFont = True
        Me.cmbPipeSectionName.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPipeSectionName.Properties.AppearanceDropDown.Options.UseFont = True
        Me.cmbPipeSectionName.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPipeSectionName.Properties.AppearanceFocused.Options.UseFont = True
        Me.cmbPipeSectionName.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPipeSectionName.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.cmbPipeSectionName.Properties.AutoHeight = False
        SerializableAppearanceObject9.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject9.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject9.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject9.Options.UseBackColor = True
        SerializableAppearanceObject9.Options.UseBorderColor = True
        Me.cmbPipeSectionName.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject9, "", Nothing, Nothing, True)})
        Me.cmbPipeSectionName.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.cmbPipeSectionName.Properties.CycleOnDblClick = False
        Me.cmbPipeSectionName.Properties.Items.AddRange(New Object() {"Convection Piping", "Radiant Piping", "Economizer Piping", "Crossover Piping", "Custom-Name Piping Slot"})
        Me.cmbPipeSectionName.Size = New System.Drawing.Size(195, 24)
        Me.cmbPipeSectionName.TabIndex = 225
        '
        'uiDeformationDataGrid
        '
        Me.uiDeformationDataGrid.DataSource = Me.bsSummaryPipeOrBendData
        Me.uiDeformationDataGrid.Location = New System.Drawing.Point(56, 145)
        Me.uiDeformationDataGrid.MainView = Me.uiDeformationDataView
        Me.uiDeformationDataGrid.Name = "uiDeformationDataGrid"
        Me.uiDeformationDataGrid.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.CountSelector, Me.decimalSelector, Me.PassSelector})
        Me.uiDeformationDataGrid.Size = New System.Drawing.Size(286, 117)
        Me.uiDeformationDataGrid.TabIndex = 226
        Me.uiDeformationDataGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uiDeformationDataView, Me.GridView1})
        '
        'uiDeformationDataView
        '
        Me.uiDeformationDataView.Appearance.SelectedRow.Options.UseBackColor = True
        Me.uiDeformationDataView.Appearance.SelectedRow.Options.UseBorderColor = True
        Me.uiDeformationDataView.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.bndAbove, Me.bndBelow, Me.bndThreshold, Me.bndWorstPipeID, Me.bndPass, Me.bndWorstDeformation})
        Me.uiDeformationDataView.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiDeformationDataView.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.colBulgingThreshold, Me.colBulgingPipeID, Me.colBulgingPass, Me.colOvalityThreshold, Me.colOvalityPipeID, Me.colOvalityPass, Me.colSwellingThreshold, Me.colSwellingPipeID, Me.colSwellingPass, Me.colDentingThreshold, Me.colDentingPipeID, Me.colBulgingNotExceedingCount, Me.colBulgingExceedingCount, Me.colOvalityNotExceedingCount, Me.colOvalityExceedingCount, Me.colSwellingNotExceedingCount, Me.colSwellingExceedingCount, Me.colDentingNotExceedingCount, Me.colDentingExceedingCount, Me.colDentingPass, Me.colBulgingWorstDefo, Me.colDentingWorstDefo, Me.colOvalityWorstDefo, Me.colSwellingWorstDefo})
        Me.uiDeformationDataView.DetailTabHeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom
        Me.uiDeformationDataView.GridControl = Me.uiDeformationDataGrid
        Me.uiDeformationDataView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden
        Me.uiDeformationDataView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never
        Me.uiDeformationDataView.Name = "uiDeformationDataView"
        Me.uiDeformationDataView.OptionsBehavior.AutoUpdateTotalSummary = False
        Me.uiDeformationDataView.OptionsCustomization.AllowFilter = False
        Me.uiDeformationDataView.OptionsCustomization.AllowSort = False
        Me.uiDeformationDataView.OptionsDetail.AllowZoomDetail = False
        Me.uiDeformationDataView.OptionsDetail.EnableMasterViewMode = False
        Me.uiDeformationDataView.OptionsDetail.ShowDetailTabs = False
        Me.uiDeformationDataView.OptionsDetail.SmartDetailExpand = False
        Me.uiDeformationDataView.OptionsMenu.EnableColumnMenu = False
        Me.uiDeformationDataView.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.uiDeformationDataView.OptionsSelection.EnableAppearanceFocusedRow = False
        Me.uiDeformationDataView.OptionsView.GroupDrawMode = DevExpress.XtraGrid.Views.Grid.GroupDrawMode.Office2003
        Me.uiDeformationDataView.OptionsView.ShowColumnHeaders = False
        Me.uiDeformationDataView.OptionsView.ShowGroupPanel = False
        Me.uiDeformationDataView.OptionsView.ShowIndicator = False
        Me.uiDeformationDataView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
        Me.uiDeformationDataView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never
        '
        'bndAbove
        '
        Me.bndAbove.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bndAbove.AppearanceHeader.Options.UseFont = True
        Me.bndAbove.AppearanceHeader.Options.UseTextOptions = True
        Me.bndAbove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.bndAbove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.bndAbove.Caption = "Count Above"
        Me.bndAbove.Columns.Add(Me.colBulgingExceedingCount)
        Me.bndAbove.Columns.Add(Me.colSwellingExceedingCount)
        Me.bndAbove.Columns.Add(Me.colOvalityExceedingCount)
        Me.bndAbove.Columns.Add(Me.colDentingExceedingCount)
        Me.bndAbove.Name = "bndAbove"
        Me.bndAbove.Width = 39
        '
        'colBulgingExceedingCount
        '
        Me.colBulgingExceedingCount.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colBulgingExceedingCount.AppearanceCell.Options.UseFont = True
        Me.colBulgingExceedingCount.AppearanceCell.Options.UseTextOptions = True
        Me.colBulgingExceedingCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colBulgingExceedingCount.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colBulgingExceedingCount.AppearanceHeader.Options.UseFont = True
        Me.colBulgingExceedingCount.ColumnEdit = Me.CountSelector
        Me.colBulgingExceedingCount.FieldName = "BulgingExceedingCount"
        Me.colBulgingExceedingCount.Name = "colBulgingExceedingCount"
        Me.colBulgingExceedingCount.Visible = True
        Me.colBulgingExceedingCount.Width = 39
        '
        'CountSelector
        '
        Me.CountSelector.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.GrayText
        Me.CountSelector.AppearanceReadOnly.BackColor2 = System.Drawing.SystemColors.GrayText
        Me.CountSelector.AppearanceReadOnly.BorderColor = System.Drawing.SystemColors.GrayText
        Me.CountSelector.AppearanceReadOnly.Options.UseBackColor = True
        Me.CountSelector.AppearanceReadOnly.Options.UseBorderColor = True
        Me.CountSelector.AutoHeight = False
        Me.CountSelector.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.CountSelector.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.CountSelector.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.CountSelector.IsFloatValue = False
        Me.CountSelector.Mask.EditMask = "N00"
        Me.CountSelector.MaxValue = New Decimal(New Integer() {25, 0, 0, 0})
        Me.CountSelector.Name = "CountSelector"
        '
        'colSwellingExceedingCount
        '
        Me.colSwellingExceedingCount.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colSwellingExceedingCount.AppearanceCell.Options.UseFont = True
        Me.colSwellingExceedingCount.AppearanceCell.Options.UseTextOptions = True
        Me.colSwellingExceedingCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colSwellingExceedingCount.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colSwellingExceedingCount.AppearanceHeader.Options.UseFont = True
        Me.colSwellingExceedingCount.ColumnEdit = Me.CountSelector
        Me.colSwellingExceedingCount.FieldName = "SwellingExceedingCount"
        Me.colSwellingExceedingCount.Name = "colSwellingExceedingCount"
        Me.colSwellingExceedingCount.RowIndex = 1
        Me.colSwellingExceedingCount.Visible = True
        Me.colSwellingExceedingCount.Width = 39
        '
        'colOvalityExceedingCount
        '
        Me.colOvalityExceedingCount.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colOvalityExceedingCount.AppearanceCell.Options.UseFont = True
        Me.colOvalityExceedingCount.AppearanceCell.Options.UseTextOptions = True
        Me.colOvalityExceedingCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colOvalityExceedingCount.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colOvalityExceedingCount.AppearanceHeader.Options.UseFont = True
        Me.colOvalityExceedingCount.ColumnEdit = Me.CountSelector
        Me.colOvalityExceedingCount.FieldName = "OvalityExceedingCount"
        Me.colOvalityExceedingCount.Name = "colOvalityExceedingCount"
        Me.colOvalityExceedingCount.RowIndex = 2
        Me.colOvalityExceedingCount.Visible = True
        Me.colOvalityExceedingCount.Width = 39
        '
        'colDentingExceedingCount
        '
        Me.colDentingExceedingCount.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDentingExceedingCount.AppearanceCell.Options.UseFont = True
        Me.colDentingExceedingCount.AppearanceCell.Options.UseTextOptions = True
        Me.colDentingExceedingCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colDentingExceedingCount.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDentingExceedingCount.AppearanceHeader.Options.UseFont = True
        Me.colDentingExceedingCount.ColumnEdit = Me.CountSelector
        Me.colDentingExceedingCount.FieldName = "DentingExceedingCount"
        Me.colDentingExceedingCount.Name = "colDentingExceedingCount"
        Me.colDentingExceedingCount.RowIndex = 3
        Me.colDentingExceedingCount.Visible = True
        Me.colDentingExceedingCount.Width = 39
        '
        'bndBelow
        '
        Me.bndBelow.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.bndBelow.AppearanceHeader.Options.UseFont = True
        Me.bndBelow.AppearanceHeader.Options.UseTextOptions = True
        Me.bndBelow.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.bndBelow.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.bndBelow.Caption = "Count Below"
        Me.bndBelow.Columns.Add(Me.colBulgingNotExceedingCount)
        Me.bndBelow.Columns.Add(Me.colSwellingNotExceedingCount)
        Me.bndBelow.Columns.Add(Me.colOvalityNotExceedingCount)
        Me.bndBelow.Columns.Add(Me.colDentingNotExceedingCount)
        Me.bndBelow.Name = "bndBelow"
        Me.bndBelow.Width = 41
        '
        'colBulgingNotExceedingCount
        '
        Me.colBulgingNotExceedingCount.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colBulgingNotExceedingCount.AppearanceCell.Options.UseFont = True
        Me.colBulgingNotExceedingCount.AppearanceCell.Options.UseTextOptions = True
        Me.colBulgingNotExceedingCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colBulgingNotExceedingCount.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colBulgingNotExceedingCount.AppearanceHeader.Options.UseFont = True
        Me.colBulgingNotExceedingCount.ColumnEdit = Me.CountSelector
        Me.colBulgingNotExceedingCount.FieldName = "BulgingNotExceedingCount"
        Me.colBulgingNotExceedingCount.Name = "colBulgingNotExceedingCount"
        Me.colBulgingNotExceedingCount.Visible = True
        Me.colBulgingNotExceedingCount.Width = 41
        '
        'colSwellingNotExceedingCount
        '
        Me.colSwellingNotExceedingCount.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colSwellingNotExceedingCount.AppearanceCell.Options.UseFont = True
        Me.colSwellingNotExceedingCount.AppearanceCell.Options.UseTextOptions = True
        Me.colSwellingNotExceedingCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colSwellingNotExceedingCount.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colSwellingNotExceedingCount.AppearanceHeader.Options.UseFont = True
        Me.colSwellingNotExceedingCount.ColumnEdit = Me.CountSelector
        Me.colSwellingNotExceedingCount.FieldName = "SwellingNotExceedingCount"
        Me.colSwellingNotExceedingCount.Name = "colSwellingNotExceedingCount"
        Me.colSwellingNotExceedingCount.RowIndex = 1
        Me.colSwellingNotExceedingCount.Visible = True
        Me.colSwellingNotExceedingCount.Width = 41
        '
        'colOvalityNotExceedingCount
        '
        Me.colOvalityNotExceedingCount.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colOvalityNotExceedingCount.AppearanceCell.Options.UseFont = True
        Me.colOvalityNotExceedingCount.AppearanceCell.Options.UseTextOptions = True
        Me.colOvalityNotExceedingCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colOvalityNotExceedingCount.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colOvalityNotExceedingCount.AppearanceHeader.Options.UseFont = True
        Me.colOvalityNotExceedingCount.ColumnEdit = Me.CountSelector
        Me.colOvalityNotExceedingCount.FieldName = "OvalityNotExceedingCount"
        Me.colOvalityNotExceedingCount.Name = "colOvalityNotExceedingCount"
        Me.colOvalityNotExceedingCount.RowIndex = 2
        Me.colOvalityNotExceedingCount.Visible = True
        Me.colOvalityNotExceedingCount.Width = 41
        '
        'colDentingNotExceedingCount
        '
        Me.colDentingNotExceedingCount.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDentingNotExceedingCount.AppearanceCell.Options.UseFont = True
        Me.colDentingNotExceedingCount.AppearanceCell.Options.UseTextOptions = True
        Me.colDentingNotExceedingCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colDentingNotExceedingCount.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDentingNotExceedingCount.AppearanceHeader.Options.UseFont = True
        Me.colDentingNotExceedingCount.ColumnEdit = Me.CountSelector
        Me.colDentingNotExceedingCount.FieldName = "DentingNotExceedingCount"
        Me.colDentingNotExceedingCount.Name = "colDentingNotExceedingCount"
        Me.colDentingNotExceedingCount.RowIndex = 3
        Me.colDentingNotExceedingCount.Visible = True
        Me.colDentingNotExceedingCount.Width = 41
        '
        'bndThreshold
        '
        Me.bndThreshold.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.bndThreshold.AppearanceHeader.Options.UseFont = True
        Me.bndThreshold.AppearanceHeader.Options.UseTextOptions = True
        Me.bndThreshold.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.bndThreshold.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.bndThreshold.Caption = "Threshold %"
        Me.bndThreshold.Columns.Add(Me.colBulgingThreshold)
        Me.bndThreshold.Columns.Add(Me.colSwellingThreshold)
        Me.bndThreshold.Columns.Add(Me.colOvalityThreshold)
        Me.bndThreshold.Columns.Add(Me.colDentingThreshold)
        Me.bndThreshold.Name = "bndThreshold"
        Me.bndThreshold.Width = 60
        '
        'colBulgingThreshold
        '
        Me.colBulgingThreshold.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colBulgingThreshold.AppearanceCell.Options.UseFont = True
        Me.colBulgingThreshold.AppearanceCell.Options.UseTextOptions = True
        Me.colBulgingThreshold.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colBulgingThreshold.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colBulgingThreshold.AppearanceHeader.Options.UseFont = True
        Me.colBulgingThreshold.ColumnEdit = Me.decimalSelector
        Me.colBulgingThreshold.FieldName = "BulgingThreshold"
        Me.colBulgingThreshold.Name = "colBulgingThreshold"
        Me.colBulgingThreshold.Visible = True
        Me.colBulgingThreshold.Width = 60
        '
        'decimalSelector
        '
        Me.decimalSelector.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.GrayText
        Me.decimalSelector.AppearanceReadOnly.BackColor2 = System.Drawing.SystemColors.GrayText
        Me.decimalSelector.AppearanceReadOnly.BorderColor = System.Drawing.SystemColors.GrayText
        Me.decimalSelector.AppearanceReadOnly.Options.UseBackColor = True
        Me.decimalSelector.AppearanceReadOnly.Options.UseBorderColor = True
        Me.decimalSelector.AutoHeight = False
        Me.decimalSelector.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.decimalSelector.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.decimalSelector.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.decimalSelector.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.decimalSelector.Mask.EditMask = "#0.0"
        Me.decimalSelector.Mask.UseMaskAsDisplayFormat = True
        Me.decimalSelector.MaxValue = New Decimal(New Integer() {100, 0, 0, 0})
        Me.decimalSelector.Name = "decimalSelector"
        '
        'colSwellingThreshold
        '
        Me.colSwellingThreshold.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colSwellingThreshold.AppearanceCell.Options.UseFont = True
        Me.colSwellingThreshold.AppearanceCell.Options.UseTextOptions = True
        Me.colSwellingThreshold.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colSwellingThreshold.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colSwellingThreshold.AppearanceHeader.Options.UseFont = True
        Me.colSwellingThreshold.ColumnEdit = Me.decimalSelector
        Me.colSwellingThreshold.FieldName = "SwellingThreshold"
        Me.colSwellingThreshold.Name = "colSwellingThreshold"
        Me.colSwellingThreshold.RowIndex = 1
        Me.colSwellingThreshold.Visible = True
        Me.colSwellingThreshold.Width = 60
        '
        'colOvalityThreshold
        '
        Me.colOvalityThreshold.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colOvalityThreshold.AppearanceCell.Options.UseFont = True
        Me.colOvalityThreshold.AppearanceCell.Options.UseTextOptions = True
        Me.colOvalityThreshold.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colOvalityThreshold.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colOvalityThreshold.AppearanceHeader.Options.UseFont = True
        Me.colOvalityThreshold.ColumnEdit = Me.decimalSelector
        Me.colOvalityThreshold.FieldName = "OvalityThreshold"
        Me.colOvalityThreshold.Name = "colOvalityThreshold"
        Me.colOvalityThreshold.RowIndex = 2
        Me.colOvalityThreshold.Visible = True
        Me.colOvalityThreshold.Width = 60
        '
        'colDentingThreshold
        '
        Me.colDentingThreshold.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDentingThreshold.AppearanceCell.Options.UseFont = True
        Me.colDentingThreshold.AppearanceCell.Options.UseTextOptions = True
        Me.colDentingThreshold.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colDentingThreshold.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDentingThreshold.AppearanceHeader.Options.UseFont = True
        Me.colDentingThreshold.ColumnEdit = Me.decimalSelector
        Me.colDentingThreshold.FieldName = "DentingThreshold"
        Me.colDentingThreshold.Name = "colDentingThreshold"
        Me.colDentingThreshold.RowIndex = 3
        Me.colDentingThreshold.Visible = True
        Me.colDentingThreshold.Width = 60
        '
        'bndWorstPipeID
        '
        Me.bndWorstPipeID.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.bndWorstPipeID.AppearanceHeader.Options.UseFont = True
        Me.bndWorstPipeID.AppearanceHeader.Options.UseTextOptions = True
        Me.bndWorstPipeID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.bndWorstPipeID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.bndWorstPipeID.Caption = "Worst Pipe ID"
        Me.bndWorstPipeID.Columns.Add(Me.colBulgingPipeID)
        Me.bndWorstPipeID.Columns.Add(Me.colSwellingPipeID)
        Me.bndWorstPipeID.Columns.Add(Me.colOvalityPipeID)
        Me.bndWorstPipeID.Columns.Add(Me.colDentingPipeID)
        Me.bndWorstPipeID.Name = "bndWorstPipeID"
        Me.bndWorstPipeID.Width = 53
        '
        'colBulgingPipeID
        '
        Me.colBulgingPipeID.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colBulgingPipeID.AppearanceCell.Options.UseFont = True
        Me.colBulgingPipeID.AppearanceCell.Options.UseTextOptions = True
        Me.colBulgingPipeID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colBulgingPipeID.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colBulgingPipeID.AppearanceHeader.Options.UseFont = True
        Me.colBulgingPipeID.FieldName = "BulgingPipeID"
        Me.colBulgingPipeID.Name = "colBulgingPipeID"
        Me.colBulgingPipeID.Visible = True
        Me.colBulgingPipeID.Width = 53
        '
        'colSwellingPipeID
        '
        Me.colSwellingPipeID.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colSwellingPipeID.AppearanceCell.Options.UseFont = True
        Me.colSwellingPipeID.AppearanceCell.Options.UseTextOptions = True
        Me.colSwellingPipeID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colSwellingPipeID.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colSwellingPipeID.AppearanceHeader.Options.UseFont = True
        Me.colSwellingPipeID.FieldName = "SwellingPipeID"
        Me.colSwellingPipeID.Name = "colSwellingPipeID"
        Me.colSwellingPipeID.RowIndex = 1
        Me.colSwellingPipeID.Visible = True
        Me.colSwellingPipeID.Width = 53
        '
        'colOvalityPipeID
        '
        Me.colOvalityPipeID.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colOvalityPipeID.AppearanceCell.Options.UseFont = True
        Me.colOvalityPipeID.AppearanceCell.Options.UseTextOptions = True
        Me.colOvalityPipeID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colOvalityPipeID.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colOvalityPipeID.AppearanceHeader.Options.UseFont = True
        Me.colOvalityPipeID.FieldName = "OvalityPipeID"
        Me.colOvalityPipeID.Name = "colOvalityPipeID"
        Me.colOvalityPipeID.RowIndex = 2
        Me.colOvalityPipeID.Visible = True
        Me.colOvalityPipeID.Width = 53
        '
        'colDentingPipeID
        '
        Me.colDentingPipeID.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDentingPipeID.AppearanceCell.Options.UseFont = True
        Me.colDentingPipeID.AppearanceCell.Options.UseTextOptions = True
        Me.colDentingPipeID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colDentingPipeID.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDentingPipeID.AppearanceHeader.Options.UseFont = True
        Me.colDentingPipeID.FieldName = "DentingPipeID"
        Me.colDentingPipeID.Name = "colDentingPipeID"
        Me.colDentingPipeID.RowIndex = 3
        Me.colDentingPipeID.Visible = True
        Me.colDentingPipeID.Width = 53
        '
        'bndPass
        '
        Me.bndPass.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.bndPass.AppearanceHeader.Options.UseFont = True
        Me.bndPass.AppearanceHeader.Options.UseTextOptions = True
        Me.bndPass.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.bndPass.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.bndPass.Caption = "Worst Pass"
        Me.bndPass.Columns.Add(Me.colBulgingPass)
        Me.bndPass.Columns.Add(Me.colSwellingPass)
        Me.bndPass.Columns.Add(Me.colOvalityPass)
        Me.bndPass.Columns.Add(Me.colDentingPass)
        Me.bndPass.Name = "bndPass"
        Me.bndPass.Width = 48
        '
        'colBulgingPass
        '
        Me.colBulgingPass.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colBulgingPass.AppearanceCell.Options.UseFont = True
        Me.colBulgingPass.AppearanceCell.Options.UseTextOptions = True
        Me.colBulgingPass.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colBulgingPass.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colBulgingPass.AppearanceHeader.Options.UseFont = True
        Me.colBulgingPass.ColumnEdit = Me.PassSelector
        Me.colBulgingPass.FieldName = "BulgingPass"
        Me.colBulgingPass.Name = "colBulgingPass"
        Me.colBulgingPass.Visible = True
        Me.colBulgingPass.Width = 48
        '
        'PassSelector
        '
        Me.PassSelector.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.GrayText
        Me.PassSelector.AppearanceReadOnly.BackColor2 = System.Drawing.SystemColors.GrayText
        Me.PassSelector.AppearanceReadOnly.BorderColor = System.Drawing.SystemColors.GrayText
        Me.PassSelector.AppearanceReadOnly.Options.UseBackColor = True
        Me.PassSelector.AppearanceReadOnly.Options.UseBorderColor = True
        Me.PassSelector.AutoHeight = False
        Me.PassSelector.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PassSelector.DropDownRows = 4
        Me.PassSelector.Items.AddRange(New Object() {"1", "2", "3", "4"})
        Me.PassSelector.Name = "PassSelector"
        '
        'colSwellingPass
        '
        Me.colSwellingPass.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colSwellingPass.AppearanceCell.Options.UseFont = True
        Me.colSwellingPass.AppearanceCell.Options.UseTextOptions = True
        Me.colSwellingPass.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colSwellingPass.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colSwellingPass.AppearanceHeader.Options.UseFont = True
        Me.colSwellingPass.ColumnEdit = Me.PassSelector
        Me.colSwellingPass.FieldName = "SwellingPass"
        Me.colSwellingPass.Name = "colSwellingPass"
        Me.colSwellingPass.RowIndex = 1
        Me.colSwellingPass.Visible = True
        Me.colSwellingPass.Width = 48
        '
        'colOvalityPass
        '
        Me.colOvalityPass.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colOvalityPass.AppearanceCell.Options.UseFont = True
        Me.colOvalityPass.AppearanceCell.Options.UseTextOptions = True
        Me.colOvalityPass.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colOvalityPass.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colOvalityPass.AppearanceHeader.Options.UseFont = True
        Me.colOvalityPass.ColumnEdit = Me.PassSelector
        Me.colOvalityPass.FieldName = "OvalityPass"
        Me.colOvalityPass.Name = "colOvalityPass"
        Me.colOvalityPass.RowIndex = 2
        Me.colOvalityPass.Visible = True
        Me.colOvalityPass.Width = 48
        '
        'colDentingPass
        '
        Me.colDentingPass.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDentingPass.AppearanceCell.Options.UseFont = True
        Me.colDentingPass.AppearanceCell.Options.UseTextOptions = True
        Me.colDentingPass.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colDentingPass.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDentingPass.AppearanceHeader.Options.UseFont = True
        Me.colDentingPass.ColumnEdit = Me.PassSelector
        Me.colDentingPass.FieldName = "DentingPass"
        Me.colDentingPass.Name = "colDentingPass"
        Me.colDentingPass.RowIndex = 3
        Me.colDentingPass.Visible = True
        Me.colDentingPass.Width = 48
        '
        'bndWorstDeformation
        '
        Me.bndWorstDeformation.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.bndWorstDeformation.AppearanceHeader.Options.UseFont = True
        Me.bndWorstDeformation.AppearanceHeader.Options.UseTextOptions = True
        Me.bndWorstDeformation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.bndWorstDeformation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.bndWorstDeformation.Caption = "Worst %"
        Me.bndWorstDeformation.Columns.Add(Me.colBulgingWorstDefo)
        Me.bndWorstDeformation.Columns.Add(Me.colSwellingWorstDefo)
        Me.bndWorstDeformation.Columns.Add(Me.colOvalityWorstDefo)
        Me.bndWorstDeformation.Columns.Add(Me.colDentingWorstDefo)
        Me.bndWorstDeformation.Name = "bndWorstDeformation"
        Me.bndWorstDeformation.RowCount = 2
        Me.bndWorstDeformation.Width = 45
        '
        'colBulgingWorstDefo
        '
        Me.colBulgingWorstDefo.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colBulgingWorstDefo.AppearanceCell.Options.UseFont = True
        Me.colBulgingWorstDefo.AppearanceCell.Options.UseTextOptions = True
        Me.colBulgingWorstDefo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colBulgingWorstDefo.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colBulgingWorstDefo.AppearanceHeader.Options.UseFont = True
        Me.colBulgingWorstDefo.ColumnEdit = Me.decimalSelector
        Me.colBulgingWorstDefo.FieldName = "BulgingWorstDefo"
        Me.colBulgingWorstDefo.Name = "colBulgingWorstDefo"
        Me.colBulgingWorstDefo.Visible = True
        Me.colBulgingWorstDefo.Width = 45
        '
        'colSwellingWorstDefo
        '
        Me.colSwellingWorstDefo.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colSwellingWorstDefo.AppearanceCell.Options.UseFont = True
        Me.colSwellingWorstDefo.AppearanceCell.Options.UseTextOptions = True
        Me.colSwellingWorstDefo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colSwellingWorstDefo.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colSwellingWorstDefo.AppearanceHeader.Options.UseFont = True
        Me.colSwellingWorstDefo.ColumnEdit = Me.decimalSelector
        Me.colSwellingWorstDefo.FieldName = "SwellingWorstDefo"
        Me.colSwellingWorstDefo.Name = "colSwellingWorstDefo"
        Me.colSwellingWorstDefo.RowIndex = 1
        Me.colSwellingWorstDefo.Visible = True
        Me.colSwellingWorstDefo.Width = 45
        '
        'colOvalityWorstDefo
        '
        Me.colOvalityWorstDefo.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colOvalityWorstDefo.AppearanceCell.Options.UseFont = True
        Me.colOvalityWorstDefo.AppearanceCell.Options.UseTextOptions = True
        Me.colOvalityWorstDefo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colOvalityWorstDefo.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colOvalityWorstDefo.AppearanceHeader.Options.UseFont = True
        Me.colOvalityWorstDefo.ColumnEdit = Me.decimalSelector
        Me.colOvalityWorstDefo.FieldName = "OvalityWorstDefo"
        Me.colOvalityWorstDefo.Name = "colOvalityWorstDefo"
        Me.colOvalityWorstDefo.RowIndex = 2
        Me.colOvalityWorstDefo.Visible = True
        Me.colOvalityWorstDefo.Width = 45
        '
        'colDentingWorstDefo
        '
        Me.colDentingWorstDefo.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDentingWorstDefo.AppearanceCell.Options.UseFont = True
        Me.colDentingWorstDefo.AppearanceCell.Options.UseTextOptions = True
        Me.colDentingWorstDefo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colDentingWorstDefo.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDentingWorstDefo.AppearanceHeader.Options.UseFont = True
        Me.colDentingWorstDefo.ColumnEdit = Me.decimalSelector
        Me.colDentingWorstDefo.FieldName = "DentingWorstDefo"
        Me.colDentingWorstDefo.Name = "colDentingWorstDefo"
        Me.colDentingWorstDefo.RowIndex = 3
        Me.colDentingWorstDefo.Visible = True
        Me.colDentingWorstDefo.Width = 45
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.uiDeformationDataGrid
        Me.GridView1.Name = "GridView1"
        '
        'lblBulge
        '
        Me.lblBulge.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.lblBulge.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblBulge.Location = New System.Drawing.Point(7, 186)
        Me.lblBulge.Name = "lblBulge"
        Me.lblBulge.Size = New System.Drawing.Size(46, 17)
        Me.lblBulge.TabIndex = 64
        Me.lblBulge.Text = "Bulging:"
        '
        'ucExecutiveSummary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.uiDeformationDataGrid)
        Me.Controls.Add(Me.cmbPipeSectionName)
        Me.Controls.Add(Me.btnPaste)
        Me.Controls.Add(Me.radPipeOrBend)
        Me.Controls.Add(Me.uiPipeOrBendDataInfo)
        Me.Controls.Add(Me.uiScheduleSize)
        Me.Controls.Add(Me.cbInternalFoulingText)
        Me.Controls.Add(Me.uiPipeSize)
        Me.Controls.Add(Me.lblInternalFouling)
        Me.Controls.Add(Me.uiInternalOrExternal)
        Me.Controls.Add(Me.uilblGeneralOrLocal)
        Me.Controls.Add(Me.uiGeneralOrLocal)
        Me.Controls.Add(Me.uilblInternalOrExternal)
        Me.Controls.Add(Me.uilblPipeSize)
        Me.Controls.Add(Me.uilblPipeSchedule)
        Me.Controls.Add(Me.btnWriteExecutiveSummary)
        Me.Controls.Add(Me.uiExecutiveSummaryText)
        Me.Controls.Add(Me.lblDeformations)
        Me.Controls.Add(Me.lblDent)
        Me.Controls.Add(Me.lblOvality)
        Me.Controls.Add(Me.lblSwell)
        Me.Controls.Add(Me.lblBulge)
        Me.Controls.Add(Me.Label93)
        Me.Controls.Add(Me.uiUnits)
        Me.Controls.Add(Me.uiPassNumber)
        Me.Controls.Add(Me.uiNewUsedPipes)
        Me.Controls.Add(Me.uilblNewUsedPipes)
        Me.Controls.Add(Me.uilblUnits)
        Me.Controls.Add(Me.uilblPassNumber)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Name = "ucExecutiveSummary"
        Me.Size = New System.Drawing.Size(898, 360)
        CType(Me.uiPipeOrBendDataInfo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiPipeOrBendDataInfoView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsSummaryPipeDataInfo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.QTT_SummaryData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsSummaryPipeOrBendData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsExecutiveSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiScheduleSize.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiPipeSize.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiInternalOrExternal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiGeneralOrLocal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiNewUsedPipes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiUnits.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiPassNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbInternalFoulingText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radPipeOrBend.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsSummaryBendDataInfo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbPipeSectionName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiDeformationDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiDeformationDataView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CountSelector, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.decimalSelector, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PassSelector, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents uilblNewUsedPipes As System.Windows.Forms.Label
    Friend WithEvents uilblPassNumber As System.Windows.Forms.Label
    Friend WithEvents uilblUnits As System.Windows.Forms.Label
    Friend WithEvents uiExecutiveSummaryText As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents uiNewUsedPipes As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiUnits As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiPassNumber As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiScheduleSize As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiPipeSize As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiInternalOrExternal As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uilblGeneralOrLocal As System.Windows.Forms.Label
    Friend WithEvents uiGeneralOrLocal As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uilblInternalOrExternal As System.Windows.Forms.Label
    Friend WithEvents uilblPipeSize As System.Windows.Forms.Label
    Friend WithEvents uilblPipeSchedule As System.Windows.Forms.Label
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents btnWriteExecutiveSummary As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblDeformations As System.Windows.Forms.Label
    Friend WithEvents lblDent As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblOvality As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblSwell As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblInternalFouling As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbInternalFoulingText As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiPipeOrBendDataInfo As DevExpress.XtraGrid.GridControl
    Friend WithEvents uiPipeOrBendDataInfoView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents radPipeOrBend As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents bsExecutiveSummary As System.Windows.Forms.BindingSource
    Friend WithEvents QTT_SummaryData As QIG.AutoReporter.QTT_SummaryData
    Friend WithEvents taTblSummary As QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryTableAdapter
    Friend WithEvents taTblSummaryPipeData As QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryPipeDataTableAdapter
    Friend WithEvents taTblSummaryBendData As QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryBendDataTableAdapter
    Friend WithEvents bsSummaryPipeOrBendData As System.Windows.Forms.BindingSource
    Friend WithEvents bsSummaryPipeDataInfo As System.Windows.Forms.BindingSource
    Friend WithEvents taTblSummaryBendDataInfo As QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryBendDataInfoTableAdapter
    Friend WithEvents taTblSummaryPipeDataInfo As QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryPipeDataInfoTableAdapter
    Friend WithEvents bsSummaryBendDataInfo As System.Windows.Forms.BindingSource
   Friend WithEvents btnPaste As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents taTblPipeTubeItems As QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblPipeTubeItemsTableAdapter
    Friend WithEvents cmbPipeSectionName As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiDeformationDataGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uiDeformationDataView As DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
    Friend WithEvents colBulgingExceedingCount As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colDentingExceedingCount As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colOvalityExceedingCount As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colSwellingExceedingCount As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colBulgingNotExceedingCount As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colDentingNotExceedingCount As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colOvalityNotExceedingCount As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colSwellingNotExceedingCount As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colBulgingThreshold As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colDentingThreshold As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colOvalityThreshold As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colSwellingThreshold As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colBulgingPipeID As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colDentingPipeID As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colOvalityPipeID As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colSwellingPipeID As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colBulgingPass As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colOvalityPass As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colSwellingPass As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents CountSelector As DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
    Friend WithEvents colDentingPass As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colBulgingWorstDefo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colDentingWorstDefo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colOvalityWorstDefo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colSwellingWorstDefo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents decimalSelector As DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
    Friend WithEvents lblBulge As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PassSelector As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents bndAbove As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents bndBelow As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents bndThreshold As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents bndWorstPipeID As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents bndPass As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents bndWorstDeformation As DevExpress.XtraGrid.Views.BandedGrid.GridBand

End Class
