Public Class CustomerDetails

    ' used to 'call back' to new inspection form for new customers or deletions of existing customers
    Private frmNewInspection As NewInspection

    ' used to store current customers ID and if customer is 'new'
    Private mintCurrentCustomerID As Integer
    Private mfNewCustomer As Boolean

    Public Property NewInspectionForm() As NewInspection
        Get
            Return frmNewInspection
        End Get
        Set(ByVal value As NewInspection)
            frmNewInspection = value
        End Set
    End Property

    Private Function SaveData() As Boolean
        bsCustomers.EndEdit()
        taTblCustomers.Update(QTT_InspectionsDataSets.tblCustomers)
        Return True
    End Function

    Private Sub NewCustomer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'KM 7/3/2011
        'Add countries and states to dropdowns
        Dim countries As List(Of String) = GetListOfCountries()
        uiCountry.Properties.Items.BeginUpdate()
        Dim i As Integer = countries.Count - 1
        For i = 0 To i
            uiCountry.Properties.Items.Add(countries(i))
        Next i
        uiCountry.Properties.Items.EndUpdate()
        uiState.Properties.Items.AddRange(GetListOfUSStates)
    End Sub
    Private Sub ProcessCallbacks(ByVal fSelectCurrentCustomer As Boolean)

        Dim intSelectID As Integer
        ' selection needed
        If fSelectCurrentCustomer Then intSelectID = mintCurrentCustomerID
        ' processes any needed callbacks to other forms
        If Not (frmNewInspection Is Nothing) Then
            frmNewInspection.SetupUI_CustomersList(intSelectID)
        End If

    End Sub

    Public Function SetupUI_Main(ByVal intCustomerID As Integer, ByVal fNewCustomer As Boolean, ByVal fReadOnly As Boolean) As Boolean

        'get customer from database
        Me.taTblCustomers.FillByCustomerID(Me.QTT_InspectionsDataSets.tblCustomers, intCustomerID)

        ' record current id and if new
        mintCurrentCustomerID = intCustomerID
        mfNewCustomer = fNewCustomer

        ' setup UI as per options
        uiDeleteButton.Enabled = Not (fNewCustomer Or fReadOnly)
        uiSaveButton.Enabled = Not (fNewCustomer Or fReadOnly)

        'KM 7/18/2011
        uiCompanyName.Properties.ReadOnly = fReadOnly
        AddressTextBox.ReadOnly = fReadOnly
        CityTextBox.ReadOnly = fReadOnly
        uiState.Properties.ReadOnly = fReadOnly
        PostalCodeTextBox.ReadOnly = fReadOnly
        uiCountry.Properties.ReadOnly = fReadOnly
        PhoneTextBox.ReadOnly = fReadOnly
        FaxTextBox.ReadOnly = fReadOnly
        ContactTextBox.ReadOnly = fReadOnly
        EmailTextBox.ReadOnly = fReadOnly

    End Function

    Private Function DeleteCurrentCustomer(ByVal fPromptFirst As Boolean) As Boolean
        'Deletes the current customer - if fPromptFirst, user is asked to confirm deletion
        'Message in legacy code: need to add code to ensure no inspections exist for current customers

        ' prompt as needed
        If fPromptFirst Then
            If MsgBoxResult.No = MsgBox("Are you sure you want to PERMANENTLY DELETE this customer from the QIG Inspection Reporter?", MsgBoxStyle.YesNo + MsgBoxStyle.Information + MsgBoxStyle.DefaultButton2, "Delete This Customer?") Then
                Exit Function
            End If
        End If

        ' if to this point, delete the current customer
        Try
            taTblCustomers.Delete(mintCurrentCustomerID)
        Catch ex As Exception
            MessageBox.Show("Some Inspections still use this Customer so it cannot be deleted." & Environment.NewLine & Environment.NewLine & "Delete the inspections using this Customer and try again.", "Cannot Delete Customer", MessageBoxButtons.OK)
        End Try

        Return True

    End Function


    Private Sub uiCountry_EditValueChanged(sender As Object, e As System.EventArgs) Handles uiCountry.EditValueChanged
        'Populate state/province field based on country selection
        'KM 7/3/2011

        If (Nz(uiCountry.EditValue) = "United States") Then
            uiState.Properties.Items.Clear()
            uiState.Properties.Items.AddRange(GetListOfUSStates)
            uiState.Properties.Buttons(0).Visible = True
        ElseIf (Nz(uiCountry.EditValue) = "Canada") Then
            uiState.Properties.Items.Clear()
            uiState.Properties.Items.AddRange(GetListOfCanadianProvinces)
            uiState.Properties.Buttons(0).Visible = True
        Else
            uiState.Properties.Items.Clear()
            uiState.Properties.Buttons(0).Visible = False
        End If

        If uiCountry.OldEditValue.ToString <> "" Then
            If uiCountry.OldEditValue.ToString <> uiCountry.EditValue Then uiState.EditValue = String.Empty
        End If

    End Sub
    'KM 7/3/2011
    Private Sub uiSaveButton_Click(sender As System.Object, e As System.EventArgs) Handles uiSaveButton.Click
        DxValidationProvider.Validate()
        If (DxValidationProvider.GetInvalidControls.Count > 0) Then
            MessageBox.Show(Me, "Please check for missing or incorrect data. The new customer cannot be created with the values you have provided.", "Missing or Incorrect Data", vbOKOnly, MessageBoxIcon.Error)
            Exit Sub
        End If

        Call SaveData()
        ' process callbacks as needed
        Call ProcessCallbacks(True)
        Me.Close()
    End Sub
    'KM 7/3/2011
    Private Sub uiDeleteButton_Click(sender As System.Object, e As System.EventArgs) Handles uiDeleteButton.Click
        Call DeleteCurrentCustomer(True)
        ' process callbacks as needed
        Call ProcessCallbacks(False)
        Me.Close()
    End Sub
    'KM 7/3/2011
    Private Sub uiCancelCloseButton_Click(sender As System.Object, e As System.EventArgs) Handles uiCancelCloseButton.Click
        ' processes user cancel
        ' delete 'new' records
        If mfNewCustomer Then
            'prompt
            ' if to here - delete new record
            Call DeleteCurrentCustomer(False)
        End If

        Me.Close()
    End Sub

    Private Sub uiCompanyName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles uiCompanyName.KeyPress
        uiSaveButton.Enabled = True
    End Sub
End Class
