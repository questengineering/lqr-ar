Imports DevExpress.XtraEditors.DXErrorProvider
Imports DevExpress.XtraRichEdit
Imports System.IO
Imports DevExpress.XtraSpellChecker
Imports System.Globalization
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraTab
Imports DevExpress.XtraGrid.Controls
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls
Imports System.Text.RegularExpressions
Imports DevExpress.XtraRichEdit.API.Native
Imports DevExpress.Utils
Imports QIG.AutoReporter.modNumToText.NumeriCon
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Core
Imports System.Linq.Expressions
Imports System.Runtime.CompilerServices
Imports System.Linq

Public Class InspectionDetails

#Region "Class Properties"

    ' Column number constants for Report Items DataViewGrid Control
    Const RPT_IMAGE_DESC_COL_INDEX = 4
    Const RPT_STATUS_COL_INDEX = 2
    Const RPT_FILENAME_COL_INDEX = 12

    ' Column number constants for Appendix Items DataViewGrid Control
    Const APX_IMAGE_DESC_COL_INDEX = 20
    Const APX_STATUS_COL_INDEX = 2
    Const APX_FILENAME_COL_INDEX = 12

    ' Constants for Default Text Entry Fields
    Const FIELD_PROJ_DESCRIPTION = "PROJECT_DESCRIPTION"
    Const FIELD_PROJ_DESCRIPTION_BOX = "PROJECT_DESCRIPTION_BOX"
    Const FIELD_PROJ_DESCRIPTION_CAN = "PROJECT_DESCRIPTION_CAN"
    Const FIELD_PROJ_SUMMARY = "PROJECT_SUMMARY"
    Const FIELD_PROJ_INSERVICE = "PROJECT_INSERVICE_SUMMARY"
    Const FIELD_PROJ_BASELINE = "PROJECT_BASELINE_SUMMARY"
    Const FIELD_INSPECTION_LAYOUT = "INSPECTION_LAYOUT"
    Const FIELD_PTI_SUMMARY = "PTI_SUMMARY"
    Const FIELD_PTI_INSPECTIONLOCATIONS = "PTI_INSPECTIONLOCATIONS"
    Const FIELD_PRIOR_HISTORY = "PRIOR_HISTORY"
    Const FIELD_FIRST_INSPECTION = "FIRST_INSPECTION"
    Const FIELD_NUMBERING_SYSTEM_CUSTOMER = "NUMBERING_SYSTEM_CUSTOMER"
    Const FIELD_NUMBERING_SYSTEM_QUEST = "NUMBERING_SYSTEM_QUEST"
    Const FIELD_TUBE_IDENTIFICATION_BOX_PLURAL = "TUBE_IDENTIFICATION_BOX_PL"
    Const FIELD_TUBE_IDENTIFICATION_BOX_SINGULAR = "TUBE_IDENTIFICATION_BOX_SING"
    Const FIELD_TUBE_IDENTIFICATION_CAN = "TUBE_IDENTIFICATION_CAN"
    Const FIELD_TUBE_IDENTIFICATION = "TUBE_IDENTIFICATION"

    'Constants for Inspection Types
    Const LOTIS = 3
    Const MANTIS = 5
    Const LOTIS_AGED = 6
    Const LOTIS_NEW = 7
    Const LOTIS_SPARE = 8
    Const MANTIS_AGED = 9
    Const MANTIS_NEW = 10
    Const MANTIS_SPARE = 11

    Private mfSetupCompleted As Boolean   'flags that main setup has finished
    Private mfVisible As Boolean = False       'flags that all fields should be visible
    Private mfTubesInAlpha As Boolean = False  'flags that Tubes for current inspection are alpha based
    Private mstrCustomTubes As String = Nothing  'holds custom tubes string
    Private mfRowsInAlpha As Boolean = False   'flags that Rows for current inspection are alpha based
    Private mstrCustomRows As String = Nothing  'holds custom rows string
    Private mstrSelectedUnits As String 'Holds the units the user wants, Imperial or Metric.
    Private mintInspectionID As Integer   ' store id of inspection
    Private mintInspectionType As Integer   ' store id of inspection type
    Private mfIsNew As Boolean  ' used to flag new versus existing inspections
    Private mstrMSWord_Template As String = ""   ' stores filename of MS Word template
    Private mfWasDeleted As Boolean ' used to flag deleted inspections
    Private mfPTI_AppendixesChecked As Boolean  ' used to flag when PipeTube appendix items sync needs to run
    Private mfCallCleanAppendixItems As Boolean  ' used to flag that 'clean appendix items should run on close
    Private bPreCheckTools As Boolean = False   ' KM used to flag precheck of checked listbox on open
    Private bVisible As Boolean = False    ' KM used to flag formatting marks on Description/Layout screen
    Private bVisible2 As Boolean = False   ' KM used to flag formatting marks on Executive Summary screen
    Private bVisible3 As Boolean = False ' KM used to flag formatting marks on Executive Summary screen 2
    Private bVisible4 As Boolean = False ' KM used to flag formatting marks on Section Summary screen
    Private mPipeTubeOptionsForcmbox As ArrayList
    Private bPreCheckPipeSchedules As Boolean = False 'JF Used to flag precheck of checklistbox on open
    Private frmCustomer As CustomerDetails 'KM used to show customer details
    Private ClearingPipeTextboxesForClarity As Boolean = False 'a flag to indicate when textboxes are being cleared internally rather than by a user so fields don't update.
    'Setup array for custom passes
    Dim arrCustomTubes As Array = Nothing
    Dim strCustomTubeNumbers As String = Nothing
    Dim arrCustomRows As Array = Nothing
    Dim strCustomRowNumbers As String = Nothing
    Dim fTubesCustom As Boolean = False
    Dim fTubesAlpha As Boolean = False
    'Detailed Summary Bullets
    Dim mstrBullet2 As String
    Dim mstrBullet3 As String
    Dim mstrBullet4 As String
    Dim mstrBullet5 As String

    Dim oREControl As DevExpress.XtraRichEdit.IRichEditControl 'KM
    Private mMSWordReport As cMSWord

    ' Data Access Components for Default Index items
    Private WithEvents m_DefaultAppendixItemsTable As QTT_LookupsData.vwDefaultAppendixItemsDataTable
    Private m_DefaultAppendixItemsTableAdapter As QTT_LookupsDataTableAdapters.vwDefaultAppendixItemsTableAdapter
    ' data access objects for Default field text
    Private WithEvents m_DefaultTextEntriesTable As QTT_LookupsData.tblDefaultTextEntriesDataTable
    Private m_DefaultTextEntriesTableAdapter As QTT_LookupsDataTableAdapters.tblDefaultTextEntriesTableAdapter

    ' June 2007 - "kludge" to accommodate Reformer Appendix B 
    Public Const REFORMER_APDX_B_3D_REFORMER_MODEL = 13
    Public Const REFORMER_APDX_B_DIAMETER_GRAPH = 27

    'March 2013 - LOTIS/MANTIS Set of four
    Public Const REFORMER_3D_REFORMER_NE = 30
    Public Const REFORMER_3D_REFORMER_NW = 31
    Public Const REFORMER_3D_REFORMER_SE = 32
    Public Const REFORMER_3D_REFORMER_SW = 33

    'October 2011 - FTIS Set of four graphics
    Public Const FTIS_3D_Wall = 6
    Public Const FTIS_2D_Wall = 7
    Public Const FTIS_3D_Rad = 8
    Public Const FTIS_2D_Rad = 9

    Property Inspection_ID() As Integer
        Get
            Inspection_ID = mintInspectionID
        End Get
        Set(ByVal value As Integer)
            mintInspectionID = value
        End Set
    End Property

#End Region

    Public Class NotBlankValidationRule
        'KM 8/1/2011  'Custom validation rule for "not blank" 
        Inherits ValidationRule

        Public Overrides Function Validate(ByVal control As Control, ByVal value As Object) As Boolean

            Dim result As Boolean = True
            If control.Text = String.Empty Then result = False

            Return result

        End Function

    End Class

    Public Class CustomNumberValidationRule
        Inherits ValidationRule
        Public Overrides Function Validate(ByVal control As Control, ByVal value As Object) As Boolean

            'KM Create a rule that the custom pass editor will never pass
            ' and only implement the rule when the control doesn't pass the
            ' rule that is implemented in the CustomFormatValid function

            Dim result As Boolean = True
            If control.Text <> "Invalid###" Then result = False

            Return result

        End Function

    End Class

    Public Class ControlDisabledValidationRule
        Inherits ConditionValidationRule

        Public Overrides Function CanValidate(control As Control) As Boolean
            'KM Custom rule that allows disabled controls to be "turned off" when validation checking
            Return MyBase.CanValidate(control) AndAlso control.Enabled
        End Function

    End Class

#Region "Main"

    Private Sub InspectionDetails_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        mfVisible = True
    End Sub

    Private Sub StandardValidate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiLeadInspector.TextChanged, uiTitleRole.TextChanged, uiPlantProcessType.TextChanged, uiTubeFlawsWelds.TextChanged, uiAddress.TextChanged, uiTubeFlawsGouges.TextChanged, uiCity.TextChanged, uiFlowDirection.TextChanged, uiReformerOEM.TextChanged, uiPostalCode.TextChanged, uiState.TextChanged, uiBurnerConfiguration.TextChanged, _
    uiTubeManufacturer.TextChanged, uiTubeMaterials.TextChanged, uiPressure.TextChanged, uiTargetTemp.TextChanged, uiThermalCycles.TextChanged, uiOperatingHours.TextChanged, uiTubeServiceDate.TextChanged, uiTubeInspectionAge.TextChanged, uiTubeNextInspectionDate.TextChanged, uiTubeInnerDiameterDisplay.TextChanged, uiTubeOuterDiameterDisplay.TextChanged, uiTubeWallThicknessDisplay.TextChanged, uiOffices.TextChanged, _
    uiTubeDesignToleranceDisplay.TextChanged, uiNumberTubesInspected.TextChanged, uiNumberTubesTotal.TextChanged, uiTubeLength.TextChanged, uiRedTubeCount.TextChanged, uiYellowTubeCount.TextChanged, uiOverallTubesInspected.TextChanged, uiSwellingOver3PercentCount.TextChanged, uiBulgingTubeCount.TextChanged, uiDiaGrowthRowID.TextChanged, uiDiaGrowthTubeID.TextChanged, uiSpareUninstalledGroup.SelectedIndexChanged, _
    uiDiaGrowthMaxPercent.TextChanged, uiMaxGrowthLocation.TextChanged, uiWorstTubeIDRow.TextChanged, uiWorstTubeIDTube.TextChanged, uiBulgingWorstPercent.TextChanged, uiOperatingPeriod.TextChanged, uiOverallMaxGrowthLocation.SelectedIndexChanged, uiNumberOfRows.TextChanged, uiNumberOfTubes.TextChanged, uiCracking.TextChanged, uiCustomers.TextChanged, uiCountry.TextChanged, uiLocation.TextChanged, uiReformerID.TextChanged, uiReformerName.TextChanged, _
    uiNewTubeCount1.TextChanged, uiNewTubeCount2.TextChanged, uiNewTubeCount3.TextChanged, uiNewTubeCount4.TextChanged, uiWorstFlawRowNumber.TextChanged, uiWorstFlawTubeNumber.TextChanged, uiTubeFlawLocation.TextChanged, uiNumberTubesWithFlaws.TextChanged, uiAgedTubeCount1.TextChanged, uiAgedTubeCount2.TextChanged, uiAgedTubeCount3.TextChanged, uiAgedTubeCount4.TextChanged, uiAgedTubeCount5.TextChanged

        If mfVisible = False Then Exit Sub
        'If the control doesn't have any other validation, but needs to be validated, it is handled here
        DxValidationProvider.Validate(sender)
        FixDisabledValidation()

        'Then the code cross-checks for changes after summaries have been generated
        Dim myControl As Control = sender

        Select Case mintInspectionType
            Case LOTIS_NEW, LOTIS_SPARE, MANTIS_NEW, MANTIS_SPARE

                'If Inspection Summary information changes
                Select Case myControl.Name
                    Case "uiCustomers", "uiNewTubeCount1", "uiNewTubeCount2", "uiNewTubeCount3", "uiNewTubeCount4", "uiNumberOfTubes", "uiNumberTubesInspected", "uiNumberTubesWithFlaws", "uiReformerID", "uiReformerName", "uiSpareUninstalledGroup", "uiTubeDesignToleranceDisplay", "uiTubeFlawLocation", "uiTubeInnerDiameterDisplay", "uiTubeOuterDiameterDisplay", "uiWorstFlawRowNumber", "uiWorstFlawTubeNumber"
                        If uiInspectionSummaryEditor.Text <> "" Then
                            'If you change this message, change the "EnsureRichTextBoxIsntEmpty" function code to match
                            SetTextMessage(uiInspectionSummaryEditor, "Data changes require that the text in this summary be regenerated.", 24)
                        End If
                End Select

            Case LOTIS_AGED, MANTIS_AGED
                'If Detailed Summary information changes
                Select Case myControl.Name
                    Case "uiAgedTubeCount1", "uiAgedTubeCount2", "uiAgedTubeCount3", "uiAgedTubeCount4", "uiAgedTubeCount5", "uiBulgingTubeCount", "uiBulgingWorstPercent", "uiCracking", "uiDiaGrowthMaxPercent", "uiDiaGrowthRowID", "uiDiaGrowthTubeID", "uiMaxGrowthLocation", "uiNumberTubesInspected", "uiNumberTubesTotal", "uiOperatingPeriod", "uiOverallMaxGrowthLocation", "uiOverallTubesInspected", "uiRedTubeCount", "uiReformerID", "uiReformerName", "uiSwellingOver3PercentCount", "uiTubeFlawsGouges", "uiTubeFlawsWelds", "uiTubeMaterials", "uiWorstTubeIDRow", "uiWorstTubeIDTube", "uiYellowTubeCount"
                        If uiExecSummaryDetailsEditor.Text <> "" Then
                            'If you change this message, change the "EnsureRichTextBoxIsntEmpty" function code to match
                            SetTextMessage(uiExecSummaryDetailsEditor, "Data changes require that the text in these summaries be regenerated.", 24)
                            SetTextMessage(uiDiaGrowthSummaryEditor, "Data changes require that the text in these summaries be regenerated.", 24)
                            SetTextMessage(uiFFSEditor, "Data changes require that the text in these summaries be regenerated.", 24)
                            SetTextMessage(uiTubeHarvestingEditor, "Data changes require that the text in these summaries be regenerated.", 24)
                        End If
                End Select

                'If Executive Summary information changes
                Select Case myControl.Name
                    Case "uiBulgingTubeCount", "uiBulgingWorstPercent", "uiCracking", "uiDiaGrowthMaxPercent", "uiDiaGrowthRowID", "uiDiaGrowthTubeID", "uiMaxGrowthLocation", "uiOperatingPeriod", "uiOverallMaxGrowthLocation", "uiOverallTubesInspected", "uiRedTubeCount", "uiReformerID", "uiReformerName", "uiSwellingOver3PercentCount", "uiWorstTubeIDRow", "uiWorstTubeIDTube", "uiYellowTubeCount"
                        If uiSummaryEditor.Text <> "" Then
                            'If you change this message, change the "EnsureRichTextBoxIsntEmpty" function code to match
                            SetTextMessage(uiSummaryEditor, "Data changes require that the text in this summary be regenerated.", 24)
                        End If
                End Select

        End Select

        'If Project Description information changes
        Select Case myControl.Name
            Case "uiCity", "uiCountry", "uiCustomers", "uiPlantNameLabel", "uiReformerID", "uiReformerName", "uiState", "uiSpareUninstalledGroup"
                If uiProjectDescriptionEditor.Text <> "" Then
                    'If you change this message, change the "EnsureRichTextBoxIsntEmpty" function code to match
                    SetTextMessage(uiProjectDescriptionEditor, "Data changes require that the text in the Project Description be regenerated.", 24)
                End If
        End Select

        'If Tube Identification information changes
        Select Case myControl.Name
            Case "uiNumberOfTubes", "uiNumberOfRows"
                If uiTubeIdentificationEditor.Text <> "" Then
                    'If you change this message, change the "EnsureRichTextBoxIsntEmpty" function code to match
                    SetTextMessage(uiTubeIdentificationEditor, "Data changes require that the text in the Tube Identification section be regenerated.", 24)
                End If
        End Select

        'If Prior History information changes
        Select Case myControl.Name
            Case "uiLocation", "uiReformerID", "uiReformerName"
                If uiPriorHistoryEditor.Text <> "" Then
                    'If you change this message, change the "EnsureRichTextBoxIsntEmpty" function code to match
                    SetTextMessage(uiPriorHistoryEditor, "Data changes require that the text in the Prior History section be regenerated.", 24)
                End If
        End Select


    End Sub

    Public Sub CheckBoxChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiExecSummaryGreen.CheckedChanged, uiExecSummaryRed.CheckedChanged, uiExecSummaryYellow.CheckedChanged, uiDetailedSummaryGreen.CheckedChanged, uiDetailedSummaryRed.CheckedChanged, uiDetailedSummaryYellow.CheckedChanged, uiGreenCreep.CheckedChanged
        Dim myControl As Control = sender

        If mfVisible = False Then Exit Sub

        'Check for changes to checkboxes

        Select Case myControl.Name
            Case "uiExecSummaryGreen", "uiExecSummaryRed", "uiExecSummaryYellow"
                If uiSummaryEditor.Text <> "" Then
                    'If you change this message, change the "EnsureRichTextBoxIsntEmpty" function code to match
                    SetTextMessage(uiSummaryEditor, "Data changes require that the text in this summary be regenerated.", 24)
                End If
            Case "uiDetailedSummaryGreen", "uiDetailedSummaryRed", "uiDetailedSummaryYellow", "uiGreenCreep"
                If uiExecSummaryDetailsEditor.Text <> "" Then
                    'If you change this message, change the "EnsureRichTextBoxIsntEmpty" function code to match
                    SetTextMessage(uiExecSummaryDetailsEditor, "Data changes require that the text in these summaries be regenerated.", 24)
                    SetTextMessage(uiDiaGrowthSummaryEditor, "Data changes require that the text in these summaries be regenerated.", 24)
                    SetTextMessage(uiFFSEditor, "Data changes require that the text in these summaries be regenerated.", 24)
                    SetTextMessage(uiTubeHarvestingEditor, "Data changes require that the text in these summaries be regenerated.", 24)
                End If
        End Select

    End Sub

    Public Sub SetTextMessage(richEditControl As RichEditControl, text As String, size As Integer)
        Dim subDocument = richEditControl.Document
        richEditControl.ResetText()
        subDocument.AppendText(text)

        Dim characterProperties = subDocument.BeginUpdateCharacters(subDocument.Range)
        Dim paragraphProperties = subDocument.BeginUpdateParagraphs(subDocument.Range)
        If True Then
            characterProperties.FontName = "Arial"
            characterProperties.FontSize = size
            characterProperties.ForeColor = Color.Red
            paragraphProperties.Alignment = ParagraphAlignment.Center
        End If
        subDocument.EndUpdateCharacters(characterProperties)
        subDocument.EndUpdateParagraphs(paragraphProperties)

    End Sub

    Private Sub InspectionDetails_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Load the spelling dictionaries in a relative path location
        Dim myPath As String = Path.GetDirectoryName(Application.ExecutablePath)
        Dim dictionary As New SpellCheckerISpellDictionary()
        Me.Cursor = Cursors.WaitCursor

        dictionary.AlphabetPath = myPath & "\Dictionaries\EnglishAlphabet.txt"
        dictionary.Culture = New CultureInfo("en-US")
        dictionary.DictionaryPath = myPath & "\Dictionaries\american.xlg"
        dictionary.GrammarPath = myPath & "\Dictionaries\english.aff"
        SpellChecker1.Dictionaries.Add(dictionary)

        Dim customDictionary As New SpellCheckerCustomDictionary()
        customDictionary.Culture = New CultureInfo("en-US")
        customDictionary.AlphabetPath = myPath & "\Dictionaries\EnglishAlphabet.txt"
        customDictionary.DictionaryPath = myPath & "\Dictionaries\CustomEnglish.dic"
        SpellChecker1.Dictionaries.Add(customDictionary)

        'start autosaving
        tmrAutoSave.Enabled = True
        'Set default open size
        Me.Size = New System.Drawing.Size(1270, 860)

    End Sub

    Public Sub OpenInspection(ByVal lngID As Long, ByVal fIsNew As Boolean, ByVal fCheckRootFolder As Boolean)

        Dim lngInsTypeID As Long
        Dim strRootFolderName As String

        'Load Inspection Data
        QTT_InspectionsDataSets.Clear()
        mintInspectionID = lngID

        'This line of code loads data into the 'QTT_InspectionsDataSets.tblQTT_PrimaryRoles' table. You can move, or remove it, as needed.
        Me.TblQTT_PrimaryRolesTableAdapter.Fill(Me.QTT_InspectionsDataSets.tblQTT_PrimaryRoles)
        'This line of code loads data into the 'QTT_InspectionsDataSets.tblQTT_LeadInspectors' table. You can move, or remove it, as needed.
        Me.TblQTT_LeadInspectorsTableAdapter.Fill(Me.QTT_InspectionsDataSets.tblQTT_LeadInspectors)
        'This line of code loads data into the 'QTT_InspectionsDataSets.tblReformerNewSummary' table. You can move, or remove it, as needed.
        Me.taTblReformerNewSummary.Fill(Me.QTT_InspectionsDataSets.tblReformerNewSummary)
        'This line of code loads data into the 'QTT_InspectionsDataSets.tblReformerAgedSummary' table. You can move, or remove it, as needed.
        Me.taTblReformerAgedSummary.Fill(Me.QTT_InspectionsDataSets.tblReformerAgedSummary)
        Me.taTblInspections.FillBy_InspectionID(Me.QTT_InspectionsDataSets.tblInspections, lngID)

        mfIsNew = fIsNew   ' recorded so that dependant list forms can be refreshed

        ' Pipe Tube Items
        Me.taTblPipeTubeItems.Fill(Me.QTT_InspectionsDataSets.tblPipeTubeItems)
        ' setup pipetube item summaries
        taPipeTubeItemSummaries.Fill(Me.QTT_InspectionsDataSets.tblPipeTubeItemSummaries)
        ' setup pipe tube item types
        taInspections_TblPipeTubeItemTypes.Fill(Me.QTT_InspectionsDataSets.tblPipeTubeItemTypes)
        ' add root folder
        If Not (Me.QTT_InspectionsDataSets.tblInspections.Item(0).IsRootFolderNameNull) Then
            strRootFolderName = Me.QTT_InspectionsDataSets.tblInspections.Item(0).RootFolderName
            Dim strRootPath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & My.Settings.RootDataFolder
            uiRootFolderName.Text = My.Computer.FileSystem.CombinePath(strRootPath, strRootFolderName)
        Else
            uiRootFolderName.Enabled = False
        End If

        'JHF 8-31-2011
        'Load up the contact information for the Customer Details data grid
        Me.TblContactInformationTableAdapter1.Fill(QTT_InspectionsDataSets.tblContactInformation)
        bstblContactInformation.Filter = "FK_Inspection = " & lngID

        With Me.QTT_InspectionsDataSets.tblInspections.Item(0)
            ' setup inspection Type
            lngInsTypeID = .FK_InspectionType
            Call SetupInspectionType(lngInsTypeID)

            'Set the radio buttons to match choices in database (not linked)
            If .TubesInAlpha Then
                TubeFormatRadioGroup.SelectedIndex = 1
            ElseIf Not (.IsCustomTubesNull) Then
                TubeFormatRadioGroup.SelectedIndex = 2
            ElseIf Not (.NumberOfTubes = 0) Then
                TubeFormatRadioGroup.SelectedIndex = 0
            End If

            If .RowsInAlpha Then
                RowFormatRadioGroup.SelectedIndex = 1
            ElseIf Not (.IsCustomRowsNull) Then
                RowFormatRadioGroup.SelectedIndex = 2
            ElseIf Not .NumberOfRows = 0 Then
                RowFormatRadioGroup.SelectedIndex = 0
            End If
            uiViewReportButton.Enabled = Not (.IsReportFilenameNull)
        End With

        ' Appendix(es)
        Me.VwAppendixDetailsTableAdapter.Fill(Me.QTT_InspectionsDataSets.vwAppendixDetails)
        ' Appendix Items
        Me.VwAppendixItem_DetailsTableAdapter.Fill(Me.QTT_InspectionsDataSets.vwAppendixItem_Details)
        ' Report Items
        ' Me.QTT_InspectionsDataSets.EnforceConstraints = False
        Me.VwReportItems_DetailsTableAdapter.Fill(Me.QTT_InspectionsDataSets.vwReportItems_Details)

        ' setup Lookups
        Call SetupLookupLists(lngInsTypeID)

        'Check selected tools
        'KM 8/17/2011
        Dim i As Integer
        Dim j As Integer
        Dim ActiveRow As DataRowView
        Dim ToolRow As QTT_InspectionsDataSets.tblInspectionsAndToolsRow

        'Check only the appropriate inspection tools in the checkbox
        'This is more complex than required by LOTIS/MANTIS where only one tool is used.
        'Tools could be a single field instead of a table. At the point in development where this was
        'noted, however, it was decided to leave it for now, both because of time constraints and in case
        'it changes at some point in the future, where multiple tools are required on a job. It is easy
        'enough for the users to make sure they only select one tool. (The user request was for option
        'buttons instead of checkboxes, where one choice would exclud all others. Could be done with the
        'checkboxes, but would go counter to standard practice.) --KM
        bPreCheckTools = True
        For i = 0 To bsInspections_InspectionsAndTools.Count - 1
            ToolRow = bsInspections_InspectionsAndTools.Item(i).Row
            For j = 0 To bsInspectionTools.Count - 1
                ActiveRow = uiToolsCheckedList.GetItem(j)
                Dim CurRow As QTT_InspectionsDataSets.tblInspectionToolsRow = ActiveRow.Row
                If CurRow.ID = ToolRow.FK_Tool Then
                    uiToolsCheckedList.SetItemChecked(j, True)
                    Exit For
                End If
            Next j
        Next i

        'If nothing is checked, select first item
        If uiToolsCheckedList.Items.Count = 0 Then uiToolsCheckedList.SetItemChecked(0, True)
        bPreCheckTools = False

        'KM set dropdowns to number of items or 15
        uiOffices.Properties.DropDownRows = Math.Min(Me.QTT_InspectionsDataSets.tblQTT_Offices.Rows.Count, 15)
        uiTitleRole.Properties.DropDownRows = Math.Min(Me.QTT_InspectionsDataSets.tblQTT_PrimaryRoles.Rows.Count, 15)
        uiCustomers.Properties.DropDownRows = Math.Min(Me.QTT_InspectionsDataSets.tblCustomers.Rows.Count, 15)
        uiAppendixItems.Properties.DropDownRows = Math.Min(Me.bsInspections_Appendixes.Count, 15)
        'Show the first appendix item
        Dim oCurApdx As QTT_InspectionsDataSets.vwAppendixDetailsRow = TryCast(bsInspections_Appendixes.Current.Row, QTT_InspectionsDataSets.vwAppendixDetailsRow)
        uiAppendixItems.EditValue = oCurApdx.ID

        'Set gridviews to best fit columns
        uiReportItemsGridView.BestFitColumns()
        uiAppendixItemsGridView.BestFitColumns()

        'Set grid sort order
        uiAppendixItemsGridView.ClearSorting()
        uiAppendixItemsGridView.Columns("PipeTubeSortOrder").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        uiAppendixItemsGridView.Columns("SortOrder").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

        ' flag that basic setup is completed
        mfSetupCompleted = True

        'Set default units
        Call SetupInspectionDetailsHeader()

        ' check root folder if requested
        If fCheckRootFolder Then
            Call CheckRootFolder(True)
        End If

        'iterate through all tabs to load data
        uiMainXtraTabControl.BeginUpdate()
        For i = 0 To uiMainXtraTabControl.TabPages.Count - 1
            If uiMainXtraTabControl.TabPages(i).PageVisible = True Then uiMainXtraTabControl.SelectedTabPageIndex = i
        Next
        uiMainXtraTabControl.EndUpdate()
        uiMainXtraTabControl.SelectedTabPageIndex = 0

        'form starts minimized so cycling through all the tabs to update data isn't seen by the user
        Me.WindowState = FormWindowState.Normal
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub SetupInspectionDetailsHeader()
        ' setup inspections data header as well as form caption
        Dim oCurRow As QTT_InspectionsDataSets.tblInspectionsRow = Me.QTT_InspectionsDataSets.tblInspections.Rows(0)
        Dim strCountry As String = ""
        Dim strUnit As String = ""
        Dim strLocation As String = ""

        Try
            With oCurRow
                If Not .IsCountryNull Then
                    strCountry = " (" & Trim(.Country) & ")"
                End If
                If Not .IsLocationDescNull Then
                    strLocation = Trim(.LocationDesc) & strCountry
                Else
                    strLocation = "Location Missing!" & strCountry
                End If
                If Not .IsInspectionTypeDesciptionNull Then
                    uiInspectionTypeLabel.Text = Trim(.InspectionTypeDesciption)
                End If
                uiLocationNameLabel.Text = strLocation
                If Not .IsUnitNameNull Then
                    strUnit = Trim(.UnitName)
                Else
                    strUnit = "Reformer Name Missing!"
                End If
                If Not .IsUnitNumberNull Then
                    strUnit = strUnit & "-" & Trim(.UnitNumber)
                End If
                uiHeaterNameLabel.Text = strUnit
                uiDateLabel.Text = String.Format("Inspection Date: {0:MMMM d, yyyy}", .InspectionDateStart)
                uiCustomerNameLabel.Text = Trim(.CustomerName)

                If Not (Me.QTT_InspectionsDataSets.tblInspections.Item(0).IsRootFolderNameNull) Then
                    Dim strRootFolderName As String = Me.QTT_InspectionsDataSets.tblInspections.Item(0).RootFolderName
                    Dim strRootPath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & My.Settings.RootDataFolder
                    uiRootFolderName.Text = My.Computer.FileSystem.CombinePath(strRootPath, strRootFolderName)
                    Me.Text = strRootFolderName
                Else
                    uiRootFolderName.Enabled = False
                End If

            End With
        Catch
            ' expected error before record is loaded - no messages / actions required
        End Try

        uiInspectionDetailsTabPage.Select()

    End Sub

    Private Sub SetupLookupLists(ByVal intInspectionType As Integer)

        ' called from OpenInspection to perform setup of cbo lists, etc.
        Dim tblPipeTubeMats As QTT_LookupsData.tblDefaultText_ListItemsDataTable = New QTT_LookupsData.tblDefaultText_ListItemsDataTable
        Dim tblPipeTubeManufacturer As QTT_LookupsData.tblDefaultText_ListItemsDataTable = New QTT_LookupsData.tblDefaultText_ListItemsDataTable
        Dim tblPlantProcessTypes As QTT_LookupsData.tblDefaultText_ListItemsDataTable = New QTT_LookupsData.tblDefaultText_ListItemsDataTable
        Dim tblFlowDirections As QTT_LookupsData.tblDefaultText_ListItemsDataTable = New QTT_LookupsData.tblDefaultText_ListItemsDataTable
        Dim tblBurnerConfigurations As QTT_LookupsData.tblDefaultText_ListItemsDataTable = New QTT_LookupsData.tblDefaultText_ListItemsDataTable
        Dim tblReformerOEMs As QTT_LookupsData.tblDefaultText_ListItemsDataTable = New QTT_LookupsData.tblDefaultText_ListItemsDataTable
        Dim taUnitType As QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter = New QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter
        Dim tblUnitType As QTT_LookupsData.tblDefaultText_ListItemsDataTable = New QTT_LookupsData.tblDefaultText_ListItemsDataTable

        'Add countries and states to dropdowns
        Dim countries As List(Of String) = GetListOfCountries()
        uiCountry.Properties.Items.BeginUpdate()
        Dim i As Integer = countries.Count - 1
        For i = 0 To i
            uiCountry.Properties.Items.Add(countries(i))
        Next i
        uiCountry.Properties.Items.EndUpdate()
        uiState.Properties.Items.AddRange(GetListOfUSStates)

        Dim taPipeTubeTypes As New QTT_LookupsDataTableAdapters.tblPipeTubeItemTypesTableAdapter
        Dim tblPipeTubeTypes As New QTT_LookupsData.tblPipeTubeItemTypesDataTable
        taPipeTubeTypes.Fill(tblPipeTubeTypes)

        Dim intGeneralInspectionType As Integer 'general inspection type to group all LOTIS and all MANTIS as one type
        Select Case intInspectionType
            Case LOTIS_AGED, LOTIS_NEW, LOTIS_SPARE   'LOTIS
                intGeneralInspectionType = LOTIS    '3
            Case MANTIS_AGED, MANTIS_NEW, MANTIS_SPARE  'MANTIS
                intGeneralInspectionType = MANTIS   '5
        End Select

        ' Customers Dropdown List
        taTblCustomers_Inspection.Fill(Me.QTT_InspectionsDataSets.tblCustomers)

        'Offices   
        Dim taOffices As New QTT_InspectionsDataSetsTableAdapters.tblQTT_OfficesTableAdapter
        taOffices.Fill(Me.QTT_InspectionsDataSets.tblQTT_Offices)
        uiOffices.DataBindings.Item(0).ReadValue()

        ' Inspections And Tools
        Me.taTblInspectionsAndTools.Fill(Me.QTT_InspectionsDataSets.tblInspectionsAndTools)
        bsInspectionTools.Sort = "SortOrder"

        'Inspection Types
        Me.taTblInspectionTypes_LU.Fill(Me.QTT_LookupsData.tblInspectionTypes)

        ' Lead Inspectors List
        Me.TblQTT_LeadInspectorsTableAdapter.Fill(Me.QTT_InspectionsDataSets.tblQTT_LeadInspectors)
        ' re-load data from main record 
        uiLeadInspector.DataBindings.Item(0).ReadValue()

        ' Lead Inspector Role
        Me.TblQTT_PrimaryRolesTableAdapter.Fill(Me.QTT_InspectionsDataSets.tblQTT_PrimaryRoles)
        uiLeadInspector.DataBindings.Item(0).ReadValue()

        ' Inspections And Inspectors
        Me.taInspectionsAndInspectors.Fill(Me.QTT_InspectionsDataSets.tblInspectionsAndInspectors)
        'Roles lists
        Me.TblQTT_RolesTableAdapter1.Fill(Me.QTT_LookupsData.tblQTT_Roles)
        ' Inspectors List - full listing
        Me.taTblQTT_Inspectors.Fill(Me.QTT_LookupsData.tblQTT_Inspectors)
        bsOtherInspectors.Sort = "Description"
        'INSPECTION TYPE RELATED - these items should be broken into separate procedure if 'change inspection type' functionality is added
        Dim sText As String = ""
        ' Unit Type text choices
        taUnitType.FillBy_InpectionTypeAndFieldID(tblUnitType, intInspectionType, DEF_TEXTLIST_UNIT_TYPE)
        Dim Col As New DevExpress.XtraEditors.Controls.LookUpColumnInfo("FieldText", "", 100)
        ' setup pipe / tube materials text choices
        Me.taDefaultText_PipeTubeMaterial.FillBy_InpectionTypeAndFieldID(tblPipeTubeMats, intGeneralInspectionType, DEF_TEXTLIST_PIPETUBE_MATERIAL)

        'Add values to combobox
        uiTubeMaterials.Properties.Items.BeginUpdate()
        For rowCount = 0 To tblPipeTubeMats.Rows.Count - 1
            uiTubeMaterials.Properties.Items.Add(tblPipeTubeMats.Rows(rowCount)("FieldText").ToString)
        Next rowCount
        uiTubeMaterials.Properties.Items.EndUpdate()
        uiTubeMaterials.Properties.DropDownRows = Math.Min(tblPipeTubeMats.Rows.Count, 15)

        ' setup pipe / tube manufacturer text choices
        Me.taDefaultText_PipeTubeManufacturer.FillBy_InpectionTypeAndFieldID(tblPipeTubeManufacturer, intGeneralInspectionType, DEF_TEXTLIST_PIPETUBE_MANUFACTURER)
        uiTubeManufacturer.Properties.Items.BeginUpdate()
        For rowCount = 0 To tblPipeTubeManufacturer.Rows.Count - 1
            uiTubeManufacturer.Properties.Items.Add(tblPipeTubeManufacturer.Rows(rowCount)("FieldText").ToString)
        Next rowCount
        uiTubeManufacturer.Properties.Items.EndUpdate()
        uiTubeManufacturer.Properties.DropDownRows = Math.Min(tblPipeTubeManufacturer.Rows.Count, 15)

        Me.taTblBurnerConfigurations.FillBy_InpectionTypeAndFieldID(tblBurnerConfigurations, intGeneralInspectionType, DEF_TEXTLIST_UNIT_TYPE)
        uiBurnerConfiguration.Properties.Items.BeginUpdate()
        For rowCount = 0 To tblBurnerConfigurations.Rows.Count - 1
            uiBurnerConfiguration.Properties.Items.Add(tblBurnerConfigurations.Rows(rowCount)("FieldText").ToString)
        Next rowCount
        uiBurnerConfiguration.Properties.Items.EndUpdate()
        uiBurnerConfiguration.Properties.DropDownRows = Math.Min(tblBurnerConfigurations.Rows.Count, 15)

        Me.taTblPlantProcessTypes.FillBy_InpectionTypeAndFieldID(tblPlantProcessTypes, intGeneralInspectionType, DEF_TEXTLIST_PLANT_PROCESS_TYPE)
        uiPlantProcessType.Properties.Items.BeginUpdate()
        For rowCount = 0 To tblPlantProcessTypes.Rows.Count - 1
            uiPlantProcessType.Properties.Items.Add(tblPlantProcessTypes.Rows(rowCount)("FieldText").ToString)
        Next rowCount
        uiPlantProcessType.Properties.Items.EndUpdate()
        uiPlantProcessType.Properties.DropDownRows = Math.Min(tblPlantProcessTypes.Rows.Count, 15)

        Me.taTblFlowDirections.FillBy_InpectionTypeAndFieldID(tblFlowDirections, intGeneralInspectionType, DEF_TEXTLIST_INITIAL_DIRECTIONS)
        uiFlowDirection.Properties.Items.BeginUpdate()
        For rowCount = 0 To tblFlowDirections.Rows.Count - 1
            uiFlowDirection.Properties.Items.Add(tblFlowDirections.Rows(rowCount)("FieldText").ToString)
        Next rowCount
        uiFlowDirection.Properties.Items.EndUpdate()
        uiFlowDirection.Properties.DropDownRows = Math.Min(tblFlowDirections.Rows.Count, 15)

        Me.taTblReformerOEMs.FillBy_InpectionTypeAndFieldID(tblReformerOEMs, intGeneralInspectionType, DEF_TEXTLIST_REFORMER_OEM)
        uiReformerOEM.Properties.Items.BeginUpdate()
        For rowCount = 0 To tblReformerOEMs.Rows.Count - 1
            uiReformerOEM.Properties.Items.Add(tblReformerOEMs.Rows(rowCount)("FieldText").ToString)
        Next rowCount
        uiReformerOEM.Properties.Items.EndUpdate()
        uiReformerOEM.Properties.DropDownRows = Math.Min(tblReformerOEMs.Rows.Count, 15)

    End Sub

    Private Sub SetupInspectionType(ByVal lngInspectionTypeID As Integer)

        'Performs UI setup based on inspection type
        Dim lngToolTypeID As Integer
        ' store current inspection type in module variable
        mintInspectionType = lngInspectionTypeID

        'Inspection Types
        Me.taTblInspectionTypes.FillBy_InspectionTypeID(Me.QTT_InspectionsDataSets.tblInspectionTypes, lngInspectionTypeID)
        With Me.QTT_InspectionsDataSets.tblInspectionTypes
            lngToolTypeID = .Rows(0).Item(.ToolTypeColumn)
            ' store report template
            mstrMSWord_Template = .Rows(0).Item(.ReportTemplateFileColumn)
        End With

        'Set UI for LOTIS vs. MANTIS
        Select Case mintInspectionType
            Case LOTIS_AGED, LOTIS_NEW, LOTIS_SPARE    'LOTIS
                uiTubeDesignToleranceLabel.Text = "Inner Diameter Tube Bore Design Tolerance:"
            Case MANTIS_AGED, MANTIS_NEW, MANTIS_SPARE  'MANTIS
                uiTubeDesignToleranceLabel.Text = "Outer Diameter Tube Design Tolerance:"
                uiCrackingLabel.Visible = True
                uiCracking.Visible = True
        End Select

        'Set UI for AGED vs. NEW vs. SPARE
        Select Case mintInspectionType
            Case LOTIS_AGED, MANTIS_AGED    'AGED
                uiExecutiveSummaryTabPage.PageVisible = True
                uiInspectionSummaryTabPage.PageVisible = False
                uiDetailedInspectionTabPage.PageVisible = True
                uiSpareUninstalledGroup.Visible = False
                uiSampleLevel3.Visible = True
            Case LOTIS_NEW, MANTIS_NEW   'NEW
                uiPriorHistoryLabel.Visible = False
                uiPriorHistoryEditor.Visible = False
                uiHistoryInsertSampleTextButton.Visible = False
                uiFirstInspectionInsertSampleTextButton.Visible = False
                uiHistoryLabel.Visible = False
                uiThermalCycles.Visible = False
                uiThermalCyclesLabel.Visible = False
                uiOperatingHours.Visible = False
                uiOperatingHoursLabel.Visible = False
                uiSpareUninstalledGroup.Visible = False
                uiTubeUnitGroup.Left = uiTubeUnitGroup.Left - 63
            Case LOTIS_SPARE, MANTIS_SPARE   'SPARE
                uiCanCircularOption.Visible = False
                uiRowsLabel.Visible = False
                uiNumberOfRows.Enabled = False
                uiNumberOfRows.Visible = False
                RowFormatRadioGroup.Enabled = False
                uiRowFormatGroupBox.Visible = False
                uiCustomRowFormatLabel.Visible = False
                uiCustomRowFormatEditor.Visible = False
                uiPriorHistoryLabel.Visible = False
                uiPriorHistoryEditor.Visible = False
                uiHistoryInsertSampleTextButton.Visible = False
                uiFirstInspectionInsertSampleTextButton.Visible = False
                uiHistoryLabel.Visible = False
                uiNumberTubesTotal.Visible = False
                uiNumberTubesTotalLabel.Visible = False
                uiTubeServiceDate.Visible = False
                uiTubeServiceDateLabel.Visible = False
                uiWorstFlawRowNumber.Visible = False
                uiRowNumberLabel.Visible = False
                uiThermalCycles.Visible = False
                uiThermalCyclesLabel.Visible = False
                uiOperatingHours.Visible = False
                uiOperatingHoursLabel.Visible = False
                uiMonthYearLabel.Visible = False
                uiSpareUninstalledGroup.Visible = True
                uiTubeUnitGroup.Left = uiTubeUnitGroup.Left - 63
                uiTubeNumberInstructionsLabel.Left = uiTubeNumberInstructionsLabel.Left - 329
                uiLabel.Left = uiLabel.Left - 329
                uiNumberOfTubes.Left = uiNumberOfTubes.Left - 329
                uiTubeFormatGroupBox.Left = uiTubeFormatGroupBox.Left - 329
                uiCustomTubeFormatLabel.Left = uiCustomTubeFormatLabel.Left - 329
                uiCustomTubeFormatEditor.Left = uiCustomTubeFormatEditor.Left - 329
                uiTubeNumberInstructionsLabel.Text = "List total number of tubes inspected."
        End Select

        'KM If using custom tube naming
        'Enable the custom editor
        If TubeFormatRadioGroup.SelectedIndex = 2 = True Then
            uiCustomTubeFormatLabel.Enabled = True
            uiCustomTubeFormatEditor.Enabled = True
            'Otherwise...
        Else
            uiCustomTubeFormatLabel.Enabled = False
            uiCustomTubeFormatEditor.Enabled = False
        End If
        'Same with row naming
        If RowFormatRadioGroup.SelectedIndex = 2 = True Then
            uiCustomRowFormatLabel.Enabled = True
            uiCustomRowFormatEditor.Enabled = True
            'Otherwise...
        Else
            uiCustomRowFormatLabel.Enabled = False
            uiCustomRowFormatEditor.Enabled = False
        End If

        Me.TblPipeTubeItemTypesTableAdapter.FillBy_InspectionType(Me.QTT_LookupsData.tblPipeTubeItemTypes, lngInspectionTypeID)
        ' setup "Tools List" as needed
        ' Loads Inspection Tools List
        Me.taTblInspectionTools.FillByToolTypeID(Me.QTT_InspectionsDataSets.tblInspectionTools, lngToolTypeID)

    End Sub

    Private Function SaveData() As Boolean

        Dim drvTemp As DataRowView
        Dim oPTI As QTT_InspectionsDataSets.tblPipeTubeItemsRow
        Dim i As Integer

        ' pipe tube items
        For i = 0 To (bsInspections_PipeTubeItems.Count - 1)
            drvTemp = bsInspections_PipeTubeItems.Item(i)
            oPTI = drvTemp.Row
            If oPTI.RowState = DataRowState.Modified Then
                oPTI.EndEdit()
            End If
        Next
        'Save PIPE TUBE ITEMS
        bsInspections_PipeTubeItems.EndEdit()
        taTblPipeTubeItems.Update(Me.QTT_InspectionsDataSets.tblPipeTubeItems)

        ' Pipe Tube Item summaries
        ' Not using in LOTIS/MANTIS
        bsPipeTubeItemsSummaries.EndEdit()
        Try
            taPipeTubeItemSummaries.Update(Me.QTT_InspectionsDataSets.tblPipeTubeItemSummaries)
        Catch ex As Exception
            MsgBox("Error saving pipe/tube item summaries - " & ex.ToString)
        End Try

        ' additional inspectors
        bsInspections_OtherInspectors.EndEdit()
        taInspectionsAndInspectors.Update(Me.QTT_InspectionsDataSets.tblInspectionsAndInspectors)
        taInspectionsAndInspectors.Fill(Me.QTT_InspectionsDataSets.tblInspectionsAndInspectors)

        ' Report Image Items
        Dim oRptImage As QTT_InspectionsDataSets.vwReportItems_DetailsRow
        For i = 0 To (bsInspections_ReportItems.Count - 1)
            drvTemp = bsInspections_ReportItems.Item(i)
            oRptImage = drvTemp.Row
            If oRptImage.RowState = DataRowState.Modified Then
                oRptImage.EndEdit()
            End If
        Next i
        bsInspections_ReportItems.EndEdit()
        VwReportItems_DetailsTableAdapter.Update(QTT_InspectionsDataSets.vwReportItems_Details)

        '  Appendix Items
        Dim oAppendixItem As QTT_InspectionsDataSets.vwAppendixItem_DetailsRow
        For i = 0 To (bsInspections_AppendixItems.Count - 1)
            drvTemp = bsInspections_AppendixItems.Item(i)
            oAppendixItem = drvTemp.Row
            If oAppendixItem.RowState = DataRowState.Modified Then
                oAppendixItem.EndEdit()
            ElseIf oAppendixItem.RowState = DataRowState.Added Then
                oAppendixItem.EndEdit()
            End If
        Next i
        bsInspections_AppendixItems.EndEdit()
        Try
            VwAppendixItem_DetailsTableAdapter.Update(QTT_InspectionsDataSets.vwAppendixItem_Details)
        Catch ex As Exception
            MsgBox("Exception: " & ex.ToString())
        End Try

        bsInspections.EndEdit()

        'Update Summary Data
        Select Case mintInspectionType
            Case LOTIS_AGED, MANTIS_AGED       'Aged
                bsInspections_AgedSummary.EndEdit()
                Try
                    taTblReformerAgedSummary.Update(QTT_InspectionsDataSets.tblReformerAgedSummary)
                Catch ex As Exception
                    MsgBox("Exception: " & ex.ToString())
                End Try

            Case LOTIS_NEW, LOTIS_SPARE, MANTIS_NEW, MANTIS_SPARE  'New, Spare
                bsInspections_NewSummary.EndEdit()
                Try
                    taTblReformerNewSummary.Update(QTT_InspectionsDataSets.tblReformerNewSummary)
                Catch ex As Exception
                    MsgBox("Exception: " & ex.ToString())
                End Try

        End Select

        ' Main Inspection Record
        taTblInspections.Update(QTT_InspectionsDataSets.tblInspections)
        taTblInspectionsAndTools.Update(QTT_InspectionsDataSets.tblInspectionsAndTools)

        Return True
    End Function

    Private Function ProcessAutoSave() As Boolean
        ' used to implement auto-save functionality - returns 'True' if Save operation was performed.
        ' currently, always calls 'Save' - but feature could be controlled via UI in the future
        ' debug use only: MsgBox("Auto Save called.", vbInformation)
        Call SaveData()
        Return True
    End Function

    Private Function CheckRootFolder(ByVal fPromptToCreate As Boolean) As Boolean
        ' checks for existence of root folder - if not found, prompts if user wants to create it

        Dim strMsgText As String = ""
        Dim strCurFldr As String = uiRootFolderName.Text
        Dim intPasses As Integer = 0            'LOTIS/MANTIS does not use pass folders
        Dim fAlphaPasses As Boolean = False     'LOTIS/MANTIS does not use pass folders
        Dim PassesCustom As Boolean = False     'LOTIS/MANTIS does not use pass folders
        Dim CustomPasses() As String = Nothing  'LOTIS/MANTIS does not use pass folders

        If Not My.Computer.FileSystem.DirectoryExists(strCurFldr) Then
            ' no root folder
            strMsgText = "The folder tree for this inspection cannot be found. Would you like to create it at this time?"
            If MsgBox(strMsgText, MsgBoxStyle.Information + MsgBoxStyle.YesNo, "Inspection Root Folder Not Found") = MsgBoxResult.No Then
                Return False
                Exit Function
            Else
                'KM 7/8/2011
                If Not modProgram.SetupInspectionFolders(strCurFldr, intPasses, fAlphaPasses, PassesCustom, CustomPasses, mintInspectionType) Then
                    Return False
                    Exit Function
                End If
            End If
        End If

        ' if to here, folder exists or was created
        CheckRootFolder = True

    End Function

    Private Sub InspectionDetails_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

        Call SaveData()
        Call InspectionClosed(mintInspectionID, 2, mfIsNew, mfWasDeleted)

    End Sub

    Private Sub uiRootFolderName_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles uiRootFolderName.ButtonClick
        If IsNothing(e.Button.Tag) Then Exit Sub

        Select Case e.Button.Tag
            Case "OPEN"
                ' shows root folder
                ' verify it exists
                If Not CheckRootFolder(True) Then
                    Exit Sub
                End If
                ' show folder
                Interaction.Shell("explorer.exe """ & uiRootFolderName.Text & """", AppWinStyle.NormalFocus, False)
            Case Else
        End Select
    End Sub

    Private Sub TabPageChange(ByVal sender As Object, ByVal e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles uiMainXtraTabControl.SelectedPageChanged

        'Save data every time a tab page is changed.
        SaveData()

        'Also, since data loading impacts certain UI elements, some code needs to run again when a tab is selected, since data loading happens on tab selection
        'Is there a way around this? Research so far has not revealed a solution
        Select Case uiMainXtraTabControl.SelectedTabPage.Name

            Case "uiInspectionDetailsTabPage"
                UpdateCanCircularOption()
                UpdateRowTubeRadioGroupFormat()

            Case "uiProjectDescriptionTabPage"
                'Enable buttons on Tube Identification
                Dim bNumberingSystem As Boolean = Not uiNumberingSystem.SelectedIndex = -1
                uiInsertTubeIdentificationButton.Enabled = bNumberingSystem
                uiClearRedTubeIdentificationButton.Enabled = bNumberingSystem

            Case "uiTubeDetailsTabPage"
                'Enable tolerance boxes
                If uiTolerancesGroup.SelectedIndex = 0 Then
                    uiPlusTubeTolerance.Enabled = False
                    uiMinusTubeTolerance.Enabled = False
                ElseIf uiTolerancesGroup.SelectedIndex = 1 Then
                    uiPlusTubeTolerance.Enabled = True
                    uiMinusTubeTolerance.Enabled = True
                End If

                'Update Length Label
                If uiLeadingUnits.SelectedIndex = 0 Then
                    uiLengthLabel.Text = "Overall Tube Design Length (in./mm):"
                ElseIf uiLeadingUnits.SelectedIndex = 1 Then
                    uiLengthLabel.Text = "Overall Tube Design Length (mm/in.):"
                End If

                'Update row/tube count label
                If uiCanCircularOption.Checked Then
                    uiInspectionDetailsRowsTubesLabel.Text = String.Format("Can/Circular Reformer, Tubes: {0}", uiNumberOfTubes.Text)
                ElseIf mintInspectionType = LOTIS_SPARE Or mintInspectionType = MANTIS_SPARE Then 'spare
                    uiInspectionDetailsRowsTubesLabel.Text = String.Format("Tubes: {0}", uiNumberOfTubes.Text)
                    uiInspectionDetailsRowsTubesLabel.Location = New Point(275, 456)
                Else
                    uiInspectionDetailsRowsTubesLabel.Text = String.Format("Rows: {0}   Tubes per Row: {1}", uiNumberOfRows.Text, uiNumberOfTubes.Text)
                End If

                'Add text to location
                If String.IsNullOrEmpty(uiTubeInspectionLocations.Text) Then
                    Select Case mintInspectionType
                        Case MANTIS_AGED, MANTIS_NEW
                            uiTubeInspectionLocations.Text = "100% of tube axial length inside firebox (unless otherwise noted)"
                        Case LOTIS_AGED, LOTIS_NEW, LOTIS_SPARE, MANTIS_SPARE
                            uiTubeInspectionLocations.Text = "100% of tube axial length (unless otherwise noted)"
                    End Select
                End If

            Case "uiExecutiveSummaryTabPage"
                UpdateOverallMaxGrowth()
                UpdateBulgingTubeCount()

            Case "uiDetailedInspectionTabPage"
                'Setup Growth Range
                If uiTubeMaterials.SelectedIndex <> -1 Then SetupGrowthRange()

                'Set Level 1 label
                Dim strSelected As String
                strSelected = "Level 1 Checkboxes Selected Are:" & ControlChars.NewLine
                If uiExecSummaryRed.Checked Then strSelected += "Red" & ControlChars.NewLine
                If uiExecSummaryYellow.Checked Then strSelected += "Yellow" & ControlChars.NewLine
                If uiExecSummaryGreen.Checked Then strSelected += "Green" & ControlChars.NewLine
                If uiExecSummaryRed.Checked + uiExecSummaryYellow.Checked + uiExecSummaryGreen.Checked = 0 Then strSelected += "None"
                uiLevel1Label.Text = strSelected
                SumTubeCounts()

            Case "uiInspectionSummaryTabPage"
                UpdateCanCircularOption()
                SetToleranceLabels()

                Try
                    UpdateTubesWithFlaws()
                Catch

                End Try

            Case "uiLinkedFilesTabPage"
                uiAppendixItems.ItemIndex = 0

            Case "uiFieldReportTabPage"
                'Update the item summaries in case they were changed
                taPipeTubeItemSummaries.Fill(Me.QTT_InspectionsDataSets.tblPipeTubeItemSummaries)

        End Select

        DxValidationProvider.Validate()
        FixDisabledValidation()

    End Sub

    Private Function FixDisabledValidation()

        'Run through all the tabs and check for disabled and invisible controls
        'If the control is disabled or invisible, remove the validation so it doesn't flag as an error

        For Each myTab As DevExpress.XtraTab.XtraTabPage In uiMainXtraTabControl.TabPages
            For Each oControl As Control In myTab.Controls
                If oControl.CausesValidation Then
                    If oControl.Enabled = False And DxValidationProvider.Validate(oControl) = False Then
                        DxValidationProvider.RemoveControlError(oControl)
                    ElseIf oControl.Visible = False Then
                        DxValidationProvider.RemoveControlError(oControl)
                    End If
                End If
                For Each child As Control In oControl.Controls
                    If child.CausesValidation Then
                        If child.Enabled = False And DxValidationProvider.Validate(child) = False Then
                            DxValidationProvider.RemoveControlError(child)
                        ElseIf child.Visible = False Then
                            DxValidationProvider.RemoveControlError(child)
                        End If
                    End If
                Next
            Next
        Next
    End Function

#End Region

#Region "Footer"

    Private Sub uiSaveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiSaveButton.Click
        'Changes are saved when tab pages change, and auto-save can be enabled. 
        'But users like to have a button to click to save changes. Close events also save.
        Call SaveData()

    End Sub

    Private Sub uiSaveExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiSaveExitButton.Click

        ' saves any changes
        Dim bCancel As Boolean = SaveData()
        If Not bCancel Then Exit Sub
        ' permanently deletes any no longer needed appendix items
        ''Call DeleteUnNeededPipeTubeAppendixItems()
        ' close this form
        Me.Close()
        Main.UpdateInspections()

    End Sub

    'USER NO LONGER SELECTS "HAS APPENDIX" SO THIS SHOULD NO LONGER BE NECESSARY. APPENDICES ARE NOT KEPT AFTER DELETION.
    'Private Sub DeleteUnNeededPipeTubeAppendixItems()
    '    ' function that manages deletion of no longer needed pipetube items (e.g. user removed 'Has Appendix' attribute) 
    '    '  (these records are not immediately deleted to allow for re-selecting the 'Has Appendix' option without loss of existing appendix linked file data)
    '    Dim intPTI As Integer
    '    Dim oPTI As QTT_InspectionsDataSets.tblPipeTubeItemsRow

    '    'If mfCallCleanAppendixItems Then  ' determine if this routine needs to run
    '    '    Dim taAppnxItemDets As QTT_InspectionsDataSetsTableAdapters.vwAppendixItem_DetailsTableAdapter = New QTT_InspectionsDataSetsTableAdapters.vwAppendixItem_DetailsTableAdapter
    '    '    For intPTI = 0 To (bsInspections_PipeTubeItems.Count - 1)
    '    '        oPTI = bsInspections_PipeTubeItems.Item(intPTI).Row
    '    '        If Not oPTI.HasAppendix Then
    '    '            Call taAppnxItemDets.sprocDeleteAppendixItemsForPipeTube(oPTI.ID)
    '    '        End If
    '    '    Next intPTI
    '    '    ' flag that procedure has run
    '    '    mfPTI_AppendixesChecked = True
    '    'End If

    'End Sub

    Private Sub uiAutoSaveInterval_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiAutoSaveInterval.EditValueChanged
        'Set the autosave interval by changing the tick for the autosave timer.
        'Disable the timer as a first step to reset its counter
        tmrAutoSave.Enabled = False
        'if the input is non-zero, set the interval and enable the timer.
        If uiAutoSaveInterval.Value <> 0 Then
            'the timer's units are milliseconds, so convert the input minutes to milliseconds.
            tmrAutoSave.Interval = uiAutoSaveInterval.Value * 60 * 1000
            tmrAutoSave.Enabled = True
        End If
        If uiAutoSaveInterval.Value = 0 Then
            MsgBox("Setting the save interval to zero will turn the AutoSave feature OFF.", MsgBoxStyle.Critical, "AutoSave Warning!")
        End If
    End Sub

    Private Sub tmrAutoSave_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrAutoSave.Tick
        'Every time this ticks, make it autosave. 
        'Change the cursor so users know it is going on.
        Me.Cursor = Cursors.WaitCursor
        uilblLastAutoSave.Text = "Autosaving now..."

        'fire off the autosave event
        Call SaveData()
        uilblLastAutoSave.Text = "Last AutoSave: " & Now.ToLongTimeString
        'change the cursor back
        Me.Cursor = Cursors.Default
    End Sub

#End Region

#Region "Inspection Details"

    Private Function ViewInspectionDocument() As Boolean
        Const ReportSubFolder = "QIG Only\Field Report"
        Dim strFN As String = ""
        Dim strFolder As String = ""
        Dim fFileOK As Boolean

        ' get root report folder
        strFolder = My.Computer.FileSystem.CombinePath(uiRootFolderName.Text, ReportSubFolder)
        strFolder = strFolder & "\"
        ' compile report data
        With Me.QTT_InspectionsDataSets.tblInspections.Item(0)
            ' see if there is an existing report
            If Not .IsReportFilenameNull Then
                strFN = .ReportFilename
                ' if to here, move old file into "old reports" folder first
                With My.Computer.FileSystem
                    Try
                        fFileOK = .FileExists(strFolder & strFN)
                    Catch eX As Exception
                        'If eX.TargetSite.Attributes = 147 Then
                        '    MsgBox("The original report file could not be moved to the 'Old' folder - the file is currently open.  Close the file and try again. ", MsgBoxStyle.Information)
                        '    Exit Function
                        'Else
                        If vbNo = MsgBox("The current report file could not be accessed for an unknown reason.", MsgBoxStyle.Information, "Error Looking for Report File") Then
                            Exit Function
                        End If
                        'End If
                    End Try
                End With
                If fFileOK Then
                    ' create new MS Word report document
                    If (mMSWordReport Is Nothing) Then
                        mMSWordReport = New cMSWord
                    End If
                    mMSWordReport.OpenDocument(strFolder & strFN)
                Else
                    MsgBox("Could not find the current report file: " & strFolder & strFN, MsgBoxStyle.Information)
                    Exit Function
                End If
            Else
                MsgBox("An inspection report has not yet been created for this inspection.", MsgBoxStyle.Information)
                Exit Function
            End If
        End With
    End Function

    Private Sub AddTool(ByVal PassedRow As DataRowView)

        'Adds checked tool to inspection
        'KM 8/17/2011
        Dim oInspTool As QTT_InspectionsDataSets.tblInspectionsAndToolsRow
        Dim oCurTool As QTT_InspectionsDataSets.tblInspectionToolsRow = PassedRow.Row
        Dim curInsp As QTT_InspectionsDataSets.tblInspectionsRow = bsInspections.Current.Row

        'Add the item, using current tool data
        Dim drvTemp As DataRowView
        drvTemp = bsInspections_InspectionsAndTools.AddNew()
        oInspTool = drvTemp.Row
        oInspTool.FK_Tool = oCurTool.ID
        oInspTool.Desc = oCurTool.Desc
        oInspTool.FK_Inspection = curInsp.ID
        bsInspections_InspectionsAndTools.EndEdit()

    End Sub

    Private Sub RemoveTool(ByVal PassedRow As DataRowView)
        'Removes unchecked tool from inspection
        'KM 8/17/2011
        Dim i As Integer
        Dim oRemRow As QTT_InspectionsDataSets.tblInspectionsAndToolsRow
        Dim drvTemp As DataRowView
        Dim oCurTool As QTT_InspectionsDataSets.tblInspectionToolsRow = PassedRow.Row

        For i = 0 To (bsInspections_InspectionsAndTools.Count - 1)
            drvTemp = bsInspections_InspectionsAndTools.Item(i)
            oRemRow = drvTemp.Row
            If oRemRow.FK_Tool = oCurTool.ID Then
                bsInspections_InspectionsAndTools.Remove(drvTemp)
                Exit Sub
            End If
        Next
        MsgBox("Selected tool could not be removed.")

    End Sub

    Private Sub uiInspectionEndDate_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiInspectionEndDate.EditValueChanged
        'Validate the dates whenever the end date is changed
        'KM 7/11/2011
        If mfVisible Then
            If (DxValidationProvider.GetInvalidControls().Contains(uiInspectionStartDate)) Then DxValidationProvider.Validate(uiInspectionEndDate)
            If (DxValidationProvider.GetInvalidControls().Contains(uiInspectionEndDate)) Then DxValidationProvider.Validate(uiInspectionStartDate)
            DxValidationProvider.Validate()
            FixDisabledValidation()
        End If

    End Sub

    Private Sub uiInspectionStartDate_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiInspectionStartDate.EditValueChanged
        'Update 'end date' as needed.
        If mfVisible Then
            'The first time you change the start date, change the end date, too.
            If uiInspectionEndDate.EditValue < uiInspectionStartDate.EditValue Or DateValue(uiInspectionEndDate.EditValue) = Today() Then
                uiInspectionEndDate.EditValue = uiInspectionStartDate.EditValue
            End If

            DxValidationProvider.Validate()
            FixDisabledValidation()

            Call UpdateFolderName() 'Update folder name

        End If
    End Sub

    Private Sub uiToolsCheckedList_ItemCheck(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ItemCheckEventArgs) Handles uiToolsCheckedList.ItemCheck
        'KM 8/17/2011
        If bPreCheckTools = True Then Exit Sub
        'Get the active row
        Dim ActiveRow As DataRowView = uiToolsCheckedList.SelectedItem

        If e.State = CheckState.Checked Then
            AddTool(ActiveRow)
        Else
            RemoveTool(ActiveRow)
        End If

    End Sub

    Private Sub uiLeadInspector_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiLeadInspector.EditValueChanged
        'This just makes sure the phone number matches with its Lead Inspector
        'KM 8/12/2011
        Dim oInspector As QTT_InspectionsDataSets.tblQTT_LeadInspectorsRow
        ' get currently selected inspector
        Dim drvTemp As DataRowView = uiLeadInspector.GetSelectedDataRow()
        If drvTemp Is Nothing Then
            Exit Sub
        End If

        'Set the phone number if this inspector has one
        oInspector = drvTemp.Row
        If oInspector.IsPhoneNumberNull = True Then
            uiInspectorPhone.Text = String.Empty
        Else
            uiInspectorPhone.Text = oInspector.PhoneNumber
        End If
    End Sub

    Private Sub uiAddInspectorButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiAddInspectorButton.Click
        ' adds inspector to report

        ' get currently selected inspector from listbox
        Dim drvTemp As DataRowView = uiOtherInspectorsList.SelectedItem
        If drvTemp Is Nothing Then
            MsgBox("Please select an Inspector to add.")
            Exit Sub
        End If
        Dim oInspector As QTT_LookupsData.tblQTT_InspectorsRow = drvTemp.Row

        ' get currently selected role from listbox
        drvTemp = uiRolesList.SelectedItem
        If drvTemp Is Nothing Then
            MsgBox("Please select a Title/Role for the inspector that you want to add.")
            Exit Sub
        End If
        Dim oRole As QTT_LookupsData.tblQTT_RolesRow = drvTemp.Row

        Dim i As Integer
        Dim oIps As QTT_InspectionsDataSets.tblInspectionsAndInspectorsRow

        drvTemp = bsInspections.Current
        Dim curInsp As QTT_InspectionsDataSets.tblInspectionsRow = drvTemp.Row
        If oInspector.ID = 0 Then
            MsgBox("Select an inspector to add.")
            Exit Sub
        ElseIf oRole.ID = 0 Then
            MsgBox("Select a title/role to add.")
            Exit Sub
        ElseIf curInsp.ID = 0 Then
            MsgBox("Error - Inspection ID not found.")
            Exit Sub
        Else
            ' verify inspector not already added
            For i = 0 To (bsInspections_OtherInspectors.Count - 1)
                drvTemp = bsInspections_OtherInspectors.Item(i)
                oIps = drvTemp.Row
                If oIps.FK_Inspector = oInspector.ID Then
                    MsgBox("The Inspector '" & oInspector.FirstName & " " & oInspector.LastName & "' has already been added to this inspection.")
                    Exit Sub
                End If
            Next
        End If
        ' add the item, using current inspector and role data
        drvTemp = bsInspections_OtherInspectors.AddNew()
        oIps = drvTemp.Row
        oIps.FK_Role = oRole.ID
        oIps.FK_Inspector = oInspector.ID
        oIps.FK_Inspection = curInsp.ID
        oIps.NameAndRoleDescription = oInspector.FirstName & " " & oInspector.LastName & " (" & oRole.Description & ")"
        oIps.SortOrder = 20
        bsInspections_OtherInspectors.EndEdit()
        'If you add a inspector, you need to regenerate  the project description
        If uiProjectDescriptionEditor.Text <> "" Then
            SetTextMessage(uiProjectDescriptionEditor, "Data changes require that the text in the Project Description be regenerated.", 24)
        End If
    End Sub

    Private Sub uiRemoveInspectorButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiRemoveInspectorButton.Click

        ' removes selected Inspector from Inspection
        Dim i As Integer
        Dim oRemRow As QTT_InspectionsDataSets.tblInspectionsAndInspectorsRow
        ' get currently selected inspector from listbox
        Dim drvTemp As DataRowView = uiProjectInspectorsList.SelectedItem
        If drvTemp Is Nothing Then
            MsgBox("Please select an inspector to remove.")
            Exit Sub
        End If
        Dim oInspector As QTT_InspectionsDataSets.tblInspectionsAndInspectorsRow = drvTemp.Row
        For i = 0 To bsInspections_OtherInspectors.Count
            drvTemp = bsInspections_OtherInspectors.Item(i)
            oRemRow = drvTemp.Row
            If oRemRow.ID = oInspector.ID Then
                bsInspections_OtherInspectors.Remove(drvTemp)
                'If you add a inspector, you need to regenerate  the project description
                If uiProjectDescriptionEditor.Text <> "" Then
                    SetTextMessage(uiProjectDescriptionEditor, "Data changes require that the text in the Project Description be regenerated.", 24)
                End If
                Exit Sub
            End If
        Next
        MsgBox("Selected inspector could not be removed.")

    End Sub

    Private Sub UpdateRowTubeRadioGroupFormat() Handles TubeFormatRadioGroup.SelectedIndexChanged, RowFormatRadioGroup.SelectedIndexChanged

        'Update UI if tube/row format and data change
        If (TubeFormatRadioGroup.SelectedIndex = 2) Then
            uiCustomTubeFormatLabel.Enabled = True
            uiCustomTubeFormatEditor.Enabled = True
            ValidateCustomNumbers("Tube")
        Else
            uiCustomTubeFormatLabel.Enabled = False
            uiCustomTubeFormatEditor.Enabled = False
            uiCustomTubeFormatEditor.EditValue = DBNull.Value
            strCustomTubeNumbers = Nothing
        End If

        If (RowFormatRadioGroup.SelectedIndex = 2) Then
            uiCustomRowFormatLabel.Enabled = True
            uiCustomRowFormatEditor.Enabled = True
            ValidateCustomNumbers("Row")
        Else
            uiCustomRowFormatLabel.Enabled = False
            uiCustomRowFormatEditor.Enabled = False
            uiCustomRowFormatEditor.EditValue = DBNull.Value
            strCustomRowNumbers = Nothing
        End If

        With Me.QTT_InspectionsDataSets.tblInspections.Item(0)
            Select Case TubeFormatRadioGroup.SelectedIndex
                Case 0 'Alpha
                    .TubesInAlpha = False
                Case 1  'Numeric
                    .TubesInAlpha = True
                Case 2  'Custom
                    .TubesInAlpha = False
                Case Else
                    .TubesInAlpha = False
            End Select

            Select Case RowFormatRadioGroup.SelectedIndex
                Case 0  'Alpha
                    .RowsInAlpha = False
                Case 1  'Numeric
                    .RowsInAlpha = True
                Case 2  'Custom
                    .RowsInAlpha = False
                Case Else
                    .RowsInAlpha = False
            End Select
        End With

        DxValidationProvider.Validate()
        FixDisabledValidation()

        If RowFormatRadioGroup.OldEditValue <> RowFormatRadioGroup.EditValue Then
            'If you change the numbering format, you need to change the tube identification text
            If uiTubeIdentificationEditor.Text <> "" Then
                SetTextMessage(uiTubeIdentificationEditor, "Data changes require that the text in the Tube Identification section be regenerated.", 24)
            End If
        End If

    End Sub

    Private Sub UpdateCanCircularOption() Handles uiCanCircularOption.CheckedChanged

        'Update the UI if can/circular is checked or unchecked
        Dim bChecked As Boolean = uiCanCircularOption.Checked
        Dim bEnabled As Boolean = Not bChecked  'If Can/Circular checked, Enabled = False

        If bEnabled = False Then uiCustomRowFormatEditor.Enabled = bEnabled
        uiCustomRowFormatLabel.Enabled = bEnabled
        uiRowFormatGroupBox.Enabled = bEnabled
        uiRowsLabel.Enabled = bEnabled
        uiNumberOfRows.Enabled = bEnabled
        RowFormatRadioGroup.Enabled = bEnabled
        uiWorstTubeIDRow.Enabled = bEnabled
        uiDiaGrowthRowID.Enabled = bEnabled
        uiWorstFlawRowNumber.Enabled = bEnabled

        If Not mintInspectionType = LOTIS_SPARE And Not mintInspectionType = MANTIS_SPARE Then
            If bChecked Then
                'Change settings if canned reformer
                uiCustomRowFormatEditor.EditValue = DBNull.Value
                uiNumberOfRows.Text = 0
                RowFormatRadioGroup.SelectedIndex = -1
                uiTubeNumberInstructionsLabel.Text = "List total number of tubes."
                uiWorstTubeIDRow.EditValue = DBNull.Value
                uiDiaGrowthRowID.EditValue = DBNull.Value
                uiWorstFlawRowNumber.EditValue = DBNull.Value
            Else
                'Change them back if box reformer
                uiTubeNumberInstructionsLabel.Text = "List number of tubes per row."
            End If
        End If

        DxValidationProvider.Validate()
        FixDisabledValidation()

        If uiCanCircularOption.OldEditValue <> uiCanCircularOption.EditValue Then
            'If you change the can/circular option, you need to change the tube identification text and the inspection summary
            If uiTubeIdentificationEditor.Text <> "" Then
                SetTextMessage(uiTubeIdentificationEditor, "Data changes require that the text in the Tube Identification section be regenerated.", 24)
            End If
            Select Case mintInspectionType
                Case LOTIS_SPARE, LOTIS_NEW, MANTIS_SPARE, MANTIS_NEW
                    If uiInspectionSummaryEditor.Text <> "" Then
                        SetTextMessage(uiInspectionSummaryEditor, "Data changes require that the text in the this summary be regenerated.", 24)
                    End If
                Case LOTIS_AGED, MANTIS_AGED
                    If uiSummaryEditor.Text <> "" Then
                        SetTextMessage(uiSummaryEditor, "Data changes require that the text in the this summary be regenerated.", 24)
                    End If
                    If uiExecSummaryDetailsEditor.Text <> "" Then
                        SetTextMessage(uiExecSummaryDetailsEditor, "Data changes require that the text in these summaries be regenerated.", 24)
                        SetTextMessage(uiDiaGrowthSummaryEditor, "Data changes require that the text in these summaries be regenerated.", 24)
                        SetTextMessage(uiFFSEditor, "Data changes require that the text in these summaries be regenerated.", 24)
                        SetTextMessage(uiTubeHarvestingEditor, "Data changes require that the text in these summaries be regenerated.", 24)
                    End If
            End Select
        End If
    End Sub

    Private Sub uiCustomTubeFormatEditor_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiCustomTubeFormatEditor.EditValueChanged

        Dim arrCount As Integer = 0

        'Format the custom numbers so there are no spaces and then compare the number of items against the number of tubes
        strCustomTubeNumbers = (uiCustomTubeFormatEditor.EditValue).ToString

        If Not strCustomTubeNumbers = Nothing Then
            While InStr(strCustomTubeNumbers, ", ") <> 0
                strCustomTubeNumbers = strCustomTubeNumbers.Replace(", ", ",")
            End While
            While InStr(strCustomTubeNumbers, " ,") <> 0
                strCustomTubeNumbers = strCustomTubeNumbers.Replace(" ,", ",")
            End While
            arrCustomTubes = strCustomTubeNumbers.Split(",")
            arrCount = arrCustomTubes.GetLength(0)
        Else
            arrCustomTubes = Nothing
        End If
        If mfVisible Then
            Call ValidateCustomNumbers("Tube")
        End If

    End Sub

    Private Sub uiCustomRowFormatEditor_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiCustomRowFormatEditor.EditValueChanged

        Dim arrCount As Integer = 0

        'Format the custom numbers so there are no spaces and then compare the number of items against the number of rows
        If Not IsDBNull(uiCustomRowFormatEditor.EditValue) Then

            strCustomRowNumbers = (uiCustomRowFormatEditor.EditValue).ToString

            If Not strCustomRowNumbers = Nothing Then
                While InStr(strCustomRowNumbers, ", ") <> 0
                    strCustomRowNumbers = strCustomRowNumbers.Replace(", ", ",")
                End While
                While InStr(strCustomRowNumbers, " ,") <> 0
                    strCustomRowNumbers = strCustomRowNumbers.Replace(" ,", ",")
                End While
                arrCustomRows = strCustomRowNumbers.Split(",")
                arrCount = arrCustomRows.GetLength(0)
            Else
                arrCustomRows = Nothing
            End If
            If mfVisible Then
                Call ValidateCustomNumbers("Row")
            End If
        End If

    End Sub

    Private Function CustomFormatValid(ByVal strCustomText As String, ByVal iNumber As Integer, ByVal strRowTube As String) As Boolean
        'KM 8/5/2011 
        'Function to determine if custom number format is valid (same number of item names as number of rows/tubes)
        Dim result As Boolean = True

        Select Case strRowTube
            Case "Row"
                If RowFormatRadioGroup.SelectedIndex <> 2 Then Return result
            Case "Tube"
                If TubeFormatRadioGroup.SelectedIndex <> 2 Then Return result
        End Select

        Dim arrCustomItems() As String

        While InStr(strCustomText, ", ") <> 0
            strCustomText = strCustomText.Replace(", ", ",")
        End While
        While InStr(strCustomText, " ,") <> 0
            strCustomText = strCustomText.Replace(" ,", ",")
        End While
        If Microsoft.VisualBasic.Right(strCustomText, 1) = "," Then
            strCustomText = strCustomText.Substring(0, strCustomText.Length - 1)
        End If
        arrCustomItems = strCustomText.Split(",")
        Dim arrCount As Integer = arrCustomItems.GetLength(0)

        If arrCount <> CInt(iNumber) Then result = False

        Return result

    End Function

    Private Sub ValidateCustomNumbers(ByVal strRowOrTube As String)
        Dim CustomNumberValidationRule As New CustomNumberValidationRule()

        If strRowOrTube = "Tube" Then
            CustomNumberValidationRule.ErrorText = "The number of custom tube names must equal the number of tubes. Please check your names and comma delimiters."

            'Does the number of tube names equal the number of tubes?
            If CustomFormatValid(uiCustomTubeFormatEditor.Text, uiNumberOfTubes.Text, strRowOrTube) Then
                DxValidationProvider.SetValidationRule(uiCustomTubeFormatEditor, Nothing)
            Else
                DxValidationProvider.SetValidationRule(uiCustomTubeFormatEditor, CustomNumberValidationRule)
            End If

            DxValidationProvider.Validate(uiCustomTubeFormatEditor)

        End If

        If strRowOrTube = "Row" Then
            CustomNumberValidationRule.ErrorText = "The number of custom row names must equal the number of rows. Please check your names and comma delimiters."

            'Does the number of row names equal the number of rows?
            'If Not IsDBNull(uiCustomRowFormatEditor.EditValue) Then
            If CustomFormatValid(uiCustomRowFormatEditor.Text, uiNumberOfRows.Text, strRowOrTube) Then
                DxValidationProvider.SetValidationRule(uiCustomRowFormatEditor, Nothing)
            Else
                DxValidationProvider.SetValidationRule(uiCustomRowFormatEditor, CustomNumberValidationRule)
            End If
            'End If

            DxValidationProvider.Validate(uiCustomRowFormatEditor)
        End If

    End Sub

    Private Sub uiNumberOfTubes_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles uiNumberOfTubes.EditValueChanged

        'If the user enters more than 26 tubes, disable alpha numbering
        Try
            If CType(uiNumberOfTubes.Text, Integer) > 26 Then
                If TubeFormatRadioGroup.SelectedIndex = 1 Then TubeFormatRadioGroup.SelectedIndex = -1
                TubeFormatRadioGroup.Properties.Items(1).Enabled = False
            Else
                TubeFormatRadioGroup.Properties.Items(1).Enabled = True
            End If
        Catch
            'no catch required
        End Try

        'Validate if custom numbering
        If TubeFormatRadioGroup.SelectedIndex = 2 Then ValidateCustomNumbers("Tube")

    End Sub

    Private Sub uiNumberOfRows_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles uiNumberOfRows.SelectedIndexChanged

        'Validate if custom numbering
        If RowFormatRadioGroup.SelectedIndex = 2 Then ValidateCustomNumbers("Row")

    End Sub

#End Region

#Region "Customer Details"

    Private Sub UpdateFolderName()
        'Process update to folder name using combination of customer, unit, and current date

        Dim oCurRow As QTT_InspectionsDataSets.tblInspectionsRow = Me.QTT_InspectionsDataSets.tblInspections.Rows(0)
        Dim strCurDesc As String

        'Make sure the data in these boxes has been refreshed/retrieved. Becoming visible for the first time usually triggers these to update, but they may not be triggered at this point.
        'Get current customer name
        Dim strCustomerName As String = Trim(Mid(oCurRow.CustomerName, 1, 20))
        strCurDesc = Trim(Mid(oCurRow.CustomerName, 1, 20))

        'Location
        Dim strLocationDesc As String = Trim(Mid(oCurRow.LocationDesc, 1, 20))
        strCurDesc = strCurDesc & "-" & Trim(Mid(oCurRow.LocationDesc, 1, 20))

        'Unit Name
        Dim strHeaterName As String = "--Unit-NA"
        Dim strHeaterID As String
        If Len(oCurRow.UnitName.ToString) > 0 Then
            strHeaterName = Trim(oCurRow.UnitName)
            strCurDesc = strCurDesc & "_" & Trim(oCurRow.UnitName)
        Else
            strCurDesc = strCurDesc & "_Unit-NA"
        End If

        'Unit Number
        If Len(oCurRow.UnitNumber.ToString) > 0 Then
            strHeaterID = Trim(oCurRow.UnitNumber)
            strCurDesc = strCurDesc & "_" & Trim(oCurRow.UnitNumber) & ""
        End If

        'LOTIS/MANTIS Type
        Select Case oCurRow.FK_InspectionType
            Case LOTIS_AGED, MANTIS_AGED   'Aged
                strCurDesc = strCurDesc & "_" & "Aged"
            Case LOTIS_NEW, MANTIS_NEW   'NewInstalled
                strCurDesc = strCurDesc & "_" & "NewInstalled"
            Case LOTIS_SPARE, MANTIS_SPARE   'Spare
                strCurDesc = strCurDesc & "_" & "Uninstalled"
            Case Else
                'Don't do anything
        End Select

        'Start Date
        strCurDesc = strCurDesc & "-" & Format(uiInspectionStartDate.EditValue, "dMMMyyyy").ToUpper

        'Strip invalid/illegal characters and limit to 64 characters
        'KM Changed to 84. May need to go back to 64.
        strCurDesc = modProgram.CleanInvalidFilenameChars(strCurDesc)
        If Len(strCurDesc) > 84 Then strCurDesc = Mid(strCurDesc, 1, 84)

        ' update
        Dim FolderName As String = strCurDesc
        If My.Computer.FileSystem.DirectoryExists(My.Computer.FileSystem.CombinePath(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & My.Settings.RootDataFolder, FolderName)) = False Then
            Call RenameInspectionFolder(uiRootFolderName.Text, FolderName)
            uiRootFolderName.Text = My.Computer.FileSystem.CombinePath(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & My.Settings.RootDataFolder, FolderName)
            Me.Text = FolderName
            uiCustomerNameLabel.Text = strCustomerName
            uiLocationNameLabel.Text = strLocationDesc
            uiHeaterNameLabel.Text = strHeaterName & "-" & strHeaterID
            uiDateLabel.Text = Format(uiInspectionStartDate.DateTime, "MMMM d, yyyy")
        Else : MsgBox("An inspection with the same folder already exists. This inspection's folder will not be changed.")
        End If

    End Sub

    Private Sub RenameInspectionFolder(ByVal OldFolder As String, ByVal NewFolderName As String)
        'Renames the inspection folder from the old folder to the new folder. If the old folder doesn't exist, it just creates the new one instead.

        Try
            With My.Computer.FileSystem
                If .DirectoryExists(OldFolder) = True Then
                    .RenameDirectory(OldFolder, NewFolderName)
                Else
                    Dim strMsgText As String = "The folder tree for this inspection cannot be found. Would you like to create it at this time?"
                    If MsgBox(strMsgText, MsgBoxStyle.Information + MsgBoxStyle.YesNo, "Inspection Root Folder Not Found") = MsgBoxResult.No Then
                        'Just exit if they say no.
                        Exit Sub
                    Else
                        'Create the folders if they say yes.
                        Dim tmpAlphaPasses As Boolean = False
                        Dim tmpCustomPasses As Boolean = False
                        Dim strcustompasses() = Nothing
                        Call SetupInspectionFolders(NewFolderName, uiNumberOfTubes.Text, tmpAlphaPasses, tmpCustomPasses, strcustompasses, mintInspectionType)
                    End If
                End If
            End With
        Catch
            MessageBox.Show("Unable to rename the folder. Make sure all files and Explorer windows are closed." & Environment.NewLine & "Click OK to try again.")
            RenameInspectionFolder(OldFolder, NewFolderName)
        End Try
    End Sub

    Private Sub uiReformerName_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiReformerName.Leave
        'Rename if reformer name changed
        If mfSetupCompleted = True Then
            Dim oCurRow As QTT_InspectionsDataSets.tblInspectionsRow = Me.QTT_InspectionsDataSets.tblInspections.Rows(0)
            Dim tmpOriginalName As String = oCurRow.UnitName
            If tmpOriginalName <> uiReformerName.Text Then
                oCurRow.UnitName = uiReformerName.Text
                Call UpdateFolderName()
            End If

        End If
    End Sub

    Private Sub uiHeaterID_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles uiReformerID.Leave
        'Rename if reformer ID changed
        If mfSetupCompleted = True Then
            Dim oCurRow As QTT_InspectionsDataSets.tblInspectionsRow = Me.QTT_InspectionsDataSets.tblInspections.Rows(0)
            Dim tmpOriginalNumber As String = oCurRow.UnitNumber
            If tmpOriginalNumber <> uiReformerID.Text Then
                oCurRow.UnitNumber = uiReformerID.Text
                Call UpdateFolderName()
            End If

        End If
    End Sub

    Private Sub uiLocation_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiLocation.Leave
        'Rename if location name changed
        If mfSetupCompleted = True Then
            Dim oCurRow As QTT_InspectionsDataSets.tblInspectionsRow = Me.QTT_InspectionsDataSets.tblInspections.Rows(0)
            Dim tmpOriginalLoc As String = oCurRow.LocationDesc
            If tmpOriginalLoc <> uiLocation.Text Then
                oCurRow.LocationDesc = uiLocation.Text
                Call UpdateFolderName()
            End If

        End If
    End Sub

    Private Sub uiCustomers_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiCustomers.EditValueChanged
        If mfSetupCompleted = True Then
            Dim oCurRow As QTT_InspectionsDataSets.tblInspectionsRow = Me.QTT_InspectionsDataSets.tblInspections.Rows(0)
            Dim OldDBValue As String = oCurRow.CustomerName
            oCurRow.CustomerName = uiCustomers.Text
            If OldDBValue <> uiCustomers.Text Then Call UpdateFolderName()

        End If
    End Sub

    Private Sub uiCountry_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uiCountry.EditValueChanged
        'Populate state/province field based on country selection
        'KM 7/3/2011

        If (Nz(uiCountry.EditValue) = "United States") Then
            uiState.Properties.Items.Clear()
            uiState.Properties.Items.AddRange(GetListOfUSStates)
            uiState.Properties.Buttons(0).Visible = True
        ElseIf (Nz(uiCountry.EditValue) = "Canada") Then
            uiState.Properties.Items.Clear()
            uiState.Properties.Items.AddRange(GetListOfCanadianProvinces)
            uiState.Properties.Buttons(0).Visible = True
        Else
            uiState.Properties.Items.Clear()
            uiState.Properties.Buttons(0).Visible = False
        End If

        If uiCountry.OldEditValue.ToString <> "" Then
            If uiCountry.OldEditValue.ToString <> uiCountry.EditValue Then uiState.EditValue = String.Empty
        End If

    End Sub

    Private Sub uiGridViewContacts_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles uiGridViewContacts.CellValueChanged
        'When the contact row is updated, update the database.
        Me.TblContactInformationTableAdapter1.Update(QTT_InspectionsDataSets.tblContactInformation)
    End Sub

    Private Sub uiGridViewContacts_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles uiGridViewContacts.InitNewRow
        'This snippet makes sure the inspection ID is automatically set to the correct value when a new customer contact is created, relating that contact to this inspection
        Dim View As ColumnView = sender
        View.SetRowCellValue(e.RowHandle, View.Columns("FK_Inspection"), Inspection_ID)
    End Sub

    Private Sub uiGridViewContacts_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles uiGridViewContacts.RowUpdated
        'When the contact row is updated, update the database.
        Me.TblContactInformationTableAdapter1.Update(QTT_InspectionsDataSets.tblContactInformation)
    End Sub

    Private Sub uiGridControlContacts_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles uiGridControlContacts.EmbeddedNavigator.ButtonClick

        'This makes sure the rows are deleted properly when you try to delete contact information for an inspection
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If MessageBox.Show("Do you want to delete the current row?", "Confirm deletion", _
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) <> DialogResult.Yes Then
                e.Handled = True
            Else
                e.Handled = True
                uiGridViewContacts.CloseEditor()
                uiGridViewContacts.UpdateCurrentRow()
                bstblContactInformation.RemoveCurrent()
                bstblContactInformation.EndEdit()
                Me.TblContactInformationTableAdapter1.Update(QTT_InspectionsDataSets.tblContactInformation)
            End If
        End If
    End Sub

    Private Sub uiHistoryInsertSampleTextButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiHistoryInsertSampleTextButton.Click
        'Inserts sample text from database using embedded field codes
        Call ProcessInsertSampleText(uiPriorHistoryEditor, FIELD_PRIOR_HISTORY, "Replace")
    End Sub

    Private Sub uiFirstInspectionInsertSampleTextButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiFirstInspectionInsertSampleTextButton.Click, uiFirstInspectionInsertSampleTextButton.Click
        'Inserts sample text from database using embedded field codes
        Call ProcessInsertSampleText(uiPriorHistoryEditor, FIELD_FIRST_INSPECTION, "Replace")

    End Sub

    Private Sub uiClearPriorHistoryRedButton_Click(sender As System.Object, e As System.EventArgs) Handles uiClearPriorHistoryRedButton.Click
        'Get rid of the red text in the document text
        Dim doc As Document = uiPriorHistoryEditor.Document
        Dim i As Integer = doc.Paragraphs.Count - 1
        Dim range As DocumentRange
        Dim cp As CharacterProperties
        For i = 0 To doc.Paragraphs.Count - 1
            range = uiPriorHistoryEditor.Document.Paragraphs(i).Range
            cp = doc.BeginUpdateCharacters(range)
            cp.ForeColor = Color.Black
            doc.EndUpdateCharacters(cp)
        Next i
    End Sub

#End Region

#Region "Tube Description & Identification"

    Private Function ProcessInsertSampleText(ByVal oTextBox As RichEditControl, ByVal DefTextFieldName As String, ByVal strOption As String) As Boolean

        ' retrieve sample text from database
        Dim sText As String = GetDefaultFieldText(mintInspectionType, DefTextFieldName)
        If sText.Length = 0 Then Exit Function
        ' process any field codes in text
        sText = ReplaceDefaultTextFieldCodes(sText)


        If DefTextFieldName = "TUBE_IDENTIFICATION_BOX_PL"
            sText= sText.Replace("Figure 7","Figure 8")
            sText= sText.Replace("Figure 6","Figure 7")
            sText= sText.Replace("Figure 4","Figure 5")
        End If

          If DefTextFieldName = "TUBE_IDENTIFICATION_CAN"
            sText= sText.Replace("Figure 7","Figure 8")
            sText= sText.Replace("Figure 6","Figure 7")
        End If

        ' update as needed
        With oTextBox
            Select Case strOption
                Case "Replace"
                    .RtfText = sText
                    .Modified = True
                Case "Append"
                    Dim pos As DocumentPosition = .Document.CaretPosition
                    Dim doc As SubDocument = pos.BeginUpdateDocument()
                    doc.AppendRtfText(sText)
                    pos.EndUpdateDocument(doc)
                Case Else
                    Exit Function
            End Select
        End With
    End Function

    Private Function ReplaceDefaultTextFieldCodes(ByVal sTextIn As String) As String
        'Only partly updated to remove fields not required for LOTIS/MANTIS

        ' performs replacing of 'Field Codes' sample text with actual data from current inspection
        ' (uses .NET regular expressions implementation)

        ' constants - note that regex escape characters ("\") have been added to these constants to accommodate the [ and ] characters, i.e. "[COMPANY]" is "\[COMPANY\]"
        ' Company related
        Const FCODE_COMPANY = "\[COMPANY\]"
        Const FCODE_CONTACTNAME = "\[CONTACTNAME\]"
        Const FCODE_CONTACT_TITLE = "\[CONTACT_TITLE\]"

        ' Inspection Related
        Const FCODE_SPARE_UNINSTALLED = "\[SPARE\]"

        ' Location Related
        Const FCODE_LOCATIONNAME = "\[LOCATIONNAME\]"
        Const FCODE_LOCATIONCITYSTATECOUNTRY = "\[LOCATIONCITYSTATECOUNTRY\]"
        Const FCODE_LOCATIONCITY = "\[LOCATIONCITY\]"

        ' Unit Related
        Const FCODE_UNITTYPE = "\[UNITTYPE\]"
        Const FCODE_UNITNUMBER = "\[UNITNUMBER\]"
        Const FCODE_UNITDESC = "\[UNITDESC\]"

        ' PipeTube Related - unless explicitly stated, these default to using the first PTI when an inspection has more than one.
        Const FCODE_PIPE_SCHEDULE = "\[PIPE_SCHEDULE\]"
        Const FCODE_TOTAL_NUMBER_OF_TUBES = "\[TOTAL_NUMBER_OF_TUBES\]"
        Const FCODE_PTI_THRESHOLD = "\[PTI_THRESHOLD\]"
        Const FCODE_PTI_CURRENT_DESC = "\[PTI_CURRENT_DESC\]"
        Const FCODE_PTI_FIRST_DESC = "\[PTI_FIRST_DESC\]"
        Const FCODE_PTI_LAST_DESC = "\[PTI_LAST_DESC\]"
        Const FCODE_PTI_LAST_NUMSECTIONS = "\[PTI_LAST_NUMSECTIONS\]"

        'Rows and Tubes (Inspection Level data)
        Const FCODE_NUMBER_ROWS = "\[NUMBER_ROWS\]"
        Const FCODE_NUMBER_TUBES = "\[NUMBER_TUBES\]"
        Const FCODE_ROW_FIRST = "\[ROW_FIRST\]"
        Const FCODE_ROW_LAST = "\[ROW_LAST\]"
        Const FCODE_TUBE_FIRST = "\[TUBE_FIRST\]"
        Const FCODE_TUBE_LAST = "\[TUBE_LAST\]"

        ' Pass Related
        Const FCODE_PASS_NUMBER = "\[PASS_NUMBER\]"
        Const FCODE_FTIS_APPENDIX_LETTER = "\[FTIS_APPENDIX_LETTER\]"
        Const FCODE_FIRST_PASS_NAME = "\[FIRST_PASS_NUMBER\]"
        Const FCODE_FINAL_PASS_NAME = "\[FINAL_PASS_NUMBER\]"

        ' # of Inspectors
        Const FCODE_NUMBER_0F_INSPECTORS = "\[NUMBER_0F_INSPECTORS\]"

        Dim oCurInsp As QTT_InspectionsDataSets.tblInspectionsRow
        Dim strTemp As String = ""
        Dim PassesCustom As Boolean = fTubesCustom
        Dim CustomPasses() As String = arrCustomTubes

        ' verify / get ref to current data
        If bsInspections.Count = 0 Then
            GoTo PROC_EXIT
        Else
            oCurInsp = bsInspections.Item(0).row
        End If
        '(1) Company related
        ' 1.A: Company Name
        If Not oCurInsp.IsCustomerNameNull Then
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_COMPANY, oCurInsp.CustomerName.ToString)
        End If
        ' 1.B: Contact Name.
        Dim dtCustomers As New QTT_InspectionsDataSets.tblContactInformationDataTable
        TblContactInformationTableAdapter1.Fill(dtCustomers)
        Dim drowsCustomersForThisInspection() As DataRow = dtCustomers.Select("FK_inspection = " & mintInspectionID)
        If drowsCustomersForThisInspection.Length > 0 AndAlso IsDBNull(drowsCustomersForThisInspection(0)("Name")) = False Then 'If there is at least one customer contact, replace the default field with the first customer's name.
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_CONTACTNAME, drowsCustomersForThisInspection(0)("Name"))
            If IsDBNull(drowsCustomersForThisInspection(0)("Title")) = False Then   'Insert Title if it exists
                sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_CONTACT_TITLE, drowsCustomersForThisInspection(0)("Title"))
            End If
        End If
        ' (2) Location Related
        ' 2.A Location Name
        If Not oCurInsp.IsLocationDescNull Then
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_LOCATIONNAME, oCurInsp.LocationDesc.ToString)
        End If

        'New City Name
        'KM 11/21/2011
        If Not oCurInsp.IsCityNull Then
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_LOCATIONCITY, oCurInsp.City.ToString)
        End If

        ' 2.B Location City, State, Zip
        If Not oCurInsp.IsCityNull Then
            Dim sbCityEtc As New System.Text.StringBuilder(oCurInsp.City)
            If Not oCurInsp.IsStateNull Then
                sbCityEtc.Append(", " & oCurInsp.State)
            End If
            If Not oCurInsp.IsCountryNull Then
                sbCityEtc.Append(", " & oCurInsp.Country)
            End If
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_LOCATIONCITYSTATECOUNTRY, sbCityEtc.ToString)
        End If

        'Inspection Related
        If Not oCurInsp.IsSpareUninstalledNull Then
            Dim strSpareUninstalled As String
            If oCurInsp.SpareUninstalled = 0 Then strSpareUninstalled = "spare"
            If oCurInsp.SpareUninstalled = 1 Then strSpareUninstalled = "uninstalled"
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_SPARE_UNINSTALLED, strSpareUninstalled)
        End If

        ' (3) Unit Related
        ' 3.A Unit Name
        If Not oCurInsp.IsUnitNameNull Then
            If oCurInsp.UnitName.ToString = "Not Supplied" Then
                sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_UNITDESC, "[NOT SUPPLIED]")
            Else
                sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_UNITDESC, oCurInsp.UnitName.ToString)
            End If
        End If
        ' 3.B Unit Number
        If Not oCurInsp.IsUnitNumberNull Then
            If oCurInsp.UnitNumber.ToString = "Not Supplied" Then
                sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_UNITNUMBER, "[NOT SUPPLIED]")
            Else
                sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_UNITNUMBER, oCurInsp.UnitNumber.ToString)
            End If
        End If
        ' 3.C Unit Type
        If Not oCurInsp.IsUnitTypeNull Then
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_UNITTYPE, oCurInsp.UnitType.ToString)
        End If

        'Row Labels
        If oCurInsp.NumberOfRows > 0 Then
            Dim RowsCustom As Boolean
            Dim CustomRows As Array
            If CustomFormatValid(uiCustomRowFormatEditor.Text, uiNumberOfRows.Text, "Row") = True And RowFormatRadioGroup.SelectedIndex = 2 Then
                RowsCustom = True
                CustomRows = Split(uiCustomRowFormatEditor.Text, ",")
            End If
            Dim strFirstRowLabel As String = GetTubeRowLabel(1, oCurInsp.RowsInAlpha, RowsCustom, CustomRows)
            Dim strLastRowLabel As String = GetTubeRowLabel(oCurInsp.NumberOfRows, oCurInsp.RowsInAlpha, RowsCustom, CustomRows)
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_ROW_FIRST, strFirstRowLabel)
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_ROW_LAST, strLastRowLabel)
        End If

        'Tube Labels
        If oCurInsp.NumberOfTubes > 0 Then
            Dim TubesCustom As Boolean
            Dim CustomTubes As Array
            If CustomFormatValid(uiCustomTubeFormatEditor.Text, uiNumberOfTubes.Text, "Tube") = True And TubeFormatRadioGroup.SelectedIndex = 2 Then
                TubesCustom = True
                CustomTubes = Split(uiCustomTubeFormatEditor.Text, ",")
            End If
            Dim strFirstTubeLabel As String = GetTubeRowLabel(1, oCurInsp.TubesInAlpha, TubesCustom, CustomTubes)
            Dim strLastTubeLabel As String = GetTubeRowLabel(oCurInsp.NumberOfTubes, oCurInsp.TubesInAlpha, TubesCustom, CustomTubes)
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_TUBE_FIRST, strFirstTubeLabel)
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_TUBE_LAST, strLastTubeLabel)
        End If

        'Number of Rows/Tubes
        Dim strNumberofRows As String
        Dim strNumberofTubes As String
        If Not oCurInsp.IsNumberOfRowsNull Then strNumberofRows = oCurInsp.NumberOfRows
        If Not oCurInsp.IsNumberOfTubesNull Then strNumberofTubes = oCurInsp.NumberOfTubes
        sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_NUMBER_ROWS, strNumberofRows)
        sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_NUMBER_TUBES, strNumberofTubes)

        ' (4) PipeTube Related - unless explicitly stated, these default to using the first PTI when an inspection has more than one.
        ' get references to current pipe tube item
        Dim cmPTI As CurrencyManager = bsInspections_PipeTubeItems.CurrencyManager
        Dim intPTIpos As Integer = cmPTI.Position
        Dim oCurPipe As QTT_InspectionsDataSets.tblPipeTubeItemsRow = cmPTI.Current.Row
        ' 4.A PipeSchedule text
        If Not oCurPipe.IsPipeScheduleNull Then
            strTemp = oCurPipe.PipeSchedule
            Const REPLACE_LF = " / "
            ' replace any linefeeds and vertical tabs with REPLACE_LF
            strTemp = System.Text.RegularExpressions.Regex.Replace(strTemp, vbVerticalTab, REPLACE_LF)
            strTemp = System.Text.RegularExpressions.Regex.Replace(strTemp, vbNewLine, REPLACE_LF)
            ' u replace on fieldcode
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_PIPE_SCHEDULE, strTemp)
        End If
        ' 4.B Number of tubes
        If Not oCurPipe.IsLengthInspectedNull Then
            strTemp = oCurPipe.LengthInspected
            ' call replace on fieldcode
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_TOTAL_NUMBER_OF_TUBES, strTemp)
        End If
        ' 4.C PTI Threshold
        If Not oCurPipe.IsLengthInspectedNull Then
            strTemp = oCurPipe.LengthInspected
            ' call replace on fieldcode
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_PTI_THRESHOLD, strTemp)
        End If
        ' 4.D.0  PIT Description - current Item (added May 2007)
        If Not oCurPipe.IsDescriptionNull Then
            strTemp = oCurPipe.Description
            ' call replace on fieldcode
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_PTI_CURRENT_DESC, strTemp)
        End If
        ' 4.D.1 PTI Description - First Item
        If cmPTI.Position <> 0 Then cmPTI.Position = 0
        oCurPipe = cmPTI.Current.Row ' bsInspections_PipeTubeItems.Current.Row
        If Not oCurPipe.IsDescriptionNull Then
            strTemp = oCurPipe.Description
            ' call replace on fieldcode
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_PTI_FIRST_DESC, strTemp)
        End If
        ' 4.E.1 PTI Description - Last Item
        If cmPTI.Position <> (cmPTI.Count - 1) Then cmPTI.Position = (cmPTI.Count - 1)
        oCurPipe = cmPTI.Current.Row ' bsInspections_PipeTubeItems.Current.Row
        If Not oCurPipe.IsDescriptionNull Then
            strTemp = oCurPipe.Description
            ' call replace on fieldcode
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_PTI_LAST_DESC, strTemp)
        End If
        ' 4.E.2 PTI Number of Sections - Last Item
        If Not oCurPipe.IsLengthInspectedNull Then
            strTemp = oCurPipe.LengthInspected
            ' call replace on fieldcode
            sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_PTI_LAST_NUMSECTIONS, strTemp)
        End If
        ' 4. cleanup: reset bs position as needed
        If cmPTI.Position <> intPTIpos Then cmPTI.Position = intPTIpos

        ' (6) Number of Inspectors
        Dim intNumInspectors As Integer = bsInspections_OtherInspectors.Count
        If Not oCurInsp.IsFK_PrimaryInspectorNull Then
            intNumInspectors += 1
        End If
        If Microsoft.VisualBasic.Left(ConvertNum(intNumInspectors), 1) = "e" Then
            strTemp = String.Format("an {0} ({1}) person", ConvertNum(intNumInspectors), intNumInspectors)
        Else
            strTemp = String.Format("a {0} ({1}) person", ConvertNum(intNumInspectors), intNumInspectors)
        End If

        sTextIn = System.Text.RegularExpressions.Regex.Replace(sTextIn, FCODE_NUMBER_0F_INSPECTORS, strTemp)

PROC_EXIT:
        Return sTextIn

    End Function

    Private Sub uiInsertProjectDescriptionButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiInsertProjectDescriptionButton.Click
        Dim RequiredInputSuggestions As String

        RequiredInputSuggestions = CheckForRequiredInput(False, "uiCustomerDetailsTabPage", uiProjectDescriptionTabPage)
        RequiredInputSuggestions += vbCrLf + CheckForRequiredInput(False, "uiInspectionDetailsTabPage", uiProjectDescriptionTabPage)
        If RequiredInputSuggestions <> Nothing Then
            'None of these matter for Project Description, and it is easier to remove them than try to exclude them.
            RequiredInputSuggestions = Replace(RequiredInputSuggestions, "Plant Process Type cannot be blank.", "")
            RequiredInputSuggestions = Replace(RequiredInputSuggestions, "Process Flow Direction cannot be blank.", "")
            RequiredInputSuggestions = Replace(RequiredInputSuggestions, "Reformer OEM cannot be blank.", "")
            RequiredInputSuggestions = Replace(RequiredInputSuggestions, "Burner Configuration cannot be blank.", "")
            RequiredInputSuggestions = Replace(RequiredInputSuggestions, "Please select a Tube # format.", "")
            RequiredInputSuggestions = Replace(RequiredInputSuggestions, "Please select a Row # format.", "")
            RequiredInputSuggestions = Replace(RequiredInputSuggestions, "Number of rows cannot be zero.", "")
            RequiredInputSuggestions = Replace(RequiredInputSuggestions, "Number of tubes cannot be zero.", "")
            If uiGridViewContacts.RowCount < 1 Then RequiredInputSuggestions += vbCrLf & "You must add a Customer Contact and Title."
            RequiredInputSuggestions = Replace(RequiredInputSuggestions, vbCrLf & vbCrLf, "")
            If RequiredInputSuggestions.Length > 5 Then
                Dim UserChoice = MessageBox.Show(RequiredInputSuggestions & vbCrLf & vbCrLf & "Would you like to continue anyway?", "Some Required Input Not Given", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
                If UserChoice = DialogResult.No Then Exit Sub
            End If
        End If

        Dim strDescription As String
        'Select description based on Spare, Can, or Box
        If mintInspectionType = LOTIS_SPARE Or mintInspectionType = MANTIS_SPARE Then 'spare 
            strDescription = FIELD_PROJ_DESCRIPTION
        ElseIf uiCanCircularOption.Checked Then
            strDescription = FIELD_PROJ_DESCRIPTION_CAN
        Else
            strDescription = FIELD_PROJ_DESCRIPTION_BOX
        End If
        'Replace existing text with inserted text
        Call ProcessInsertSampleText(uiProjectDescriptionEditor, strDescription, "Replace")

    End Sub

    Private Sub uiClearRedProjectDescriptionButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiClearRedProjectDescriptionButton.Click

        'Clear red text
        Dim doc As Document = uiProjectDescriptionEditor.Document
        Dim i As Integer = doc.Paragraphs.Count - 1
        Dim range As DocumentRange
        Dim cp As CharacterProperties
        For i = 0 To doc.Paragraphs.Count - 1
            range = uiProjectDescriptionEditor.Document.Paragraphs(i).Range
            cp = doc.BeginUpdateCharacters(range)
            cp.ForeColor = Color.Black
            doc.EndUpdateCharacters(cp)
        Next i


    End Sub

    Private Sub uiNumberingSystem_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles uiNumberingSystem.SelectedIndexChanged

        'Enable buttons when numbering system is selected
        If uiNumberingSystem.SelectedIndex <> -1 Then
            uiInsertTubeIdentificationButton.Enabled = True
            uiClearRedTubeIdentificationButton.Enabled = True
        End If

        'If you change the numbering system, you have to regenerate the text
        If mfVisible Then
            If uiTubeIdentificationEditor.Text <> "" Then
                SetTextMessage(uiTubeIdentificationEditor, "Data changes require that the text in the Tube Identification section be regenerated.", 24)
            End If
        End If
    End Sub

    Private Sub uiInsertTubeIdentificationButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiInsertTubeIdentificationButton.Click
        Dim RequiredInputSuggestions As String

        RequiredInputSuggestions = CheckForRequiredInput(False, "uiInspectionDetailsTabPage", uiProjectDescriptionTabPage)
        If RequiredInputSuggestions <> Nothing Then
            RequiredInputSuggestions = Replace(RequiredInputSuggestions, "Lead Inspector cannot be blank.", "")
            RequiredInputSuggestions = Replace(RequiredInputSuggestions, "Title/Role cannot be blank.", "")
            RequiredInputSuggestions = Trim(Replace(RequiredInputSuggestions, vbCrLf & vbCrLf, ""))
            If RequiredInputSuggestions.Length > 5 Then
                Dim UserChoice = MessageBox.Show(RequiredInputSuggestions & vbCrLf & vbCrLf & "Would you like to continue anyway?", "Some Required Input Not Given", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
                If UserChoice = DialogResult.No Then Exit Sub
            End If
        End If
        'Insert first text block based on numbering system selected
        Select Case uiNumberingSystem.SelectedIndex
            Case 0
                Call ProcessInsertSampleText(uiTubeIdentificationEditor, FIELD_NUMBERING_SYSTEM_QUEST, "Replace")
            Case 1
                Call ProcessInsertSampleText(uiTubeIdentificationEditor, FIELD_NUMBERING_SYSTEM_CUSTOMER, "Replace")
        End Select

        'Insert second text block based on can or box/spare
        Select Case uiCanCircularOption.Checked
            Case True
                Call ProcessInsertSampleText(uiTubeIdentificationEditor, FIELD_TUBE_IDENTIFICATION_CAN, "Append")
            Case False
                If mintInspectionType = LOTIS_SPARE Or mintInspectionType = MANTIS_SPARE Then 'spare
                    Call ProcessInsertSampleText(uiTubeIdentificationEditor, FIELD_TUBE_IDENTIFICATION, "Append")
                Else
                    'If box, use singular text if only one row, otherwise plural text
                    If CType(uiNumberOfRows.Text, Integer) = 1 Then
                        Call ProcessInsertSampleText(uiTubeIdentificationEditor, FIELD_TUBE_IDENTIFICATION_BOX_SINGULAR, "Append")
                    Else
                        Call ProcessInsertSampleText(uiTubeIdentificationEditor, FIELD_TUBE_IDENTIFICATION_BOX_PLURAL, "Append")
                    End If
                End If
            Case Else

        End Select

    End Sub

    Private Sub uiClearRedTubeIdentificationButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiClearRedTubeIdentificationButton.Click

        'Clear red text
        Dim doc As Document = uiTubeIdentificationEditor.Document
        Dim i As Integer = doc.Paragraphs.Count - 1
        Dim range As DocumentRange
        Dim cp As CharacterProperties
        For i = 0 To doc.Paragraphs.Count - 1
            range = uiTubeIdentificationEditor.Document.Paragraphs(i).Range
            cp = doc.BeginUpdateCharacters(range)
            cp.ForeColor = Color.Black
            doc.EndUpdateCharacters(cp)
        Next i

    End Sub

    Private Sub uiMonthCalendar_DateSelected(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiMonthYearButton.Click, uiDayDateButton.Click, uiDateButton.Click
        Dim myButton As DevExpress.XtraEditors.SimpleButton
        myButton = sender
        Dim oRange As DevExpress.XtraRichEdit.API.Native.Implementation.NativeDocumentRange
        Dim pos As API.Native.DocumentPosition
        'Replace selected text with date in format determined by button text
        If oREControl.Document.Selection.Length > 0 Then
            Dim tempRange As DevExpress.XtraRichEdit.API.Native.Implementation.NativeDocumentRange = oREControl.Document.Selection
            oREControl.Document.Delete(tempRange)
        End If
        oRange = oREControl.Document.Selection
        pos = oRange.Start
        Select Case myButton.Name.ToString
            Case "uiMonthYearButton"
                oREControl.Document.InsertText(pos, uiMonthCalendar.SelectionStart.ToString("MMMM o\f yyyy"))
            Case "uiDayDateButton"
                oREControl.Document.InsertText(pos, uiMonthCalendar.SelectionStart.ToString("dddd, MMMM d"))
            Case "uiDateButton"
                oREControl.Document.InsertText(pos, uiMonthCalendar.SelectionStart.ToString("MMMM d, yyyy"))
            Case Else
                oREControl.Document.InsertText(pos, uiMonthCalendar.SelectionStart.ToString("MMMM d, yyyy"))
        End Select

    End Sub

    Private Sub uiTubeIdentificationEditor_LostFocus(sender As Object, e As System.EventArgs) Handles uiTubeIdentificationEditor.LostFocus
        'Pass control back to Month/Calendar control on Lost Focus
        oREControl = uiTubeIdentificationEditor

    End Sub

    Private Sub uiProjectDescriptionEditor_LostFocus(sender As Object, e As System.EventArgs) Handles uiProjectDescriptionEditor.LostFocus
        'Pass control back to Month/Calendar control on Lost Focus
        oREControl = uiProjectDescriptionEditor

    End Sub

    Private Sub uiShowHideButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiShowHideButton.Click

        'Show/Hide hidden marks (paragraphs/spaces/tabs) in richtextedit controls
        If bVisible Then
            With uiProjectDescriptionEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Hidden
                .TabCharacter = RichEditFormattingMarkVisibility.Hidden
                .Space = RichEditFormattingMarkVisibility.Hidden
            End With
            With uiTubeIdentificationEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Hidden
                .TabCharacter = RichEditFormattingMarkVisibility.Hidden
                .Space = RichEditFormattingMarkVisibility.Hidden
            End With
            bVisible = False
        Else
            With uiProjectDescriptionEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Visible
                .TabCharacter = RichEditFormattingMarkVisibility.Visible
                .Space = RichEditFormattingMarkVisibility.Visible
            End With
            With uiTubeIdentificationEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Visible
                .TabCharacter = RichEditFormattingMarkVisibility.Visible
                .Space = RichEditFormattingMarkVisibility.Visible
            End With
            bVisible = True
        End If

    End Sub

#End Region

#Region "Tube Details"

    Private Sub uiTubeMaterials_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles uiTubeMaterials.SelectedIndexChanged, uiTubeMaterials.TextChanged

        'If the user changes the type of material used, change the diametrical growth labels and enable the tube count cells
        Call SetupGrowthRange()

        If Not uiAgedTubeCount1.Enabled Then
            uiAgedTubeCount1.Enabled = True
            uiAgedTubeCount2.Enabled = True
            uiAgedTubeCount3.Enabled = True
            uiAgedTubeCount4.Enabled = True
            uiAgedTubeCount5.Enabled = True
        End If

    End Sub

    Private Sub uiTolerancesGroup_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles uiTolerancesGroup.SelectedIndexChanged

        'If the user changes the tolerance choice from Quest to customer or customer to Quest, change the options available
        If mfVisible Then
            If uiTolerancesGroup.SelectedIndex = 0 Then         'Quest Standard
                uiTubeDesignToleranceDisplay.Text = Nothing
                uiPlusTubeTolerance.Text = Nothing
                uiMinusTubeTolerance.Text = Nothing
                Call SetupStandardTolerances()
            ElseIf uiTolerancesGroup.SelectedIndex = 1 Then     'Customer defined
                uiTubeDesignToleranceDisplay.Text = Nothing
                uiPlusTubeTolerance.Text = Nothing
                uiMinusTubeTolerance.Text = Nothing
                uiPlusTubeTolerance.Enabled = True
                uiMinusTubeTolerance.Enabled = True
                Call SetToleranceLabels()
            End If
        End If
    End Sub

    Private Function SetupStandardTolerances()

        'if Quest standard is being used, then use this routine
        If uiTolerancesGroup.SelectedIndex = 0 Then
            Select Case mintInspectionType
                Case LOTIS_AGED, LOTIS_NEW, LOTIS_SPARE    'LOTIS
                    If uiLeadingUnits.SelectedIndex = 0 Then 'imperial
                        uiPlusTubeTolerance.Text = "0.000"
                        uiMinusTubeTolerance.Text = "0.039"
                        uiTubeDesignToleranceDisplay.Text = "+0.000 in. (0.00 mm) / -0.039 in. (1.00 mm)"
                    Else    'metric
                        uiPlusTubeTolerance.Text = "0.00"
                        uiMinusTubeTolerance.Text = "1.00"
                        uiTubeDesignToleranceDisplay.Text = "+0.00 mm (0.000 in.) / -1.00 mm (0.039 in.)"
                    End If

                Case MANTIS_AGED, MANTIS_NEW, MANTIS_SPARE  'MANTIS
                    If uiLeadingUnits.SelectedIndex = 0 Then 'imperial
                        uiPlusTubeTolerance.Text = "0.079"
                        uiMinusTubeTolerance.Text = "0.000"
                        uiTubeDesignToleranceDisplay.Text = "+0.079 in. (2.00 mm) / -0.000 in. (0.00 mm)"
                    Else    'metric
                        uiPlusTubeTolerance.Text = "2.00"
                        uiMinusTubeTolerance.Text = "0.0"
                        uiTubeDesignToleranceDisplay.Text = "+2.00 mm (0.079 in.) / -0.00 mm (0.000 in.)"
                    End If
            End Select
        End If
        uiPlusTubeTolerance.Enabled = False
        uiMinusTubeTolerance.Enabled = False
        'Otherwise, use the customer values

    End Function

    Private Sub SetupCustomerTolerances(sender As System.Object, e As System.EventArgs) Handles uiPlusTubeTolerance.EditValueChanged, uiMinusTubeTolerance.EditValueChanged

        'Routine for using customer tolerances (when values are entered in the plus/minus textboxes)
        If mfVisible Then
            Dim dblPlus As Double
            Dim dblMinus As Double
            If Not String.IsNullOrEmpty(uiPlusTubeTolerance.Text) And Not String.IsNullOrEmpty(uiMinusTubeTolerance.Text) Then
                Try
                    dblPlus = CType(uiPlusTubeTolerance.Text, Double)
                    dblMinus = CType(uiMinusTubeTolerance.Text, Double)
                    If uiLeadingUnits.SelectedIndex = 0 Then 'imperial
                        uiTubeDesignToleranceDisplay.Text = "+" & Math.Round(dblPlus, 3).ToString("0.000") & " in. (" & Math.Round(dblPlus * 25.4, 2).ToString("0.00") & " mm) / -" & Math.Round(dblMinus, 3).ToString("0.000") & " in. (" & Math.Round(dblMinus * 25.4, 2).ToString("0.00") & " mm)"
                    ElseIf uiLeadingUnits.SelectedIndex = 1 Then    'metric
                        uiTubeDesignToleranceDisplay.Text = "+" & Math.Round(dblPlus, 2).ToString("0.00") & " mm (" & Math.Round(dblPlus / 25.4, 3).ToString("0.000") & " in.) / -" & Math.Round(dblMinus, 2).ToString("0.00") & " mm (" & Math.Round(dblMinus / 25.4, 3).ToString("0.000") & " in.)"
                    End If
                Catch
                End Try

            End If
        End If

    End Sub

    Private Function SetupGrowthRange()

        Select Case uiTubeMaterials.Text
            Case "MicroAlloy", "HP Modified"
                uiDiaGrowthLabel1.Text = "Diametrical growth greater than 5.50%"
                uiDiaGrowthLabel2.Text = "Diametrical growth between 3.00% and 5.50%"
                uiDiaGrowthLabel3.Text = "Diametrical growth between 2.00% and 2.99%"
                uiDiaGrowthLabel4.Text = "Diametrical growth between 1.00% and 1.99%"
                uiDiaGrowthLabel5.Text = "Diametrical growth less than 1.00%"
            Case "HK-40"
                uiDiaGrowthLabel1.Text = "Diametrical growth greater than 3.00%"
                uiDiaGrowthLabel2.Text = "Diametrical growth between 2.00% and 3.00%"
                uiDiaGrowthLabel3.Text = "Diametrical growth between 1.50% and 1.99%"
                uiDiaGrowthLabel4.Text = "Diametrical growth between 1.00% and 1.49%"
                uiDiaGrowthLabel5.Text = "Diametrical growth less than 1.00%"
            Case Else
                uiDiaGrowthLabel1.Text = "Diametrical growth greater than 5.50% (edit as necessary)"
                uiDiaGrowthLabel2.Text = "Diametrical growth between 3.00% and 5.50% (edit as necessary)"
                uiDiaGrowthLabel3.Text = "Diametrical growth between 2.00% and 2.99% (edit as necessary)"
                uiDiaGrowthLabel4.Text = "Diametrical growth between 1.00% and 1.99% (edit as necessary)"
                uiDiaGrowthLabel5.Text = "Diametrical growth less than 1.00% (edit as necessary)"
        End Select

    End Function

    Private Sub uiTubeInnerDiameter_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles uiTubeInnerDiameter.EditValueChanged
        Dim dblID As Double
        'if the inner diameter value is changed, update the display value (imperial or metric)
        Try
            dblID = CType(uiTubeInnerDiameter.Text, Double)
        Catch ex As Exception
            uiTubeInnerDiameterDisplay.Text = "Invalid data"
            Return
        End Try

        If uiLeadingUnits.SelectedIndex = 0 Then 'Corresponds to Imperial first and metric in parenthesis
            uiTubeInnerDiameterDisplay.Text = Math.Round(dblID, 3).ToString("0.000") & " in. (" & Math.Round(dblID * 25.4, 2).ToString("0.00") & " mm)"
        ElseIf uiLeadingUnits.SelectedIndex = 1 Then 'Corresponds to Metric first instead
            uiTubeInnerDiameterDisplay.Text = Math.Round(dblID, 2).ToString("0.00") & " mm (" & Math.Round(dblID / 25.4, 3).ToString("0.000") & " in.)"
        End If
    End Sub

    Private Sub uiTubeOuterDiameter_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles uiTubeOuterDiameter.EditValueChanged
        Dim dblOD As Double
        'if the outer diameter value is changed, update the display value (imperial or metric)
        Try
            dblOD = CType(uiTubeOuterDiameter.Text, Double)
        Catch ex As Exception
            uiTubeOuterDiameterDisplay.Text = "Invalid data"
            Return
        End Try

        If uiLeadingUnits.SelectedIndex = 0 Then 'Corresponds to Imperial first and metric in parenthesis
            uiTubeOuterDiameterDisplay.Text = Math.Round(dblOD, 3).ToString("0.000") & " in. (" & Math.Round(dblOD * 25.4, 2).ToString("0.00") & " mm)"
        ElseIf uiLeadingUnits.SelectedIndex = 1 Then 'Corresponds to Metric first
            uiTubeOuterDiameterDisplay.Text = Math.Round(dblOD, 2).ToString("0.00") & " mm (" & Math.Round(dblOD / 25.4, 3).ToString("0.000") & " in.)"
        End If

    End Sub

    Private Sub uiTubeWallThickness_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles uiTubeWallThickness.EditValueChanged
        Dim dblWall As Double
        'if the wall thickness value is changed, update the display value (imperial or metric)
        Try
            dblWall = CType(uiTubeWallThickness.Text, Double)
        Catch ex As Exception
            uiTubeWallThicknessDisplay.Text = "Invalid data"
            Return
        End Try

        If uiLeadingUnits.SelectedIndex = 0 Then 'Corresponds to imperial first and metric in parenthesis
            uiTubeWallThicknessDisplay.Text = Math.Round(dblWall, 3).ToString("0.000") & " in. (" & Math.Round(dblWall * 25.4, 2).ToString("0.00") & " mm)"
        ElseIf uiLeadingUnits.SelectedIndex = 1 Then 'Corresponds to metric first
            uiTubeWallThicknessDisplay.Text = Math.Round(dblWall, 2).ToString("0.00") & " mm (" & Math.Round(dblWall / 25.4, 3).ToString("0.000") & " in.)"
        End If

    End Sub

    Private Sub uiTubeLength_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles uiTubeLength.EditValueChanged
        Dim dblLen As Double
        'if the length value is changed, update the display value (imperial or metric)
        Try
            dblLen = CType(uiTubeLength.Text, Double)
        Catch ex As Exception
            uiTubeLengthDisplay.Text = "Invalid data"
            Return
        End Try

        If uiLeadingUnits.SelectedIndex = 0 Then 'Corresponds to imperial first and metric in parenthesis
            uiTubeLengthDisplay.Text = Math.Round(dblLen, 1).ToString("0.0") & " in. (" & Math.Round(dblLen * 0.0254, 2).ToString("0.00") & " m)"
        ElseIf uiLeadingUnits.SelectedIndex = 1 Then 'Corresponds to metric first
            uiTubeLengthDisplay.Text = Math.Round(dblLen / 1000, 2).ToString("0.00") & " m (" & Math.Round(dblLen / 25.4, 1).ToString("0.0") & " in.)"
        End If

    End Sub

    Private Sub uiLeadingUnits_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiLeadingUnits.SelectedIndexChanged

        Dim Units As String = "imperial"
        Dim sDT As String = uiTubeDesignToleranceDisplay.Text
        Dim sID As String = uiTubeInnerDiameterDisplay.Text
        Dim sOD As String = uiTubeOuterDiameterDisplay.Text
        Dim sWT As String = uiTubeWallThicknessDisplay.Text
        Dim sLen As String = uiTubeLengthDisplay.Text

        If mfVisible Then
            If uiLeadingUnits.SelectedIndex = 0 Then
                Units = "imperial"
                If ReverseRequired(sDT, "m") Then
                    uiPlusTubeTolerance.Text = Math.Round(CType(uiPlusTubeTolerance.Text, Double) / 25.4, 3).ToString("0.000")
                    uiMinusTubeTolerance.Text = Math.Round(CType(uiMinusTubeTolerance.Text, Double) / 25.4, 3).ToString("0.000")
                End If
                If ReverseRequired(sID, "m") Then
                    uiTubeInnerDiameter.Text = Math.Round(CType(uiTubeInnerDiameter.Text, Double) / 25.4, 3).ToString("0.000")
                End If
                If ReverseRequired(sOD, "m") Then
                    uiTubeOuterDiameter.Text = Math.Round(CType(uiTubeOuterDiameter.Text, Double) / 25.4, 3).ToString("0.000")
                End If
                If ReverseRequired(sWT, "m") Then
                    uiTubeWallThickness.Text = Math.Round(CType(uiTubeWallThickness.Text, Double) / 25.4, 3).ToString("0.000")
                End If
                If ReverseRequired(sLen, "m") Then
                    uiTubeLength.Text = Math.Round(CType(uiTubeLength.Text, Double) / 25.4, 1).ToString("0.0")
                End If
                'If you change text of label here, also change it in TabPageChange procedure
                uiLengthLabel.Text = "Overall Tube Design Length (in./mm):"
            Else
                Units = "metric"
                If ReverseRequired(sDT, "i") Then
                    uiPlusTubeTolerance.Text = Math.Round(CType(uiPlusTubeTolerance.Text, Double) * 25.4, 2).ToString("0.00")
                    uiMinusTubeTolerance.Text = Math.Round(CType(uiMinusTubeTolerance.Text, Double) * 25.4, 2).ToString("0.00")
                End If
                If ReverseRequired(sID, "i") Then
                    uiTubeInnerDiameter.Text = Math.Round(CType(uiTubeInnerDiameter.Text, Double) * 25.4, 2).ToString("0.00")
                End If
                If ReverseRequired(sOD, "i") Then
                    uiTubeOuterDiameter.Text = Math.Round(CType(uiTubeOuterDiameter.Text, Double) * 25.4, 2).ToString("0.00")
                End If
                If ReverseRequired(sWT, "i") Then
                    uiTubeWallThickness.Text = Math.Round(CType(uiTubeWallThickness.Text, Double) * 25.4, 2).ToString("0.00")
                End If
                If ReverseRequired(sLen, "i") Then
                    uiTubeLength.Text = Math.Round(CType(uiTubeLength.Text, Double) * 25.4, 2).ToString("0.00")
                End If
                'If you change text of label here, also change it in TabPageChange procedure
                uiLengthLabel.Text = "Overall Tube Design Length (mm/in.):"
            End If

            uiTubeUnitGroup.Text = "Units (enter data in " & Units & " units):"
            SetupStandardTolerances()
        End If

    End Sub

    Private Function ReverseRequired(ByVal sInput As String, ByVal sChar As String)

        If Mid(sInput, InStr(sInput, " ") + 1, 1) = sChar Then
            Return True
        Else
            Return False
        End If

    End Function

#End Region

#Region "Executive Summary"

    Private Sub uiSummaryEditorButton_Click(sender As System.Object, e As System.EventArgs) Handles uiSummaryEditorButton.Click

        Dim strRedCount As String = "No"
        Dim iRedCount As Integer
        Dim iYellowCount As Integer
        Dim iRedYellow As Integer
        Dim strTubes As String
        Dim RequiredInputSuggestions As String

        RequiredInputSuggestions = CheckForRequiredInput(False, "uiExecutiveSummaryTabPage")
        If RequiredInputSuggestions <> Nothing Then
            Dim UserChoice = MessageBox.Show(RequiredInputSuggestions & vbCrLf & vbCrLf & "Would you like to continue anyway?", "Some Required Input Not Given", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
            If UserChoice = DialogResult.No Then Exit Sub
        End If

        'Set red and yellow count values (will error if values are left blank)
        Try
            iRedCount = CType(uiRedTubeCount.Text, Integer)
        Catch ex As Exception
            'MsgBox("You must enter a value for ""Red Tube Count;"" enter 0 if none.")
            'Exit Sub
            'User has already been warned about the red tube count.
            'Do nothing.
        End Try

        Try
            iYellowCount = CType(uiYellowTubeCount.Text, Integer)
            iRedYellow = CType(uiRedTubeCount.Text, Integer) + CType(uiYellowTubeCount.Text, Integer)
        Catch ex As Exception
            'MsgBox("You must enter a value for ""Yellow Tube Count;"" enter 0 if none.")
            'Exit Sub
            'User has already been warned about the yellow tube count.
            'Do nothing.
        End Try

        Dim strRedTubes As String = "tubes"
        Dim strYellowTubes As String = "tubes"
        Dim strRedYellowTubes As String = "tubes"
        If iRedCount = 1 Then strRedTubes = "tube"
        If iYellowCount = 1 Then strYellowTubes = "tube"
        If iRedYellow = 1 Then strRedYellowTubes = "tube"

        uiSummaryEditor.ResetText()
        Dim doc As Document = uiSummaryEditor.Document
        Dim i As Integer = 0  'Paragraph numbering
        doc.BeginUpdate()

        Dim list As AbstractNumberingList = uiSummaryEditor.Document.AbstractNumberingLists.Add()
        list.NumberingType = NumberingType.Bullet

        Dim level As ListLevel = list.Levels(0)
        level.ParagraphProperties.LeftIndent = 100
        level.ParagraphProperties.FirstLineIndentType = ParagraphFirstLineIndent.Hanging
        level.ParagraphProperties.FirstLineIndent = 50
        level.CharacterProperties.FontName = "Symbol"
        level.DisplayFormatString = New String(ChrW(&HB7), 1)
        level.ParagraphProperties.SpacingAfter = 30
        level.ParagraphProperties.SpacingBefore = 30

        ' Create a numbering list. It is based on a previously defined abstract list with ID = 0.
        doc.NumberingLists.Add(0)

        'First paragraph
        Dim strPara1 As String
        Dim strTotalTubes As String = uiOverallTubesInspected.Text
        Dim strUnit As String = uiReformerName.Text
        Dim strUnitID As String = uiReformerID.Text
        doc.AppendText(String.Format("The inspection of the tubes within {0}, {1}, included {2} aged tubes.", strUnit, strUnitID, strTotalTubes))
        doc.AppendParagraph()

        'Bullet Items
        '1st bullet

        i = 1

        If iRedYellow > 0 Then
            AppendListParagraph(doc, String.Format("{0} {1} failed the API 579-1 Level 1 assessment for the next {2}-year operating period.", iRedYellow.ToString, strRedYellowTubes, uiOperatingPeriod.Text), i, 0, 0)
            i = i + 1
        ElseIf iRedYellow = 0 Then
            AppendListParagraph(doc, String.Format("No {1} failed the API 579-1 Level 1 assessment for the next {2}-year operating period.", iRedYellow.ToString, strRedYellowTubes, uiOperatingPeriod.Text), i, 0, 0)
            i = i + 1
        End If

        '2nd bullet
        Dim strSwelling As String = "None"
        Dim strHasHasNot As String = "has not"

        If Not String.IsNullOrEmpty(uiSwellingOver3PercentCount.Text) Then
            If CType(uiSwellingOver3PercentCount.Text, Integer) > 0 Then
                strSwelling = uiSwellingOver3PercentCount.Text
                strHasHasNot = "has"
            End If
        End If
        AppendListParagraph(doc, String.Format("{0} of the {1} tubes inspected contained internal diametrical growth greater than or equal to 3.00%, indicating significant creep damage {2} occurred in these tubes.", strSwelling, strTotalTubes, strHasHasNot), i, 0, 0)
        i = i + 1

        '3rd bullet
        Dim strDiaGrowthMaxPercent As String = uiDiaGrowthMaxPercent.Text
        Dim strDiaGrowthRowID As String = uiDiaGrowthRowID.Text
        Dim strDiaGrowthTubeID As String = uiDiaGrowthTubeID.Text
        Dim strMaxGrowthLocation As String = uiMaxGrowthLocation.Text
        If uiCanCircularOption.Checked Then
            AppendListParagraph(doc, String.Format("The maximum internal diametrical growth detected during the inspection was {0}% in Tube {1}. The growth was located {2}. Damage appeared equally distributed around the circumference in the form of swelling rather than bulging.", strDiaGrowthMaxPercent, strDiaGrowthTubeID, strMaxGrowthLocation), i, 0, 0)
        Else
            AppendListParagraph(doc, String.Format("The maximum internal diametrical growth detected during the inspection was {0}% in Tube {1} located in Row {2}. The growth was located {3}. Damage appeared equally distributed around the circumference in the form of swelling rather than bulging.", strDiaGrowthMaxPercent, strDiaGrowthTubeID, strDiaGrowthRowID, strMaxGrowthLocation), i, 0, 0)
        End If

        i = i + 1

        '4th bullet
        Dim intOverallMaxGrowth As Integer = uiOverallMaxGrowth.SelectedIndex 'A majority, all or none
        Dim strOverallMaxGrowth As String
        Dim strOverallMaxGrowthLocation As String = uiOverallMaxGrowthLocation.Text
        If intOverallMaxGrowth <> 2 Then
            If intOverallMaxGrowth = 0 Then strOverallMaxGrowth = "A majority"
            If intOverallMaxGrowth = 1 Then strOverallMaxGrowth = "All"
            AppendListParagraph(doc, String.Format("{0} of the internal diametrical growth found during this inspection was located {1}.", strOverallMaxGrowth, strOverallMaxGrowthLocation), i, 0, 0)
            i = i + 1
        End If

        '5th bullet
        If mintInspectionType = MANTIS_AGED Then 'MANTIS Aged 
            Dim strCrackingCount As String
            Dim strCrackingCountText As String
            Dim strCrackingTubes As String = "tubes"
            If Not String.IsNullOrEmpty(uiCracking.Text) Then
                If CType(uiCracking.Text, Integer) > 0 Then
                    strCrackingCount = uiCracking.Text
                    strCrackingCountText = ConvertNum(strCrackingCount)
                    strCrackingCountText = Mid(strCrackingCountText, 1, 1).ToUpper & Mid(strCrackingCountText, 2)
                    If CType(strCrackingCount, Integer) = 1 Then strCrackingTubes = "tube"
                    AppendListParagraph(doc, String.Format("{0} tubes were inspected with eddy current during the inspection to detect the presence of cracking. {1} ({2}) {3} contained cracking.", strTotalTubes, strCrackingCountText, strCrackingCount, strCrackingTubes), i, 0, 0)
                Else
                    AppendListParagraph(doc, String.Format("{0} tubes were inspected with eddy current during the inspection to detect the presence of cracking. None of the tubes contained cracking.", strTotalTubes, strCrackingCount, strCrackingTubes), i, 0, 0)
                End If
            End If
            i = i + 1
        End If

        '6th bullet
        Dim strBulging As String
        Dim strBulgingPercent As String
        Dim strWorstBulgingTubeID As String
        Dim strWorstBulgingRowID As String
        If Not String.IsNullOrEmpty(uiBulgingTubeCount.Text) Then
            If CType(uiBulgingTubeCount.Text, Integer) > 0 Then
                strBulging = uiBulgingTubeCount.Text
                strBulgingPercent = uiBulgingWorstPercent.Text
                strWorstBulgingRowID = uiWorstTubeIDRow.Text
                strWorstBulgingTubeID = uiWorstTubeIDTube.Text
                If uiCanCircularOption.Checked Then
                    If CType(strBulging, Integer) = 1 Then 'Singular
                        AppendListParagraph(doc, String.Format("Tube {0} contain a {1}% bulge. Damage appeared on one side of the circumference in the form of bulging rather than swelling.", strWorstBulgingTubeID, strBulgingPercent), i, 0, 0)
                    Else 'Plural
                        AppendListParagraph(doc, String.Format("{0} tubes contained bulges with the worst bulge ({1}%) located in Tube {2}. Damage appeared on one side of the circumference in the form of bulging rather than swelling.", strBulging, strBulgingPercent, strWorstBulgingTubeID), i, 0, 0)
                    End If
                Else
                    If CType(strBulging, Integer) = 1 Then 'Singular
                        AppendListParagraph(doc, String.Format("Tube {0} in Row {1} contained a {2}% bulge. Damage appeared on one side of the circumference in the form of bulging rather than swelling.", strWorstBulgingTubeID, strWorstBulgingRowID, strBulgingPercent), i, 0, 0)
                    Else 'Plural
                        AppendListParagraph(doc, String.Format("{0} tubes contained bulges with the worst bulge ({1}%) located in Tube {2} in Row {3}. Damage appeared on one side of the circumference in the form of bulging rather than swelling.", strBulging, strBulgingPercent, strWorstBulgingTubeID, strWorstBulgingRowID), i, 0, 0)
                    End If
                End If
                i = i + 1
            End If
        End If

        'Level 1 results (red/yellow/green)
        Dim strLevel1 As String
        If uiExecSummaryRed.Checked And Not uiExecSummaryYellow.Checked Then
            strLevel1 = String.Format("Quest Integrity Group recommends that the {0} {1} that failed the API 579-1 Level 1 assessment be retired now or a higher level engineering assessment be accomplished to determine suitability for operation during the next operating period. ", iRedCount.ToString, strRedTubes)
        ElseIf uiExecSummaryRed.Checked And uiExecSummaryYellow.Checked Then
            strLevel1 = String.Format("Quest Integrity Group recommends that the {0} {1} that failed the API 579-1 Level 1 assessment in the Red Category be retired now and the {2} {3} that failed the API 579-1 Level 1 assessment in the Yellow Category be evaluated with a higher level engineering assessment to determine suitability for operation during the next operating period. ", iRedCount.ToString, strRedTubes, iYellowCount.ToString, strYellowTubes)
        ElseIf uiExecSummaryYellow.Checked Then
            strLevel1 = String.Format("Quest Integrity Group recommends that the {0} {1} that failed the API 579-1 Level 1 assessment be evaluated with a higher level engineering assessment to determine suitability for operation during the next operating period. ", iYellowCount.ToString, strYellowTubes)
        ElseIf uiExecSummaryGreen.Checked Then
            strLevel1 = String.Format("All inspected tubes passed the API 579-1 Level 1 assessment. ")
        End If

        If uiExecSummaryRed.Checked And uiExecSummaryYellow.Checked Then
            strLevel1 += String.Format("If the reformer is operated under the same conditions that resulted in the measured long-term creep damage, it is likely that tube failures would occur for the {0} in the Red Category and unlikely tube failures would occur for the {1} in the Yellow Category prior to the next scheduled shutdown.", strRedTubes, strYellowTubes)
        ElseIf uiExecSummaryRed.Checked Then
            strLevel1 += String.Format("If the reformer is operated under the same conditions that resulted in the measured long-term creep damage, it is likely that tube failures would occur prior to the next scheduled shutdown.")
        ElseIf uiExecSummaryYellow.Checked Then
            strLevel1 += String.Format("If the reformer is operated under the same conditions that resulted in the measured long-term creep damage, it is unlikely that tube failures would occur prior to the next scheduled shutdown.")
        ElseIf uiExecSummaryGreen.Checked Then
            strLevel1 += String.Format("If the reformer is operated under the same conditions that resulted in the measured long-term creep damage, it is unlikely that tube failures would occur prior to the next scheduled shutdown.")
        End If

        'Paragraph Items
        doc.AppendText(strLevel1)
        doc.EndUpdate()

    End Sub

    Private Sub AppendListParagraph(ByVal doc As Document, ByVal text As String, ByVal paragraphIndex As Integer, ByVal numberingLevel As Integer, ByVal numberingIndex As Integer)

        doc.AppendText(text)
        doc.AppendParagraph()
        Dim paragraph As Paragraph = doc.Paragraphs(paragraphIndex)
        paragraph.ListIndex = numberingIndex
        paragraph.ListLevel = numberingLevel

    End Sub

    Private Sub uiSummaryEditorShowHideButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiSummaryEditorShowHideButton.Click
        'Show/Hide paragraph marks

        If bVisible2 Then
            With uiSummaryEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Hidden
                .TabCharacter = RichEditFormattingMarkVisibility.Hidden
                .Space = RichEditFormattingMarkVisibility.Hidden
            End With
            bVisible2 = False
        Else
            With uiSummaryEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Visible
                .TabCharacter = RichEditFormattingMarkVisibility.Visible
                .Space = RichEditFormattingMarkVisibility.Visible
            End With
            bVisible2 = True
        End If
    End Sub

    Private Sub UpdateBulgingTubeCount() Handles uiBulgingTubeCount.EditValueChanged
        'Disable bulging fields if 0 flaws
        Dim bEnabled As Boolean = True
        If Not String.IsNullOrEmpty(uiBulgingTubeCount.Text) Then
            If CType(uiBulgingTubeCount.Text, Integer) = 0 Then
                bEnabled = False
                If (uiWorstTubeIDRow.Text <> String.Empty) Then uiWorstTubeIDRow.Text = String.Empty
                If (uiWorstTubeIDTube.Text <> String.Empty) Then uiWorstTubeIDTube.Text = String.Empty
                If (uiBulgingWorstPercent.Text <> String.Empty) Then uiBulgingWorstPercent.Text = String.Empty
            End If
        End If
        If Not uiCanCircularOption.Checked Then uiWorstTubeIDRow.Enabled = bEnabled
        uiWorstTubeIDTube.Enabled = bEnabled
        uiBulgingWorstPercent.Enabled = bEnabled

        If bEnabled = True Then DxValidationProvider.Validate()
        FixDisabledValidation()

    End Sub

    Private Sub UpdateOverallMaxGrowth() Handles uiOverallMaxGrowth.SelectedIndexChanged

        'If overall max growth choice changed, update options
        If uiOverallMaxGrowth.SelectedIndex = 2 Then
            uiOverallMaxGrowthLocation.SelectedIndex = -1
            uiOverallMaxGrowthLocation.Enabled = False
            uiOverallMaxGrowthLocationLabel.Enabled = False
            FixDisabledValidation()
        Else
            uiOverallMaxGrowthLocation.Enabled = True
            uiOverallMaxGrowthLocationLabel.Enabled = True
            uiOverallMaxGrowthLocation.DoValidate()
        End If

        Try
            If uiOverallMaxGrowth.OldEditValue <> uiOverallMaxGrowth.EditValue Then
                'If you change the max growth option, you need to change the summary text
                If uiSummaryEditor.Text <> "" Then
                    SetTextMessage(uiSummaryEditor, "Data changes require that the text in this summary be regenerated.", 24)
                End If
            End If
        Catch
            'If caught, it is because overallmaxgrowth is null. No action required.
        End Try

    End Sub

    Private Sub uiBulgingWorstPercent_Leave(sender As Object, e As System.EventArgs) Handles uiBulgingWorstPercent.Leave
        SaveData()
        uiBulgingWorstPercent.Refresh()
    End Sub

    Private Sub uiDiaGrowthMaxPercent_Leave(sender As Object, e As System.EventArgs) Handles uiDiaGrowthMaxPercent.Leave
        SaveData()
        uiDiaGrowthMaxPercent.Refresh()
    End Sub

#End Region

#Region "Inspection Summary"

    Private Sub uiInspectionSummaryButton_Click(sender As System.Object, e As System.EventArgs) Handles uiInspectionSummaryButton.Click
        uiInspectionSummaryEditor.ResetText()
        Dim doc As Document = uiInspectionSummaryEditor.Document
        Dim i As Integer = 0  'Paragraph numbering
        Dim RequiredInputSuggestions As String

        RequiredInputSuggestions = CheckForRequiredInput(False, "uiInspectionSummaryTabPage")
        If RequiredInputSuggestions <> Nothing Then
            Dim UserChoice = MessageBox.Show(RequiredInputSuggestions & vbCrLf & vbCrLf & "Would you like to continue anyway?", "Some Required Input Not Given", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
            If UserChoice = DialogResult.No Then Exit Sub
        End If

        doc.BeginUpdate()

        Dim list As AbstractNumberingList = uiInspectionSummaryEditor.Document.AbstractNumberingLists.Add()
        list.NumberingType = NumberingType.Bullet

        Dim level As ListLevel = list.Levels(0)
        level.ParagraphProperties.LeftIndent = 100
        level.ParagraphProperties.FirstLineIndentType = ParagraphFirstLineIndent.Hanging
        level.ParagraphProperties.FirstLineIndent = 50
        level.CharacterProperties.FontName = "Symbol"
        level.DisplayFormatString = New String(ChrW(&HB7), 1)
        level.ParagraphProperties.SpacingAfter = 30
        level.ParagraphProperties.SpacingBefore = 30

        ' Create a numbering list. It is based on a previously defined abstract list with ID = 0.
        doc.NumberingLists.Add(0)

        'First sentence
        Dim strSpare As String
        Dim strTubes As String
        Dim strNumberTubes As String

        Select Case mintInspectionType
            Case LOTIS_SPARE, MANTIS_SPARE
                If uiSpareUninstalledGroup.SelectedIndex = 0 Then
                    strSpare = " spare"
                ElseIf uiSpareUninstalledGroup.SelectedIndex = 1 Then
                    strSpare = " uninstalled"
                End If

                strNumberTubes = uiNumberOfTubes.Text
                If CType(strNumberTubes, Integer) = 1 Then
                    strTubes = "tube for"
                Else
                    strTubes = "tubes for"
                End If
            Case LOTIS_AGED, LOTIS_NEW, MANTIS_AGED, MANTIS_NEW
                strSpare = ""
                strNumberTubes = uiNumberTubesInspected.Text
                Try
                    If CType(strNumberTubes, Integer) = 1 Then
                        strTubes = "tube installed in"
                    Else
                        strTubes = "tubes installed in"
                    End If
                Catch
                    'User didn't enter a number of tubes
                    'He's already been warned. Don't do anything.
                End Try
        End Select

        If uiReformerName.Text = "Not Supplied" Or uiReformerID.Text = "Not Supplied" Then
            strTubes = Mid(strTubes, 1, Len(strTubes) - 4)
            doc.AppendText(String.Format("The inspection included {0} new{1} {2}. ", strNumberTubes, strSpare, strTubes))
        Else
            doc.AppendText(String.Format("The inspection included {0} new{1} {2} {3}, {4}. ", strNumberTubes, strSpare, strTubes, uiReformerName.Text, uiReformerID.Text))
        End If

        'LOTIS/MANTIS variables
        Dim strODID1 As String
        Dim strODID2 As String
        Dim strODID3 As String
        Dim strODID4 As String
        Dim strBullet1 As String = uiTubeDesignToleranceDisplay.Text
        Dim strBullet2 As String
        Dim strBullet3 As String
        Dim dID As Double
        Dim dOD As Double
        Dim dPlus As Double
        Dim dMinus As Double

        Try
            dID = CType(uiTubeInnerDiameter.Text, Double)
            dOD = CType(uiTubeOuterDiameter.Text, Double)
            dPlus = CType(uiPlusTubeTolerance.Text, Double)
            dMinus = CType(uiMinusTubeTolerance.Text, Double)
        Catch ex As Exception
            MsgBox("Please fill in tube details before generating summary.")
            doc.EndUpdate()
            uiInspectionSummaryEditor.ResetText()
            Exit Sub
        End Try

        Dim toolToleranceText As String
        Select Case mintInspectionType
            Case LOTIS_NEW, LOTIS_SPARE    'LOTIS
                strODID1 = "internal diameter tube bore design"
                strODID2 = "internal tube bore manufacturing"
                strODID3 = "inner diameters"
                strODID4 = "inner diameter tube bore"
                strBullet2 = uiTubeInnerDiameterDisplay.Text
                If uiLeadingUnits.SelectedIndex = 0 Then
                    strBullet3 = Math.Round(dID - dMinus, 3).ToString("0.000") & " in. (" & Math.Round((dID - dMinus) * 25.4, 2).ToString("0.00") & " mm)"
                Else
                    strBullet3 = Math.Round(dID - dMinus, 2).ToString("0.00") & " mm (" & Math.Round((dID - dMinus) / 25.4, 3).ToString("0.000") & " in.)"
                End If

                ' Add the tool tolerance text
                Dim strToolTolNumbers As String = If(uiLeadingUnits.SelectedIndex = 0, "0.005 in. (0.13 mm)", "0.13 mm (0.005 in.)")
                toolToleranceText = String.Format("The minimum and maximum tolerance thresholds indicated below also include the LOTIS tool accuracy +/- {0}. ", strToolTolNumbers)

            Case MANTIS_NEW, MANTIS_SPARE  'MANTIS
                strODID1 = "outer diameter tube"
                strODID2 = "outer diameter manufacture tube"
                strODID3 = "outer diameters"
                strODID4 = "outer diameter tube"

                If uiLeadingUnits.SelectedIndex = 0 Then
                    strBullet2 = Math.Round(dOD + dPlus, 3).ToString("0.000") & " in. (" & Math.Round((dOD + dPlus) * 25.4, 2).ToString("0.00") & " mm)"
                Else
                    strBullet2 = Math.Round(dOD + dPlus, 2).ToString("0.00") & " mm (" & Math.Round((dOD + dPlus) / 25.4, 3).ToString("0.000") & " in.)"
                End If
                strBullet3 = uiTubeOuterDiameterDisplay.Text

                ' Add the tool tolerance text
                Dim strToolTolNumbers As String = If(uiLeadingUnits.SelectedIndex = 0, "0.015 in. (0.381 mm)", "0.381 mm (0.015 in.)")
                toolToleranceText = String.Format("The minimum and maximum tolerance thresholds indicated below also include the MANTIS tool accuracy +/- {0}. ", strToolTolNumbers)
        End Select

        'Second sentence
        If uiTolerancesGroup.SelectedIndex = 0 Then
            doc.AppendText(String.Format("Quest Integrity utilized an industry standard {0} tolerance for this inspection in the absence of a specific tolerance from {1}. ", strODID1, uiCustomers.Text))
        Else
            doc.AppendText(String.Format("The specified {0} tolerance for this inspection was provided by {1}. ", strODID2, uiCustomers.Text))
        End If

        doc.AppendText(toolToleranceText)
        doc.AppendParagraph()

        'bullets
        Dim strToleranceBy As String
        If uiTolerancesGroup.SelectedIndex = 0 Then
            strToleranceBy = "industry standard"
        Else
            strToleranceBy = "customer specified"
        End If

        i = 1

        'bullet 1
        AppendListParagraph(doc, String.Format("{0} of the tubes inspected contained {1} within the {2} tube design tolerance of {3}.", uiNewTubeCount1.Text, strODID3, strToleranceBy, strBullet1), i, 0, 0)
        i = i + 1

        'bullet 2
        AppendListParagraph(doc, String.Format("{0} of the tubes inspected exceeded the maximum {1} {2} design tolerance of {3}.", uiNewTubeCount2.Text, strToleranceBy, strODID4, strBullet2), i, 0, 0)
        i = i + 1

        'bullet 3
        AppendListParagraph(doc, String.Format("{0} of the tubes inspected fell below the minimum {1} {2} design tolerance of {3}.", uiNewTubeCount3.Text, strToleranceBy, strODID4, strBullet3), i, 0, 0)
        i = i + 1

        'bullet 4
        AppendListParagraph(doc, String.Format("{0} of the tubes inspected fell outside of the maximum and minimum {1} {2} design tolerance of {3} and {4}.", uiNewTubeCount4.Text, strToleranceBy, strODID4, strBullet2, strBullet3), i, 0, 0)
        i = i + 1

        'final bullet
        Select Case uiNumberTubesWithFlaws.Text
            Case "0"
            Case "1"
                If mintInspectionType = LOTIS_SPARE Or mintInspectionType = MANTIS_SPARE Or uiCanCircularOption.Checked Then   'Spare or Can
                    AppendListParagraph(doc, String.Format("A localized manufacturing flaw (e.g., excessive grinding, gouges) was found in one of the tubes inspected. This flaw is located in Tube {0} {1}. This indication is noted in Appendix B.", uiWorstFlawTubeNumber.Text, uiTubeFlawLocation.Text), i, 0, 0)
                    i = i + 1
                Else                                                        'New
                    AppendListParagraph(doc, String.Format("A localized manufacturing flaw (e.g., excessive grinding, gouges) was found in one of the tubes inspected. This flaw is located in Tube {0} in Row {1} {2}. This indication is noted in Appendix C.", uiWorstFlawTubeNumber.Text, uiWorstFlawRowNumber.Text, uiTubeFlawLocation.Text), i, 0, 0)
                    i = i + 1
                End If
            Case Else
                If mintInspectionType = LOTIS_SPARE Or mintInspectionType = MANTIS_SPARE Or uiCanCircularOption.Checked Then   'Spare or Can
                    AppendListParagraph(doc, String.Format("Localized manufacturing flaws (e.g., excessive grinding, gouges) were found in {0} of the tubes inspected. The worst flaw is located in Tube {1} {2}. These indications are noted in Appendix B.", uiNumberTubesWithFlaws.Text, uiWorstFlawTubeNumber.Text, uiTubeFlawLocation.Text), i, 0, 0)
                    i = i + 1
                Else                                                         'New
                    AppendListParagraph(doc, String.Format("Localized manufacturing flaws (e.g., excessive grinding, gouges) were found in {0} of the tubes inspected. The worst flaw is located in Tube {1} in Row {2} {3}. These indications are noted in Appendix C.", uiNumberTubesWithFlaws.Text, uiWorstFlawTubeNumber.Text, uiWorstFlawRowNumber.Text, uiTubeFlawLocation.Text), i, 0, 0)
                    i = i + 1
                End If
        End Select

        doc.EndUpdate()

    End Sub

    Private Function SetToleranceLabels()
        'LOTIS/MANTIS variables
        Dim strBullet1 As String = uiTubeDesignToleranceDisplay.Text
        Dim strBullet2 As String
        Dim strBullet3 As String
        Dim dID As Double
        Dim dOD As Double
        Dim dPlus As Double
        Dim dMinus As Double

        Try
            dID = CType(uiTubeInnerDiameter.Text, Double)
            dOD = CType(uiTubeOuterDiameter.Text, Double)
            dPlus = CType(uiPlusTubeTolerance.Text, Double)
            dMinus = CType(uiMinusTubeTolerance.Text, Double)
        Catch ex As Exception
            uiTubeToleranceLabel1.Text = "You must set tube dimensions and tolerances on the ""Tube Details"" tab!"
            uiTubeToleranceLabel2.Text = "You must set tube dimensions and tolerances on the ""Tube Details"" tab!"
            uiTubeToleranceLabel3.Text = "You must set tube dimensions and tolerances on the ""Tube Details"" tab!"
            uiTubeToleranceLabel4.Text = "You must set tube dimensions and tolerances on the ""Tube Details"" tab!"
            Exit Function
        End Try
        Select Case mintInspectionType
            Case LOTIS_NEW, LOTIS_SPARE    'LOTIS
                strBullet2 = uiTubeInnerDiameterDisplay.Text
                If uiLeadingUnits.SelectedIndex = 0 Then
                    strBullet3 = Math.Round(dID - dMinus, 3).ToString("0.000") & " in. (" & Math.Round((dID - dMinus) * 25.4, 2).ToString("0.00") & " mm)"
                Else
                    strBullet3 = Math.Round(dID - dMinus, 2).ToString("0.00") & " mm (" & Math.Round((dID - dMinus) / 25.4, 3).ToString("0.000") & " in.)"
                End If

            Case MANTIS_NEW, MANTIS_SPARE  'MANTIS
                If uiLeadingUnits.SelectedIndex = 0 Then
                    strBullet2 = Math.Round(dOD + dPlus, 3).ToString("0.000") & " in. (" & Math.Round((dOD + dPlus) * 25.4, 2).ToString("0.00") & " mm)"
                Else
                    strBullet2 = Math.Round(dOD + dPlus, 2).ToString("0.00") & " mm (" & Math.Round((dOD + dPlus) / 25.4, 3).ToString("0.000") & " in.)"
                End If
                strBullet3 = uiTubeOuterDiameterDisplay.Text
        End Select

        uiTubeToleranceLabel1.Text = String.Format("Within the design tolerance of {0}.", strBullet1)
        uiTubeToleranceLabel2.Text = String.Format("Exceeded the design tolerance of {0}.", strBullet2)
        uiTubeToleranceLabel3.Text = String.Format("Below the design tolerance of {0}.", strBullet3)
        uiTubeToleranceLabel4.Text = String.Format("Outside the design tolerance range of {0} and {1}.", strBullet2, strBullet3)

    End Function

    Private Sub UpdateTubesWithFlaws() Handles uiNumberTubesWithFlaws.EditValueChanged
        Dim bEnabled As Boolean = True

        'Disable controls if 0 flaws
        If IsDBNull(uiNumberTubesWithFlaws.EditValue) OrElse uiNumberTubesWithFlaws.EditValue = "0" Then bEnabled = False
        uiWorstFlawTubeNumber.Enabled = bEnabled
        uiTubeFlawLocation.Enabled = bEnabled

        If uiCanCircularOption.Checked Then bEnabled = False
        uiWorstFlawRowNumber.Enabled = bEnabled

    End Sub

    Private Sub uiInspectionSummaryShowHideButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiInspectionShowHideButton.Click
        'Show/Hide paragraph marks
        If bVisible2 Then
            With uiInspectionSummaryEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Hidden
                .TabCharacter = RichEditFormattingMarkVisibility.Hidden
                .Space = RichEditFormattingMarkVisibility.Hidden
            End With
            bVisible2 = False
        Else
            With uiInspectionSummaryEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Visible
                .TabCharacter = RichEditFormattingMarkVisibility.Visible
                .Space = RichEditFormattingMarkVisibility.Visible
            End With
            bVisible2 = True
        End If
    End Sub

#End Region

#Region "Detailed Inspection"

    Private Sub uiDetailedSummaryButton_Click(sender As System.Object, e As System.EventArgs) Handles uiDetailedSummaryButton.Click

        Dim strRedCount As String = "No"
        Dim iRedCount As Integer
        Dim iYellowCount As Integer
        Dim iRedYellow As Integer
        Dim strTubes As String
        Dim RequiredInputSuggestions As String

        RequiredInputSuggestions = CheckForRequiredInput(False, "uiDetailedInspectionTabPage")
        If RequiredInputSuggestions <> Nothing Then
            Dim UserChoice = MessageBox.Show(RequiredInputSuggestions & vbCrLf & vbCrLf & "Would you like to continue anyway?", "Some Required Input Not Given", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
            If UserChoice = DialogResult.No Then Exit Sub
        End If

        'Set red and yellow count values
        Try
            iRedCount = CType(uiRedTubeCount.Text, Integer)
        Catch ex As Exception
            'MsgBox("You must enter a value for ""Red Tube Count"" on the ""Executive Summary"" tab; enter 0 if none.")
            'Exit Sub
            'The user has already been warned about the red tube count.
            'Do nothing.
        End Try

        'Update the data links in the tab controls
        uiDetailedSummaryTabControl.SelectedTabPageIndex = 1
        uiDetailedSummaryTabControl.SelectedTabPageIndex = 2
        uiDetailedSummaryTabControl.SelectedTabPageIndex = 3
        uiDetailedSummaryTabControl.SelectedTabPageIndex = 0

        Try
            iYellowCount = CType(uiYellowTubeCount.Text, Integer)
            iRedYellow = CType(uiRedTubeCount.Text, Integer) + CType(uiYellowTubeCount.Text, Integer)
        Catch ex As Exception
            'MsgBox("You must enter a value for ""Yellow Tube Count"" on the ""Executive Summary"" tab; enter 0 if none.")
            'Exit Sub
            'The user has already been warned about the yellow tube count.
            'Do nothing.
        End Try


        Dim strRedTubes As String = "tubes"
        Dim strYellowTubes As String = "tubes"
        Dim strRedYellowTubes As String = "tubes"
        If iRedCount = 1 Then strRedTubes = "tube"
        If iYellowCount = 1 Then strYellowTubes = "tube"
        If iRedYellow = 1 Then strRedYellowTubes = "tube"
        'uiDetailedSummaryEditor.Document.AppendText("This is Text")
        uiExecSummaryDetailsEditor.ResetText()
        uiDiaGrowthSummaryEditor.ResetText()
        uiFFSEditor.ResetText()
        uiTubeHarvestingEditor.ResetText()

        Dim doc As Document = uiExecSummaryDetailsEditor.Document
        Dim doc2 As Document = uiDiaGrowthSummaryEditor.Document
        Dim doc3 As Document = uiFFSEditor.Document
        Dim doc4 As Document = uiTubeHarvestingEditor.Document

        Dim i As Integer = 0  'Paragraph numbering
        doc.BeginUpdate()
        doc2.BeginUpdate()
        doc3.BeginUpdate()
        doc4.BeginUpdate()

        Dim list As AbstractNumberingList = uiExecSummaryDetailsEditor.Document.AbstractNumberingLists.Add()
        Dim list2 As AbstractNumberingList = uiDiaGrowthSummaryEditor.Document.AbstractNumberingLists.Add()
        Dim list4 As AbstractNumberingList = uiTubeHarvestingEditor.Document.AbstractNumberingLists.Add()
        list.NumberingType = NumberingType.Bullet
        list2.NumberingType = NumberingType.Bullet
        list4.NumberingType = NumberingType.Simple

        Dim level As ListLevel = list.Levels(0)
        level.ParagraphProperties.LeftIndent = 100
        level.ParagraphProperties.FirstLineIndentType = ParagraphFirstLineIndent.Hanging
        level.ParagraphProperties.FirstLineIndent = 50
        level.CharacterProperties.FontName = "Symbol"
        level.DisplayFormatString = New String(ChrW(&HB7), 1)
        level.ParagraphProperties.SpacingAfter = 30
        level.ParagraphProperties.SpacingBefore = 30
        Dim level2 As ListLevel = list2.Levels(0)
        level2.ParagraphProperties.LeftIndent = 100
        level2.ParagraphProperties.FirstLineIndentType = ParagraphFirstLineIndent.Hanging
        level2.ParagraphProperties.FirstLineIndent = 50
        level2.CharacterProperties.FontName = "Symbol"
        level2.DisplayFormatString = New String(ChrW(&HB7), 1)
        level2.ParagraphProperties.SpacingAfter = 30
        level2.ParagraphProperties.SpacingBefore = 30
        Dim level4 As ListLevel = list4.Levels(0)
        level4.ParagraphProperties.LeftIndent = 100
        level4.ParagraphProperties.FirstLineIndentType = ParagraphFirstLineIndent.Hanging
        level4.ParagraphProperties.FirstLineIndent = 50
        level4.ParagraphProperties.SpacingAfter = 30
        level4.ParagraphProperties.SpacingBefore = 30
        ' Create a numbering list. It is based on a previously defined abstract list with ID = 0.
        doc.NumberingLists.Add(0)
        doc2.NumberingLists.Add(0)
        doc4.NumberingLists.Add(0)

        '1st paragraph
        Dim strPara1 As String
        Dim strTotalTubes As String = uiOverallTubesInspected.Text
        Dim strOverallTotal As String = uiNumberTubesTotal.Text
        Dim strUnit As String = uiReformerName.Text
        Dim strUnitID As String = uiReformerID.Text

        doc.AppendText(String.Format("The inspection of {0}, {1}, included {2} aged tubes out of the {3} aged tubes. Below is a summary of the inspection findings.", strUnit, strUnitID, strTotalTubes, strOverallTotal))
        doc.AppendParagraph()

        ' Bullet Items from Executive Summery
        i = 1

        '2nd bullet
        Dim strSwelling As String = "None"
        Dim strHasHasNot As String = "has not"

        If Not String.IsNullOrEmpty(uiSwellingOver3PercentCount.Text) Then
            If CType(uiSwellingOver3PercentCount.Text, Integer) > 0 Then
                strSwelling = uiSwellingOver3PercentCount.Text
                strHasHasNot = "has"
            End If
        End If
        AppendListParagraph(doc, String.Format("{0} of the {1} tubes inspected contained internal diametrical growth greater than or equal to 3.00%, indicating significant creep damage {2} occurred in these tubes.", strSwelling, strTotalTubes, strHasHasNot), i, 0, 0)
        i = i + 1

        '3rd bullet
        Dim strDiaGrowthMaxPercent As String = uiDiaGrowthMaxPercent.Text
        Dim strDiaGrowthRowID As String = uiDiaGrowthRowID.Text
        Dim strDiaGrowthTubeID As String = uiDiaGrowthTubeID.Text
        Dim strMaxGrowthLocation As String = uiMaxGrowthLocation.Text
        If uiCanCircularOption.Checked Then
            AppendListParagraph(doc, String.Format("The maximum internal diametrical growth detected during the inspection was {0}% in Tube {1}. The growth was located {2}. Damage appeared equally distributed around the circumference in the form of swelling rather than bulging.", strDiaGrowthMaxPercent, strDiaGrowthTubeID, strMaxGrowthLocation), i, 0, 0)
        Else
            AppendListParagraph(doc, String.Format("The maximum internal diametrical growth detected during the inspection was {0}% in Tube {1} located in Row {2}. The growth was located {3}. Damage appeared equally distributed around the circumference in the form of swelling rather than bulging.", strDiaGrowthMaxPercent, strDiaGrowthTubeID, strDiaGrowthRowID, strMaxGrowthLocation), i, 0, 0)
        End If

        i = i + 1

        '4th bullet
        Dim intOverallMaxGrowth As Integer = uiOverallMaxGrowth.SelectedIndex 'A majority, all or none
        Dim strOverallMaxGrowth As String
        Dim strOverallMaxGrowthLocation As String = uiOverallMaxGrowthLocation.Text
        If intOverallMaxGrowth <> 2 Then
            If intOverallMaxGrowth = 0 Then strOverallMaxGrowth = "A majority"
            If intOverallMaxGrowth = 1 Then strOverallMaxGrowth = "All"
            AppendListParagraph(doc, String.Format("{0} of the internal diametrical growth found during this inspection was located {1}.", strOverallMaxGrowth, strOverallMaxGrowthLocation), i, 0, 0)
            i = i + 1
        End If

        '5th bullet
        If mintInspectionType = MANTIS_AGED Then 'MANTIS Aged 
            Dim strCrackingCount As String
            Dim strCrackingCountText As String
            Dim strCrackingTubes As String = "tubes"
            If Not String.IsNullOrEmpty(uiCracking.Text) Then
                If CType(uiCracking.Text, Integer) > 0 Then
                    strCrackingCount = uiCracking.Text
                    strCrackingCountText = ConvertNum(strCrackingCount)
                    strCrackingCountText = Mid(strCrackingCountText, 1, 1).ToUpper & Mid(strCrackingCountText, 2)
                    If CType(strCrackingCount, Integer) = 1 Then strCrackingTubes = "tube"
                    AppendListParagraph(doc, String.Format("{0} tubes were inspected with eddy current during the inspection to detect the presence of cracking. {1} ({2}) {3} contained cracking.", strTotalTubes, strCrackingCountText, strCrackingCount, strCrackingTubes), i, 0, 0)
                Else
                    AppendListParagraph(doc, String.Format("{0} tubes were inspected with eddy current during the inspection to detect the presence of cracking. None of the tubes contained cracking.", strTotalTubes, strCrackingCount, strCrackingTubes), i, 0, 0)
                End If
                i = i + 1
            End If
        End If

        '6th bullet
        Dim strBulging As String
        Dim strBulgingPercent As String
        Dim strWorstBulgingTubeID As String
        Dim strWorstBulgingRowID As String
        Dim strBulgingTubes As String = "tubes"
        If Not String.IsNullOrEmpty(uiBulgingTubeCount.Text) Then
            If CType(uiBulgingTubeCount.Text, Integer) > 0 Then
                strBulging = uiBulgingTubeCount.Text
                strBulgingPercent = uiBulgingWorstPercent.Text
                strWorstBulgingRowID = uiWorstTubeIDRow.Text
                strWorstBulgingTubeID = uiWorstTubeIDTube.Text
                If CType(strBulging, Integer) = 1 Then strBulgingTubes = "tube"
                If uiCanCircularOption.Checked Then
                    AppendListParagraph(doc, String.Format("{0} {1} contained bulges with the worst bulge ({2}%) located in Tube {3}. Damage appeared on one side of the circumference in the form of bulging rather than swelling.", strBulging, strBulgingTubes, strBulgingPercent, strWorstBulgingTubeID), i, 0, 0)
                Else
                    AppendListParagraph(doc, String.Format("{0} {1} contained bulges with the worst bulge ({2}%) located in Tube {3} in Row {3}. Damage appeared on one side of the circumference in the form of bulging rather than swelling.", strBulging, strBulgingTubes, strBulgingPercent, strWorstBulgingTubeID, strWorstBulgingRowID), i, 0, 0)
                End If
                i = i + 1
            End If
        End If

        'Final bullet
        Dim strFlaws As String
        Dim strSingularPlural As String = "Singular"
        Select Case uiTubeFlawsWelds.SelectedIndex
            Case 0
                strFlaws = "An abnormal weld signature was"
            Case 1
                strFlaws = "Abnormal weld signatures were"
        End Select

        Select Case uiTubeFlawsGouges.SelectedIndex
            Case 0
                If Not String.IsNullOrEmpty(strFlaws) Then
                    strSingularPlural = "Plural"
                    strFlaws = Mid(strFlaws, 1, InStrRev(strFlaws, " ") - 1)
                    strFlaws += " and a tube gouge were"
                Else
                    strFlaws = "A tube gouge was"
                End If
            Case 1
                If Not String.IsNullOrEmpty(strFlaws) Then
                    strSingularPlural = "Plural"
                    strFlaws = Mid(strFlaws, 1, InStrRev(strFlaws, " ") - 1)
                    strFlaws += " and tube gouges were"
                Else
                    strFlaws = "Tube gouges were"
                End If
        End Select

        If Not String.IsNullOrEmpty(strFlaws) Then
            Select Case strSingularPlural
                Case "Singular"
                    strFlaws += " detected during this inspection which could be an indication of a manufacturing flaw and may affect the expected remaining life of a tube. This indication is noted in Appendix C."
                Case "Plural"
                    strFlaws += " detected during this inspection which could be an indication of a manufacturing flaw and may affect the expected remaining life of a tube. These indications are noted in Appendix C."
            End Select
            AppendListParagraph(doc, strFlaws, i, 0, 0)
            i = i + 1
        End If

        'Diametrical Growth Summary
        Dim strBullet1 As String
        Dim strBullet2 As String
        Dim strBullet3 As String
        Dim strBullet4 As String
        Dim strBUllet5 As String
        Dim dvalue1 As Double
        Dim dvalue2 As Double
        Dim dvalue3 As Double
        Dim dvalue4 As Double
        Dim dvalue5 As Double
        Dim dtotal As Double
        Dim dsum As Decimal

        Try
            dvalue1 = CType(uiAgedTubeCount1.Text, Double)
            dvalue2 = CType(uiAgedTubeCount2.Text, Double)
            dvalue3 = CType(uiAgedTubeCount3.Text, Double)
            dvalue4 = CType(uiAgedTubeCount4.Text, Double)
            dvalue5 = CType(uiAgedTubeCount5.Text, Double)
            dtotal = CType(uiNumberTubesInspected.Text, Double)

        Catch ex As Exception
            MsgBox("Please fill in all values before generating summary.")
            doc.EndUpdate()
            doc2.EndUpdate()
            doc3.EndUpdate()
            doc4.EndUpdate()
            Exit Sub
        End Try

        Dim strPercent
        Select Case uiTubeMaterials.SelectedItem
            Case "MicroAlloy", "HP Modified"
                strPercent = "5.50"
            Case Else
                strPercent = "3.00"
        End Select

        'Opening paragraphs
        Dim NBSP As Char = ChrW(&HA0)
        doc2.AppendText(String.Format("A diametrical growth distribution graph for the tubes inspected is shown in Figure 1. This distribution graph provides a summary of the creep damage level for these reformer tubes from no damage (<{0}1.00% growth) to highly damaged (>{0}{1}% growth). The extent of creep damage is further discussed in the next section (Level 1 Fitness-for-Service Results). ", NBSP, strPercent))
        doc2.AppendParagraph()
        doc2.AppendParagraph()
        doc2.AppendText(String.Format("A total of {0} aged tubes were inspected:", strTotalTubes))
        doc2.AppendParagraph()
        'doc2.AppendParagraph()

        i = 3

        Select Case uiTubeMaterials.SelectedItem
            Case "MicroAlloy", "HP Modified"
                dsum = (dvalue1 / dtotal) * 100
                AppendListParagraph(doc2, String.Format("{0} of the {1} tubes inspected ({2}%) contained internal diameters greater than 5.50% diametrical growth.", dvalue1, dtotal, Math.Round(dsum, 2)), i, 0, 0)
                i = i + 1
                dsum = (dvalue2 / dtotal) * 100
                AppendListParagraph(doc2, String.Format("{0} of the {1} tubes inspected ({2}%) contained internal diameters between 3.00 and 5.50% diametrical growth.", dvalue2, dtotal, Math.Round(dsum, 2)), i, 0, 0)
                i = i + 1
                dsum = (dvalue3 / dtotal) * 100
                AppendListParagraph(doc2, String.Format("{0} of the {1} tubes inspected ({2}%) contained internal diameters between 2.00 and 2.99% diametrical growth.", dvalue3, dtotal, Math.Round(dsum, 2)), i, 0, 0)
                i = i + 1
                dsum = (dvalue4 / dtotal) * 100
                AppendListParagraph(doc2, String.Format("{0} of the {1} tubes inspected ({2}%) contained internal diameters between 1.00 and 1.99% diametrical growth.", dvalue4, dtotal, Math.Round(dsum, 2)), i, 0, 0)
                i = i + 1
                dsum = (dvalue5 / dtotal) * 100
                AppendListParagraph(doc2, String.Format("{0} of the {1} tubes inspected ({2}%) contained internal diameters less than 1.00% diametrical growth.", dvalue5, dtotal, Math.Round(dsum, 2)), i, 0, 0)
                i = i + 1
            Case "HK-40"
                dsum = (dvalue1 / dtotal) * 100
                AppendListParagraph(doc2, String.Format("{0} of the {1} tubes inspected ({2}%) contained internal diameters greater than 3.00% diametrical growth.", dvalue1, dtotal, Math.Round(dsum, 2)), i, 0, 0)
                i = i + 1
                dsum = (dvalue2 / dtotal) * 100
                AppendListParagraph(doc2, String.Format("{0} of the {1} tubes inspected ({2}%) contained internal diameters between 2.00 and 3.00% diametrical growth.", dvalue2, dtotal, Math.Round(dsum, 2)), i, 0, 0)
                i = i + 1
                dsum = (dvalue3 / dtotal) * 100
                AppendListParagraph(doc2, String.Format("{0} of the {1} tubes inspected ({2}%) contained internal diameters between 1.50 and 1.99% diametrical growth.", dvalue3, dtotal, Math.Round(dsum, 2)), i, 0, 0)
                i = i + 1
                dsum = (dvalue4 / dtotal) * 100
                AppendListParagraph(doc2, String.Format("{0} of the {1} tubes inspected ({2}%) contained internal diameters between 1.00 and 1.49% diametrical growth.", dvalue4, dtotal, Math.Round(dsum, 2)), i, 0, 0)
                i = i + 1
                dsum = (dvalue5 / dtotal) * 100
                AppendListParagraph(doc2, String.Format("{0} of the {1} tubes inspected ({2}%) contained internal diameters less than 1.00% diametrical growth.", dvalue5, dtotal, Math.Round(dsum, 2)), i, 0, 0)
                i = i + 1
            Case Else
                dsum = (dvalue1 / dtotal) * 100
                AppendListParagraph(doc2, String.Format("{0} of the {1} tubes inspected ({2}%) contained internal diameters greater than 5.50% [EDIT AS NECESSARY] diametrical growth.", dvalue1, dtotal, Math.Round(dsum, 2)), i, 0, 0)
                i = i + 1
                dsum = (dvalue2 / dtotal) * 100
                AppendListParagraph(doc2, String.Format("{0} of the {1} tubes inspected ({2}%) contained internal diameters between 3.00 and 5.50% [EDIT AS NECESSARY] diametrical growth.", dvalue2, dtotal, Math.Round(dsum, 2)), i, 0, 0)
                i = i + 1
                dsum = (dvalue3 / dtotal) * 100
                AppendListParagraph(doc2, String.Format("{0} of the {1} tubes inspected ({2}%) contained internal diameters between 2.00 and 2.99% [EDIT AS NECESSARY] diametrical growth.", dvalue3, dtotal, Math.Round(dsum, 2)), i, 0, 0)
                i = i + 1
                dsum = (dvalue4 / dtotal) * 100
                AppendListParagraph(doc2, String.Format("{0} of the {1} tubes inspected ({2}%) contained internal diameters between 1.00 and 1.99% [EDIT AS NECESSARY] diametrical growth.", dvalue4, dtotal, Math.Round(dsum, 2)), i, 0, 0)
                i = i + 1
                dsum = (dvalue5 / dtotal) * 100
                AppendListParagraph(doc2, String.Format("{0} of the {1} tubes inspected ({2}%) contained internal diameters less than 1.00% [EDIT AS NECESSARY] diametrical growth.", dvalue5, dtotal, Math.Round(dsum, 2)), i, 0, 0)
                i = i + 1
        End Select

        i = 1

        'Fitness-for-Service results (red/yellow/green)
        Dim strFFS As String
        Dim bRed As Boolean = uiDetailedSummaryRed.Checked
        Dim bYellow As Boolean = uiDetailedSummaryYellow.Checked
        Dim bGreen As Boolean = uiDetailedSummaryGreen.Checked
        Dim bCreep As Boolean = uiGreenCreep.Checked

        If (bRed Or bYellow) Or (bGreen And bCreep) Then
            strFFS = String.Format("The tubes in the {0}, {1}, are showing creep damage. ", strUnit, strUnitID)
        ElseIf bGreen Then
            strFFS = String.Format("No tubes in the {0}, {1}, show creep damage at this time. ", strUnit, strUnitID)
        End If

        If bRed Or bYellow Then
            strFFS += String.Format("The damage is significant in some tubes. At the observed creep damage rates, {0} of the {1} tubes inspected in the {2}, {3}, fail ", iRedYellow.ToString, uiNumberTubesInspected.Text, strUnit, strUnitID)
        ElseIf bGreen Then
            strFFS += "All tubes pass "
        End If

        If uiCanCircularOption.Checked Then
            strFFS += String.Format("the API 579-1 Level 1 assessment for the next {0}-year operating period. The tube which contained the most significant creep damage (Tube {1}) ", uiOperatingPeriod.Text, uiDiaGrowthTubeID.Text)
        Else
            strFFS += String.Format("the API 579-1 Level 1 assessment for the next {0}-year operating period. The tube which contained the most significant creep damage (Row {1} Tube {2}) ", uiOperatingPeriod.Text, uiDiaGrowthRowID.Text, uiDiaGrowthTubeID.Text)
        End If

        If bGreen And Not (bRed Or bYellow) Then strFFS += ", however slight, "
        strFFS += "is shown in Figure 2. If the reformer is operated under the same conditions that resulted in the measured long-term creep damage, it is "

        If bRed And bYellow Then
            strFFS += String.Format("likely that tube failures would occur for the {0} {1} in the Red Category and unlikely tube failures would occur for the {2} {3} in the Yellow Category prior to the next scheduled shutdown.", iRedCount.ToString, strRedTubes, iYellowCount.ToString, strYellowTubes)
        ElseIf bRed Then
            strFFS += String.Format("likely that tube failures would occur for the {0} {1} in the Red Category prior to the next scheduled shutdown.", iRedCount.ToString, strRedTubes)
        ElseIf bYellow Or bGreen Then
            strFFS += "unlikely that tube failures would occur prior to the next scheduled shutdown."
        End If

        doc3.AppendText(strFFS)

        'Tube Harvesting

        Dim iTotalCount As Integer = CType(strTotalTubes, Integer)
        Dim iGreenCount As Integer = iTotalCount - iRedYellow
        Dim strGreenTubes As String = "tubes"
        If iGreenCount = 1 Then strGreenTubes = "tube"
        Dim strRed As String = ConvertNum(iRedCount)
        Dim strYellow As String = ConvertNum(iYellowCount)
        Dim strGreen As String = ConvertNum(iGreenCount)
        strRed = Mid(strRed, 1, 1).ToUpper & Mid(strRed, 2)
        strYellow = Mid(strYellow, 1, 1).ToUpper & Mid(strYellow, 2)
        strGreen = Mid(strGreen, 1, 1).ToUpper & Mid(strGreen, 2)

        i = 0
        AppendListParagraph(doc4, String.Format("Pass Level 1 Assessment (Green): Tubes that pass the level 1 assessment have a current total creep damage less than 0.25 (life fraction consumed) and current creep damage rate that would not exceed total creep damage of 0.25 during the next operating period. {0} ({1}) tubes fell into this category.", strGreen, iGreenCount, strGreenTubes), i, 0, 0)
        i = i + 1
        AppendListParagraph(doc4, String.Format("Fail Level 1 Assessment, Consult Engineering (Yellow): Tubes that fall into this category exceed a current total creep damage of 0.25, but have a measured creep damage rate that would not likely exceed total creep damage of 0.80 during the next operating period. {0} ({1}) {2} fell into this category.", strYellow, iYellowCount, strYellowTubes), i, 0, 0)
        i = i + 1
        AppendListParagraph(doc4, String.Format("Fail Level 1 Assessment, Retire Now (Red): Tubes that fall into this category either exceed a total creep damage of 0.80 or have a measured creep damage rate that would likely exceed total creep damage of 0.80 during the next operating period. {0} ({1}) {2} fell into this category.", strRed, iRedCount, strRedTubes), i, 0, 0)

        doc.EndUpdate()
        doc2.EndUpdate()
        doc3.EndUpdate()
        doc4.EndUpdate()

    End Sub

    Private Sub uiDetailedSummaryShowHideButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiDetailedSummaryShowHideButton.Click
        'Show/Hide paragraph marks
        If bVisible3 Then
            With uiFFSEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Hidden
                .TabCharacter = RichEditFormattingMarkVisibility.Hidden
                .Space = RichEditFormattingMarkVisibility.Hidden
            End With
            With uiDiaGrowthSummaryEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Hidden
                .TabCharacter = RichEditFormattingMarkVisibility.Hidden
                .Space = RichEditFormattingMarkVisibility.Hidden
            End With
            With uiExecSummaryDetailsEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Hidden
                .TabCharacter = RichEditFormattingMarkVisibility.Hidden
                .Space = RichEditFormattingMarkVisibility.Hidden
            End With
            bVisible3 = False
        Else
            With uiFFSEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Visible
                .TabCharacter = RichEditFormattingMarkVisibility.Visible
                .Space = RichEditFormattingMarkVisibility.Visible
            End With
            With uiDiaGrowthSummaryEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Visible
                .TabCharacter = RichEditFormattingMarkVisibility.Visible
                .Space = RichEditFormattingMarkVisibility.Visible
            End With
            With uiExecSummaryDetailsEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Visible
                .TabCharacter = RichEditFormattingMarkVisibility.Visible
                .Space = RichEditFormattingMarkVisibility.Visible
            End With
            bVisible3 = True
        End If
    End Sub

    Private Function SumTubeCounts() Handles uiAgedTubeCount1.EditValueChanged, uiAgedTubeCount2.EditValueChanged, uiAgedTubeCount3.EditValueChanged, uiAgedTubeCount4.EditValueChanged, uiAgedTubeCount5.EditValueChanged
        Dim strTotal As String = "##"
        Dim iTotal As Integer
        'show a running total of tube count
        If Not String.IsNullOrEmpty(uiNumberTubesInspected.Text) Then strTotal = uiNumberTubesInspected.Text

        Try
            iTotal = CType(uiAgedTubeCount1.Text, Integer) + CType(uiAgedTubeCount2.Text, Integer) + CType(uiAgedTubeCount3.Text, Integer) + CType(uiAgedTubeCount4.Text, Integer) + CType(uiAgedTubeCount5.Text, Integer)
        Catch
            iTotal = 0
        End Try
        uiDiaGrowthTotalLabel.Text = String.Format("Includes {0} out of {1} tubes inspected", iTotal.ToString, strTotal)
    End Function

#End Region

#Region "Linked Files"

    Private Function SaveAppendixItems(ByVal fPromptUser As Boolean, ByVal sMsgText As String) As Boolean
        ' checks to see if AppendixItems need saving (i.e. have changes been made)
        ' if they need saving, either saves automatically, or if fPromptUser is true, prompts the user
        ' returns TRUE, unless user specifically cancels changes when prompted
        Const UPDATE_MSG_TEXT = "The Appendix Linked File Items must be saved and re-loaded during this process. Click OK to Proceed (if you Cancel, one or more required Appendix Linked files may not be included)."

        Dim fSaveIsNeeded As Boolean = False
        Dim drvTemp As DataRowView
        Dim oAppendixItem As QTT_InspectionsDataSets.vwAppendixItem_DetailsRow
        Dim i As Integer

        ' determine if any un-saved changes exist (this condition is not expected if "Auto-Save" is working
        For i = 0 To (bsInspections_AppendixItems.Count - 1)
            drvTemp = bsInspections_AppendixItems.Item(i)
            oAppendixItem = drvTemp.Row
            If (oAppendixItem.RowState = DataRowState.Modified) Or (oAppendixItem.RowState = DataRowState.Added) Then
                fSaveIsNeeded = True
                Exit For
            End If
        Next i

        ' warn user if pending changes need saving - save if OK, or exit if not.
        If fSaveIsNeeded Then
            If fPromptUser Then
                If MsgBoxResult.Cancel = MsgBox(sMsgText & UPDATE_MSG_TEXT, MsgBoxStyle.OkCancel + MsgBoxStyle.Information) Then
                    Return False
                    Exit Function
                End If
            End If
            ' save existing items
            '  Appendix Items
            Try
                VwAppendixItem_DetailsTableAdapter.Update(QTT_InspectionsDataSets.vwAppendixItem_Details)
            Catch ex As Exception
                MsgBox("Exception: " & ex.ToString())
            End Try
            For i = 0 To (bsInspections_AppendixItems.Count - 1)
                drvTemp = bsInspections_AppendixItems.Item(i)
                oAppendixItem = drvTemp.Row
                If (oAppendixItem.RowState = DataRowState.Modified) Or (oAppendixItem.RowState = DataRowState.Added) Then
                    oAppendixItem.EndEdit()
                End If
            Next i
            bsInspections_AppendixItems.EndEdit()

        End If

        Return True

    End Function

    Private Function UpdateAppendixSectionItems(ByVal pApndxSectionID As Integer, Optional ByVal pSelectApndxSection As Boolean = False) As Boolean

        ' calls SP to add any missing pipe / tube related appendix items - can be used when 'has appendix' changes, and or appendixes are added
        ' this code runs against a singly Appendix Section
        Const MSG_TEXT = "The Appendix Linked Files for this Inspection need to be updated (this is usually because there are one or more Linked Files that appear to be missing based on your current settings)." & vbNewLine

        ' warn user if pending changes need saving - save if OK, or exit if not.
        If Not SaveAppendixItems(True, MSG_TEXT) Then
            Return False
            Exit Function
        End If

        ' run Stored Proc
        If modProgram.ResetAppendixSectionItems(pApndxSectionID, False) Then
            ' reload appendix items
            Me.VwAppendixItem_DetailsTableAdapter.Fill(Me.QTT_InspectionsDataSets.vwAppendixItem_Details)
        End If

        Return True

    End Function

    Private Function UpdateAppendixItems() As Boolean

        ' calls SP to add any missing pipe / tube related appendix items - can be used when 'has appendix' changes, and or appendixes are added
        ' note - this code is "older" and is generally superseded by the UpdateAppendixSectionItems procedure - this procedure (UpdateAppendixItems) may be removed in the future
        Const MSG_TEXT = "The Appendix Linked Files for this Inspection need to be updated (this is usually because there are one or more Linked Files that appear to be missing based on your current settings)." & vbNewLine

        ' warn user if pending changes need saving - save if OK, or exit if not.
        If Not SaveAppendixItems(True, MSG_TEXT) Then
            Return False
            Exit Function
        End If

        ' run Stored Proc
        Dim oInsp As QTT_InspectionsDataSets.tblInspectionsRow = bsInspections.Current.Row
        Dim intInspID As Integer = oInsp.ID
        If taTblInspections.AddMissingAppendixItems(intInspID).Value <> 0 Then
            MsgBox("Un-expected database result calling AddMissingItems stored procedure.")
        End If

        ' reload appendix items
        Me.VwAppendixItem_DetailsTableAdapter.Fill(Me.QTT_InspectionsDataSets.vwAppendixItem_Details)
        Return True

    End Function

    Private Sub BrowseForReportFile(ByVal oCurRow As DataRow)
        ' called to handle 'browse for file' feature for main report items

        Dim oCurReportItem As QTT_InspectionsDataSets.vwReportItems_DetailsRow
        Dim UI_Description As String = ""
        Dim FileSuffix As String = ""
        Dim Subfolder As String = ""
        Dim PassesSubfolder As Boolean = False
        Dim PassNumber As Integer = 1
        Dim PassesInAlpha As Boolean = False
        Dim PassesCustom As Boolean = fTubesCustom
        Dim CustomPasses() As String = arrCustomTubes
        Dim PassSubfolderName As String = ""

        UI_Description = oCurRow.Item(RPT_IMAGE_DESC_COL_INDEX).ToString
        oCurReportItem = TryCast(oCurRow, QTT_InspectionsDataSets.vwReportItems_DetailsRow)
        If oCurReportItem IsNot Nothing Then
            ' read properties
            With oCurReportItem
                ' default subfolder?
                If Not .IsSubFolderNull Then
                    Subfolder = .SubFolder
                End If
                If Not .IsPassSubFolderNameNull Then
                    PassSubfolderName = .PassSubFolderName
                End If
                ' file filter string
                If Not .IsFilenameSuffixNull Then
                    FileSuffix = Microsoft.VisualBasic.Right(.FilenameSuffix, Len(.FilenameSuffix) + 1 - InStr(.FilenameSuffix, "."))
                End If
                If Not .IsPassSubFolderNameNull Then
                    PassSubfolderName = .PassSubFolderName
                End If
            End With
        End If

        'KM 7/8/2011
        'FTIS-specific fields maintained for expediency. Values not used for LOTIS/MANTIS.
        Dim strFN As String = BrowseLinkedFile(UI_Description, FileSuffix, Subfolder, PassesSubfolder, PassNumber, PassesInAlpha, PassesCustom, CustomPasses, PassSubfolderName)
        If Len(strFN) > 0 Then
            oCurRow.Item(RPT_FILENAME_COL_INDEX) = strFN
            uiReportItemsGridView.UpdateCurrentRow()
        End If

    End Sub

    Private Sub BrowseForAppendixFile(ByVal oCurRow As DataRow)
        ' called to handle 'browse for file' feature for main report items
        Dim oCurAppendixItem As QTT_InspectionsDataSets.vwAppendixItem_DetailsRow
        Dim UI_Description As String = ""
        Dim FileSuffix As String = ""
        Dim Subfolder As String = ""
        Dim PassesSubfolder As Boolean = False
        Dim PassNumber As Integer = 0
        Dim PassesInAlpha As Boolean = False
        Dim PassesCustom As Boolean = fTubesCustom
        Dim CustomPasses() As String = arrCustomTubes
        Dim PassSubfolderName As String = ""

        ' basic row - file desc
        UI_Description = oCurRow.Item(APX_IMAGE_DESC_COL_INDEX).ToString
        oCurAppendixItem = TryCast(oCurRow, QTT_InspectionsDataSets.vwAppendixItem_DetailsRow)
        If oCurAppendixItem IsNot Nothing Then
            ' read properties
            With oCurAppendixItem
                ' default subfolder?
                If Not .IsSubFolderNull Then
                    Subfolder = .SubFolder
                End If
                ' pass number if present
                If Not .IsPassNumberNull Then
                    PassNumber = .PassNumber
                End If
                ' file filter string
                If Not .IsFilenameSuffixNull Then
                    FileSuffix = Microsoft.VisualBasic.Right(.FilenameSuffix, Len(.FilenameSuffix) + 1 - InStr(.FilenameSuffix, "."))
                End If
                If Not .IsPassSubFolderNameNull Then
                    PassSubfolderName = .PassSubFolderName
                End If
            End With
        End If

        'KM 7/8/2011
        Dim strFN As String = BrowseLinkedFile(UI_Description, FileSuffix, Subfolder, PassesSubfolder, PassNumber, PassesInAlpha, PassesCustom, CustomPasses, PassSubfolderName)
        If Len(strFN) > 0 Then
            oCurRow.Item(APX_FILENAME_COL_INDEX) = strFN
            uiReportItemsGridView.UpdateCurrentRow()
        End If

    End Sub

    Private Function BrowseLinkedFile(ByVal UI_Description As String, ByVal FileSuffix As String, ByVal Subfolder As String, ByVal PassesSubfolder As Boolean, ByVal PassNumber As Integer, ByVal PassesInAlpha As Boolean, ByVal PassesCustom As Boolean, ByVal CustomPasses() As String, ByVal PassSubfolderName As String)
        ' generic function called to handle 'browse for file' feature for main report and appendix items
        ' params:  UI_Description - user interface description of file being browsed for
        '          FileSuffix - expected suffix for filename
        '          Subfolder - string that ID's subfolder 
        '          PassesSubfolder - T/F : look in 'Pass' subfolder
        '          PassNumber - if PassesSubfolder, identifies current pass to use
        '          PassesInAlpha - if true, assumes that pass folder / name is alpha
        '          PassSubfolderName - If graphics are in a subfolder under a passfoler

        Const FILTER_ALLFILES = "All files (*.*)|*.*"
        'KM 7/7/2011
        'KM 8/2/2011 Changed to MyDocuments
        Dim strDefFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & My.Settings.RootDataFolder
        Dim strInspRoot As String
        Dim strDefFilter As String = FILTER_ALLFILES
        Dim strTmp As String = ""

        ' determine path to inspection root folder
        If Not (Me.QTT_InspectionsDataSets.tblInspections.Item(0).IsRootFolderNameNull) Then
            strInspRoot = Me.QTT_InspectionsDataSets.tblInspections.Item(0).RootFolderName
            strDefFolder = My.Computer.FileSystem.CombinePath(strDefFolder, strInspRoot)
        End If

        ' read properties
        ' default subfolder?
        If Len(Subfolder) > 0 Then
            strTmp = Subfolder
            If Microsoft.VisualBasic.Left(strTmp, 1) = "\" Then
                strTmp = Mid(strTmp, 2)
            End If
            strDefFolder = My.Computer.FileSystem.CombinePath(strDefFolder, strTmp)
        End If
        ' passes based subfolder?
        'If PassesSubfolder Then
        '    'KM 7/8/2011
        '    strTmp = GetPassFolderName(PassNumber, PassesInAlpha, PassesCustom, CustomPasses)
        '    strDefFolder = My.Computer.FileSystem.CombinePath(strDefFolder, strTmp)
        '    strDefFolder = My.Computer.FileSystem.CombinePath(strDefFolder, PassSubfolderName)
        'End If
        ' file filter string
        If Len(FileSuffix) > 0 Then
            strTmp = FileSuffix
            'Updated to include Office 2007/2010 file extensions
            If Microsoft.VisualBasic.Right(strTmp, 4) = ".jpg" Then
                Dim strAltSuffix As String = Microsoft.VisualBasic.Left(strTmp, Len(strTmp) - 4) & ".jpeg"
                strDefFilter = "QIG Expected Files (*" & strTmp & "; *" & strAltSuffix & ")|*" & strTmp & ";*" & strAltSuffix & "|" & FILTER_ALLFILES
            ElseIf Microsoft.VisualBasic.Right(strTmp, 5) = ".jpeg" Then
                Dim strAltSuffix As String = Microsoft.VisualBasic.Left(strTmp, Len(strTmp) - 5) & ".jpg"
                strDefFilter = "QIG Expected Files (*" & strTmp & "; *" & strAltSuffix & ")|*" & strTmp & ";*" & strAltSuffix & "|" & FILTER_ALLFILES
            ElseIf Microsoft.VisualBasic.Right(strTmp, 4) = ".doc" Then
                Dim strAltSuffix As String = Microsoft.VisualBasic.Left(strTmp, Len(strTmp) - 4) & ".docx"
                strDefFilter = "QIG Expected Files (*" & strTmp & "; *" & strAltSuffix & ")|*" & strTmp & ";*" & strAltSuffix & "|" & FILTER_ALLFILES
            ElseIf Microsoft.VisualBasic.Right(strTmp, 5) = ".docx" Then
                Dim strAltSuffix As String = Microsoft.VisualBasic.Left(strTmp, Len(strTmp) - 5) & ".doc"
                strDefFilter = "QIG Expected Files (*" & strTmp & "; *" & strAltSuffix & ")|*" & strTmp & ";*" & strAltSuffix & "|" & FILTER_ALLFILES
            ElseIf Microsoft.VisualBasic.Right(strTmp, 4) = ".xls" Then
                Dim strAltSuffix As String = Microsoft.VisualBasic.Left(strTmp, Len(strTmp) - 4) & ".xlsx"
                strDefFilter = "QIG Expected Files (*" & strTmp & "; *" & strAltSuffix & ")|*" & strTmp & ";*" & strAltSuffix & "|" & FILTER_ALLFILES
            ElseIf Microsoft.VisualBasic.Right(strTmp, 5) = ".xlsx" Then
                Dim strAltSuffix As String = Microsoft.VisualBasic.Left(strTmp, Len(strTmp) - 5) & ".xls"
                strDefFilter = "QIG Expected Files (*" & strTmp & "; *" & strAltSuffix & ")|*" & strTmp & ";*" & strAltSuffix & "|" & FILTER_ALLFILES
            Else
                strDefFilter = "QIG Expected Files (*" & strTmp & ")|*" & strTmp & "|" & FILTER_ALLFILES
            End If
        End If

        ' browse for file
        Dim fo As OpenFileDialog = New OpenFileDialog()
        fo.Title = "Select a File for " & UI_Description
        fo.InitialDirectory = strDefFolder
        fo.Filter = strDefFilter
        fo.FilterIndex = 1
        fo.ShowDialog()
        ' return result
        Return fo.FileName
    End Function

    Private Sub AutoSelectAppendixFile(ByVal oCurRow As DataRow, Optional ByVal ImageNumber As String = "")
        ' called to handle 'autoselect' feature for appendix report items
        Dim oCurAppendixItem As QTT_InspectionsDataSets.vwAppendixItem_DetailsRow
        Dim UI_Description As String = ""
        Dim FileName As String = ""
        Dim Subfolder As String = ""
        Dim LocationPrefix As String = ""
        Dim PipeTubeType As String = ""
        Dim PipeTubeID As Integer = 0
        Dim UnitPrefix As String = ""
        Dim PassesPrefix As Boolean = False
        Dim PassesSubfolder As Boolean = False
        Dim PassNumber As Integer = 0
        Dim PassesInAlpha As Boolean = False
        Dim CustomPasses As Boolean = False
        Dim CustomPassStrings() As String = Nothing
        Dim PassSubfolderName As String = ""

        'First reset sorting
        ' basic row - file desc
        UI_Description = oCurRow.Item(APX_IMAGE_DESC_COL_INDEX).ToString

        'oCurAppendixItem = TryCast(oCurRow.DataBoundItem.Row, QTT_InspectionsDataSets.vwAppendixItem_DetailsRow)
        oCurAppendixItem = TryCast(oCurRow, QTT_InspectionsDataSets.vwAppendixItem_DetailsRow)
        If oCurAppendixItem IsNot Nothing Then
            ' read properties
            With oCurAppendixItem
                ' default subfolder?
                If Not .IsSubFolderNull Then
                    Subfolder = .SubFolder
                End If
                ' location prefix
                If Not .IsLocationPrefixNull Then
                    If .LocationPrefix Then
                        LocationPrefix = GetLocationPrefix()
                    End If
                End If
                ' Unit Prefix?
                If .UnitPrefix Then
                    UnitPrefix = GetUnitNumPrefix()
                End If

                ' PipeTube Type Text
                If Not .IsPipeTypeTextNull Then
                    ' include pipe/tube type text in filename
                    If .PipeTypeText Then
                        If Not .IsPipeTubeDescriptionNull Then
                            PipeTubeType = .PipeTubeDescription
                        End If
                    End If
                End If

                ' file filter string
                If Not .IsFilenameSuffixNull Then
                    FileName = .FilenameSuffix
                End If
            End With
        End If

        Dim strFN As String = GetAutoSelectFileSpec(UI_Description, LocationPrefix, UnitPrefix, PipeTubeType, FileName, Subfolder, PassesPrefix, PassesSubfolder, PassNumber, PassesInAlpha, CustomPasses, CustomPassStrings, PassSubfolderName, ImageNumber)
        If Len(strFN) > 0 Then
            oCurRow.Item(APX_FILENAME_COL_INDEX) = strFN
            uiAppendixItemsGridView.UpdateCurrentRow()
        End If

    End Sub

    Private Sub AutoSelectReportFile(ByVal oCurRow As DataRow)
        ' called to handle 'auto-select' feature for main report item files
        Dim oCurReportItem As QTT_InspectionsDataSets.vwReportItems_DetailsRow
        Dim UI_Description As String = ""
        Dim FileSuffix As String = ""
        Dim Subfolder As String = ""
        Dim LocationPrefix As String = ""
        Dim UnitPrefix As String = ""
        Dim PipeTubeType As String = ""
        Dim PassesPrefix As Boolean = False
        Dim PassesSubfolder As Boolean = False
        Dim PassNumber As Integer = 1
        Dim PassesInAlpha As Boolean = False
        Dim CustomPasses As Boolean = False
        Dim CustomPassStrings As String() = Nothing
        Dim PassSubfolderName As String = ""


        UI_Description = oCurRow.Item(RPT_IMAGE_DESC_COL_INDEX).ToString
        oCurReportItem = TryCast(oCurRow, QTT_InspectionsDataSets.vwReportItems_DetailsRow)
        If oCurReportItem IsNot Nothing Then
            ' read properties
            With oCurReportItem
                ' default subfolder?
                If Not .IsSubFolderNull Then
                    Subfolder = .SubFolder
                End If
                ' location prefix
                If Not .IsLocationPrefixNull Then
                    If .LocationPrefix Then
                        LocationPrefix = GetLocationPrefix()
                    End If
                End If
                ' Unit Prefix?
                If .UnitPrefix Then
                    UnitPrefix = GetUnitNumPrefix()
                End If
                ' PipeTube Type Text
                If Not .IsPipeTypeTextNull Then
                    ' include pipe/tube type text in filename
                    If .PipeTypeText Then
                        '' use first pipe-tube item type
                        'Use Convection
                        PipeTubeType = "Convection"
                    End If
                End If

                PassesInAlpha = mfTubesInAlpha
                CustomPasses = fTubesCustom
                CustomPassStrings = arrCustomTubes
                If Not .IsPassSubFolderNameNull Then PassSubfolderName = .PassSubFolderName
                ' file filter string
                If Not .IsFilenameSuffixNull Then
                    FileSuffix = .FilenameSuffix
                End If

            End With
        End If

        Dim strFN As String = GetAutoSelectFileSpec(UI_Description, LocationPrefix, UnitPrefix, PipeTubeType, FileSuffix, Subfolder, PassesPrefix, PassesSubfolder, PassNumber, PassesInAlpha, CustomPasses, CustomPassStrings, PassSubfolderName)
        'If that file doesn't exist, go to the folder and grab the first file of the desired type if it exists.
        'But allow the user to choose if it is one of these types.
        If Not UI_Description = "3D Tube Image" And Not UI_Description = "Diameter Graph" And Not UI_Description = "Tube with Creep Damage" Then
            If My.Computer.FileSystem.FileExists(strFN) = False Then
                strFN = GrabFirstFileOfTypeInFolder(strFN, FileSuffix)
            End If
        End If

        If Len(strFN) > 0 Then
            oCurRow.Item(RPT_FILENAME_COL_INDEX) = strFN
            uiReportItemsGridView.UpdateCurrentRow()
        End If

    End Sub

    Private Function GrabFirstFileOfTypeInFolder(ByVal NonExistantFile As String, ByVal FileSpec As String)
        'This function will derive the folder that the nonexistant file was in and will try to find a different file that matches its name
        Dim NewFileToReturn As String = ""
        'Find the folder it was looking in before.
        Dim strFolder As String = NonExistantFile.Substring(0, NonExistantFile.LastIndexOf("\"))
        'Find each file that exists in that folder that matches the filespec
        Dim strFileList As IList = My.Computer.FileSystem.GetFiles(strFolder, FileIO.SearchOption.SearchTopLevelOnly, "*" & FileSpec)
        If strFileList.Count > 0 Then NewFileToReturn = strFileList(0)
        'if it couldn't find a file in the folder, then just return the old value.
        If NewFileToReturn = "" Then NewFileToReturn = NonExistantFile
        Return NewFileToReturn
    End Function

    Private Function GetAutoSelectFileSpec(ByVal UI_Description As String, ByVal LocPrefix As String, ByVal UnitPrefix As String, ByVal PipeTubeText As String, ByVal FileSpec As String, ByVal Subfolder As String, ByVal PassesPrefix As Boolean, ByVal PassesSubfolder As Boolean, ByVal PassNumber As Integer, ByVal PassesInAlpha As Boolean, ByVal PassesCustom As Boolean, ByVal CustomPasses() As String, ByVal PassSubfolderName As String, Optional ByVal ImageNumber As String = "")
        ' generic function called to handle 'autoselect file' feature for main report and appendix items
        ' params:  UI_Description - user interface description of file being browsed for
        '          Filename - expected filename
        '          Subfolder - string that ID's subfolder 
        '          PassesSubfolder - T/F : look in 'Pass' subfolder
        '          PassNumber - if PassesSubfolder, identifies current pass to use
        '          PassesInAlpha - if true, assumes that pass folder / name is alpha
        '          PassSubfolderName - if graphics are in subfolder under pass name
        '          ImageNumber - if graphics are identified by a number string 'KM 10/20/2011
        Const FILTER_ALLFILES = "All files (*.*)|*.*"
        'KM 7/8/2011
        'KM 8/2/2011 Changed to MyDocuments
        Dim strDefFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & My.Settings.RootDataFolder
        'Dim strDefFolder As String = My.Settings.RootDataFolder
        Dim strInspRoot As String
        Dim strDefFilter As String = FILTER_ALLFILES
        Dim strTmp As String = ""
        Dim strReturn As String = ""

        ' determine path to inspection root folder
        If Not (Me.QTT_InspectionsDataSets.tblInspections.Item(0).IsRootFolderNameNull) Then
            strInspRoot = Me.QTT_InspectionsDataSets.tblInspections.Item(0).RootFolderName
            strDefFolder = My.Computer.FileSystem.CombinePath(strDefFolder, strInspRoot)
        End If

        ' read properties
        ' default subfolder?
        If Len(Subfolder) > 0 Then
            strTmp = Subfolder
            If Microsoft.VisualBasic.Left(strTmp, 1) = "\" Then
                strTmp = Mid(strTmp, 2)
            End If
            strDefFolder = My.Computer.FileSystem.CombinePath(strDefFolder, strTmp)
        End If
        ' passes based subfolder?
        'If PassesSubfolder Then
        '    'KM 7/8/2011
        '    strTmp = GetPassFolderName(PassNumber, PassesInAlpha, PassesCustom, CustomPasses)
        '    strDefFolder = My.Computer.FileSystem.CombinePath(strDefFolder, strTmp)
        '    strDefFolder = My.Computer.FileSystem.CombinePath(strDefFolder, PassSubfolderName)
        'End If
        ' file name string
        'If Len(FileSpec) > 0 Then
        '    If PassesPrefix Then
        '        'KM 7/8/2011
        '        strReturn = GetPassFilePrefix(PassNumber, PassesInAlpha, PassesCustom, CustomPasses) & "-"
        '    End If
        'End If
        If Len(LocPrefix) > 0 Then
            strReturn = strReturn & Trim(LocPrefix) & "-"
        End If
        If Len(UnitPrefix) > 0 Then
            strReturn = strReturn & Trim(UnitPrefix) & "-"
        End If
        If Len(PipeTubeText) > 0 Then
            Dim intPos As Integer = InStr(PipeTubeText, " ")
            If intPos > 0 Then
                PipeTubeText = Microsoft.VisualBasic.Left(PipeTubeText, (intPos - 1))
            End If
            strReturn = strReturn & PipeTubeText & "-"
        End If
        'get image number
        If ImageNumber <> "" Then
            strReturn = strReturn & ImageNumber & "-"
        End If

        ' strip any trailing "-" characters
        If Microsoft.VisualBasic.Right(strReturn, 1) = "-" Then
            strReturn = Microsoft.VisualBasic.Left(strReturn, Len(strReturn) - 1)
        End If
        strReturn = My.Computer.FileSystem.CombinePath(strDefFolder, strReturn)
        If Microsoft.VisualBasic.Right(strReturn, 1) = "\" Or ImageNumber <> "" Then
            strReturn = strReturn & FileSpec
        Else
            strReturn = strReturn & "\" & FileSpec
        End If
        ' return file
        Return strReturn
    End Function

    Private Function GetLinkedFileStatus(ByVal strFilename As String) As String

        ' returns standard linked file 'status' text
        Const STATUS_OK = "OK"
        Const STATUS_BAD = "NOT FOUND"
        Const STATUS_NONE = "(none)"

        Dim strStatus As String

        If Len(strFilename) > 0 Then
            If My.Computer.FileSystem.FileExists(strFilename) Then
                strStatus = STATUS_OK
            Else
                strStatus = STATUS_BAD
            End If
        Else
            strStatus = STATUS_NONE
        End If
        Return strStatus
    End Function

    Private Function GetUnitNumPrefix() As String

        ' gets and returns the first word of the Location - used for 'Auto-Select' of certain files
        Dim strReturn As String = ""
        Dim strTmp As String = ""
        Dim intTmp As Integer = 0

        With Me.QTT_InspectionsDataSets.tblInspections.Item(0)
            ' record report creation date
            If Not (.IsUnitNumberNull) Then
                strTmp = .UnitNumber
            End If
        End With

        Return strTmp

    End Function

    Private Function GetLocationPrefix() As String

        ' gets and returns the first word of the Location - used for 'Auto-Select' of certain files
        Dim strReturn As String = ""
        Dim strTmp As String = ""
        Dim intTmp As Integer = 0

        With Me.QTT_InspectionsDataSets.tblInspections.Item(0)
            ' record report creation date
            If Not (.IsLocationDescNull) Then
                strTmp = .LocationDesc
                ' trim
                intTmp = InStr(strTmp, " ")
                If intTmp > 0 Then
                    strTmp = Microsoft.VisualBasic.Left(strTmp, intTmp)
                End If
            End If
        End With

        Return strTmp

    End Function

    Private Function GetLastAppendixItemSort(ByVal ReportItemType As Integer, ByVal PipeTubeID As Integer, ByVal oAppendix As QTT_InspectionsDataSets.vwAppendixDetailsRow) As Integer

        ' returns the highest sort order of the currently displayed appendix items based on the specified ReportItemType and PipeTube Item
        Dim ReturnSortOrder As Integer = 0
        Dim oCurItem As QTT_InspectionsDataSets.vwAppendixItem_DetailsRow
        Dim drvT As DataRowView
        Dim fMyTest As Boolean = False

        'If FTIS Set, Sort after last report type 9
        If ReportItemType >= 6 And ReportItemType <= 8 Then ReportItemType = 9

        'For Each drvT In bsInspections_AppendixItems
        For Each oCurItem In oAppendix.GetvwAppendixItem_DetailsRows  ' bsInspections_AppendixItems
            With oCurItem
                If (PipeTubeID = 0) Then
                    fMyTest = (.FK_ReportItemType = ReportItemType) And (.SortOrder > ReturnSortOrder)
                Else
                    fMyTest = (.FK_RelatedPipeTube = PipeTubeID) And (.FK_ReportItemType = ReportItemType) And (.SortOrder > ReturnSortOrder)
                End If
                If fMyTest Then
                    ReturnSortOrder = .SortOrder
                End If
            End With
        Next

        Return ReturnSortOrder

    End Function

    Private Sub uiResetMainButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiResetMainButton.Click
        ' reset the sort order to 'default' - used in case user has sorted the list and wants to reset
        uiReportItemsGridView.ClearSorting()
        uiReportItemsGridView.Columns("SortOrder").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
    End Sub

    Private Sub uiResetAppendixButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiResetAppendixButton.Click
        ' reset the sort order to 'default' - used in case user has sorted the list and wants to reset
        uiAppendixItemsGridView.ClearSorting()
        uiAppendixItemsGridView.Columns("PipeTubeSortOrder").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        uiAppendixItemsGridView.Columns("SortOrder").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
    End Sub

    Private Sub uiAddMissingAppendixButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiAddMissingAppendixButton.Click
        ' adds any missing default items to selected appendix
        ' specify current appendix
        Dim oCurApdx As QTT_InspectionsDataSets.vwAppendixDetailsRow = TryCast(bsInspections_Appendixes.Current.Row, QTT_InspectionsDataSets.vwAppendixDetailsRow)
        Dim intApndxID As Integer = 0
        With oCurApdx
            intApndxID = .ID
            If intApndxID <> 0 Then

                If UpdateAppendixSectionItems(intApndxID) Then
                    MsgBox("Completed reset of missing Appendix items for 'Appendix " & .AppendixLetter & "'.", MsgBoxStyle.Information)
                Else
                    MsgBox("No Appendix items were missing for 'Appendix " & .AppendixLetter & "'.", MsgBoxStyle.Information)
                End If

            Else
                MsgBox("Error; can't add Appendix Items - No Appendix is currently selected.", MsgBoxStyle.Information)
            End If
        End With
    End Sub

    Private Sub uiAddNewAppendixButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiAddNewAppendixButton.Click
        ' calls dialog box to allow user to add one or more items

        ' get current appendix ID
        Dim oCurApdx As QTT_InspectionsDataSets.vwAppendixDetailsRow
        Dim dlgAddNew As fdlgAddAppendixItems = New fdlgAddAppendixItems
        Dim ItemTypeToAdd_ID As Integer = 0
        Dim ItemBookmark As String = ""
        Dim NumberOfItemsToAdd As Integer = 0
        Dim AddToAllAppendixes As Boolean = False
        Dim SkipAddToAppendix As Boolean = False
        Dim LoopCounter As Integer
        Dim Counter As Integer

        ' June 2007 - code that adds additional 'linked item' as needed (e.g. Reformer Appendix B graphs)
        Dim LinkedItemType_ID As Integer = 0
        Dim LinkedItem1_ID As Integer = 0
        Dim LinkedItem2_ID As Integer = 0
        Dim LinkedItem3_ID As Integer = 0

        With dlgAddNew
            .ParentForm_InspectionDetails2 = Me
            ' specify current appendix
            oCurApdx = TryCast(bsInspections_Appendixes.Current.Row, QTT_InspectionsDataSets.vwAppendixDetailsRow)
            Dim SelectedAppendixTypeID As Integer = oCurApdx.FK_AppendixDef
            .AppendixSectionID = oCurApdx.ID
            ' get current pipe tube ID 
            Dim CurrentPTI_Position As Integer = bsInspections_PipeTubeItems.Position   ' stores selected Pipe / Tube selected
            Dim oDRV As DataRowView = bsInspections_PipeTubeItems.Current
            Dim oCurPTI As QTT_InspectionsDataSets.tblPipeTubeItemsRow = oDRV.Row
            Dim CurPipeTubeItemID As Integer = oCurPTI.ID
            ' create copy of Pipe Tube items table
            .SetupUI_PipeTubeItems(bsInspections_PipeTubeItems, CurPipeTubeItemID)
            ' show dialog
            Select Case .ShowDialog()
                Case Windows.Forms.DialogResult.OK
                    ' item(s) selected - call function to add item and update grid
                    ItemTypeToAdd_ID = .ReportItemTypeToAdd.ReportItemTypeID
                    If Not .ReportItemTypeToAdd.IsBookmark_DefaultNull Then
                        ItemBookmark = .ReportItemTypeToAdd.Bookmark_Default
                    End If
                    NumberOfItemsToAdd = .NumberOfItemsToAdd
                    AddToAllAppendixes = (.rbAddTo_All.Checked)
                    ' Sort Order
                    Dim NewSortOrder As Integer
                    Dim MinSort As Integer = .ReportItemTypeToAdd.SortOrder
                    ' related pipe tub item type
                    Dim PTI_ID As Integer = 0

                    If Not .ReportItemTypeToAdd.IsFK_PipeTubeTypeNull Then
                        PTI_ID = .RelatedPipeTubeItem.ID
                    Else
                        PTI_ID = 0
                    End If

                    'LOTIS/MANTIS
                    If ItemTypeToAdd_ID = InspectionDetails.REFORMER_APDX_B_3D_REFORMER_MODEL Then
                        LinkedItem1_ID = InspectionDetails.REFORMER_APDX_B_DIAMETER_GRAPH
                    End If

                    'LOTIS/MANTIS 3D
                    If ItemTypeToAdd_ID = InspectionDetails.REFORMER_3D_REFORMER_NE Then
                        LinkedItem1_ID = InspectionDetails.REFORMER_3D_REFORMER_NW
                        LinkedItem2_ID = InspectionDetails.REFORMER_3D_REFORMER_SE
                        LinkedItem3_ID = InspectionDetails.REFORMER_3D_REFORMER_SW
                    End If

                    Dim CurPosition As Integer
                    Dim SectionCount As Integer = 0
                    Dim SectionDescription As String

                    ' add item(s)
                    If AddToAllAppendixes Then
                        ' May 2007 - add to all appendixes of similar type
                        CurPosition = bsInspections_Appendixes.Position
                        LoopCounter = bsInspections_Appendixes.Count - 1
                    Else
                        LoopCounter = 0  ' single Appendix Section
                    End If

                    ' save appendix items as needed
                    Call SaveAppendixItems(False, "")

                    Dim X As Integer
                    For X = 0 To LoopCounter
                        ' for "Add to All", additional code that walks through all appendixes, and flags skipping of Appendix Types that do not match original selection
                        If AddToAllAppendixes Then
                            bsInspections_Appendixes.Position = X
                            oCurApdx = TryCast(bsInspections_Appendixes.Current.Row, QTT_InspectionsDataSets.vwAppendixDetailsRow)
                            If (oCurApdx.FK_AppendixDef = SelectedAppendixTypeID) Then
                                SectionCount += 1
                            Else
                                SkipAddToAppendix = True
                            End If
                        End If
                        ' Main loop that adds Item(s) to appendix
                        If Not SkipAddToAppendix Then
                            NewSortOrder = GetLastAppendixItemSort(ItemTypeToAdd_ID, PTI_ID, oCurApdx)
                            If NewSortOrder < MinSort Then
                                NewSortOrder = MinSort - 1
                            End If

                            Dim NewSortOrder1 As Integer
                            Dim NewSortOrder2 As Integer
                            Dim NewSortOrder3 As Integer

                            If LinkedItem1_ID <> 0 Then NewSortOrder1 = GetLastAppendixItemSort(LinkedItem1_ID, PTI_ID, oCurApdx)
                            If LinkedItem2_ID <> 0 Then NewSortOrder2 = GetLastAppendixItemSort(LinkedItem2_ID, PTI_ID, oCurApdx)
                            If LinkedItem3_ID <> 0 Then NewSortOrder3 = GetLastAppendixItemSort(LinkedItem3_ID, PTI_ID, oCurApdx)

                            For Counter = 1 To NumberOfItemsToAdd
                                ' increment sort order
                                NewSortOrder += 10
                                oCurApdx = TryCast(bsInspections_Appendixes.Current.Row, QTT_InspectionsDataSets.vwAppendixDetailsRow)
                                ' add item
                                Call modProgram.AddAppendixItem(oCurApdx.ID, ItemTypeToAdd_ID, NewSortOrder, PTI_ID, ItemBookmark)
                                If LinkedItem1_ID <> 0 Then
                                    NewSortOrder1 += 10 ' increment sort order
                                    Call modProgram.AddAppendixItem(oCurApdx.ID, LinkedItem1_ID, NewSortOrder1, PTI_ID, ItemBookmark)
                                End If
                                If LinkedItem2_ID <> 0 Then
                                    NewSortOrder2 += 10 ' increment sort order
                                    Call modProgram.AddAppendixItem(oCurApdx.ID, LinkedItem2_ID, NewSortOrder2, PTI_ID, ItemBookmark)
                                End If
                                If LinkedItem3_ID <> 0 Then
                                    NewSortOrder3 += 10 ' increment sort order
                                    Call modProgram.AddAppendixItem(oCurApdx.ID, LinkedItem3_ID, NewSortOrder3, PTI_ID, ItemBookmark)
                                End If
                            Next Counter
                        End If
                    Next X
                    ' reposition Appendix Binding source as needed / prepare section description for UI message
                    If AddToAllAppendixes Then
                        If Not (bsInspections_Appendixes.Position = CurPosition) Then
                            bsInspections_Appendixes.Position = CurPosition
                        End If
                        SectionDescription = "to " & SectionCount & " Report Appendix(es)."
                    Else
                        SectionDescription = "to the current Appendix."
                    End If

                    ' save appendix items as needed
                    Call SaveAppendixItems(False, "")

                    ' reload appendix items
                    Me.VwAppendixItem_DetailsTableAdapter.Fill(Me.QTT_InspectionsDataSets.vwAppendixItem_Details)
                    MsgBox("Successfully Added " & NumberOfItemsToAdd & " new " & .ReportItemTypeToAdd.Description & " Item(s) " & SectionDescription, MsgBoxStyle.Information)

                Case Windows.Forms.DialogResult.Cancel
                    ' no action needed
                Case Else
                    ' unexpected
            End Select
            ' reset Pipe Tube binding source position as needed
            If bsInspections_PipeTubeItems.Position <> CurrentPTI_Position Then
                bsInspections_PipeTubeItems.Position = CurrentPTI_Position
            End If
        End With

    End Sub

    Private Sub RepositoryItemButtonEditMain_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles RepositoryItemButtonEditMain.ButtonClick

        Dim Editor As ButtonEdit = CType(sender, ButtonEdit)
        Dim Button As EditorButton = e.Button
        Dim oCurRow As DataRow
        oCurRow = uiReportItemsGridView.GetDataRow(uiReportItemsGridView.FocusedRowHandle)

        If IsNothing(Button.Tag) Then Exit Sub

        Select Case Button.Tag
            Case "BROWSE"
                Call BrowseForReportFile(oCurRow)
                'Removed per Tim Haugen
                'uiReportItemsGridView.BestFitColumns()
            Case "AUTO"
                Call AutoSelectReportFile(oCurRow)
                uiReportItemsGridView.BestFitColumns()
            Case "NONE"
                oCurRow.Item("LinkedFile") = ""
                uiReportItemsGridView.UpdateCurrentRow()
        End Select

        SaveData()

    End Sub

    Private Sub uiAutoSelectMainButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiAutoSelectMainButton.Click
        Dim CurrentRowHandle As Integer = uiReportItemsGridView.GetVisibleRowHandle(0)
        While CurrentRowHandle <> DevExpress.XtraGrid.GridControl.InvalidRowHandle
            If uiReportItemsGridView.GetDataRow(CurrentRowHandle).Item("LinkedFile").ToString() = "" Or GetLinkedFileStatus(uiReportItemsGridView.GetDataRow(CurrentRowHandle).Item("LinkedFile").ToString()) = "NOT FOUND" Then
                Call AutoSelectReportFile(uiReportItemsGridView.GetDataRow(CurrentRowHandle))
            End If
            CurrentRowHandle = uiReportItemsGridView.GetNextVisibleRow(CurrentRowHandle)
        End While
        uiReportItemsGridView.BestFitColumns()
    End Sub

    Private Sub uiRemoveMainButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiRemoveMainButton.Click
        Dim CurrentRowHandle As Integer = uiReportItemsGridView.GetVisibleRowHandle(0)
        While CurrentRowHandle <> DevExpress.XtraGrid.GridControl.InvalidRowHandle
            uiReportItemsGridView.GetDataRow(CurrentRowHandle).Item("LinkedFile") = ""
            uiReportItemsGridView.UpdateCurrentRow()
            CurrentRowHandle = uiReportItemsGridView.GetNextVisibleRow(CurrentRowHandle)
        End While
    End Sub

    Private Sub uiAutoSelectAppendixButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiAutoSelectAppendixButton.Click
        Dim CurrentRowHandle As Integer = uiAppendixItemsGridView.GetVisibleRowHandle(0)
        Dim ImageNumbers As String
        Dim arrImageNumbers As Array
        Dim ImageNumber As String = ""
        Dim arrCount As Integer = 0
        Dim i3D As Integer '3D Tube Image
        Dim iD As Integer 'Diameter Graph

        'First, reset sorting so correct numbering will be applied
        uiResetAppendixButton_Click(sender, New System.EventArgs())

        'Get the array of numbers
        ImageNumbers = uiImageNameFormat.Text
        If InStr(ImageNumbers, "i.e.,") = 0 And Not (String.IsNullOrEmpty(ImageNumbers)) Then

            While InStr(ImageNumbers, ", ") <> 0
                ImageNumbers = ImageNumbers.Replace(", ", ",")
            End While
            While InStr(ImageNumbers, " ,") <> 0
                ImageNumbers = ImageNumbers.Replace(" ,", ",")
            End While
            arrImageNumbers = ImageNumbers.Split(",")
            arrCount = arrImageNumbers.GetLength(0)
        End If

        'Loop through the rows, assigning file names. If 3D Model or Diameter Graph, add a number from the array if it exists
        'Go through the array, using each number once for each type of image. If there are
        'more images than numbers, stop numbering.
        Dim iHyphen As Integer
        While CurrentRowHandle <> DevExpress.XtraGrid.GridControl.InvalidRowHandle
            If uiAppendixItemsGridView.GetDataRow(CurrentRowHandle).Item("LinkedFile").ToString() = "" Or GetLinkedFileStatus(uiAppendixItemsGridView.GetDataRow(CurrentRowHandle).Item("LinkedFile").ToString()) = "NOT FOUND" Then
                If uiAppendixItemsGridView.GetDataRow(CurrentRowHandle).Item("FK_ReportItemType") = 13 Then
                    iHyphen = InStr(arrImageNumbers(i3D), "-")
                    If i3D = arrCount Then
                        ImageNumber = ""
                    Else
                        If arrCount > 0 Then
                            If iHyphen = 0 Then
                                ImageNumber = String.Format("R01T{0}", arrImageNumbers(i3D).ToString.PadLeft(3, "0"c))
                            Else
                                ImageNumber = String.Format("R0{0}T{1}", Mid(arrImageNumbers(i3D), 1, iHyphen - 1), Mid(arrImageNumbers(i3D), iHyphen + 1).ToString.PadLeft(3, "0"c))
                            End If
                            i3D = i3D + 1
                        End If
                    End If
                End If
                'Use a different loop, just in case the sets are not paired
                If uiAppendixItemsGridView.GetDataRow(CurrentRowHandle).Item("FK_ReportItemType") = 27 Then
                    iHyphen = InStr(arrImageNumbers(iD), "-")
                    If iD = arrCount Then
                        ImageNumber = ""
                    Else
                        If arrCount > 0 Then
                            If iHyphen = 0 Then
                                ImageNumber = String.Format("R01T{0}", arrImageNumbers(iD).ToString.PadLeft(3, "0"c))
                            Else
                                ImageNumber = String.Format("R0{0}T{1}", Mid(arrImageNumbers(iD), 1, iHyphen - 1), Mid(arrImageNumbers(iD), iHyphen + 1).ToString.PadLeft(3, "0"c))
                            End If
                            iD = iD + 1
                        End If
                    End If
                End If
                Call AutoSelectAppendixFile(uiAppendixItemsGridView.GetDataRow(CurrentRowHandle), ImageNumber)
                ImageNumber = ""
            End If
            CurrentRowHandle = uiAppendixItemsGridView.GetNextVisibleRow(CurrentRowHandle)
        End While
        uiAppendixItemsGridView.BestFitColumns()

    End Sub

    Private Sub uiRemoveAppendixButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiRemoveAppendixButton.Click
        Dim CurrentRowHandle As Integer = uiAppendixItemsGridView.GetVisibleRowHandle(0)
        While CurrentRowHandle <> DevExpress.XtraGrid.GridControl.InvalidRowHandle
            uiAppendixItemsGridView.GetDataRow(CurrentRowHandle).Item("LinkedFile") = ""
            uiAppendixItemsGridView.UpdateCurrentRow()
            CurrentRowHandle = uiAppendixItemsGridView.GetNextVisibleRow(CurrentRowHandle)
        End While
    End Sub

    Private Sub RepositoryItemButtonEditAppendix_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles RepositoryItemButtonEditAppendix.ButtonClick

        Dim Editor As ButtonEdit = CType(sender, ButtonEdit)
        Dim Button As EditorButton = e.Button
        Dim oCurRow As DataRow
        oCurRow = uiAppendixItemsGridView.GetDataRow(uiAppendixItemsGridView.FocusedRowHandle)

        If IsNothing(Button.Tag) Then Exit Sub

        Select Case Button.Tag
            Case "BROWSE"
                Call BrowseForAppendixFile(oCurRow)
                'Removed per Tim Haugen
                'uiAppendixItemsGridView.BestFitColumns()
            Case "AUTO"
                Call AutoSelectAppendixFile(oCurRow)
                uiAppendixItemsGridView.BestFitColumns()
            Case "NONE"
                oCurRow.Item("LinkedFile") = ""
                uiAppendixItemsGridView.UpdateCurrentRow()
        End Select

        SaveData()

    End Sub

    Private Sub uiReportItemsGridView_CustomUnboundColumnData(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs) Handles uiReportItemsGridView.CustomUnboundColumnData
        If e.Column.FieldName = "StatusMain" And e.IsGetData Then
            Dim fileName As String
            Dim row = Ctype(e.Row, DataRowView).Row.Item("LinkedFile")
            
            if (typeof(row) is String)
                filename = Ctype(row, string)
            else
                fileName = ""
            End If
            
            e.Value = GetLinkedFileStatus(fileName)
        End If
    End Sub

    Private Sub uiAppendixItemsGridView_CustomUnboundColumnData(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs) Handles uiAppendixItemsGridView.CustomUnboundColumnData
        If e.Column.FieldName = "StatusAppendix" And e.IsGetData Then
            dim fileName = Ctype(Ctype(e.Row, DataRowView).Row.Item("LinkedFile"), string)
            e.Value = GetLinkedFileStatus(fileName)
        End If
    End Sub

    Private Sub uiAppendixItems_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles uiAppendixItems.ButtonClick

        If IsNothing(e.Button.Tag) Then Exit Sub

        Dim intCurPos As Integer = uiAppendixItems.ItemIndex

        Select Case e.Button.Tag
            Case "PREV"
                If intCurPos > 0 Then
                    bsInspections_Appendixes.Position = intCurPos - 1
                    uiAppendixItems.ItemIndex = bsInspections_Appendixes.Position
                End If
            Case "NEXT"
                If intCurPos < (bsInspections_Appendixes.Count - 1) Then
                    bsInspections_Appendixes.Position = intCurPos + 1
                    uiAppendixItems.ItemIndex = bsInspections_Appendixes.Position
                End If
        End Select

    End Sub

    Private Sub uiAppendixItems_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiAppendixItems.EditValueChanged

        'save changes to other appendices
        Call SaveData()

        Dim oTR As QTT_InspectionsDataSets.vwAppendixItem_DetailsRow
        Dim oldindex As Integer
        Dim RefreshData As Boolean = False
        'If Appendix B
        If uiAppendixItems.Text = "B. 3D Modeling Images & Tube Diameter Graphs" Then
            'And a Can Reformer, remove box appendix items
            If uiCanCircularOption.Checked Then
                For Each oDRV As System.Data.DataRowView In bsInspections_AppendixItems.List
                    oTR = oDRV.Row
                    If oTR.FK_ReportItemType = 30 And String.IsNullOrEmpty(oTR.LinkedFile) Then modProgram.DeleteAppendixItem(oTR.AppendixItemID)
                    If oTR.FK_ReportItemType = 31 And String.IsNullOrEmpty(oTR.LinkedFile) Then modProgram.DeleteAppendixItem(oTR.AppendixItemID)
                    If oTR.FK_ReportItemType = 32 And String.IsNullOrEmpty(oTR.LinkedFile) Then modProgram.DeleteAppendixItem(oTR.AppendixItemID)
                    If oTR.FK_ReportItemType = 33 And String.IsNullOrEmpty(oTR.LinkedFile) Then modProgram.DeleteAppendixItem(oTR.AppendixItemID)
                Next
            Else
                'Or if a Box Reformer, remove can appendix item
                For Each oDRV As System.Data.DataRowView In bsInspections_AppendixItems.List
                    oTR = oDRV.Row
                    If oTR.FK_ReportItemType = 12 And String.IsNullOrEmpty(oTR.LinkedFile) Then modProgram.DeleteAppendixItem(oTR.AppendixItemID)
                Next
            End If
            'save the appendix
            Call SaveAppendixItems(False, "")
            Me.VwAppendixItem_DetailsTableAdapter.Fill(Me.QTT_InspectionsDataSets.vwAppendixItem_Details)
        End If

         oldindex =  bsInspections_Appendixes.Position
         bsInspections_Appendixes.Position = uiAppendixItems.ItemIndex

        If uiAppendixItems.Text = "B. 3D Modeling Images & Tube Diameter Graphs" Then
                For Each oDRV As System.Data.DataRowView In bsInspections_AppendixItems.List
                 oTR = oDRV.Row
                 If oTR.FK_ReportItemType = 12 And String.IsNullOrEmpty(oTR.LinkedFile) 
                     modProgram.DeleteAppendixItem(oTR.AppendixItemID)
                    RefreshData = True
                 End If
                Next
                Call SaveAppendixItems(False, "")
                Me.VwAppendixItem_DetailsTableAdapter.Fill(Me.QTT_InspectionsDataSets.vwAppendixItem_Details)
        End If

            If uiAppendixItems.Text.Contains("Individual Tube Identification & Analysis Tables") AndAlso uiInspectionTypeLabel.Text.ToLower.Contains("new") Then
                For Each oDRV As System.Data.DataRowView In bsInspections_AppendixItems.List
                 oTR = oDRV.Row
                 If oTR.FK_ReportItemType = 19 And String.IsNullOrEmpty(oTR.LinkedFile) 
                     modProgram.DeleteAppendixItem(oTR.AppendixItemID)
                    RefreshData = True
                 End If
                Next
                Call SaveAppendixItems(False, "")
                Me.VwAppendixItem_DetailsTableAdapter.Fill(Me.QTT_InspectionsDataSets.vwAppendixItem_Details)
        End If
       
        
        'If RefreshData
        '    bsInspections_Appendixes.Position = oldindex
        '     bsInspections_Appendixes.Position = uiAppendixItems.ItemIndex
        'End If

        uiAppendixItemsGridView.BestFitColumns()

    End Sub

    Private Sub uiAppendixItemsGridControl_(ByVal sender As Object, ByVal e As NavigatorButtonClickEventArgs) Handles uiAppendixItemsGridControl.EmbeddedNavigator.ButtonClick

        If e.Button.ButtonType = NavigatorButtonType.Remove Then

            ' deletes selected item
            ' June 2007 - added reformer appendix b kludge to add a pair of related items

            Dim ConfMessage As String = ""
            Dim oRemRow As QTT_InspectionsDataSets.vwAppendixItem_DetailsRow
            Dim oRemRelatedRow As QTT_InspectionsDataSets.vwAppendixItem_DetailsRow = Nothing
            Dim oTR As QTT_InspectionsDataSets.vwAppendixItem_DetailsRow = Nothing
            Dim TargetType As Integer = 0
            Dim TargetSort As Integer = 0

            oRemRow = bsInspections_AppendixItems.Current.Row

            ' June 2007 - find related item to delete as per hard coded item types and logic
            Select Case oRemRow.FK_ReportItemType
                Case REFORMER_APDX_B_3D_REFORMER_MODEL, REFORMER_APDX_B_DIAMETER_GRAPH
                    ' attempt to find 'linked' item
                    If (oRemRow.FK_ReportItemType = REFORMER_APDX_B_3D_REFORMER_MODEL) Then
                        TargetType = REFORMER_APDX_B_DIAMETER_GRAPH
                        TargetSort = oRemRow.SortOrder + 5
                    ElseIf oRemRow.FK_ReportItemType = REFORMER_APDX_B_DIAMETER_GRAPH Then
                        TargetType = REFORMER_APDX_B_3D_REFORMER_MODEL
                        TargetSort = oRemRow.SortOrder - 5
                    End If
                    For Each oDRV As System.Data.DataRowView In bsInspections_AppendixItems.List
                        oTR = oDRV.Row
                        If oTR.FK_ReportItemType = TargetType And oTR.SortOrder = TargetSort Then
                            oRemRelatedRow = oTR
                            Exit For
                        End If
                    Next
                    If Not (oRemRelatedRow Is Nothing) Then
                        ConfMessage = "Remove the Appendix Items: " & oRemRow.FullDescription & vbNewLine & " As well as linked Item: " & oRemRelatedRow.FullDescription & "?"
                    Else
                        ConfMessage = "Remove the Appendix Item: " & oRemRow.FullDescription & "?"
                    End If

                Case Else
                    ConfMessage = "Remove the Appendix Item: " & oRemRow.FullDescription & "?"

            End Select

            ' remove the row(s) from DB
            Dim taTemp As New QTT_InspectionsDataSetsTableAdapters.vwAppendixItem_DetailsTableAdapter

            ' delete from db
            taTemp.sprocDeleteAppendixItem(oRemRow.FK_AppendixSectionItem)
            ' remove from Binding Source

            ' June 2007: check for related item
            If Not (oRemRelatedRow Is Nothing) Then
                ' delete from db
                taTemp.sprocDeleteAppendixItem(oRemRelatedRow.FK_AppendixSectionItem)
                ' remove from Binding Source
                For Each oDRV As System.Data.DataRowView In bsInspections_AppendixItems.List
                    oTR = oDRV.Row
                    If oTR.FK_ReportItemType = TargetType And oTR.SortOrder = TargetSort Then
                        oTR.Delete()
                        Exit For
                    End If
                Next
            End If

        End If

    End Sub

#End Region

#Region "Report"

    Sub AddAppendixReportData(ByVal oMainRpt As cReportData)
        ' adds all needed appendix data objects to oMainRpt

        Dim intCurrencyPos As Integer = bsInspections_Appendixes.CurrencyManager.Position

        ' add appendix info
        Dim oApnx As QTT_InspectionsDataSets.vwAppendixDetailsRow
        Dim oApdxItem As QTT_InspectionsDataSets.vwAppendixItem_DetailsRow
        Dim oARD As cAppendixReportData
        Dim oRLF As cReportLinkedFile
        Dim intApdx As Integer
        Dim intX As Integer

        With bsInspections_Appendixes
            ' populate rows
            For intApdx = 0 To (.Count - 1)
                .CurrencyManager.Position = intApdx
                oApnx = .Current.Row
                oARD = New cAppendixReportData
                With oApnx ' oApnx
                    oARD.AppendixTitle = ("APPENDIX " & .AppendixLetter)
                    oARD.AppendixLetter = .AppendixLetter
                    oARD.PassDescription = .AppendixTitle
                    oARD.AppendixDescription = .Description
                    ' add images
                    With bsInspections_AppendixItems
                        For intX = 0 To (.Count - 1)
                            oRLF = New cReportLinkedFile
                            oApdxItem = .Item(intX).Row
                            With oApdxItem
                                If Not (.IsLinkedFileNull) Then
                                    oRLF.LinkedFileName = .LinkedFile
                                End If
                                If Len(oRLF.LinkedFileName) > 0 Then
                                    If Not (.IsBookmarkNull) Then
                                        oRLF.BookMark = .Bookmark
                                        Debug.Print("ID: " & .ID & " File:" & oRLF.LinkedFileName & " - BM: " & oRLF.BookMark)
                                    End If
                                    If Not (.IsItemTypeIDNull) Then
                                        oRLF.IsImageFile = (.ItemTypeID = 2)  ' 2 denotes an image file
                                    End If
                                    ' is File OK
                                    If Not (My.Computer.FileSystem.FileExists(oRLF.LinkedFileName)) Then
                                        oRLF.FileNotFound = True
                                        oRLF.ReportText = "The Linked Report File: '" & oRLF.LinkedFileName & "' was not found."
                                    End If
                                End If
                            End With
                            If Len(oRLF.LinkedFileName) > 0 Then
                                oARD.LinkedAppendixFiles.Add(oRLF)
                            End If
                        Next
                    End With
                End With
                ' add appendix to report data object
                oMainRpt.AppendixReportDatas.Add(oARD)
            Next intApdx
        End With

        ' reset appendix binding source position
        bsInspections_Appendixes.CurrencyManager.Position = intCurrencyPos

    End Sub

    Private Function CheckForRequiredInput(ByVal bAll As Boolean, Optional ByVal strTabName As String = "", Optional ByVal CurrentTab As DevExpress.XtraTab.XtraTabPage = Nothing)
        'This function will go through the validation for each item and return a string with the names of controls that did not pass their validation.
        Dim strReturnData As String = ""

        uiMainXtraTabControl.BeginUpdate()
        If bAll Then
            'Go through each tab to make sure all of the controls are populated.
            For i = 0 To uiMainXtraTabControl.TabPages.Count - 1
                If uiMainXtraTabControl.TabPages(i).PageVisible = True Then
                    uiMainXtraTabControl.SelectedTabPageIndex = i
                    'Validate the data 
                    'Don't check disabled or invisible controls 
                    DxValidationProvider.Validate()
                    FixDisabledValidation()
                    'If there is invalid data, record its error message and append it to the overall message that will be returned (assuming it's not on a disabled page)
                    If (DxValidationProvider.GetInvalidControls.Count > 0) Then
                        Dim ListOfBadControls As New List(Of Control)
                        For Each c As Control In DxValidationProvider.GetInvalidControls()
                            ListOfBadControls.Add(c)
                        Next
                        ListOfBadControls.Sort(AddressOf CompareControlsByTabIndex)
                        For Each BadControl As Control In ListOfBadControls
                            'Add a carriage return if this isn't the first item.
                            If strReturnData = "" Then
                                strReturnData = DxValidationProvider.GetValidationRule(BadControl).ErrorText
                            Else
                                strReturnData = strReturnData & vbCrLf & DxValidationProvider.GetValidationRule(BadControl).ErrorText
                            End If
                        Next
                    End If
                End If
            Next
        Else
            For i = 0 To uiMainXtraTabControl.TabPages.Count - 1
                If strTabName = uiMainXtraTabControl.TabPages(i).Name Then
                    uiMainXtraTabControl.SelectedTabPageIndex = i
                    'Validate the data 
                    'Don't check disabled or invisible controls 
                    DxValidationProvider.Validate()
                    FixDisabledValidation()
                    'If there is invalid data, record its error message and append it to the overall message that will be returned (assuming it's not on a disabled page)
                    If (DxValidationProvider.GetInvalidControls.Count > 0) Then
                        Dim ListOfBadControls As New List(Of Control)
                        For Each c As Control In DxValidationProvider.GetInvalidControls()
                            ListOfBadControls.Add(c)
                        Next
                        ListOfBadControls.Sort(AddressOf CompareControlsByTabIndex)
                        For Each BadControl As Control In ListOfBadControls
                            'Add a carriage return if this isn't the first item
                            If strReturnData = "" Then
                                strReturnData = DxValidationProvider.GetValidationRule(BadControl).ErrorText
                            Else
                                strReturnData = strReturnData & vbCrLf & DxValidationProvider.GetValidationRule(BadControl).ErrorText
                            End If
                        Next
                    End If
                    If Not CurrentTab Is Nothing Then uiMainXtraTabControl.SelectedTabPage = CurrentTab
                End If
            Next
        End If
        uiMainXtraTabControl.EndUpdate()

        If bAll Then strReturnData = CheckForNonDXValidatedFields(strReturnData)
        Return strReturnData
    End Function

    Private Shared Function CompareControlsByTabIndex(ByVal x As Control, ByVal y As Control) As Integer

        If String.IsNullOrEmpty(x.TabIndex) Then
            If String.IsNullOrEmpty(y.TabIndex) Then
                ' If x is Nothing and y is Nothing, they're equal.  
                Return 0
            Else
                ' If x is Nothing and y is not Nothing, y is greater.  
                Return -1
            End If
        Else
            ' If x is not Nothing... 
            If String.IsNullOrEmpty(y.TabIndex) Then
                ' ...and y is Nothing, x is greater. 
                Return 1
            Else
                ' if y is not Nothing, compare the tab indexes. 
                ' 
                Dim retval As Integer = x.TabIndex.CompareTo(y.TabIndex)
                If retval <> 0 Then
                    Return retval
                Else
                    Return x.TabIndex.CompareTo(y.TabIndex)
                End If
            End If
        End If

    End Function

    Private Function CheckForNonDXValidatedFields(ByVal strReturnData As String)
        'Make a list of the controls that will be checked.
        Dim lstControlsToCheck As New ArrayList
        lstControlsToCheck.Add(uiToolsCheckedList)
        lstControlsToCheck.Add(uiProjectDescriptionEditor)
        lstControlsToCheck.Add(uiTubeIdentificationEditor)
        lstControlsToCheck.Add(uiTubeInspectionLocations)
        Select Case mintInspectionType
            Case LOTIS_AGED, MANTIS_AGED
                lstControlsToCheck.Add(uiSummaryEditor)
                lstControlsToCheck.Add(uiExecSummaryDetailsEditor)
                lstControlsToCheck.Add(uiDiaGrowthSummaryEditor)
                lstControlsToCheck.Add(uiFFSEditor)
                lstControlsToCheck.Add(uiTubeHarvestingEditor)
                lstControlsToCheck.Add(uiPriorHistoryEditor)
            Case Else
                lstControlsToCheck.Add(uiInspectionSummaryEditor)
        End Select

        'Check some generic types of controls.
        For Each ControlInTheList As Control In lstControlsToCheck
            If TypeOf (ControlInTheList) Is CheckedListBoxControl Then strReturnData = strReturnData & EnsureAtLeastOneIsChecked(ControlInTheList, "Project Tools")
            If TypeOf (ControlInTheList) Is RichEditControl Then strReturnData = strReturnData & EnsureRichTextBoxIsntEmpty(ControlInTheList)
        Next
        strReturnData = strReturnData & EnsureGridControlIsntEmpty(uiGridViewContacts, "Contacts Grid")
        Return strReturnData

    End Function

    Private Function GetParentTab(ByVal SomeControl As Control)
        'This goes up the heirarchy of containers by looking at its parent object and seeing if it's a tab page. If it finds one, it will return the tab page's text
        Dim FoundTabPage As Boolean = False
        Dim ReturnText As String = ""
        Try
            While FoundTabPage = False
                If TypeOf (SomeControl.Parent) Is XtraTabPage Then
                    ReturnText = SomeControl.Parent.Text
                    FoundTabPage = True
                Else : SomeControl = SomeControl.Parent
                End If
            End While
        Catch ex As Exception
        End Try
        Return ReturnText
    End Function

    Private Function EnsureGridControlIsntEmpty(ByVal GridView As GridView, ByVal strText As String)
        'Error text for grid controls
        Dim strReturnString As String = ""
        If GridView.RowCount < 1 Then strReturnString = vbCrLf & "The " & strText & " on the " & GridView.GridControl.Parent.Text & " tab is empty."

        Return strReturnString
    End Function

    Private Function EnsureRichTextBoxIsntEmpty(ByVal RichTextBox As RichEditControl, Optional ByVal PassNumber As String = "")
        'Error text for rich text controls
        Dim strReturnString As String = ""
        If RichTextBox.Text = "" Or Microsoft.VisualBasic.Left(RichTextBox.Text, 12) = "Data changes" Then strReturnString = vbCrLf & RichTextBox.Tag & " cannot be blank."
        Return strReturnString

    End Function

    Private Function EnsureAtLeastOneIsChecked(ByVal CheckBox As CheckedListBoxControl, ByVal strText As String)
        'Error text for checkbox controls
        Dim strReturnString As String = ""
        If CheckBox.SelectedItems.Count < 0 Then strReturnString = vbCrLf & "Nothing is selected in the " & strText & " checkbox on the " & GetParentTab(CheckBox) & " tab."
        Return strReturnString
    End Function

    Private Function CreateInspectionDocument() As Boolean
        Const ReportSubFolder = "QIG Only\Field Report"
        Const OldReportsFolder = "Old Reports"
        Dim oRptData As cReportData = New cReportData
        Dim intNumTmp As Integer = 0
        Dim strFN As String = ""
        Dim strFolder As String = ""
        Dim strTemp As String = ""
        Dim strTemplateName As String = ""
        Dim fPaperSize_A4 As Boolean = False
        'Save data
        Call SaveData()
        Dim RequiredInputSuggestions As String

        RequiredInputSuggestions = CheckForRequiredInput(True)
        If RequiredInputSuggestions <> Nothing Then
            Dim UserChoice = MessageBox.Show(RequiredInputSuggestions & vbCrLf & vbCrLf & "Would you like to continue anyway?", "Some Required Input Not Given", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
            If UserChoice = DialogResult.No Then Exit Function
        End If

        'Call function to create and populate report data object
        If Not AssembleReportData(oRptData) Then
            MsgBox("Data Error. Cannot create inspection report. Please correct any reported errors and try again.", MsgBoxStyle.Exclamation, "Error Compiling Report Data")
            Exit Function
        End If

        'Get root report folder
        strFolder = My.Computer.FileSystem.CombinePath(uiRootFolderName.Text, ReportSubFolder)
        If Not My.Computer.FileSystem.DirectoryExists(strFolder) Then
            'First check root folder
            If Not My.Computer.FileSystem.DirectoryExists(uiRootFolderName.Text) Then
                Call CheckRootFolder(True)
            Else
                'Root folder exists, but no 'reports' folder
                If MsgBox("The report folder for this inspection (" & strFolder & ") was not found. Do you want to try to create this folder at this time?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Inspection Report Folder not found") = MsgBoxResult.Yes Then
                    My.Computer.FileSystem.CreateDirectory(strFolder)
                End If
            End If
            'Verify that folder now exists
            If Not My.Computer.FileSystem.DirectoryExists(strFolder) Then
                MsgBox("Cannot create a field report because the inspection report folder does not exist.", vbInformation, "Inspection Report Folder Not Found")
                Return False
                Exit Function
            End If
        End If

        strFolder = strFolder & "\"

        'Check Version / Revision / Prior Report Status
        With Me.QTT_InspectionsDataSets.tblInspections.Item(0)
            'See if there is an existing report
            If Not .IsReportFilenameNull Then
                strFN = .ReportFilename
                If MsgBoxResult.No = MsgBox("A report file (""" & strFN & """) has already been created for this inspection. Do you want to generate a new report file?", MsgBoxStyle.Information + MsgBoxStyle.YesNo, "Create New Report File") Then
                    Exit Function
                End If
                'If to here, move old file into "old reports" folder first
                With My.Computer.FileSystem
                    Try
                        .MoveFile(strFolder & strFN, (.CombinePath(strFolder, OldReportsFolder) & "\x_" & strFN))
                    Catch eX As Exception
                        'Catching error when old file already exists in the Old Reports folder
                        If eX.TargetSite.Attributes = 147 Then
                            MsgBox("The original report file could not be moved to the ""Old"" folder. The file is currently open or already exists in the ""Old"" folder. Close or delete the file and try again.", MsgBoxStyle.Information)
                            Exit Function
                        Else
                            If vbNo = MsgBox("The current report file could not be moved for an unknown reason. Continue anyway?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Could not move current report file") Then
                                Exit Function
                            End If
                        End If
                    End Try
                End With
                'Increment revision number
                oRptData.QTT_ReportRevision = oRptData.QTT_ReportRevision + 1
            End If
        End With
        ReportStatusLabel.Visible = True
        'Setup FooterID (can't be done until now, because revision # could change in code above)
        Dim sRptNum As String
        If Len(oRptData.QTT_ReportNumber) > 0 Then
            oRptData.FooterID = oRptData.QTT_ReportNumber
            sRptNum = oRptData.QTT_ReportNumber
        Else
            oRptData.FooterID = "Field Report"
            sRptNum = "Field Report"  ' used for file naming convention
        End If

        'Create new MS Word report document
        If (mMSWordReport Is Nothing) Then
            mMSWordReport = New cMSWord
        End If
        mMSWordReport.CreateNewTemplateDoc(oRptData.MSWord_ReportTemplate, CBool(PaperSizeOptionGroup.SelectedIndex))

        'Tell Report Data object to populate report with data
        oRptData.WriteToInspectionReport(mMSWordReport, CBool(PaperSizeOptionGroup.SelectedIndex))

        'Cleanup extra spaces, returns, etc.
        cMSWord.wd.Visible = True
        cMSWord.wd.WindowState = Microsoft.Office.Interop.Word.WdWindowState.wdWindowStateMinimize
        Call cMSWord.CleanUpReport()

        'Save report
        strFN = sRptNum & " R" & oRptData.QTT_ReportVersion & "-" & Format(oRptData.QTT_ReportRevision, "00") & ".docx"
        'Update report fields
        If mMSWordReport.SaveReportFile(strFolder & strFN) Then
            With Me.QTT_InspectionsDataSets.tblInspections.Item(0)
                .ReportFilename = strFN
                .ReportVersion = oRptData.QTT_ReportVersion
                .ReportRevision = oRptData.QTT_ReportRevision
            End With
            uiReportFilename.Text = strFN
            Call SaveData()
            If Not uiViewReportButton.Enabled Then uiViewReportButton.Enabled = True
        End If

        '"Release" references to word report object
        Call mMSWordReport.ReleaseCurrentReportFile()
        Return True

    End Function

    Private Function GetReportData_OfficeInfo(ByVal strOfficePhone As String) As String

        Const RELATION_INSPECTIONS_OFFICE = "tblInspections_tblQTTOffices"

        Dim strReturn As String = ""

        Try
            Dim oOffice As QTT_InspectionsDataSets.tblQTT_OfficesRow = bsInspections.CurrencyManager.Current.Row.GetParentRow(RELATION_INSPECTIONS_OFFICE)
            If Not oOffice Is Nothing Then
                With oOffice
                    ' assemble City, State, Postal Code, Country line 1st
                    If Not .IsCityNull Then
                        strReturn = .City
                    End If
                    If Not .IsStateNull Then
                        If Len(strReturn) > 0 Then strReturn = strReturn & ", "
                        strReturn = strReturn & .State
                    End If

                    If Not .IsPostalCodeNull Then
                        If Len(strReturn) > 0 Then strReturn = strReturn & " "
                        strReturn = strReturn & .PostalCode
                    End If

                    If Not .IsCountryNull Then
                        If Len(strReturn) > 0 Then
                            strReturn = strReturn & vbVerticalTab  ' for other offices, Country is its own line 
                            strReturn = strReturn & .Country
                        End If
                    End If
                    ' add address
                    If Not .IsAddressNull Then
                        If Len(strReturn) > 0 Then
                            ' this should always be the case in 'reality'
                            strReturn = .Address & vbVerticalTab & strReturn
                        Else
                            ' this should occur only if none of: City / State / Postal Code / Country exists
                            strReturn = .Address
                        End If
                    End If
                    ' add phone and fax
                    If Not .IsPhoneNull Then
                        strOfficePhone = "Tel: " & .Phone
                        strReturn = strReturn & vbVerticalTab & strOfficePhone
                    End If
                    If Not .IsFaxNull Then
                        strReturn = strReturn & vbVerticalTab & "Fax: " & .Fax
                    End If
                End With
            End If

        Catch ex As Exception
            MsgBox("The following error occurred trying to retrieve data about the specified QIG Office: " & ex.Message)

        End Try

        Return strReturn

    End Function

    Private Function AssembleReportData(ByVal oRptData As cReportData) As Boolean
        ' compiles report data from current dataset and  populates cReportData parameter
        ' returns True if successful, False if error condition occurs

        Dim intItem As Integer
        Dim intNumTmp As Integer = 0
        Dim strFN As String = ""
        Dim strFolder As String = ""
        Dim strTemp As String = ""
        Dim strOfficePhone As String = ""
        Dim PassesCustom As Boolean = fTubesCustom
        Dim CustomPasses() As String = arrCustomTubes

        ' set Office Info
        strTemp = GetReportData_OfficeInfo(strOfficePhone)
        oRptData.QTT_OfficeInfo = strTemp
        strTemp = ""   ' reset for other use below

        ' Report Template
        oRptData.MSWord_ReportTemplate = mstrMSWord_Template

        ' Read 'top level' inspection data
        With Me.QTT_InspectionsDataSets.tblInspections.Item(0)
            ' report version and revision
            If .IsReportRevisionNull Then
                .ReportRevision = 1
            End If
            If .IsReportVersionNull Then
                .ReportVersion = 1
            ElseIf .ReportVersion < 1 Then
                .ReportVersion = 1
            End If

            oRptData.QTT_ReportVersion = .ReportVersion
            oRptData.QTT_ReportRevision = .ReportRevision
            ' record report creation date
            .ReportCreateDate = System.DateTime.Today

            'NumberOfPasses and Prior History
            oRptData.NumberOfPasses = .NumberOfPasses
            If Not (.IsPriorHistoryNull) Then
                oRptData.PriorHistory = .PriorHistory
            Else
                oRptData.PriorHistory = ""
            End If
            'CustomerName
            If Not .IsCustomerNameNull Then
                oRptData.CustomerName = .CustomerName
            End If
            'InspectionDate
            If Not .IsInspectionDateStartNull Then
                oRptData.InspectionDate = .InspectionDateStart
            End If
            'LocationDescription
            If Not .IsLocationDescNull Then
                oRptData.LocationDescription = .LocationDesc
            End If
            'InspectionDescription
            If Not .IsProjectDescriptionNull Then
                oRptData.InspectionDescription = .ProjectDescription
            End If
            'QTT_ProjectNum
            If Not .IsProjectNumberNull Then
                oRptData.QTT_ProjectNum = .ProjectNumber
            End If
            'ExecutiveSummary
            If Not .IsProjectSummaryNull Then
                oRptData.ExecutiveSummary = .ProjectSummary
            End If
            'InspectionSummary
            If Not .IsInspectionSummaryNull Then
                oRptData.InspectionSummary = .InspectionSummary
            End If
            'DetailsSummary
            If Not .IsDetailsSummaryNull Then
                oRptData.DetailsSummary = .DetailsSummary
            End If
            'DiaGrowthSummary
            If Not .IsDiaGrowthSummaryNull Then
                oRptData.DiaGrowthSummary = .DiaGrowthSummary
            End If
            'FFSSummary
            If Not .IsFFSSummaryNull Then
                oRptData.FFSSummary = .FFSSummary
            End If
            'TubeHarvestingSummary
            If Not .IsTubeHarvestingSummaryNull Then
                oRptData.TubeHarvestingSummary = .TubeHarvestingSummary
            End If
            'Inspection Layout
            If Not .IsInspectionLayoutDescriptionNull Then
                oRptData.InspectionLayout = .InspectionLayoutDescription
            End If
            'Client_PO
            If Not .IsPurchaseOrderNumberNull Then
                oRptData.Client_PO = .PurchaseOrderNumber
            End If
            'QTT_ReportNumber
            If Not .IsQTT_Report_NoNull Then
                oRptData.QTT_ReportNumber = .QTT_Report_No
            End If
            'QTT_ReportDate
            If Not .IsReportCreateDateNull Then
                oRptData.QTT_ReportDate = .ReportCreateDate
            End If
            'UnitName
            If Not .IsUnitNameNull Then
                oRptData.UnitName = .UnitName
            End If
            'UnitNumber
            If Not .IsUnitNumberNull Then
                oRptData.UnitNumber = .UnitNumber
            End If
            'UnitService
            If Not .IsUnitStyleNull Then
                oRptData.UnitService = .UnitStyle
            End If
            'UnitManufacturer
            If Not .IsReformerOEMNull Then
                oRptData.UnitManufacturer = .ReformerOEM
            End If
            'PlantProcessType
            If Not .IsPlantProcessTypeNull Then
                oRptData.PlantProcessType = .PlantProcessType
            End If
            'SpareUninstalled
            If Not .IsSpareUninstalledNull Then
                If .SpareUninstalled = 0 Then
                    oRptData.SpareUninstalled = "Spare"
                Else
                    oRptData.SpareUninstalled = "Uninstalled"
                End If
            End If
            'BurnerConfiguration
            If Not .IsBurnerConfigurationNull Then
                oRptData.BurnerConfiguration = .BurnerConfiguration
            End If
            'ProcessFlowDirection
            If Not .IsProcessFlowDirectionNull Then
                oRptData.ProcessFlowDirection = .ProcessFlowDirection
            End If
            'NumberingSystem
            If Not .IsNumberingSystemNull Then
                oRptData.NumberingSystem = .NumberingSystem
            End If
            'SampleLevel3
            oRptData.SampleLevel3 = .SampleLevel3
            'JMC
            oRptData.JMC = .JMC
            ' Primary Inspector - Use Parent Row
            Const RELATION_INSPECTIONS_LEADINSPECTOR = "tblInspections_tblQTT_LeadInspectors"

            'PrimaryInspectorName
            Try
                'use Parent Row
                Dim oLead As QTT_InspectionsDataSets.tblQTT_LeadInspectorsRow = bsInspections.CurrencyManager.Current.Row.GetParentRow(RELATION_INSPECTIONS_LEADINSPECTOR)
                If Not oLead.IsLastNameNull Then
                    With oLead
                        If Not (.IsLastNameNull) And Not (.IsFirstNameNull) Then
                            oRptData.PrimaryInspectorName = .FullName
                        End If
                        If Not (.IsPhoneNumberNull) Then
                            oRptData.PrimaryInspectorPhone = .PhoneNumber
                        End If
                    End With
                End If
            Catch Ex As Exception
                MsgBox("Error reading Primary Inspector information details: " & Ex.Message, MsgBoxStyle.Information)
            End Try

            'PrimaryInspectorTitle
            Const RELATION_INSPECTIONS_LEADROLE = "tblInspections_tblQTT_PrimaryRoles"
            Try
                'use Parent Row
                Dim oRole As QTT_InspectionsDataSets.tblQTT_PrimaryRolesRow = bsInspections.CurrencyManager.Current.Row.GetParentRow(RELATION_INSPECTIONS_LEADROLE)
                If Not oRole.IsDescriptionNull Then
                    oRptData.PrimaryInspectorTitle = oRole.Description
                End If
            Catch Ex As Exception
                MsgBox("Error reading Primary Inspector Role information - details: " & Ex.Message, MsgBoxStyle.Information)
            End Try

            'Address
            If Not .IsAddressNull Then
                oRptData.Address = .Address
            End If
            'City
            If Not .IsCityNull Then
                oRptData.City = .City
            End If
            'State
            If Not .IsStateNull Then
                oRptData.State = .State
            End If
            'PostalCode
            If Not .IsPostalCodeNull Then
                oRptData.PostalCode = .PostalCode
            End If
            'Country
            If Not .IsCountryNull Then
                oRptData.Country = .Country
            End If
            'ClientContacts
            If Not .IsClientContactsNull Then
                oRptData.ClientContacts = .ClientContacts
            End If
        End With

        'Contacts
        Dim oContact As QTT_InspectionsDataSets.tblContactInformationRow
        Dim strTitle As String, strSalutation As String, strPhone As String, strFax As String, strEmail As String
        With Me.bstblContactInformation
            intNumTmp = .Count
            For intItem = 0 To (intNumTmp - 1)
                .CurrencyManager.Position = intItem
                oContact = .Current.Row
                With oContact
                    If Not .IsNameNull Then
                        If intItem > 0 Then strTemp = strTemp & vbVerticalTab & vbVerticalTab
                        If Not .IsPrefixNull Then
                            strSalutation = .Prefix & " "
                        End If
                        strTemp = strTemp  & .Name 'GK - 2/16/2016 - removed salutation
                        If Not .IsTitleNull Then strTitle = ", " & .Title
                        If Not .IsEmailNull Then strEmail = vbVerticalTab & "Email: " & .Email
                        If Not .IsPhoneNull Then strPhone = vbVerticalTab & "Phone: " & .Phone
                        If Not .IsFaxNull Then strFax = vbVerticalTab & "Fax: " & .Fax
                        strTemp = strTemp & strTitle & strEmail & strPhone & strFax
                        strSalutation = ""
                        strTitle = ""
                        strEmail = ""
                        strPhone = ""
                        strFax = ""
                    End If
                End With
            Next intItem
        End With
        oRptData.ClientContacts = strTemp
        strTemp = ""

        'Other QTT_Inspectors
        strTemp = ""  'reset
        Dim oQTT_Inspector As QTT_InspectionsDataSets.tblInspectionsAndInspectorsRow
        With bsInspections_OtherInspectors   ' returns only records for current inspection
            Debug.Print("Num=" & .Count())
            For intItem = 0 To (.Count - 1)
                .CurrencyManager.Position = intItem
                oQTT_Inspector = .Current.Row
                With oQTT_Inspector
                    If Not .IsFullNameAndRoleDescriptionNull Then
                        If Len(strTemp) > 0 Then strTemp = strTemp & vbVerticalTab
                        strTemp = strTemp & .FullNameAndRoleDescription
                    End If
                End With
            Next intItem
        End With
        oRptData.QTT_InspectorsOther = strTemp

        'DocumentProperties
        oRptData.Imperial = Not CBool(uiLeadingUnits.SelectedIndex)

        ' Report Items
        Dim oRLF As cReportLinkedFile
        Dim oLinkedFile As QTT_InspectionsDataSets.vwReportItems_DetailsRow
        With Me.bsInspections_ReportItems
            For intItem = 0 To (.Count - 1)
                oRLF = New cReportLinkedFile
                .CurrencyManager.Position = intItem
                oLinkedFile = .Current.Row
                'OLD & BAD: With .Item(intItem)
                With oLinkedFile
                    If Not (.IsLinkedFileNull) Then
                        oRLF.LinkedFileName = .LinkedFile
                    End If
                    If Len(oRLF.LinkedFileName) > 0 Then
                        If Not (.IsBookMarkNull) Then
                            oRLF.BookMark = .BookMark
                        End If
                        If Not (.IsItemTypeIDNull) Then
                            oRLF.IsImageFile = (.ItemTypeID = 2)  ' 2 denotes an image file
                        End If
                        ' is File OK
                        If Not (My.Computer.FileSystem.FileExists(oRLF.LinkedFileName)) Then
                            oRLF.FileNotFound = True
                            oRLF.ReportText = "The linked report file: '" & oRLF.LinkedFileName & "' was not found."
                        End If
                    End If
                End With
                If Len(oRLF.LinkedFileName) > 0 And Len(oRLF.BookMark) > 0 Then
                    oRptData.ReportLinkedFiles.Add(oRLF)
                End If
            Next
        End With

        ' Section that populates Piping / Tubing Item(s) for report
        Dim oPTI_Data As cPipeTubeReportData
        Dim intPass As Integer = 0
        Dim intCurPTI_ID As Integer = 0
        Dim oPTI_Summary As cPipeTubeSummary
        Dim oPTISum_Row As QTT_InspectionsDataSets.tblPipeTubeItemSummariesRow
        Dim intX As Integer
        Dim oPTI_Row As QTT_InspectionsDataSets.tblPipeTubeItemsRow
        With Me.bsInspections_PipeTubeItems
            For intItem = 0 To (.Count - 1)
                oPTI_Data = New cPipeTubeReportData   ' create new item for each PipeTube Instance
                .CurrencyManager.Position = intItem
                oPTI_Row = .Current.Row
                With oPTI_Row
                    'InspectionThreshold
                    If Not .IsInspectionThresholdNull Then
                        oPTI_Data.InspectionThreshold = .InspectionThreshold
                    End If
                    'Description
                    If Not .IsDescriptionNull Then
                        oPTI_Data.Description = .Description
                    End If
                    'InnerDims
                    If Not .IsInnerDiameterDisplayNull Then
                        oPTI_Data.InnerDims = .InnerDiameterDisplay
                    End If
                    'Lengths
                    If Not .IsLengthInspectedDisplayNull Then
                        oPTI_Data.Lengths = .LengthInspectedDisplay
                    End If
                    'Material
                    If Not .IsMaterialNull Then
                        oPTI_Data.Material = .Material
                    End If
                    'OuterDims
                    If Not .IsOuterDiameterDisplayNull Then
                        oPTI_Data.OuterDims = .OuterDiameterDisplay
                    End If
                    'NumberTubesInspected
                    If Not .IsNumberTubesInspectedNull Then
                        oPTI_Data.NumberTubesInspected = .NumberTubesInspected
                    End If
                    'TotalNumberTubes
                    If Not .IsNumberTubesTotalNull Then
                        oPTI_Data.NumberTubesTotal = .NumberTubesTotal
                    End If
                    'PutIntoServiceDate
                    If Not .IsDatePutIntoServiceNull Then
                        oPTI_Data.PutIntoServiceDate = .DatePutIntoService
                    End If
                    'Surface
                    If Not .IsSurfaceDescriptionNull Then
                        oPTI_Data.Surface = .SurfaceDescription
                    End If
                    'Thickness
                    If Not .IsWallThicknessDisplayNull Then
                        oPTI_Data.Thickness = .WallThicknessDisplay
                    End If
                    'Manufacturer
                    If Not .IsManufacturerNull Then
                        oPTI_Data.Manufacturer = .Manufacturer
                    End If
                    'Orientation/Direction
                    If Not .IsOrientationOrDirectionNull Then
                        oPTI_Data.OrientationOrDirection = .OrientationOrDirection
                    End If
                    'Pressure
                    If Not .IsPressureNull Then
                        oPTI_Data.Pressure = .Pressure
                    End If
                    'Target Temperature
                    If Not .IsTargetTempNull Then
                        oPTI_Data.TargetTemperature = .TargetTemp
                    End If
                    'Degrees Marker
                    If Not .IsDegreesMarkerNull Then
                        oPTI_Data.DegreesMarker = .DegreesMarker
                    End If
                    'Inspection Locations
                    If Not .IsInspectionLocationsNull Then
                        oPTI_Data.InspectionLocations = .InspectionLocations
                    End If
                    'Age At Inspection
                    If Not .IsAgeAtInspectionNull Then
                        oPTI_Data.AgeAtInspection = .AgeAtInspection
                    End If
                    'Est thermal cycles
                    If Not .IsNumberThermalCyclesNull Then
                        oPTI_Data.NumberThermalCycles = .NumberThermalCycles
                    End If
                    'Operating Hours
                    If Not .IsOperatingHoursNull Then
                        oPTI_Data.OperatingHours = .OperatingHours
                    End If
                    'Next Inspection Date
                    If Not .IsNextTubeInspectionDateNull Then
                        oPTI_Data.NextInspectionDate = .NextTubeInspectionDate
                    End If
                    'Tolerance Provided By
                    If Not .IsToleranceProvidedByNull Then
                        oPTI_Data.TolerancesProvidedBy = .ToleranceProvidedBy
                    End If
                End With
                ' Add object to ReportData object's collection
                oRptData.PipeTubeDataItems.Add(oPTI_Data)
            Next intItem
        End With

        'Add appendix info
        Call AddAppendixReportData(oRptData)
        'Return True (success) if to here
        Return True

    End Function

    Private Sub CreateReportButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiCreateReportButton.Click

        Dim bResult As Boolean

        'Change the cursor and let the user know the report is being created
        Me.Cursor = Cursors.WaitCursor
        ReportStatusLabel.Visible = True

        bResult = CreateInspectionDocument()

        Me.Cursor = Cursors.Default
        ReportStatusLabel.Visible = False
        If bResult Then
            MsgBox("Your report is complete.", MsgBoxStyle.OkOnly, "Inspection Report")
            cMSWord.wd.Activate()
            'Update the page numbers in the TOC
            'Already done during report generation, but there were some issues, so we're doing 
            'it again here, just to be sure
            Dim toc As Microsoft.Office.Interop.Word.TableOfContents
            For Each toc In cMSWord.wd.ActiveDocument.TablesOfContents
                toc.UpdatePageNumbers()
            Next
            cMSWord.wd.ActiveDocument.Save()
            cMSWord.wd.WindowState = Microsoft.Office.Interop.Word.WdWindowState.wdWindowStateMaximize
        End If

    End Sub

    Private Sub ViewReportButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiViewReportButton.Click

        Call ViewInspectionDocument()

    End Sub

    Private Sub uiDateLabel_Click(sender As Object, e As EventArgs) Handles uiDateLabel.Click

    End Sub

#End Region

End Class
