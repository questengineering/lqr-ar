﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucProjectSummary
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
      Me.uilblSummaryWriteup = New System.Windows.Forms.Label()
      Me.uilblNewUsedPipes = New System.Windows.Forms.Label()
      Me.uilblPassNumber = New System.Windows.Forms.Label()
      Me.uilblUnits = New System.Windows.Forms.Label()
      Me.uilblAppendix = New System.Windows.Forms.Label()
      Me.uiSummaryTabs = New DevExpress.XtraTab.XtraTabControl()
      Me.uiSummaryTabConPipes = New DevExpress.XtraTab.XtraTabPage()
      Me.uiConPipeScheduleSize = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.uiConPipePipeSize = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.uiConPipeInternalOrExternal = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.uilblGeneralOrLocal = New System.Windows.Forms.Label()
      Me.uiConPipeGeneralOrLocal = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.uilblInternalOrExternal = New System.Windows.Forms.Label()
      Me.uilblPipeSize = New System.Windows.Forms.Label()
      Me.uilblPipeSchedule = New System.Windows.Forms.Label()
      Me.btnConPipesUpdate = New DevExpress.XtraEditors.SimpleButton()
      Me.PipePanel = New System.Windows.Forms.Panel()
      Me.lblPipeInputAvgDiameter = New System.Windows.Forms.Label()
      Me.lblPipeInputDesignedWall = New System.Windows.Forms.Label()
      Me.lblPipeInputAvgWall = New System.Windows.Forms.Label()
      Me.lblPipeInputEstWallRemaining = New System.Windows.Forms.Label()
      Me.lblPipeInputWallLoss = New System.Windows.Forms.Label()
      Me.lblPipeInputLocation = New System.Windows.Forms.Label()
      Me.lblPipeInputMinWall = New System.Windows.Forms.Label()
      Me.lblPipeInputID = New System.Windows.Forms.Label()
      Me.txtConPipeAvgDiameter0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeAvgDiameter7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeAvgDiameter6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeID1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeAvgDiameter5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeMinWall0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeAvgDiameter4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeLocation0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeAvgDiameter3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeDesWall0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeAvgDiameter2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstWallLoss0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeAvgDiameter1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeAvgWall0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeDesWall7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstRemWall0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeDesWall6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeDesWall5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeDesWall4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeDesWall3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeDesWall2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeDesWall1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeAvgWall7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeAvgWall6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeAvgWall5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeAvgWall4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeAvgWall3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeAvgWall2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeAvgWall1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstRemWall7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstRemWall6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstRemWall5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstRemWall4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstRemWall3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstRemWall2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstRemWall1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstWallLoss7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstWallLoss6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstWallLoss5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstWallLoss4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstWallLoss3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstWallLoss2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeEstWallLoss1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeLocation7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeLocation6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeLocation5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeLocation4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeLocation3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeLocation2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeLocation1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeMinWall7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeMinWall6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeMinWall5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeMinWall4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeMinWall3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeMinWall2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeMinWall1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeID7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeID6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeID5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeID4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeID3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeID2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConPipeID0 = New DevExpress.XtraEditors.TextEdit()
      Me.uiSummaryConPipeTextbox = New DevExpress.XtraRichEdit.RichEditControl()
      Me.uiSummaryTabConBends = New DevExpress.XtraTab.XtraTabPage()
      Me.btnConBendsUpdate = New DevExpress.XtraEditors.SimpleButton()
      Me.uiSummaryConBendTextbox = New DevExpress.XtraRichEdit.RichEditControl()
      Me.Panel2 = New System.Windows.Forms.Panel()
      Me.Label17 = New System.Windows.Forms.Label()
      Me.Label21 = New System.Windows.Forms.Label()
      Me.Label23 = New System.Windows.Forms.Label()
      Me.Label24 = New System.Windows.Forms.Label()
      Me.txtConBendID1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendMinWall0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstWallLoss0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstRemWall0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstRemWall7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstRemWall6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstRemWall5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstRemWall4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstRemWall3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstRemWall2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstRemWall1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstWallLoss7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstWallLoss6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstWallLoss5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstWallLoss4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstWallLoss3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstWallLoss2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendEstWallLoss1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendMinWall7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendMinWall6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendMinWall5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendMinWall4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendMinWall3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendMinWall2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendMinWall1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendID7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendID6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendID5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendID4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendID3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendID2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtConBendID0 = New DevExpress.XtraEditors.TextEdit()
      Me.uiConBendScheduleSize = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.uiConBendBendSize = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.uiConBendInternalOrExternal = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.Label13 = New System.Windows.Forms.Label()
      Me.uiConBendGeneralOrLocal = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.Label14 = New System.Windows.Forms.Label()
      Me.Label15 = New System.Windows.Forms.Label()
      Me.Label16 = New System.Windows.Forms.Label()
      Me.uiSummaryTabRadPipes = New DevExpress.XtraTab.XtraTabPage()
      Me.uiRadPipeScheduleSize = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.uiRadPipePipeSize = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.uiRadPipeInternalOrExternal = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.Label1 = New System.Windows.Forms.Label()
      Me.uiRadPipeGeneralOrLocal = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.Label2 = New System.Windows.Forms.Label()
      Me.Label3 = New System.Windows.Forms.Label()
      Me.Label4 = New System.Windows.Forms.Label()
      Me.btnRadPipesUpdate = New DevExpress.XtraEditors.SimpleButton()
      Me.Panel1 = New System.Windows.Forms.Panel()
      Me.Label5 = New System.Windows.Forms.Label()
      Me.Label6 = New System.Windows.Forms.Label()
      Me.Label7 = New System.Windows.Forms.Label()
      Me.Label8 = New System.Windows.Forms.Label()
      Me.Label9 = New System.Windows.Forms.Label()
      Me.Label10 = New System.Windows.Forms.Label()
      Me.Label11 = New System.Windows.Forms.Label()
      Me.Label12 = New System.Windows.Forms.Label()
      Me.txtRadPipeAvgDiameter0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeAvgDiameter7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeAvgDiameter6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeID1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeAvgDiameter5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeMinWall0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeAvgDiameter4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeLocation0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeAvgDiameter3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeDesWall0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeAvgDiameter2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstWallLoss0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeAvgDiameter1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeAvgWall0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeDesWall7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstRemWall0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeDesWall6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeDesWall5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeDesWall4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeDesWall3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeDesWall2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeDesWall1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeAvgWall7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeAvgWall6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeAvgWall5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeAvgWall4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeAvgWall3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeAvgWall2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeAvgWall1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstRemWall7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstRemWall6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstRemWall5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstRemWall4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstRemWall3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstRemWall2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstRemWall1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstWallLoss7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstWallLoss6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstWallLoss5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstWallLoss4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstWallLoss3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstWallLoss2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeEstWallLoss1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeLocation7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeLocation6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeLocation5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeLocation4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeLocation3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeLocation2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeLocation1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeMinWall7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeMinWall6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeMinWall5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeMinWall4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeMinWall3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeMinWall2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeMinWall1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeID7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeID6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeID5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeID4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeID3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeID2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadPipeID0 = New DevExpress.XtraEditors.TextEdit()
      Me.uiSummaryRadPipeTextbox = New DevExpress.XtraRichEdit.RichEditControl()
      Me.uiSummaryTabRadBends = New DevExpress.XtraTab.XtraTabPage()
      Me.btnRadBendsUpdate = New DevExpress.XtraEditors.SimpleButton()
      Me.uiSummaryRadBendTextbox = New DevExpress.XtraRichEdit.RichEditControl()
      Me.Panel3 = New System.Windows.Forms.Panel()
      Me.Label18 = New System.Windows.Forms.Label()
      Me.Label19 = New System.Windows.Forms.Label()
      Me.Label20 = New System.Windows.Forms.Label()
      Me.Label22 = New System.Windows.Forms.Label()
      Me.txtRadBendID1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendMinWall0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstWallLoss0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstRemWall0 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstRemWall7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstRemWall6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstRemWall5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstRemWall4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstRemWall3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstRemWall2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstRemWall1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstWallLoss7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstWallLoss6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstWallLoss5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstWallLoss4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstWallLoss3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstWallLoss2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendEstWallLoss1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendMinWall7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendMinWall6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendMinWall5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendMinWall4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendMinWall3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendMinWall2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendMinWall1 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendID7 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendID6 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendID5 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendID4 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendID3 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendID2 = New DevExpress.XtraEditors.TextEdit()
      Me.txtRadBendID0 = New DevExpress.XtraEditors.TextEdit()
      Me.uiRadBendScheduleSize = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.uiRadBendBendSize = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.uiRadBendInternalOrExternal = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.Label25 = New System.Windows.Forms.Label()
      Me.uiRadBendGeneralOrLocal = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.Label26 = New System.Windows.Forms.Label()
      Me.Label27 = New System.Windows.Forms.Label()
      Me.Label28 = New System.Windows.Forms.Label()
      Me.lblGeneralOptions = New System.Windows.Forms.Label()
      Me.uiNewUsedPipes = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.uiSummaryWriteUp = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.uiUnits = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.uiPassNumber = New DevExpress.XtraEditors.ComboBoxEdit()
      Me.uiAppendix = New DevExpress.XtraEditors.ComboBoxEdit()
      CType(Me.uiSummaryTabs, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.uiSummaryTabs.SuspendLayout()
      Me.uiSummaryTabConPipes.SuspendLayout()
      CType(Me.uiConPipeScheduleSize.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiConPipePipeSize.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiConPipeInternalOrExternal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiConPipeGeneralOrLocal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.PipePanel.SuspendLayout()
      CType(Me.txtConPipeAvgDiameter0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeAvgDiameter7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeAvgDiameter6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeAvgDiameter5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeMinWall0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeAvgDiameter4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeLocation0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeAvgDiameter3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeDesWall0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeAvgDiameter2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstWallLoss0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeAvgDiameter1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeAvgWall0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeDesWall7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstRemWall0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeDesWall6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeDesWall5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeDesWall4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeDesWall3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeDesWall2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeDesWall1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeAvgWall7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeAvgWall6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeAvgWall5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeAvgWall4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeAvgWall3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeAvgWall2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeAvgWall1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstRemWall7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstRemWall6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstRemWall5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstRemWall4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstRemWall3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstRemWall2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstRemWall1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstWallLoss7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstWallLoss6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstWallLoss5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstWallLoss4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstWallLoss3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstWallLoss2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeEstWallLoss1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeLocation7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeLocation6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeLocation5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeLocation4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeLocation3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeLocation2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeLocation1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeMinWall7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeMinWall6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeMinWall5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeMinWall4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeMinWall3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeMinWall2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeMinWall1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeID7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeID6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeID5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeID4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeID3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeID2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConPipeID0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.uiSummaryTabConBends.SuspendLayout()
      Me.Panel2.SuspendLayout()
      CType(Me.txtConBendID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendMinWall0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstWallLoss0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstRemWall0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstRemWall7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstRemWall6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstRemWall5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstRemWall4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstRemWall3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstRemWall2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstRemWall1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstWallLoss7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstWallLoss6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstWallLoss5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstWallLoss4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstWallLoss3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstWallLoss2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendEstWallLoss1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendMinWall7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendMinWall6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendMinWall5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendMinWall4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendMinWall3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendMinWall2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendMinWall1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendID7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendID6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendID5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendID4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendID3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendID2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtConBendID0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiConBendScheduleSize.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiConBendBendSize.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiConBendInternalOrExternal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiConBendGeneralOrLocal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.uiSummaryTabRadPipes.SuspendLayout()
      CType(Me.uiRadPipeScheduleSize.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiRadPipePipeSize.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiRadPipeInternalOrExternal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiRadPipeGeneralOrLocal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.Panel1.SuspendLayout()
      CType(Me.txtRadPipeAvgDiameter0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeAvgDiameter7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeAvgDiameter6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeAvgDiameter5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeMinWall0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeAvgDiameter4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeLocation0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeAvgDiameter3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeDesWall0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeAvgDiameter2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstWallLoss0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeAvgDiameter1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeAvgWall0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeDesWall7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstRemWall0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeDesWall6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeDesWall5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeDesWall4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeDesWall3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeDesWall2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeDesWall1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeAvgWall7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeAvgWall6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeAvgWall5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeAvgWall4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeAvgWall3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeAvgWall2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeAvgWall1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstRemWall7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstRemWall6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstRemWall5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstRemWall4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstRemWall3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstRemWall2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstRemWall1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstWallLoss7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstWallLoss6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstWallLoss5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstWallLoss4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstWallLoss3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstWallLoss2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeEstWallLoss1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeLocation7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeLocation6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeLocation5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeLocation4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeLocation3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeLocation2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeLocation1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeMinWall7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeMinWall6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeMinWall5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeMinWall4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeMinWall3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeMinWall2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeMinWall1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeID7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeID6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeID5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeID4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeID3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeID2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadPipeID0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.uiSummaryTabRadBends.SuspendLayout()
      Me.Panel3.SuspendLayout()
      CType(Me.txtRadBendID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendMinWall0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstWallLoss0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstRemWall0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstRemWall7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstRemWall6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstRemWall5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstRemWall4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstRemWall3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstRemWall2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstRemWall1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstWallLoss7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstWallLoss6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstWallLoss5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstWallLoss4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstWallLoss3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstWallLoss2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendEstWallLoss1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendMinWall7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendMinWall6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendMinWall5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendMinWall4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendMinWall3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendMinWall2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendMinWall1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendID7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendID6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendID5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendID4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendID3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendID2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.txtRadBendID0.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiRadBendScheduleSize.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiRadBendBendSize.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiRadBendInternalOrExternal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiRadBendGeneralOrLocal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiNewUsedPipes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiSummaryWriteUp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiUnits.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiPassNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.uiAppendix.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.SuspendLayout()
      '
      'uilblSummaryWriteup
      '
      Me.uilblSummaryWriteup.AutoSize = True
      Me.uilblSummaryWriteup.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.uilblSummaryWriteup.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.uilblSummaryWriteup.Location = New System.Drawing.Point(67, 31)
      Me.uilblSummaryWriteup.Name = "uilblSummaryWriteup"
      Me.uilblSummaryWriteup.Size = New System.Drawing.Size(53, 17)
      Me.uilblSummaryWriteup.TabIndex = 23
      Me.uilblSummaryWriteup.Text = "Section:"
      Me.uilblSummaryWriteup.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'uilblNewUsedPipes
      '
      Me.uilblNewUsedPipes.AutoSize = True
      Me.uilblNewUsedPipes.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.uilblNewUsedPipes.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.uilblNewUsedPipes.Location = New System.Drawing.Point(5, 61)
      Me.uilblNewUsedPipes.Name = "uilblNewUsedPipes"
      Me.uilblNewUsedPipes.Size = New System.Drawing.Size(115, 17)
      Me.uilblNewUsedPipes.TabIndex = 25
      Me.uilblNewUsedPipes.Text = "New / Used Pipes:"
      Me.uilblNewUsedPipes.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'uilblPassNumber
      '
      Me.uilblPassNumber.AutoSize = True
      Me.uilblPassNumber.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.uilblPassNumber.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.uilblPassNumber.Location = New System.Drawing.Point(247, 31)
      Me.uilblPassNumber.Name = "uilblPassNumber"
      Me.uilblPassNumber.Size = New System.Drawing.Size(89, 17)
      Me.uilblPassNumber.TabIndex = 29
      Me.uilblPassNumber.Text = "Pass Number:"
      Me.uilblPassNumber.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'uilblUnits
      '
      Me.uilblUnits.AutoSize = True
      Me.uilblUnits.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.uilblUnits.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.uilblUnits.Location = New System.Drawing.Point(30, 93)
      Me.uilblUnits.Name = "uilblUnits"
      Me.uilblUnits.Size = New System.Drawing.Size(90, 17)
      Me.uilblUnits.TabIndex = 27
      Me.uilblUnits.Text = "Leading Units:"
      Me.uilblUnits.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'uilblAppendix
      '
      Me.uilblAppendix.AutoSize = True
      Me.uilblAppendix.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.uilblAppendix.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.uilblAppendix.Location = New System.Drawing.Point(270, 61)
      Me.uilblAppendix.Name = "uilblAppendix"
      Me.uilblAppendix.Size = New System.Drawing.Size(66, 17)
      Me.uilblAppendix.TabIndex = 31
      Me.uilblAppendix.Text = "Appendix:"
      Me.uilblAppendix.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'uiSummaryTabs
      '
      Me.uiSummaryTabs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
      Me.uiSummaryTabs.Location = New System.Drawing.Point(0, 121)
      Me.uiSummaryTabs.Name = "uiSummaryTabs"
      Me.uiSummaryTabs.SelectedTabPage = Me.uiSummaryTabConPipes
      Me.uiSummaryTabs.Size = New System.Drawing.Size(595, 346)
      Me.uiSummaryTabs.TabIndex = 40
      Me.uiSummaryTabs.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.uiSummaryTabConPipes, Me.uiSummaryTabConBends, Me.uiSummaryTabRadPipes, Me.uiSummaryTabRadBends})
      '
      'uiSummaryTabConPipes
      '
      Me.uiSummaryTabConPipes.Controls.Add(Me.uiConPipeScheduleSize)
      Me.uiSummaryTabConPipes.Controls.Add(Me.uiConPipePipeSize)
      Me.uiSummaryTabConPipes.Controls.Add(Me.uiConPipeInternalOrExternal)
      Me.uiSummaryTabConPipes.Controls.Add(Me.uilblGeneralOrLocal)
      Me.uiSummaryTabConPipes.Controls.Add(Me.uiConPipeGeneralOrLocal)
      Me.uiSummaryTabConPipes.Controls.Add(Me.uilblInternalOrExternal)
      Me.uiSummaryTabConPipes.Controls.Add(Me.uilblPipeSize)
      Me.uiSummaryTabConPipes.Controls.Add(Me.uilblPipeSchedule)
      Me.uiSummaryTabConPipes.Controls.Add(Me.btnConPipesUpdate)
      Me.uiSummaryTabConPipes.Controls.Add(Me.PipePanel)
      Me.uiSummaryTabConPipes.Controls.Add(Me.uiSummaryConPipeTextbox)
      Me.uiSummaryTabConPipes.Name = "uiSummaryTabConPipes"
      Me.uiSummaryTabConPipes.Size = New System.Drawing.Size(589, 320)
      Me.uiSummaryTabConPipes.Text = "Convective - Pipes"
      '
      'uiConPipeScheduleSize
      '
      Me.uiConPipeScheduleSize.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiConPipeScheduleSize.Location = New System.Drawing.Point(387, 27)
      Me.uiConPipeScheduleSize.Name = "uiConPipeScheduleSize"
      Me.uiConPipeScheduleSize.Properties.AutoHeight = False
      Me.uiConPipeScheduleSize.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiConPipeScheduleSize.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiConPipeScheduleSize.Properties.Items.AddRange(New Object() {"Tube", "5", "10", "20", "30", "40", "50", "60", "80", "120", "140", "160", "XXS"})
      Me.uiConPipeScheduleSize.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiConPipeScheduleSize.Size = New System.Drawing.Size(92, 24)
      Me.uiConPipeScheduleSize.TabIndex = 4
      '
      'uiConPipePipeSize
      '
      Me.uiConPipePipeSize.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiConPipePipeSize.Location = New System.Drawing.Point(318, 27)
      Me.uiConPipePipeSize.Name = "uiConPipePipeSize"
      Me.uiConPipePipeSize.Properties.AutoHeight = False
      Me.uiConPipePipeSize.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiConPipePipeSize.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiConPipePipeSize.Properties.Items.AddRange(New Object() {"Other", "2", "2.5", "3", "3.5", "4", "5", "6", "8", "10", "12"})
      Me.uiConPipePipeSize.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiConPipePipeSize.Size = New System.Drawing.Size(63, 24)
      Me.uiConPipePipeSize.TabIndex = 3
      '
      'uiConPipeInternalOrExternal
      '
      Me.uiConPipeInternalOrExternal.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiConPipeInternalOrExternal.Location = New System.Drawing.Point(188, 27)
      Me.uiConPipeInternalOrExternal.Name = "uiConPipeInternalOrExternal"
      Me.uiConPipeInternalOrExternal.Properties.AutoHeight = False
      Me.uiConPipeInternalOrExternal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiConPipeInternalOrExternal.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiConPipeInternalOrExternal.Properties.Items.AddRange(New Object() {"internal", "external", "internal and external"})
      Me.uiConPipeInternalOrExternal.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiConPipeInternalOrExternal.Size = New System.Drawing.Size(123, 24)
      Me.uiConPipeInternalOrExternal.TabIndex = 2
      '
      'uilblGeneralOrLocal
      '
      Me.uilblGeneralOrLocal.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uilblGeneralOrLocal.AutoSize = True
      Me.uilblGeneralOrLocal.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.uilblGeneralOrLocal.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.uilblGeneralOrLocal.Location = New System.Drawing.Point(78, 8)
      Me.uilblGeneralOrLocal.Name = "uilblGeneralOrLocal"
      Me.uilblGeneralOrLocal.Size = New System.Drawing.Size(104, 17)
      Me.uilblGeneralOrLocal.TabIndex = 57
      Me.uilblGeneralOrLocal.Text = "General or Local"
      Me.uilblGeneralOrLocal.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'uiConPipeGeneralOrLocal
      '
      Me.uiConPipeGeneralOrLocal.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiConPipeGeneralOrLocal.Location = New System.Drawing.Point(79, 27)
      Me.uiConPipeGeneralOrLocal.Name = "uiConPipeGeneralOrLocal"
      Me.uiConPipeGeneralOrLocal.Properties.AutoHeight = False
      Me.uiConPipeGeneralOrLocal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiConPipeGeneralOrLocal.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiConPipeGeneralOrLocal.Properties.Items.AddRange(New Object() {"General", "Local", "General and local"})
      Me.uiConPipeGeneralOrLocal.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiConPipeGeneralOrLocal.Size = New System.Drawing.Size(103, 24)
      Me.uiConPipeGeneralOrLocal.TabIndex = 1
      '
      'uilblInternalOrExternal
      '
      Me.uilblInternalOrExternal.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uilblInternalOrExternal.AutoSize = True
      Me.uilblInternalOrExternal.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.uilblInternalOrExternal.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.uilblInternalOrExternal.Location = New System.Drawing.Point(191, 8)
      Me.uilblInternalOrExternal.Name = "uilblInternalOrExternal"
      Me.uilblInternalOrExternal.Size = New System.Drawing.Size(118, 17)
      Me.uilblInternalOrExternal.TabIndex = 58
      Me.uilblInternalOrExternal.Text = "Internal or External"
      Me.uilblInternalOrExternal.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'uilblPipeSize
      '
      Me.uilblPipeSize.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uilblPipeSize.AutoSize = True
      Me.uilblPipeSize.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.uilblPipeSize.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.uilblPipeSize.Location = New System.Drawing.Point(318, 8)
      Me.uilblPipeSize.Name = "uilblPipeSize"
      Me.uilblPipeSize.Size = New System.Drawing.Size(60, 17)
      Me.uilblPipeSize.TabIndex = 59
      Me.uilblPipeSize.Text = "Pipe Size"
      Me.uilblPipeSize.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'uilblPipeSchedule
      '
      Me.uilblPipeSchedule.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uilblPipeSchedule.AutoSize = True
      Me.uilblPipeSchedule.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.uilblPipeSchedule.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.uilblPipeSchedule.Location = New System.Drawing.Point(387, 8)
      Me.uilblPipeSchedule.Name = "uilblPipeSchedule"
      Me.uilblPipeSchedule.Size = New System.Drawing.Size(89, 17)
      Me.uilblPipeSchedule.TabIndex = 60
      Me.uilblPipeSchedule.Text = "Pipe Schedule"
      Me.uilblPipeSchedule.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'btnConPipesUpdate
      '
      Me.btnConPipesUpdate.Anchor = System.Windows.Forms.AnchorStyles.Bottom
      Me.btnConPipesUpdate.Location = New System.Drawing.Point(167, 294)
      Me.btnConPipesUpdate.Name = "btnConPipesUpdate"
      Me.btnConPipesUpdate.Size = New System.Drawing.Size(241, 23)
      Me.btnConPipesUpdate.TabIndex = 2
      Me.btnConPipesUpdate.Text = "Update Convective Pipe Summary"
      '
      'PipePanel
      '
      Me.PipePanel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
      Me.PipePanel.AutoScroll = True
      Me.PipePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
      Me.PipePanel.Controls.Add(Me.lblPipeInputAvgDiameter)
      Me.PipePanel.Controls.Add(Me.lblPipeInputDesignedWall)
      Me.PipePanel.Controls.Add(Me.lblPipeInputAvgWall)
      Me.PipePanel.Controls.Add(Me.lblPipeInputEstWallRemaining)
      Me.PipePanel.Controls.Add(Me.lblPipeInputWallLoss)
      Me.PipePanel.Controls.Add(Me.lblPipeInputLocation)
      Me.PipePanel.Controls.Add(Me.lblPipeInputMinWall)
      Me.PipePanel.Controls.Add(Me.lblPipeInputID)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgDiameter0)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgDiameter7)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgDiameter6)
      Me.PipePanel.Controls.Add(Me.txtConPipeID1)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgDiameter5)
      Me.PipePanel.Controls.Add(Me.txtConPipeMinWall0)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgDiameter4)
      Me.PipePanel.Controls.Add(Me.txtConPipeLocation0)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgDiameter3)
      Me.PipePanel.Controls.Add(Me.txtConPipeDesWall0)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgDiameter2)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstWallLoss0)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgDiameter1)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgWall0)
      Me.PipePanel.Controls.Add(Me.txtConPipeDesWall7)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstRemWall0)
      Me.PipePanel.Controls.Add(Me.txtConPipeDesWall6)
      Me.PipePanel.Controls.Add(Me.txtConPipeDesWall5)
      Me.PipePanel.Controls.Add(Me.txtConPipeDesWall4)
      Me.PipePanel.Controls.Add(Me.txtConPipeDesWall3)
      Me.PipePanel.Controls.Add(Me.txtConPipeDesWall2)
      Me.PipePanel.Controls.Add(Me.txtConPipeDesWall1)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgWall7)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgWall6)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgWall5)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgWall4)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgWall3)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgWall2)
      Me.PipePanel.Controls.Add(Me.txtConPipeAvgWall1)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstRemWall7)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstRemWall6)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstRemWall5)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstRemWall4)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstRemWall3)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstRemWall2)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstRemWall1)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstWallLoss7)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstWallLoss6)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstWallLoss5)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstWallLoss4)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstWallLoss3)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstWallLoss2)
      Me.PipePanel.Controls.Add(Me.txtConPipeEstWallLoss1)
      Me.PipePanel.Controls.Add(Me.txtConPipeLocation7)
      Me.PipePanel.Controls.Add(Me.txtConPipeLocation6)
      Me.PipePanel.Controls.Add(Me.txtConPipeLocation5)
      Me.PipePanel.Controls.Add(Me.txtConPipeLocation4)
      Me.PipePanel.Controls.Add(Me.txtConPipeLocation3)
      Me.PipePanel.Controls.Add(Me.txtConPipeLocation2)
      Me.PipePanel.Controls.Add(Me.txtConPipeLocation1)
      Me.PipePanel.Controls.Add(Me.txtConPipeMinWall7)
      Me.PipePanel.Controls.Add(Me.txtConPipeMinWall6)
      Me.PipePanel.Controls.Add(Me.txtConPipeMinWall5)
      Me.PipePanel.Controls.Add(Me.txtConPipeMinWall4)
      Me.PipePanel.Controls.Add(Me.txtConPipeMinWall3)
      Me.PipePanel.Controls.Add(Me.txtConPipeMinWall2)
      Me.PipePanel.Controls.Add(Me.txtConPipeMinWall1)
      Me.PipePanel.Controls.Add(Me.txtConPipeID7)
      Me.PipePanel.Controls.Add(Me.txtConPipeID6)
      Me.PipePanel.Controls.Add(Me.txtConPipeID5)
      Me.PipePanel.Controls.Add(Me.txtConPipeID4)
      Me.PipePanel.Controls.Add(Me.txtConPipeID3)
      Me.PipePanel.Controls.Add(Me.txtConPipeID2)
      Me.PipePanel.Controls.Add(Me.txtConPipeID0)
      Me.PipePanel.Location = New System.Drawing.Point(0, 54)
      Me.PipePanel.Margin = New System.Windows.Forms.Padding(0)
      Me.PipePanel.Name = "PipePanel"
      Me.PipePanel.Size = New System.Drawing.Size(587, 100)
      Me.PipePanel.TabIndex = 1
      '
      'lblPipeInputAvgDiameter
      '
      Me.lblPipeInputAvgDiameter.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.lblPipeInputAvgDiameter.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lblPipeInputAvgDiameter.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.lblPipeInputAvgDiameter.Location = New System.Drawing.Point(447, 8)
      Me.lblPipeInputAvgDiameter.Name = "lblPipeInputAvgDiameter"
      Me.lblPipeInputAvgDiameter.Size = New System.Drawing.Size(65, 29)
      Me.lblPipeInputAvgDiameter.TabIndex = 144
      Me.lblPipeInputAvgDiameter.Text = "Average Diameter"
      Me.lblPipeInputAvgDiameter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'lblPipeInputDesignedWall
      '
      Me.lblPipeInputDesignedWall.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.lblPipeInputDesignedWall.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lblPipeInputDesignedWall.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.lblPipeInputDesignedWall.Location = New System.Drawing.Point(376, 8)
      Me.lblPipeInputDesignedWall.Name = "lblPipeInputDesignedWall"
      Me.lblPipeInputDesignedWall.Size = New System.Drawing.Size(71, 29)
      Me.lblPipeInputDesignedWall.TabIndex = 143
      Me.lblPipeInputDesignedWall.Text = "Designed Wall"
      Me.lblPipeInputDesignedWall.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'lblPipeInputAvgWall
      '
      Me.lblPipeInputAvgWall.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.lblPipeInputAvgWall.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lblPipeInputAvgWall.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.lblPipeInputAvgWall.Location = New System.Drawing.Point(313, 8)
      Me.lblPipeInputAvgWall.Name = "lblPipeInputAvgWall"
      Me.lblPipeInputAvgWall.Size = New System.Drawing.Size(63, 29)
      Me.lblPipeInputAvgWall.TabIndex = 142
      Me.lblPipeInputAvgWall.Text = "Average Wall"
      Me.lblPipeInputAvgWall.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'lblPipeInputEstWallRemaining
      '
      Me.lblPipeInputEstWallRemaining.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.lblPipeInputEstWallRemaining.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lblPipeInputEstWallRemaining.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.lblPipeInputEstWallRemaining.Location = New System.Drawing.Point(217, 8)
      Me.lblPipeInputEstWallRemaining.Name = "lblPipeInputEstWallRemaining"
      Me.lblPipeInputEstWallRemaining.Size = New System.Drawing.Size(96, 29)
      Me.lblPipeInputEstWallRemaining.TabIndex = 141
      Me.lblPipeInputEstWallRemaining.Text = "Estimated Remaining Wall (%)"
      Me.lblPipeInputEstWallRemaining.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'lblPipeInputWallLoss
      '
      Me.lblPipeInputWallLoss.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.lblPipeInputWallLoss.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lblPipeInputWallLoss.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.lblPipeInputWallLoss.Location = New System.Drawing.Point(149, 8)
      Me.lblPipeInputWallLoss.Name = "lblPipeInputWallLoss"
      Me.lblPipeInputWallLoss.Size = New System.Drawing.Size(68, 29)
      Me.lblPipeInputWallLoss.TabIndex = 140
      Me.lblPipeInputWallLoss.Text = "Estimated Wall Loss"
      Me.lblPipeInputWallLoss.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'lblPipeInputLocation
      '
      Me.lblPipeInputLocation.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.lblPipeInputLocation.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lblPipeInputLocation.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.lblPipeInputLocation.Location = New System.Drawing.Point(88, 8)
      Me.lblPipeInputLocation.Name = "lblPipeInputLocation"
      Me.lblPipeInputLocation.Size = New System.Drawing.Size(61, 29)
      Me.lblPipeInputLocation.TabIndex = 139
      Me.lblPipeInputLocation.Text = "Location"
      Me.lblPipeInputLocation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'lblPipeInputMinWall
      '
      Me.lblPipeInputMinWall.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.lblPipeInputMinWall.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lblPipeInputMinWall.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.lblPipeInputMinWall.Location = New System.Drawing.Point(25, 8)
      Me.lblPipeInputMinWall.Name = "lblPipeInputMinWall"
      Me.lblPipeInputMinWall.Size = New System.Drawing.Size(63, 29)
      Me.lblPipeInputMinWall.TabIndex = 138
      Me.lblPipeInputMinWall.Text = "Minimum Wall"
      Me.lblPipeInputMinWall.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'lblPipeInputID
      '
      Me.lblPipeInputID.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.lblPipeInputID.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lblPipeInputID.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.lblPipeInputID.Location = New System.Drawing.Point(-25, 8)
      Me.lblPipeInputID.Name = "lblPipeInputID"
      Me.lblPipeInputID.Size = New System.Drawing.Size(50, 29)
      Me.lblPipeInputID.TabIndex = 137
      Me.lblPipeInputID.Text = "Pipe ID"
      Me.lblPipeInputID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'txtConPipeAvgDiameter0
      '
      Me.txtConPipeAvgDiameter0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgDiameter0.Location = New System.Drawing.Point(447, 37)
      Me.txtConPipeAvgDiameter0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgDiameter0.Name = "txtConPipeAvgDiameter0"
      Me.txtConPipeAvgDiameter0.Size = New System.Drawing.Size(65, 20)
      Me.txtConPipeAvgDiameter0.TabIndex = 8
      '
      'txtConPipeAvgDiameter7
      '
      Me.txtConPipeAvgDiameter7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgDiameter7.Location = New System.Drawing.Point(425, 177)
      Me.txtConPipeAvgDiameter7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgDiameter7.Name = "txtConPipeAvgDiameter7"
      Me.txtConPipeAvgDiameter7.Size = New System.Drawing.Size(65, 20)
      Me.txtConPipeAvgDiameter7.TabIndex = 64
      '
      'txtConPipeAvgDiameter6
      '
      Me.txtConPipeAvgDiameter6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgDiameter6.Location = New System.Drawing.Point(425, 157)
      Me.txtConPipeAvgDiameter6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgDiameter6.Name = "txtConPipeAvgDiameter6"
      Me.txtConPipeAvgDiameter6.Size = New System.Drawing.Size(65, 20)
      Me.txtConPipeAvgDiameter6.TabIndex = 56
      '
      'txtConPipeID1
      '
      Me.txtConPipeID1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeID1.Location = New System.Drawing.Point(-25, 57)
      Me.txtConPipeID1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeID1.Name = "txtConPipeID1"
      Me.txtConPipeID1.Size = New System.Drawing.Size(50, 20)
      Me.txtConPipeID1.TabIndex = 9
      '
      'txtConPipeAvgDiameter5
      '
      Me.txtConPipeAvgDiameter5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgDiameter5.Location = New System.Drawing.Point(447, 137)
      Me.txtConPipeAvgDiameter5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgDiameter5.Name = "txtConPipeAvgDiameter5"
      Me.txtConPipeAvgDiameter5.Size = New System.Drawing.Size(65, 20)
      Me.txtConPipeAvgDiameter5.TabIndex = 48
      '
      'txtConPipeMinWall0
      '
      Me.txtConPipeMinWall0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeMinWall0.Location = New System.Drawing.Point(25, 37)
      Me.txtConPipeMinWall0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeMinWall0.Name = "txtConPipeMinWall0"
      Me.txtConPipeMinWall0.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeMinWall0.TabIndex = 2
      '
      'txtConPipeAvgDiameter4
      '
      Me.txtConPipeAvgDiameter4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgDiameter4.Location = New System.Drawing.Point(447, 117)
      Me.txtConPipeAvgDiameter4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgDiameter4.Name = "txtConPipeAvgDiameter4"
      Me.txtConPipeAvgDiameter4.Size = New System.Drawing.Size(65, 20)
      Me.txtConPipeAvgDiameter4.TabIndex = 40
      '
      'txtConPipeLocation0
      '
      Me.txtConPipeLocation0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeLocation0.Location = New System.Drawing.Point(88, 37)
      Me.txtConPipeLocation0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeLocation0.Name = "txtConPipeLocation0"
      Me.txtConPipeLocation0.Size = New System.Drawing.Size(61, 20)
      Me.txtConPipeLocation0.TabIndex = 3
      '
      'txtConPipeAvgDiameter3
      '
      Me.txtConPipeAvgDiameter3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgDiameter3.Location = New System.Drawing.Point(447, 97)
      Me.txtConPipeAvgDiameter3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgDiameter3.Name = "txtConPipeAvgDiameter3"
      Me.txtConPipeAvgDiameter3.Size = New System.Drawing.Size(65, 20)
      Me.txtConPipeAvgDiameter3.TabIndex = 32
      '
      'txtConPipeDesWall0
      '
      Me.txtConPipeDesWall0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeDesWall0.Location = New System.Drawing.Point(376, 37)
      Me.txtConPipeDesWall0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeDesWall0.Name = "txtConPipeDesWall0"
      Me.txtConPipeDesWall0.Size = New System.Drawing.Size(71, 20)
      Me.txtConPipeDesWall0.TabIndex = 7
      '
      'txtConPipeAvgDiameter2
      '
      Me.txtConPipeAvgDiameter2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgDiameter2.Location = New System.Drawing.Point(447, 77)
      Me.txtConPipeAvgDiameter2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgDiameter2.Name = "txtConPipeAvgDiameter2"
      Me.txtConPipeAvgDiameter2.Size = New System.Drawing.Size(65, 20)
      Me.txtConPipeAvgDiameter2.TabIndex = 24
      '
      'txtConPipeEstWallLoss0
      '
      Me.txtConPipeEstWallLoss0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstWallLoss0.Location = New System.Drawing.Point(149, 37)
      Me.txtConPipeEstWallLoss0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstWallLoss0.Name = "txtConPipeEstWallLoss0"
      Me.txtConPipeEstWallLoss0.Size = New System.Drawing.Size(68, 20)
      Me.txtConPipeEstWallLoss0.TabIndex = 4
      '
      'txtConPipeAvgDiameter1
      '
      Me.txtConPipeAvgDiameter1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgDiameter1.Location = New System.Drawing.Point(447, 57)
      Me.txtConPipeAvgDiameter1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgDiameter1.Name = "txtConPipeAvgDiameter1"
      Me.txtConPipeAvgDiameter1.Size = New System.Drawing.Size(65, 20)
      Me.txtConPipeAvgDiameter1.TabIndex = 16
      '
      'txtConPipeAvgWall0
      '
      Me.txtConPipeAvgWall0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgWall0.Location = New System.Drawing.Point(313, 37)
      Me.txtConPipeAvgWall0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgWall0.Name = "txtConPipeAvgWall0"
      Me.txtConPipeAvgWall0.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeAvgWall0.TabIndex = 6
      '
      'txtConPipeDesWall7
      '
      Me.txtConPipeDesWall7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeDesWall7.Location = New System.Drawing.Point(354, 177)
      Me.txtConPipeDesWall7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeDesWall7.Name = "txtConPipeDesWall7"
      Me.txtConPipeDesWall7.Size = New System.Drawing.Size(71, 20)
      Me.txtConPipeDesWall7.TabIndex = 63
      '
      'txtConPipeEstRemWall0
      '
      Me.txtConPipeEstRemWall0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstRemWall0.Location = New System.Drawing.Point(217, 37)
      Me.txtConPipeEstRemWall0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstRemWall0.Name = "txtConPipeEstRemWall0"
      Me.txtConPipeEstRemWall0.Size = New System.Drawing.Size(96, 20)
      Me.txtConPipeEstRemWall0.TabIndex = 5
      '
      'txtConPipeDesWall6
      '
      Me.txtConPipeDesWall6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeDesWall6.Location = New System.Drawing.Point(354, 157)
      Me.txtConPipeDesWall6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeDesWall6.Name = "txtConPipeDesWall6"
      Me.txtConPipeDesWall6.Size = New System.Drawing.Size(71, 20)
      Me.txtConPipeDesWall6.TabIndex = 55
      '
      'txtConPipeDesWall5
      '
      Me.txtConPipeDesWall5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeDesWall5.Location = New System.Drawing.Point(376, 137)
      Me.txtConPipeDesWall5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeDesWall5.Name = "txtConPipeDesWall5"
      Me.txtConPipeDesWall5.Size = New System.Drawing.Size(71, 20)
      Me.txtConPipeDesWall5.TabIndex = 47
      '
      'txtConPipeDesWall4
      '
      Me.txtConPipeDesWall4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeDesWall4.Location = New System.Drawing.Point(376, 117)
      Me.txtConPipeDesWall4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeDesWall4.Name = "txtConPipeDesWall4"
      Me.txtConPipeDesWall4.Size = New System.Drawing.Size(71, 20)
      Me.txtConPipeDesWall4.TabIndex = 39
      '
      'txtConPipeDesWall3
      '
      Me.txtConPipeDesWall3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeDesWall3.Location = New System.Drawing.Point(376, 97)
      Me.txtConPipeDesWall3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeDesWall3.Name = "txtConPipeDesWall3"
      Me.txtConPipeDesWall3.Size = New System.Drawing.Size(71, 20)
      Me.txtConPipeDesWall3.TabIndex = 31
      '
      'txtConPipeDesWall2
      '
      Me.txtConPipeDesWall2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeDesWall2.Location = New System.Drawing.Point(376, 77)
      Me.txtConPipeDesWall2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeDesWall2.Name = "txtConPipeDesWall2"
      Me.txtConPipeDesWall2.Size = New System.Drawing.Size(71, 20)
      Me.txtConPipeDesWall2.TabIndex = 23
      '
      'txtConPipeDesWall1
      '
      Me.txtConPipeDesWall1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeDesWall1.Location = New System.Drawing.Point(376, 57)
      Me.txtConPipeDesWall1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeDesWall1.Name = "txtConPipeDesWall1"
      Me.txtConPipeDesWall1.Size = New System.Drawing.Size(71, 20)
      Me.txtConPipeDesWall1.TabIndex = 15
      '
      'txtConPipeAvgWall7
      '
      Me.txtConPipeAvgWall7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgWall7.Location = New System.Drawing.Point(291, 177)
      Me.txtConPipeAvgWall7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgWall7.Name = "txtConPipeAvgWall7"
      Me.txtConPipeAvgWall7.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeAvgWall7.TabIndex = 62
      '
      'txtConPipeAvgWall6
      '
      Me.txtConPipeAvgWall6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgWall6.Location = New System.Drawing.Point(291, 157)
      Me.txtConPipeAvgWall6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgWall6.Name = "txtConPipeAvgWall6"
      Me.txtConPipeAvgWall6.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeAvgWall6.TabIndex = 54
      '
      'txtConPipeAvgWall5
      '
      Me.txtConPipeAvgWall5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgWall5.Location = New System.Drawing.Point(313, 137)
      Me.txtConPipeAvgWall5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgWall5.Name = "txtConPipeAvgWall5"
      Me.txtConPipeAvgWall5.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeAvgWall5.TabIndex = 46
      '
      'txtConPipeAvgWall4
      '
      Me.txtConPipeAvgWall4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgWall4.Location = New System.Drawing.Point(313, 117)
      Me.txtConPipeAvgWall4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgWall4.Name = "txtConPipeAvgWall4"
      Me.txtConPipeAvgWall4.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeAvgWall4.TabIndex = 38
      '
      'txtConPipeAvgWall3
      '
      Me.txtConPipeAvgWall3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgWall3.Location = New System.Drawing.Point(313, 97)
      Me.txtConPipeAvgWall3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgWall3.Name = "txtConPipeAvgWall3"
      Me.txtConPipeAvgWall3.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeAvgWall3.TabIndex = 30
      '
      'txtConPipeAvgWall2
      '
      Me.txtConPipeAvgWall2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgWall2.Location = New System.Drawing.Point(313, 77)
      Me.txtConPipeAvgWall2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgWall2.Name = "txtConPipeAvgWall2"
      Me.txtConPipeAvgWall2.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeAvgWall2.TabIndex = 22
      '
      'txtConPipeAvgWall1
      '
      Me.txtConPipeAvgWall1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeAvgWall1.Location = New System.Drawing.Point(313, 57)
      Me.txtConPipeAvgWall1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeAvgWall1.Name = "txtConPipeAvgWall1"
      Me.txtConPipeAvgWall1.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeAvgWall1.TabIndex = 14
      '
      'txtConPipeEstRemWall7
      '
      Me.txtConPipeEstRemWall7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstRemWall7.Location = New System.Drawing.Point(195, 177)
      Me.txtConPipeEstRemWall7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstRemWall7.Name = "txtConPipeEstRemWall7"
      Me.txtConPipeEstRemWall7.Size = New System.Drawing.Size(96, 20)
      Me.txtConPipeEstRemWall7.TabIndex = 61
      '
      'txtConPipeEstRemWall6
      '
      Me.txtConPipeEstRemWall6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstRemWall6.Location = New System.Drawing.Point(195, 157)
      Me.txtConPipeEstRemWall6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstRemWall6.Name = "txtConPipeEstRemWall6"
      Me.txtConPipeEstRemWall6.Size = New System.Drawing.Size(96, 20)
      Me.txtConPipeEstRemWall6.TabIndex = 53
      '
      'txtConPipeEstRemWall5
      '
      Me.txtConPipeEstRemWall5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstRemWall5.Location = New System.Drawing.Point(217, 137)
      Me.txtConPipeEstRemWall5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstRemWall5.Name = "txtConPipeEstRemWall5"
      Me.txtConPipeEstRemWall5.Size = New System.Drawing.Size(96, 20)
      Me.txtConPipeEstRemWall5.TabIndex = 45
      '
      'txtConPipeEstRemWall4
      '
      Me.txtConPipeEstRemWall4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstRemWall4.Location = New System.Drawing.Point(217, 117)
      Me.txtConPipeEstRemWall4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstRemWall4.Name = "txtConPipeEstRemWall4"
      Me.txtConPipeEstRemWall4.Size = New System.Drawing.Size(96, 20)
      Me.txtConPipeEstRemWall4.TabIndex = 37
      '
      'txtConPipeEstRemWall3
      '
      Me.txtConPipeEstRemWall3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstRemWall3.Location = New System.Drawing.Point(217, 97)
      Me.txtConPipeEstRemWall3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstRemWall3.Name = "txtConPipeEstRemWall3"
      Me.txtConPipeEstRemWall3.Size = New System.Drawing.Size(96, 20)
      Me.txtConPipeEstRemWall3.TabIndex = 29
      '
      'txtConPipeEstRemWall2
      '
      Me.txtConPipeEstRemWall2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstRemWall2.Location = New System.Drawing.Point(217, 77)
      Me.txtConPipeEstRemWall2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstRemWall2.Name = "txtConPipeEstRemWall2"
      Me.txtConPipeEstRemWall2.Size = New System.Drawing.Size(96, 20)
      Me.txtConPipeEstRemWall2.TabIndex = 21
      '
      'txtConPipeEstRemWall1
      '
      Me.txtConPipeEstRemWall1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstRemWall1.Location = New System.Drawing.Point(217, 57)
      Me.txtConPipeEstRemWall1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstRemWall1.Name = "txtConPipeEstRemWall1"
      Me.txtConPipeEstRemWall1.Size = New System.Drawing.Size(96, 20)
      Me.txtConPipeEstRemWall1.TabIndex = 13
      '
      'txtConPipeEstWallLoss7
      '
      Me.txtConPipeEstWallLoss7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstWallLoss7.Location = New System.Drawing.Point(127, 177)
      Me.txtConPipeEstWallLoss7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstWallLoss7.Name = "txtConPipeEstWallLoss7"
      Me.txtConPipeEstWallLoss7.Size = New System.Drawing.Size(68, 20)
      Me.txtConPipeEstWallLoss7.TabIndex = 60
      '
      'txtConPipeEstWallLoss6
      '
      Me.txtConPipeEstWallLoss6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstWallLoss6.Location = New System.Drawing.Point(127, 157)
      Me.txtConPipeEstWallLoss6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstWallLoss6.Name = "txtConPipeEstWallLoss6"
      Me.txtConPipeEstWallLoss6.Size = New System.Drawing.Size(68, 20)
      Me.txtConPipeEstWallLoss6.TabIndex = 52
      '
      'txtConPipeEstWallLoss5
      '
      Me.txtConPipeEstWallLoss5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstWallLoss5.Location = New System.Drawing.Point(149, 137)
      Me.txtConPipeEstWallLoss5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstWallLoss5.Name = "txtConPipeEstWallLoss5"
      Me.txtConPipeEstWallLoss5.Size = New System.Drawing.Size(68, 20)
      Me.txtConPipeEstWallLoss5.TabIndex = 44
      '
      'txtConPipeEstWallLoss4
      '
      Me.txtConPipeEstWallLoss4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstWallLoss4.Location = New System.Drawing.Point(149, 117)
      Me.txtConPipeEstWallLoss4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstWallLoss4.Name = "txtConPipeEstWallLoss4"
      Me.txtConPipeEstWallLoss4.Size = New System.Drawing.Size(68, 20)
      Me.txtConPipeEstWallLoss4.TabIndex = 36
      '
      'txtConPipeEstWallLoss3
      '
      Me.txtConPipeEstWallLoss3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstWallLoss3.Location = New System.Drawing.Point(149, 97)
      Me.txtConPipeEstWallLoss3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstWallLoss3.Name = "txtConPipeEstWallLoss3"
      Me.txtConPipeEstWallLoss3.Size = New System.Drawing.Size(68, 20)
      Me.txtConPipeEstWallLoss3.TabIndex = 28
      '
      'txtConPipeEstWallLoss2
      '
      Me.txtConPipeEstWallLoss2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstWallLoss2.Location = New System.Drawing.Point(149, 77)
      Me.txtConPipeEstWallLoss2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstWallLoss2.Name = "txtConPipeEstWallLoss2"
      Me.txtConPipeEstWallLoss2.Size = New System.Drawing.Size(68, 20)
      Me.txtConPipeEstWallLoss2.TabIndex = 20
      '
      'txtConPipeEstWallLoss1
      '
      Me.txtConPipeEstWallLoss1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeEstWallLoss1.Location = New System.Drawing.Point(149, 57)
      Me.txtConPipeEstWallLoss1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeEstWallLoss1.Name = "txtConPipeEstWallLoss1"
      Me.txtConPipeEstWallLoss1.Size = New System.Drawing.Size(68, 20)
      Me.txtConPipeEstWallLoss1.TabIndex = 12
      '
      'txtConPipeLocation7
      '
      Me.txtConPipeLocation7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeLocation7.Location = New System.Drawing.Point(66, 177)
      Me.txtConPipeLocation7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeLocation7.Name = "txtConPipeLocation7"
      Me.txtConPipeLocation7.Size = New System.Drawing.Size(61, 20)
      Me.txtConPipeLocation7.TabIndex = 59
      '
      'txtConPipeLocation6
      '
      Me.txtConPipeLocation6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeLocation6.Location = New System.Drawing.Point(66, 157)
      Me.txtConPipeLocation6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeLocation6.Name = "txtConPipeLocation6"
      Me.txtConPipeLocation6.Size = New System.Drawing.Size(61, 20)
      Me.txtConPipeLocation6.TabIndex = 51
      '
      'txtConPipeLocation5
      '
      Me.txtConPipeLocation5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeLocation5.Location = New System.Drawing.Point(88, 137)
      Me.txtConPipeLocation5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeLocation5.Name = "txtConPipeLocation5"
      Me.txtConPipeLocation5.Size = New System.Drawing.Size(61, 20)
      Me.txtConPipeLocation5.TabIndex = 43
      '
      'txtConPipeLocation4
      '
      Me.txtConPipeLocation4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeLocation4.Location = New System.Drawing.Point(88, 117)
      Me.txtConPipeLocation4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeLocation4.Name = "txtConPipeLocation4"
      Me.txtConPipeLocation4.Size = New System.Drawing.Size(61, 20)
      Me.txtConPipeLocation4.TabIndex = 35
      '
      'txtConPipeLocation3
      '
      Me.txtConPipeLocation3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeLocation3.Location = New System.Drawing.Point(88, 97)
      Me.txtConPipeLocation3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeLocation3.Name = "txtConPipeLocation3"
      Me.txtConPipeLocation3.Size = New System.Drawing.Size(61, 20)
      Me.txtConPipeLocation3.TabIndex = 27
      '
      'txtConPipeLocation2
      '
      Me.txtConPipeLocation2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeLocation2.Location = New System.Drawing.Point(88, 77)
      Me.txtConPipeLocation2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeLocation2.Name = "txtConPipeLocation2"
      Me.txtConPipeLocation2.Size = New System.Drawing.Size(61, 20)
      Me.txtConPipeLocation2.TabIndex = 19
      '
      'txtConPipeLocation1
      '
      Me.txtConPipeLocation1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeLocation1.Location = New System.Drawing.Point(88, 57)
      Me.txtConPipeLocation1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeLocation1.Name = "txtConPipeLocation1"
      Me.txtConPipeLocation1.Size = New System.Drawing.Size(61, 20)
      Me.txtConPipeLocation1.TabIndex = 11
      '
      'txtConPipeMinWall7
      '
      Me.txtConPipeMinWall7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeMinWall7.Location = New System.Drawing.Point(3, 177)
      Me.txtConPipeMinWall7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeMinWall7.Name = "txtConPipeMinWall7"
      Me.txtConPipeMinWall7.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeMinWall7.TabIndex = 58
      '
      'txtConPipeMinWall6
      '
      Me.txtConPipeMinWall6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeMinWall6.Location = New System.Drawing.Point(3, 157)
      Me.txtConPipeMinWall6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeMinWall6.Name = "txtConPipeMinWall6"
      Me.txtConPipeMinWall6.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeMinWall6.TabIndex = 50
      '
      'txtConPipeMinWall5
      '
      Me.txtConPipeMinWall5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeMinWall5.Location = New System.Drawing.Point(25, 137)
      Me.txtConPipeMinWall5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeMinWall5.Name = "txtConPipeMinWall5"
      Me.txtConPipeMinWall5.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeMinWall5.TabIndex = 42
      '
      'txtConPipeMinWall4
      '
      Me.txtConPipeMinWall4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeMinWall4.Location = New System.Drawing.Point(25, 117)
      Me.txtConPipeMinWall4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeMinWall4.Name = "txtConPipeMinWall4"
      Me.txtConPipeMinWall4.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeMinWall4.TabIndex = 34
      '
      'txtConPipeMinWall3
      '
      Me.txtConPipeMinWall3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeMinWall3.Location = New System.Drawing.Point(25, 97)
      Me.txtConPipeMinWall3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeMinWall3.Name = "txtConPipeMinWall3"
      Me.txtConPipeMinWall3.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeMinWall3.TabIndex = 26
      '
      'txtConPipeMinWall2
      '
      Me.txtConPipeMinWall2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeMinWall2.Location = New System.Drawing.Point(25, 77)
      Me.txtConPipeMinWall2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeMinWall2.Name = "txtConPipeMinWall2"
      Me.txtConPipeMinWall2.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeMinWall2.TabIndex = 18
      '
      'txtConPipeMinWall1
      '
      Me.txtConPipeMinWall1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeMinWall1.Location = New System.Drawing.Point(25, 57)
      Me.txtConPipeMinWall1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeMinWall1.Name = "txtConPipeMinWall1"
      Me.txtConPipeMinWall1.Size = New System.Drawing.Size(63, 20)
      Me.txtConPipeMinWall1.TabIndex = 10
      '
      'txtConPipeID7
      '
      Me.txtConPipeID7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeID7.Location = New System.Drawing.Point(-47, 177)
      Me.txtConPipeID7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeID7.Name = "txtConPipeID7"
      Me.txtConPipeID7.Size = New System.Drawing.Size(50, 20)
      Me.txtConPipeID7.TabIndex = 57
      '
      'txtConPipeID6
      '
      Me.txtConPipeID6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeID6.Location = New System.Drawing.Point(-47, 157)
      Me.txtConPipeID6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeID6.Name = "txtConPipeID6"
      Me.txtConPipeID6.Size = New System.Drawing.Size(50, 20)
      Me.txtConPipeID6.TabIndex = 49
      '
      'txtConPipeID5
      '
      Me.txtConPipeID5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeID5.Location = New System.Drawing.Point(-25, 137)
      Me.txtConPipeID5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeID5.Name = "txtConPipeID5"
      Me.txtConPipeID5.Size = New System.Drawing.Size(50, 20)
      Me.txtConPipeID5.TabIndex = 41
      '
      'txtConPipeID4
      '
      Me.txtConPipeID4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeID4.Location = New System.Drawing.Point(-25, 117)
      Me.txtConPipeID4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeID4.Name = "txtConPipeID4"
      Me.txtConPipeID4.Size = New System.Drawing.Size(50, 20)
      Me.txtConPipeID4.TabIndex = 33
      '
      'txtConPipeID3
      '
      Me.txtConPipeID3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeID3.Location = New System.Drawing.Point(-25, 97)
      Me.txtConPipeID3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeID3.Name = "txtConPipeID3"
      Me.txtConPipeID3.Size = New System.Drawing.Size(50, 20)
      Me.txtConPipeID3.TabIndex = 25
      '
      'txtConPipeID2
      '
      Me.txtConPipeID2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeID2.Location = New System.Drawing.Point(-25, 77)
      Me.txtConPipeID2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeID2.Name = "txtConPipeID2"
      Me.txtConPipeID2.Size = New System.Drawing.Size(50, 20)
      Me.txtConPipeID2.TabIndex = 17
      '
      'txtConPipeID0
      '
      Me.txtConPipeID0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConPipeID0.Location = New System.Drawing.Point(-25, 37)
      Me.txtConPipeID0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConPipeID0.Name = "txtConPipeID0"
      Me.txtConPipeID0.Size = New System.Drawing.Size(50, 20)
      Me.txtConPipeID0.TabIndex = 1
      '
      'uiSummaryConPipeTextbox
      '
      Me.uiSummaryConPipeTextbox.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
      Me.uiSummaryConPipeTextbox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
      Me.uiSummaryConPipeTextbox.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.uiSummaryConPipeTextbox.Appearance.Text.Options.UseFont = True
      Me.uiSummaryConPipeTextbox.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
      Me.uiSummaryConPipeTextbox.Location = New System.Drawing.Point(-1, 152)
      Me.uiSummaryConPipeTextbox.Margin = New System.Windows.Forms.Padding(0)
      Me.uiSummaryConPipeTextbox.Name = "uiSummaryConPipeTextbox"
      Me.uiSummaryConPipeTextbox.Size = New System.Drawing.Size(588, 138)
      Me.uiSummaryConPipeTextbox.TabIndex = 0
      '
      'uiSummaryTabConBends
      '
      Me.uiSummaryTabConBends.Controls.Add(Me.btnConBendsUpdate)
      Me.uiSummaryTabConBends.Controls.Add(Me.uiSummaryConBendTextbox)
      Me.uiSummaryTabConBends.Controls.Add(Me.Panel2)
      Me.uiSummaryTabConBends.Controls.Add(Me.uiConBendScheduleSize)
      Me.uiSummaryTabConBends.Controls.Add(Me.uiConBendBendSize)
      Me.uiSummaryTabConBends.Controls.Add(Me.uiConBendInternalOrExternal)
      Me.uiSummaryTabConBends.Controls.Add(Me.Label13)
      Me.uiSummaryTabConBends.Controls.Add(Me.uiConBendGeneralOrLocal)
      Me.uiSummaryTabConBends.Controls.Add(Me.Label14)
      Me.uiSummaryTabConBends.Controls.Add(Me.Label15)
      Me.uiSummaryTabConBends.Controls.Add(Me.Label16)
      Me.uiSummaryTabConBends.Name = "uiSummaryTabConBends"
      Me.uiSummaryTabConBends.Size = New System.Drawing.Size(589, 320)
      Me.uiSummaryTabConBends.Text = "Convective - Bends"
      '
      'btnConBendsUpdate
      '
      Me.btnConBendsUpdate.Anchor = System.Windows.Forms.AnchorStyles.Bottom
      Me.btnConBendsUpdate.Location = New System.Drawing.Point(163, 294)
      Me.btnConBendsUpdate.Name = "btnConBendsUpdate"
      Me.btnConBendsUpdate.Size = New System.Drawing.Size(241, 23)
      Me.btnConBendsUpdate.TabIndex = 999
      Me.btnConBendsUpdate.Text = "Update Convective Bend Summary"
      '
      'uiSummaryConBendTextbox
      '
      Me.uiSummaryConBendTextbox.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
      Me.uiSummaryConBendTextbox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
      Me.uiSummaryConBendTextbox.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.uiSummaryConBendTextbox.Appearance.Text.Options.UseFont = True
      Me.uiSummaryConBendTextbox.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
      Me.uiSummaryConBendTextbox.Location = New System.Drawing.Point(-1, 152)
      Me.uiSummaryConBendTextbox.Margin = New System.Windows.Forms.Padding(0)
      Me.uiSummaryConBendTextbox.Name = "uiSummaryConBendTextbox"
      Me.uiSummaryConBendTextbox.Size = New System.Drawing.Size(588, 139)
      Me.uiSummaryConBendTextbox.TabIndex = 74
      Me.uiSummaryConBendTextbox.TabStop = False
      '
      'Panel2
      '
      Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
      Me.Panel2.AutoScroll = True
      Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
      Me.Panel2.Controls.Add(Me.Label17)
      Me.Panel2.Controls.Add(Me.Label21)
      Me.Panel2.Controls.Add(Me.Label23)
      Me.Panel2.Controls.Add(Me.Label24)
      Me.Panel2.Controls.Add(Me.txtConBendID1)
      Me.Panel2.Controls.Add(Me.txtConBendMinWall0)
      Me.Panel2.Controls.Add(Me.txtConBendEstWallLoss0)
      Me.Panel2.Controls.Add(Me.txtConBendEstRemWall0)
      Me.Panel2.Controls.Add(Me.txtConBendEstRemWall7)
      Me.Panel2.Controls.Add(Me.txtConBendEstRemWall6)
      Me.Panel2.Controls.Add(Me.txtConBendEstRemWall5)
      Me.Panel2.Controls.Add(Me.txtConBendEstRemWall4)
      Me.Panel2.Controls.Add(Me.txtConBendEstRemWall3)
      Me.Panel2.Controls.Add(Me.txtConBendEstRemWall2)
      Me.Panel2.Controls.Add(Me.txtConBendEstRemWall1)
      Me.Panel2.Controls.Add(Me.txtConBendEstWallLoss7)
      Me.Panel2.Controls.Add(Me.txtConBendEstWallLoss6)
      Me.Panel2.Controls.Add(Me.txtConBendEstWallLoss5)
      Me.Panel2.Controls.Add(Me.txtConBendEstWallLoss4)
      Me.Panel2.Controls.Add(Me.txtConBendEstWallLoss3)
      Me.Panel2.Controls.Add(Me.txtConBendEstWallLoss2)
      Me.Panel2.Controls.Add(Me.txtConBendEstWallLoss1)
      Me.Panel2.Controls.Add(Me.txtConBendMinWall7)
      Me.Panel2.Controls.Add(Me.txtConBendMinWall6)
      Me.Panel2.Controls.Add(Me.txtConBendMinWall5)
      Me.Panel2.Controls.Add(Me.txtConBendMinWall4)
      Me.Panel2.Controls.Add(Me.txtConBendMinWall3)
      Me.Panel2.Controls.Add(Me.txtConBendMinWall2)
      Me.Panel2.Controls.Add(Me.txtConBendMinWall1)
      Me.Panel2.Controls.Add(Me.txtConBendID7)
      Me.Panel2.Controls.Add(Me.txtConBendID6)
      Me.Panel2.Controls.Add(Me.txtConBendID5)
      Me.Panel2.Controls.Add(Me.txtConBendID4)
      Me.Panel2.Controls.Add(Me.txtConBendID3)
      Me.Panel2.Controls.Add(Me.txtConBendID2)
      Me.Panel2.Controls.Add(Me.txtConBendID0)
      Me.Panel2.Location = New System.Drawing.Point(0, 54)
      Me.Panel2.Margin = New System.Windows.Forms.Padding(0)
      Me.Panel2.Name = "Panel2"
      Me.Panel2.Size = New System.Drawing.Size(587, 100)
      Me.Panel2.TabIndex = 73
      '
      'Label17
      '
      Me.Label17.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label17.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label17.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label17.Location = New System.Drawing.Point(306, 8)
      Me.Label17.Name = "Label17"
      Me.Label17.Size = New System.Drawing.Size(96, 29)
      Me.Label17.TabIndex = 142
      Me.Label17.Text = "Estimated Remaining Wall (%)"
      Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'Label21
      '
      Me.Label21.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label21.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label21.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label21.Location = New System.Drawing.Point(244, 8)
      Me.Label21.Name = "Label21"
      Me.Label21.Size = New System.Drawing.Size(68, 29)
      Me.Label21.TabIndex = 140
      Me.Label21.Text = "Estimated Wall Loss"
      Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'Label23
      '
      Me.Label23.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label23.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label23.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label23.Location = New System.Drawing.Point(182, 8)
      Me.Label23.Name = "Label23"
      Me.Label23.Size = New System.Drawing.Size(63, 29)
      Me.Label23.TabIndex = 138
      Me.Label23.Text = "Minimum Wall"
      Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'Label24
      '
      Me.Label24.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label24.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label24.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label24.Location = New System.Drawing.Point(132, 8)
      Me.Label24.Name = "Label24"
      Me.Label24.Size = New System.Drawing.Size(50, 29)
      Me.Label24.TabIndex = 137
      Me.Label24.Text = "Bend ID"
      Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'txtConBendID1
      '
      Me.txtConBendID1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendID1.Location = New System.Drawing.Point(132, 57)
      Me.txtConBendID1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendID1.Name = "txtConBendID1"
      Me.txtConBendID1.Size = New System.Drawing.Size(50, 20)
      Me.txtConBendID1.TabIndex = 5
      '
      'txtConBendMinWall0
      '
      Me.txtConBendMinWall0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendMinWall0.Location = New System.Drawing.Point(182, 37)
      Me.txtConBendMinWall0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendMinWall0.Name = "txtConBendMinWall0"
      Me.txtConBendMinWall0.Size = New System.Drawing.Size(63, 20)
      Me.txtConBendMinWall0.TabIndex = 2
      '
      'txtConBendEstWallLoss0
      '
      Me.txtConBendEstWallLoss0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstWallLoss0.Location = New System.Drawing.Point(245, 37)
      Me.txtConBendEstWallLoss0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstWallLoss0.Name = "txtConBendEstWallLoss0"
      Me.txtConBendEstWallLoss0.Size = New System.Drawing.Size(61, 20)
      Me.txtConBendEstWallLoss0.TabIndex = 3
      '
      'txtConBendEstRemWall0
      '
      Me.txtConBendEstRemWall0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstRemWall0.Location = New System.Drawing.Point(306, 37)
      Me.txtConBendEstRemWall0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstRemWall0.Name = "txtConBendEstRemWall0"
      Me.txtConBendEstRemWall0.Size = New System.Drawing.Size(96, 20)
      Me.txtConBendEstRemWall0.TabIndex = 4
      '
      'txtConBendEstRemWall7
      '
      Me.txtConBendEstRemWall7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstRemWall7.Location = New System.Drawing.Point(306, 177)
      Me.txtConBendEstRemWall7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstRemWall7.Name = "txtConBendEstRemWall7"
      Me.txtConBendEstRemWall7.Size = New System.Drawing.Size(96, 20)
      Me.txtConBendEstRemWall7.TabIndex = 32
      '
      'txtConBendEstRemWall6
      '
      Me.txtConBendEstRemWall6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstRemWall6.Location = New System.Drawing.Point(306, 157)
      Me.txtConBendEstRemWall6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstRemWall6.Name = "txtConBendEstRemWall6"
      Me.txtConBendEstRemWall6.Size = New System.Drawing.Size(96, 20)
      Me.txtConBendEstRemWall6.TabIndex = 28
      '
      'txtConBendEstRemWall5
      '
      Me.txtConBendEstRemWall5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstRemWall5.Location = New System.Drawing.Point(306, 137)
      Me.txtConBendEstRemWall5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstRemWall5.Name = "txtConBendEstRemWall5"
      Me.txtConBendEstRemWall5.Size = New System.Drawing.Size(96, 20)
      Me.txtConBendEstRemWall5.TabIndex = 24
      '
      'txtConBendEstRemWall4
      '
      Me.txtConBendEstRemWall4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstRemWall4.Location = New System.Drawing.Point(306, 117)
      Me.txtConBendEstRemWall4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstRemWall4.Name = "txtConBendEstRemWall4"
      Me.txtConBendEstRemWall4.Size = New System.Drawing.Size(96, 20)
      Me.txtConBendEstRemWall4.TabIndex = 20
      '
      'txtConBendEstRemWall3
      '
      Me.txtConBendEstRemWall3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstRemWall3.Location = New System.Drawing.Point(306, 97)
      Me.txtConBendEstRemWall3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstRemWall3.Name = "txtConBendEstRemWall3"
      Me.txtConBendEstRemWall3.Size = New System.Drawing.Size(96, 20)
      Me.txtConBendEstRemWall3.TabIndex = 16
      '
      'txtConBendEstRemWall2
      '
      Me.txtConBendEstRemWall2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstRemWall2.Location = New System.Drawing.Point(306, 77)
      Me.txtConBendEstRemWall2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstRemWall2.Name = "txtConBendEstRemWall2"
      Me.txtConBendEstRemWall2.Size = New System.Drawing.Size(96, 20)
      Me.txtConBendEstRemWall2.TabIndex = 12
      '
      'txtConBendEstRemWall1
      '
      Me.txtConBendEstRemWall1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstRemWall1.Location = New System.Drawing.Point(306, 57)
      Me.txtConBendEstRemWall1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstRemWall1.Name = "txtConBendEstRemWall1"
      Me.txtConBendEstRemWall1.Size = New System.Drawing.Size(96, 20)
      Me.txtConBendEstRemWall1.TabIndex = 8
      '
      'txtConBendEstWallLoss7
      '
      Me.txtConBendEstWallLoss7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstWallLoss7.Location = New System.Drawing.Point(245, 177)
      Me.txtConBendEstWallLoss7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstWallLoss7.Name = "txtConBendEstWallLoss7"
      Me.txtConBendEstWallLoss7.Size = New System.Drawing.Size(61, 20)
      Me.txtConBendEstWallLoss7.TabIndex = 31
      '
      'txtConBendEstWallLoss6
      '
      Me.txtConBendEstWallLoss6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstWallLoss6.Location = New System.Drawing.Point(245, 157)
      Me.txtConBendEstWallLoss6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstWallLoss6.Name = "txtConBendEstWallLoss6"
      Me.txtConBendEstWallLoss6.Size = New System.Drawing.Size(61, 20)
      Me.txtConBendEstWallLoss6.TabIndex = 27
      '
      'txtConBendEstWallLoss5
      '
      Me.txtConBendEstWallLoss5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstWallLoss5.Location = New System.Drawing.Point(245, 137)
      Me.txtConBendEstWallLoss5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstWallLoss5.Name = "txtConBendEstWallLoss5"
      Me.txtConBendEstWallLoss5.Size = New System.Drawing.Size(61, 20)
      Me.txtConBendEstWallLoss5.TabIndex = 23
      '
      'txtConBendEstWallLoss4
      '
      Me.txtConBendEstWallLoss4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstWallLoss4.Location = New System.Drawing.Point(245, 117)
      Me.txtConBendEstWallLoss4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstWallLoss4.Name = "txtConBendEstWallLoss4"
      Me.txtConBendEstWallLoss4.Size = New System.Drawing.Size(61, 20)
      Me.txtConBendEstWallLoss4.TabIndex = 19
      '
      'txtConBendEstWallLoss3
      '
      Me.txtConBendEstWallLoss3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstWallLoss3.Location = New System.Drawing.Point(245, 97)
      Me.txtConBendEstWallLoss3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstWallLoss3.Name = "txtConBendEstWallLoss3"
      Me.txtConBendEstWallLoss3.Size = New System.Drawing.Size(61, 20)
      Me.txtConBendEstWallLoss3.TabIndex = 15
      '
      'txtConBendEstWallLoss2
      '
      Me.txtConBendEstWallLoss2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstWallLoss2.Location = New System.Drawing.Point(245, 77)
      Me.txtConBendEstWallLoss2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstWallLoss2.Name = "txtConBendEstWallLoss2"
      Me.txtConBendEstWallLoss2.Size = New System.Drawing.Size(61, 20)
      Me.txtConBendEstWallLoss2.TabIndex = 11
      '
      'txtConBendEstWallLoss1
      '
      Me.txtConBendEstWallLoss1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendEstWallLoss1.Location = New System.Drawing.Point(245, 57)
      Me.txtConBendEstWallLoss1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendEstWallLoss1.Name = "txtConBendEstWallLoss1"
      Me.txtConBendEstWallLoss1.Size = New System.Drawing.Size(61, 20)
      Me.txtConBendEstWallLoss1.TabIndex = 7
      '
      'txtConBendMinWall7
      '
      Me.txtConBendMinWall7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendMinWall7.Location = New System.Drawing.Point(182, 177)
      Me.txtConBendMinWall7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendMinWall7.Name = "txtConBendMinWall7"
      Me.txtConBendMinWall7.Size = New System.Drawing.Size(63, 20)
      Me.txtConBendMinWall7.TabIndex = 30
      '
      'txtConBendMinWall6
      '
      Me.txtConBendMinWall6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendMinWall6.Location = New System.Drawing.Point(182, 157)
      Me.txtConBendMinWall6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendMinWall6.Name = "txtConBendMinWall6"
      Me.txtConBendMinWall6.Size = New System.Drawing.Size(63, 20)
      Me.txtConBendMinWall6.TabIndex = 26
      '
      'txtConBendMinWall5
      '
      Me.txtConBendMinWall5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendMinWall5.Location = New System.Drawing.Point(182, 137)
      Me.txtConBendMinWall5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendMinWall5.Name = "txtConBendMinWall5"
      Me.txtConBendMinWall5.Size = New System.Drawing.Size(63, 20)
      Me.txtConBendMinWall5.TabIndex = 22
      '
      'txtConBendMinWall4
      '
      Me.txtConBendMinWall4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendMinWall4.Location = New System.Drawing.Point(182, 117)
      Me.txtConBendMinWall4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendMinWall4.Name = "txtConBendMinWall4"
      Me.txtConBendMinWall4.Size = New System.Drawing.Size(63, 20)
      Me.txtConBendMinWall4.TabIndex = 18
      '
      'txtConBendMinWall3
      '
      Me.txtConBendMinWall3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendMinWall3.Location = New System.Drawing.Point(182, 97)
      Me.txtConBendMinWall3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendMinWall3.Name = "txtConBendMinWall3"
      Me.txtConBendMinWall3.Size = New System.Drawing.Size(63, 20)
      Me.txtConBendMinWall3.TabIndex = 14
      '
      'txtConBendMinWall2
      '
      Me.txtConBendMinWall2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendMinWall2.Location = New System.Drawing.Point(182, 77)
      Me.txtConBendMinWall2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendMinWall2.Name = "txtConBendMinWall2"
      Me.txtConBendMinWall2.Size = New System.Drawing.Size(63, 20)
      Me.txtConBendMinWall2.TabIndex = 10
      '
      'txtConBendMinWall1
      '
      Me.txtConBendMinWall1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendMinWall1.Location = New System.Drawing.Point(182, 57)
      Me.txtConBendMinWall1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendMinWall1.Name = "txtConBendMinWall1"
      Me.txtConBendMinWall1.Size = New System.Drawing.Size(63, 20)
      Me.txtConBendMinWall1.TabIndex = 6
      '
      'txtConBendID7
      '
      Me.txtConBendID7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendID7.Location = New System.Drawing.Point(132, 177)
      Me.txtConBendID7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendID7.Name = "txtConBendID7"
      Me.txtConBendID7.Size = New System.Drawing.Size(50, 20)
      Me.txtConBendID7.TabIndex = 29
      '
      'txtConBendID6
      '
      Me.txtConBendID6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendID6.Location = New System.Drawing.Point(132, 157)
      Me.txtConBendID6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendID6.Name = "txtConBendID6"
      Me.txtConBendID6.Size = New System.Drawing.Size(50, 20)
      Me.txtConBendID6.TabIndex = 25
      '
      'txtConBendID5
      '
      Me.txtConBendID5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendID5.Location = New System.Drawing.Point(132, 137)
      Me.txtConBendID5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendID5.Name = "txtConBendID5"
      Me.txtConBendID5.Size = New System.Drawing.Size(50, 20)
      Me.txtConBendID5.TabIndex = 21
      '
      'txtConBendID4
      '
      Me.txtConBendID4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendID4.Location = New System.Drawing.Point(132, 117)
      Me.txtConBendID4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendID4.Name = "txtConBendID4"
      Me.txtConBendID4.Size = New System.Drawing.Size(50, 20)
      Me.txtConBendID4.TabIndex = 17
      '
      'txtConBendID3
      '
      Me.txtConBendID3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendID3.Location = New System.Drawing.Point(132, 97)
      Me.txtConBendID3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendID3.Name = "txtConBendID3"
      Me.txtConBendID3.Size = New System.Drawing.Size(50, 20)
      Me.txtConBendID3.TabIndex = 13
      '
      'txtConBendID2
      '
      Me.txtConBendID2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendID2.Location = New System.Drawing.Point(132, 77)
      Me.txtConBendID2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendID2.Name = "txtConBendID2"
      Me.txtConBendID2.Size = New System.Drawing.Size(50, 20)
      Me.txtConBendID2.TabIndex = 9
      '
      'txtConBendID0
      '
      Me.txtConBendID0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtConBendID0.Location = New System.Drawing.Point(132, 37)
      Me.txtConBendID0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtConBendID0.Name = "txtConBendID0"
      Me.txtConBendID0.Size = New System.Drawing.Size(50, 20)
      Me.txtConBendID0.TabIndex = 1
      '
      'uiConBendScheduleSize
      '
      Me.uiConBendScheduleSize.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiConBendScheduleSize.Location = New System.Drawing.Point(387, 27)
      Me.uiConBendScheduleSize.Name = "uiConBendScheduleSize"
      Me.uiConBendScheduleSize.Properties.AutoHeight = False
      Me.uiConBendScheduleSize.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiConBendScheduleSize.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiConBendScheduleSize.Properties.Items.AddRange(New Object() {"Tube", "5", "10", "20", "30", "40", "50", "60", "80", "120", "140", "160", "XXS"})
      Me.uiConBendScheduleSize.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiConBendScheduleSize.Size = New System.Drawing.Size(92, 24)
      Me.uiConBendScheduleSize.TabIndex = 4
      '
      'uiConBendBendSize
      '
      Me.uiConBendBendSize.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiConBendBendSize.Location = New System.Drawing.Point(318, 27)
      Me.uiConBendBendSize.Name = "uiConBendBendSize"
      Me.uiConBendBendSize.Properties.AutoHeight = False
      Me.uiConBendBendSize.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiConBendBendSize.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiConBendBendSize.Properties.Items.AddRange(New Object() {"Other", "2", "2.5", "3", "3.5", "4", "5", "6", "8", "10", "12"})
      Me.uiConBendBendSize.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiConBendBendSize.Size = New System.Drawing.Size(63, 24)
      Me.uiConBendBendSize.TabIndex = 3
      '
      'uiConBendInternalOrExternal
      '
      Me.uiConBendInternalOrExternal.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiConBendInternalOrExternal.Location = New System.Drawing.Point(188, 27)
      Me.uiConBendInternalOrExternal.Name = "uiConBendInternalOrExternal"
      Me.uiConBendInternalOrExternal.Properties.AutoHeight = False
      Me.uiConBendInternalOrExternal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiConBendInternalOrExternal.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiConBendInternalOrExternal.Properties.Items.AddRange(New Object() {"internal", "external", "internal and external"})
      Me.uiConBendInternalOrExternal.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiConBendInternalOrExternal.Size = New System.Drawing.Size(123, 24)
      Me.uiConBendInternalOrExternal.TabIndex = 2
      '
      'Label13
      '
      Me.Label13.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label13.AutoSize = True
      Me.Label13.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label13.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label13.Location = New System.Drawing.Point(78, 8)
      Me.Label13.Name = "Label13"
      Me.Label13.Size = New System.Drawing.Size(104, 17)
      Me.Label13.TabIndex = 65
      Me.Label13.Text = "General or Local"
      Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'uiConBendGeneralOrLocal
      '
      Me.uiConBendGeneralOrLocal.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiConBendGeneralOrLocal.Location = New System.Drawing.Point(79, 27)
      Me.uiConBendGeneralOrLocal.Name = "uiConBendGeneralOrLocal"
      Me.uiConBendGeneralOrLocal.Properties.AutoHeight = False
      Me.uiConBendGeneralOrLocal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiConBendGeneralOrLocal.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiConBendGeneralOrLocal.Properties.Items.AddRange(New Object() {"General", "Local", "General and local"})
      Me.uiConBendGeneralOrLocal.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiConBendGeneralOrLocal.Size = New System.Drawing.Size(103, 24)
      Me.uiConBendGeneralOrLocal.TabIndex = 1
      '
      'Label14
      '
      Me.Label14.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label14.AutoSize = True
      Me.Label14.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label14.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label14.Location = New System.Drawing.Point(191, 8)
      Me.Label14.Name = "Label14"
      Me.Label14.Size = New System.Drawing.Size(118, 17)
      Me.Label14.TabIndex = 0
      Me.Label14.Text = "Internal or External"
      Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label15
      '
      Me.Label15.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label15.AutoSize = True
      Me.Label15.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label15.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label15.Location = New System.Drawing.Point(318, 8)
      Me.Label15.Name = "Label15"
      Me.Label15.Size = New System.Drawing.Size(60, 17)
      Me.Label15.TabIndex = 67
      Me.Label15.Text = "Pipe Size"
      Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label16
      '
      Me.Label16.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label16.AutoSize = True
      Me.Label16.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label16.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label16.Location = New System.Drawing.Point(387, 8)
      Me.Label16.Name = "Label16"
      Me.Label16.Size = New System.Drawing.Size(89, 17)
      Me.Label16.TabIndex = 68
      Me.Label16.Text = "Pipe Schedule"
      Me.Label16.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'uiSummaryTabRadPipes
      '
      Me.uiSummaryTabRadPipes.Controls.Add(Me.uiRadPipeScheduleSize)
      Me.uiSummaryTabRadPipes.Controls.Add(Me.uiRadPipePipeSize)
      Me.uiSummaryTabRadPipes.Controls.Add(Me.uiRadPipeInternalOrExternal)
      Me.uiSummaryTabRadPipes.Controls.Add(Me.Label1)
      Me.uiSummaryTabRadPipes.Controls.Add(Me.uiRadPipeGeneralOrLocal)
      Me.uiSummaryTabRadPipes.Controls.Add(Me.Label2)
      Me.uiSummaryTabRadPipes.Controls.Add(Me.Label3)
      Me.uiSummaryTabRadPipes.Controls.Add(Me.Label4)
      Me.uiSummaryTabRadPipes.Controls.Add(Me.btnRadPipesUpdate)
      Me.uiSummaryTabRadPipes.Controls.Add(Me.Panel1)
      Me.uiSummaryTabRadPipes.Controls.Add(Me.uiSummaryRadPipeTextbox)
      Me.uiSummaryTabRadPipes.Name = "uiSummaryTabRadPipes"
      Me.uiSummaryTabRadPipes.Size = New System.Drawing.Size(589, 320)
      Me.uiSummaryTabRadPipes.Text = "Radiant - Pipes"
      '
      'uiRadPipeScheduleSize
      '
      Me.uiRadPipeScheduleSize.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiRadPipeScheduleSize.Location = New System.Drawing.Point(387, 27)
      Me.uiRadPipeScheduleSize.Name = "uiRadPipeScheduleSize"
      Me.uiRadPipeScheduleSize.Properties.AutoHeight = False
      Me.uiRadPipeScheduleSize.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiRadPipeScheduleSize.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiRadPipeScheduleSize.Properties.Items.AddRange(New Object() {"Tube", "5", "10", "20", "30", "40", "50", "60", "80", "120", "140", "160", "XXS"})
      Me.uiRadPipeScheduleSize.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiRadPipeScheduleSize.Size = New System.Drawing.Size(92, 24)
      Me.uiRadPipeScheduleSize.TabIndex = 4
      '
      'uiRadPipePipeSize
      '
      Me.uiRadPipePipeSize.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiRadPipePipeSize.Location = New System.Drawing.Point(318, 27)
      Me.uiRadPipePipeSize.Name = "uiRadPipePipeSize"
      Me.uiRadPipePipeSize.Properties.AutoHeight = False
      Me.uiRadPipePipeSize.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiRadPipePipeSize.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiRadPipePipeSize.Properties.Items.AddRange(New Object() {"Other", "2", "2.5", "3", "3.5", "4", "5", "6", "8", "10", "12"})
      Me.uiRadPipePipeSize.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiRadPipePipeSize.Size = New System.Drawing.Size(63, 24)
      Me.uiRadPipePipeSize.TabIndex = 3
      '
      'uiRadPipeInternalOrExternal
      '
      Me.uiRadPipeInternalOrExternal.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiRadPipeInternalOrExternal.Location = New System.Drawing.Point(188, 27)
      Me.uiRadPipeInternalOrExternal.Name = "uiRadPipeInternalOrExternal"
      Me.uiRadPipeInternalOrExternal.Properties.AutoHeight = False
      Me.uiRadPipeInternalOrExternal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiRadPipeInternalOrExternal.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiRadPipeInternalOrExternal.Properties.Items.AddRange(New Object() {"internal", "external", "internal and external"})
      Me.uiRadPipeInternalOrExternal.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiRadPipeInternalOrExternal.Size = New System.Drawing.Size(123, 24)
      Me.uiRadPipeInternalOrExternal.TabIndex = 2
      '
      'Label1
      '
      Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label1.AutoSize = True
      Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label1.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label1.Location = New System.Drawing.Point(78, 8)
      Me.Label1.Name = "Label1"
      Me.Label1.Size = New System.Drawing.Size(104, 17)
      Me.Label1.TabIndex = 79
      Me.Label1.Text = "General or Local"
      Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'uiRadPipeGeneralOrLocal
      '
      Me.uiRadPipeGeneralOrLocal.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiRadPipeGeneralOrLocal.Location = New System.Drawing.Point(79, 27)
      Me.uiRadPipeGeneralOrLocal.Name = "uiRadPipeGeneralOrLocal"
      Me.uiRadPipeGeneralOrLocal.Properties.AutoHeight = False
      Me.uiRadPipeGeneralOrLocal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiRadPipeGeneralOrLocal.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiRadPipeGeneralOrLocal.Properties.Items.AddRange(New Object() {"General", "Local", "General and local"})
      Me.uiRadPipeGeneralOrLocal.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiRadPipeGeneralOrLocal.Size = New System.Drawing.Size(103, 24)
      Me.uiRadPipeGeneralOrLocal.TabIndex = 1
      '
      'Label2
      '
      Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label2.AutoSize = True
      Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label2.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label2.Location = New System.Drawing.Point(191, 8)
      Me.Label2.Name = "Label2"
      Me.Label2.Size = New System.Drawing.Size(118, 17)
      Me.Label2.TabIndex = 80
      Me.Label2.Text = "Internal or External"
      Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label3
      '
      Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label3.AutoSize = True
      Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label3.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label3.Location = New System.Drawing.Point(318, 8)
      Me.Label3.Name = "Label3"
      Me.Label3.Size = New System.Drawing.Size(60, 17)
      Me.Label3.TabIndex = 81
      Me.Label3.Text = "Pipe Size"
      Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label4
      '
      Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label4.AutoSize = True
      Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label4.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label4.Location = New System.Drawing.Point(387, 8)
      Me.Label4.Name = "Label4"
      Me.Label4.Size = New System.Drawing.Size(89, 17)
      Me.Label4.TabIndex = 82
      Me.Label4.Text = "Pipe Schedule"
      Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'btnRadPipesUpdate
      '
      Me.btnRadPipesUpdate.Anchor = System.Windows.Forms.AnchorStyles.Bottom
      Me.btnRadPipesUpdate.Location = New System.Drawing.Point(165, 294)
      Me.btnRadPipesUpdate.Name = "btnRadPipesUpdate"
      Me.btnRadPipesUpdate.Size = New System.Drawing.Size(241, 23)
      Me.btnRadPipesUpdate.TabIndex = 78
      Me.btnRadPipesUpdate.Text = "Update Radiant Pipe Summary"
      '
      'Panel1
      '
      Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
      Me.Panel1.AutoScroll = True
      Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
      Me.Panel1.Controls.Add(Me.Label5)
      Me.Panel1.Controls.Add(Me.Label6)
      Me.Panel1.Controls.Add(Me.Label7)
      Me.Panel1.Controls.Add(Me.Label8)
      Me.Panel1.Controls.Add(Me.Label9)
      Me.Panel1.Controls.Add(Me.Label10)
      Me.Panel1.Controls.Add(Me.Label11)
      Me.Panel1.Controls.Add(Me.Label12)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgDiameter0)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgDiameter7)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgDiameter6)
      Me.Panel1.Controls.Add(Me.txtRadPipeID1)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgDiameter5)
      Me.Panel1.Controls.Add(Me.txtRadPipeMinWall0)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgDiameter4)
      Me.Panel1.Controls.Add(Me.txtRadPipeLocation0)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgDiameter3)
      Me.Panel1.Controls.Add(Me.txtRadPipeDesWall0)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgDiameter2)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstWallLoss0)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgDiameter1)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgWall0)
      Me.Panel1.Controls.Add(Me.txtRadPipeDesWall7)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstRemWall0)
      Me.Panel1.Controls.Add(Me.txtRadPipeDesWall6)
      Me.Panel1.Controls.Add(Me.txtRadPipeDesWall5)
      Me.Panel1.Controls.Add(Me.txtRadPipeDesWall4)
      Me.Panel1.Controls.Add(Me.txtRadPipeDesWall3)
      Me.Panel1.Controls.Add(Me.txtRadPipeDesWall2)
      Me.Panel1.Controls.Add(Me.txtRadPipeDesWall1)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgWall7)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgWall6)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgWall5)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgWall4)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgWall3)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgWall2)
      Me.Panel1.Controls.Add(Me.txtRadPipeAvgWall1)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstRemWall7)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstRemWall6)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstRemWall5)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstRemWall4)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstRemWall3)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstRemWall2)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstRemWall1)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstWallLoss7)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstWallLoss6)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstWallLoss5)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstWallLoss4)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstWallLoss3)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstWallLoss2)
      Me.Panel1.Controls.Add(Me.txtRadPipeEstWallLoss1)
      Me.Panel1.Controls.Add(Me.txtRadPipeLocation7)
      Me.Panel1.Controls.Add(Me.txtRadPipeLocation6)
      Me.Panel1.Controls.Add(Me.txtRadPipeLocation5)
      Me.Panel1.Controls.Add(Me.txtRadPipeLocation4)
      Me.Panel1.Controls.Add(Me.txtRadPipeLocation3)
      Me.Panel1.Controls.Add(Me.txtRadPipeLocation2)
      Me.Panel1.Controls.Add(Me.txtRadPipeLocation1)
      Me.Panel1.Controls.Add(Me.txtRadPipeMinWall7)
      Me.Panel1.Controls.Add(Me.txtRadPipeMinWall6)
      Me.Panel1.Controls.Add(Me.txtRadPipeMinWall5)
      Me.Panel1.Controls.Add(Me.txtRadPipeMinWall4)
      Me.Panel1.Controls.Add(Me.txtRadPipeMinWall3)
      Me.Panel1.Controls.Add(Me.txtRadPipeMinWall2)
      Me.Panel1.Controls.Add(Me.txtRadPipeMinWall1)
      Me.Panel1.Controls.Add(Me.txtRadPipeID7)
      Me.Panel1.Controls.Add(Me.txtRadPipeID6)
      Me.Panel1.Controls.Add(Me.txtRadPipeID5)
      Me.Panel1.Controls.Add(Me.txtRadPipeID4)
      Me.Panel1.Controls.Add(Me.txtRadPipeID3)
      Me.Panel1.Controls.Add(Me.txtRadPipeID2)
      Me.Panel1.Controls.Add(Me.txtRadPipeID0)
      Me.Panel1.Location = New System.Drawing.Point(-1, 53)
      Me.Panel1.Margin = New System.Windows.Forms.Padding(0)
      Me.Panel1.Name = "Panel1"
      Me.Panel1.Size = New System.Drawing.Size(587, 100)
      Me.Panel1.TabIndex = 77
      '
      'Label5
      '
      Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label5.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label5.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label5.Location = New System.Drawing.Point(480, 9)
      Me.Label5.Name = "Label5"
      Me.Label5.Size = New System.Drawing.Size(65, 29)
      Me.Label5.TabIndex = 144
      Me.Label5.Text = "Average Diameter"
      Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'Label6
      '
      Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label6.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label6.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label6.Location = New System.Drawing.Point(409, 9)
      Me.Label6.Name = "Label6"
      Me.Label6.Size = New System.Drawing.Size(71, 29)
      Me.Label6.TabIndex = 143
      Me.Label6.Text = "Designed Wall"
      Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'Label7
      '
      Me.Label7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label7.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label7.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label7.Location = New System.Drawing.Point(346, 9)
      Me.Label7.Name = "Label7"
      Me.Label7.Size = New System.Drawing.Size(63, 29)
      Me.Label7.TabIndex = 142
      Me.Label7.Text = "Average Wall"
      Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'Label8
      '
      Me.Label8.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label8.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label8.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label8.Location = New System.Drawing.Point(250, 9)
      Me.Label8.Name = "Label8"
      Me.Label8.Size = New System.Drawing.Size(96, 29)
      Me.Label8.TabIndex = 141
      Me.Label8.Text = "Estimated Remaining Wall (%)"
      Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'Label9
      '
      Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label9.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label9.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label9.Location = New System.Drawing.Point(182, 9)
      Me.Label9.Name = "Label9"
      Me.Label9.Size = New System.Drawing.Size(68, 29)
      Me.Label9.TabIndex = 140
      Me.Label9.Text = "Estimated Wall Loss"
      Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'Label10
      '
      Me.Label10.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label10.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label10.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label10.Location = New System.Drawing.Point(121, 9)
      Me.Label10.Name = "Label10"
      Me.Label10.Size = New System.Drawing.Size(61, 29)
      Me.Label10.TabIndex = 139
      Me.Label10.Text = "Location"
      Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'Label11
      '
      Me.Label11.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label11.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label11.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label11.Location = New System.Drawing.Point(58, 9)
      Me.Label11.Name = "Label11"
      Me.Label11.Size = New System.Drawing.Size(63, 29)
      Me.Label11.TabIndex = 138
      Me.Label11.Text = "Minimum Wall"
      Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'Label12
      '
      Me.Label12.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label12.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label12.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label12.Location = New System.Drawing.Point(8, 9)
      Me.Label12.Name = "Label12"
      Me.Label12.Size = New System.Drawing.Size(50, 29)
      Me.Label12.TabIndex = 137
      Me.Label12.Text = "Pipe ID"
      Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'txtRadPipeAvgDiameter0
      '
      Me.txtRadPipeAvgDiameter0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgDiameter0.Location = New System.Drawing.Point(480, 38)
      Me.txtRadPipeAvgDiameter0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgDiameter0.Name = "txtRadPipeAvgDiameter0"
      Me.txtRadPipeAvgDiameter0.Size = New System.Drawing.Size(65, 20)
      Me.txtRadPipeAvgDiameter0.TabIndex = 8
      '
      'txtRadPipeAvgDiameter7
      '
      Me.txtRadPipeAvgDiameter7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgDiameter7.Location = New System.Drawing.Point(480, 178)
      Me.txtRadPipeAvgDiameter7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgDiameter7.Name = "txtRadPipeAvgDiameter7"
      Me.txtRadPipeAvgDiameter7.Size = New System.Drawing.Size(65, 20)
      Me.txtRadPipeAvgDiameter7.TabIndex = 64
      '
      'txtRadPipeAvgDiameter6
      '
      Me.txtRadPipeAvgDiameter6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgDiameter6.Location = New System.Drawing.Point(480, 158)
      Me.txtRadPipeAvgDiameter6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgDiameter6.Name = "txtRadPipeAvgDiameter6"
      Me.txtRadPipeAvgDiameter6.Size = New System.Drawing.Size(65, 20)
      Me.txtRadPipeAvgDiameter6.TabIndex = 56
      '
      'txtRadPipeID1
      '
      Me.txtRadPipeID1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeID1.Location = New System.Drawing.Point(8, 58)
      Me.txtRadPipeID1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeID1.Name = "txtRadPipeID1"
      Me.txtRadPipeID1.Size = New System.Drawing.Size(50, 20)
      Me.txtRadPipeID1.TabIndex = 9
      '
      'txtRadPipeAvgDiameter5
      '
      Me.txtRadPipeAvgDiameter5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgDiameter5.Location = New System.Drawing.Point(480, 138)
      Me.txtRadPipeAvgDiameter5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgDiameter5.Name = "txtRadPipeAvgDiameter5"
      Me.txtRadPipeAvgDiameter5.Size = New System.Drawing.Size(65, 20)
      Me.txtRadPipeAvgDiameter5.TabIndex = 48
      '
      'txtRadPipeMinWall0
      '
      Me.txtRadPipeMinWall0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeMinWall0.Location = New System.Drawing.Point(58, 38)
      Me.txtRadPipeMinWall0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeMinWall0.Name = "txtRadPipeMinWall0"
      Me.txtRadPipeMinWall0.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeMinWall0.TabIndex = 2
      '
      'txtRadPipeAvgDiameter4
      '
      Me.txtRadPipeAvgDiameter4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgDiameter4.Location = New System.Drawing.Point(480, 118)
      Me.txtRadPipeAvgDiameter4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgDiameter4.Name = "txtRadPipeAvgDiameter4"
      Me.txtRadPipeAvgDiameter4.Size = New System.Drawing.Size(65, 20)
      Me.txtRadPipeAvgDiameter4.TabIndex = 40
      '
      'txtRadPipeLocation0
      '
      Me.txtRadPipeLocation0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeLocation0.Location = New System.Drawing.Point(121, 38)
      Me.txtRadPipeLocation0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeLocation0.Name = "txtRadPipeLocation0"
      Me.txtRadPipeLocation0.Size = New System.Drawing.Size(61, 20)
      Me.txtRadPipeLocation0.TabIndex = 3
      '
      'txtRadPipeAvgDiameter3
      '
      Me.txtRadPipeAvgDiameter3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgDiameter3.Location = New System.Drawing.Point(480, 98)
      Me.txtRadPipeAvgDiameter3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgDiameter3.Name = "txtRadPipeAvgDiameter3"
      Me.txtRadPipeAvgDiameter3.Size = New System.Drawing.Size(65, 20)
      Me.txtRadPipeAvgDiameter3.TabIndex = 32
      '
      'txtRadPipeDesWall0
      '
      Me.txtRadPipeDesWall0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeDesWall0.Location = New System.Drawing.Point(409, 38)
      Me.txtRadPipeDesWall0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeDesWall0.Name = "txtRadPipeDesWall0"
      Me.txtRadPipeDesWall0.Size = New System.Drawing.Size(71, 20)
      Me.txtRadPipeDesWall0.TabIndex = 7
      '
      'txtRadPipeAvgDiameter2
      '
      Me.txtRadPipeAvgDiameter2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgDiameter2.Location = New System.Drawing.Point(480, 78)
      Me.txtRadPipeAvgDiameter2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgDiameter2.Name = "txtRadPipeAvgDiameter2"
      Me.txtRadPipeAvgDiameter2.Size = New System.Drawing.Size(65, 20)
      Me.txtRadPipeAvgDiameter2.TabIndex = 24
      '
      'txtRadPipeEstWallLoss0
      '
      Me.txtRadPipeEstWallLoss0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstWallLoss0.Location = New System.Drawing.Point(182, 38)
      Me.txtRadPipeEstWallLoss0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstWallLoss0.Name = "txtRadPipeEstWallLoss0"
      Me.txtRadPipeEstWallLoss0.Size = New System.Drawing.Size(68, 20)
      Me.txtRadPipeEstWallLoss0.TabIndex = 4
      '
      'txtRadPipeAvgDiameter1
      '
      Me.txtRadPipeAvgDiameter1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgDiameter1.Location = New System.Drawing.Point(480, 58)
      Me.txtRadPipeAvgDiameter1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgDiameter1.Name = "txtRadPipeAvgDiameter1"
      Me.txtRadPipeAvgDiameter1.Size = New System.Drawing.Size(65, 20)
      Me.txtRadPipeAvgDiameter1.TabIndex = 16
      '
      'txtRadPipeAvgWall0
      '
      Me.txtRadPipeAvgWall0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgWall0.Location = New System.Drawing.Point(346, 38)
      Me.txtRadPipeAvgWall0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgWall0.Name = "txtRadPipeAvgWall0"
      Me.txtRadPipeAvgWall0.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeAvgWall0.TabIndex = 6
      '
      'txtRadPipeDesWall7
      '
      Me.txtRadPipeDesWall7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeDesWall7.Location = New System.Drawing.Point(409, 178)
      Me.txtRadPipeDesWall7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeDesWall7.Name = "txtRadPipeDesWall7"
      Me.txtRadPipeDesWall7.Size = New System.Drawing.Size(71, 20)
      Me.txtRadPipeDesWall7.TabIndex = 63
      '
      'txtRadPipeEstRemWall0
      '
      Me.txtRadPipeEstRemWall0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstRemWall0.Location = New System.Drawing.Point(250, 38)
      Me.txtRadPipeEstRemWall0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstRemWall0.Name = "txtRadPipeEstRemWall0"
      Me.txtRadPipeEstRemWall0.Size = New System.Drawing.Size(96, 20)
      Me.txtRadPipeEstRemWall0.TabIndex = 5
      '
      'txtRadPipeDesWall6
      '
      Me.txtRadPipeDesWall6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeDesWall6.Location = New System.Drawing.Point(409, 158)
      Me.txtRadPipeDesWall6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeDesWall6.Name = "txtRadPipeDesWall6"
      Me.txtRadPipeDesWall6.Size = New System.Drawing.Size(71, 20)
      Me.txtRadPipeDesWall6.TabIndex = 55
      '
      'txtRadPipeDesWall5
      '
      Me.txtRadPipeDesWall5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeDesWall5.Location = New System.Drawing.Point(409, 138)
      Me.txtRadPipeDesWall5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeDesWall5.Name = "txtRadPipeDesWall5"
      Me.txtRadPipeDesWall5.Size = New System.Drawing.Size(71, 20)
      Me.txtRadPipeDesWall5.TabIndex = 47
      '
      'txtRadPipeDesWall4
      '
      Me.txtRadPipeDesWall4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeDesWall4.Location = New System.Drawing.Point(409, 118)
      Me.txtRadPipeDesWall4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeDesWall4.Name = "txtRadPipeDesWall4"
      Me.txtRadPipeDesWall4.Size = New System.Drawing.Size(71, 20)
      Me.txtRadPipeDesWall4.TabIndex = 39
      '
      'txtRadPipeDesWall3
      '
      Me.txtRadPipeDesWall3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeDesWall3.Location = New System.Drawing.Point(409, 98)
      Me.txtRadPipeDesWall3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeDesWall3.Name = "txtRadPipeDesWall3"
      Me.txtRadPipeDesWall3.Size = New System.Drawing.Size(71, 20)
      Me.txtRadPipeDesWall3.TabIndex = 31
      '
      'txtRadPipeDesWall2
      '
      Me.txtRadPipeDesWall2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeDesWall2.Location = New System.Drawing.Point(409, 78)
      Me.txtRadPipeDesWall2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeDesWall2.Name = "txtRadPipeDesWall2"
      Me.txtRadPipeDesWall2.Size = New System.Drawing.Size(71, 20)
      Me.txtRadPipeDesWall2.TabIndex = 23
      '
      'txtRadPipeDesWall1
      '
      Me.txtRadPipeDesWall1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeDesWall1.Location = New System.Drawing.Point(409, 58)
      Me.txtRadPipeDesWall1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeDesWall1.Name = "txtRadPipeDesWall1"
      Me.txtRadPipeDesWall1.Size = New System.Drawing.Size(71, 20)
      Me.txtRadPipeDesWall1.TabIndex = 15
      '
      'txtRadPipeAvgWall7
      '
      Me.txtRadPipeAvgWall7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgWall7.Location = New System.Drawing.Point(346, 178)
      Me.txtRadPipeAvgWall7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgWall7.Name = "txtRadPipeAvgWall7"
      Me.txtRadPipeAvgWall7.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeAvgWall7.TabIndex = 62
      '
      'txtRadPipeAvgWall6
      '
      Me.txtRadPipeAvgWall6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgWall6.Location = New System.Drawing.Point(346, 158)
      Me.txtRadPipeAvgWall6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgWall6.Name = "txtRadPipeAvgWall6"
      Me.txtRadPipeAvgWall6.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeAvgWall6.TabIndex = 54
      '
      'txtRadPipeAvgWall5
      '
      Me.txtRadPipeAvgWall5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgWall5.Location = New System.Drawing.Point(346, 138)
      Me.txtRadPipeAvgWall5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgWall5.Name = "txtRadPipeAvgWall5"
      Me.txtRadPipeAvgWall5.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeAvgWall5.TabIndex = 46
      '
      'txtRadPipeAvgWall4
      '
      Me.txtRadPipeAvgWall4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgWall4.Location = New System.Drawing.Point(346, 118)
      Me.txtRadPipeAvgWall4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgWall4.Name = "txtRadPipeAvgWall4"
      Me.txtRadPipeAvgWall4.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeAvgWall4.TabIndex = 38
      '
      'txtRadPipeAvgWall3
      '
      Me.txtRadPipeAvgWall3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgWall3.Location = New System.Drawing.Point(346, 98)
      Me.txtRadPipeAvgWall3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgWall3.Name = "txtRadPipeAvgWall3"
      Me.txtRadPipeAvgWall3.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeAvgWall3.TabIndex = 30
      '
      'txtRadPipeAvgWall2
      '
      Me.txtRadPipeAvgWall2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgWall2.Location = New System.Drawing.Point(346, 78)
      Me.txtRadPipeAvgWall2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgWall2.Name = "txtRadPipeAvgWall2"
      Me.txtRadPipeAvgWall2.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeAvgWall2.TabIndex = 22
      '
      'txtRadPipeAvgWall1
      '
      Me.txtRadPipeAvgWall1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeAvgWall1.Location = New System.Drawing.Point(346, 58)
      Me.txtRadPipeAvgWall1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeAvgWall1.Name = "txtRadPipeAvgWall1"
      Me.txtRadPipeAvgWall1.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeAvgWall1.TabIndex = 14
      '
      'txtRadPipeEstRemWall7
      '
      Me.txtRadPipeEstRemWall7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstRemWall7.Location = New System.Drawing.Point(250, 178)
      Me.txtRadPipeEstRemWall7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstRemWall7.Name = "txtRadPipeEstRemWall7"
      Me.txtRadPipeEstRemWall7.Size = New System.Drawing.Size(96, 20)
      Me.txtRadPipeEstRemWall7.TabIndex = 61
      '
      'txtRadPipeEstRemWall6
      '
      Me.txtRadPipeEstRemWall6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstRemWall6.Location = New System.Drawing.Point(250, 158)
      Me.txtRadPipeEstRemWall6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstRemWall6.Name = "txtRadPipeEstRemWall6"
      Me.txtRadPipeEstRemWall6.Size = New System.Drawing.Size(96, 20)
      Me.txtRadPipeEstRemWall6.TabIndex = 53
      '
      'txtRadPipeEstRemWall5
      '
      Me.txtRadPipeEstRemWall5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstRemWall5.Location = New System.Drawing.Point(250, 138)
      Me.txtRadPipeEstRemWall5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstRemWall5.Name = "txtRadPipeEstRemWall5"
      Me.txtRadPipeEstRemWall5.Size = New System.Drawing.Size(96, 20)
      Me.txtRadPipeEstRemWall5.TabIndex = 45
      '
      'txtRadPipeEstRemWall4
      '
      Me.txtRadPipeEstRemWall4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstRemWall4.Location = New System.Drawing.Point(250, 118)
      Me.txtRadPipeEstRemWall4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstRemWall4.Name = "txtRadPipeEstRemWall4"
      Me.txtRadPipeEstRemWall4.Size = New System.Drawing.Size(96, 20)
      Me.txtRadPipeEstRemWall4.TabIndex = 37
      '
      'txtRadPipeEstRemWall3
      '
      Me.txtRadPipeEstRemWall3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstRemWall3.Location = New System.Drawing.Point(250, 98)
      Me.txtRadPipeEstRemWall3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstRemWall3.Name = "txtRadPipeEstRemWall3"
      Me.txtRadPipeEstRemWall3.Size = New System.Drawing.Size(96, 20)
      Me.txtRadPipeEstRemWall3.TabIndex = 29
      '
      'txtRadPipeEstRemWall2
      '
      Me.txtRadPipeEstRemWall2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstRemWall2.Location = New System.Drawing.Point(250, 78)
      Me.txtRadPipeEstRemWall2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstRemWall2.Name = "txtRadPipeEstRemWall2"
      Me.txtRadPipeEstRemWall2.Size = New System.Drawing.Size(96, 20)
      Me.txtRadPipeEstRemWall2.TabIndex = 21
      '
      'txtRadPipeEstRemWall1
      '
      Me.txtRadPipeEstRemWall1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstRemWall1.Location = New System.Drawing.Point(250, 58)
      Me.txtRadPipeEstRemWall1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstRemWall1.Name = "txtRadPipeEstRemWall1"
      Me.txtRadPipeEstRemWall1.Size = New System.Drawing.Size(96, 20)
      Me.txtRadPipeEstRemWall1.TabIndex = 13
      '
      'txtRadPipeEstWallLoss7
      '
      Me.txtRadPipeEstWallLoss7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstWallLoss7.Location = New System.Drawing.Point(182, 178)
      Me.txtRadPipeEstWallLoss7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstWallLoss7.Name = "txtRadPipeEstWallLoss7"
      Me.txtRadPipeEstWallLoss7.Size = New System.Drawing.Size(68, 20)
      Me.txtRadPipeEstWallLoss7.TabIndex = 60
      '
      'txtRadPipeEstWallLoss6
      '
      Me.txtRadPipeEstWallLoss6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstWallLoss6.Location = New System.Drawing.Point(182, 158)
      Me.txtRadPipeEstWallLoss6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstWallLoss6.Name = "txtRadPipeEstWallLoss6"
      Me.txtRadPipeEstWallLoss6.Size = New System.Drawing.Size(68, 20)
      Me.txtRadPipeEstWallLoss6.TabIndex = 52
      '
      'txtRadPipeEstWallLoss5
      '
      Me.txtRadPipeEstWallLoss5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstWallLoss5.Location = New System.Drawing.Point(182, 138)
      Me.txtRadPipeEstWallLoss5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstWallLoss5.Name = "txtRadPipeEstWallLoss5"
      Me.txtRadPipeEstWallLoss5.Size = New System.Drawing.Size(68, 20)
      Me.txtRadPipeEstWallLoss5.TabIndex = 44
      '
      'txtRadPipeEstWallLoss4
      '
      Me.txtRadPipeEstWallLoss4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstWallLoss4.Location = New System.Drawing.Point(182, 118)
      Me.txtRadPipeEstWallLoss4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstWallLoss4.Name = "txtRadPipeEstWallLoss4"
      Me.txtRadPipeEstWallLoss4.Size = New System.Drawing.Size(68, 20)
      Me.txtRadPipeEstWallLoss4.TabIndex = 36
      '
      'txtRadPipeEstWallLoss3
      '
      Me.txtRadPipeEstWallLoss3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstWallLoss3.Location = New System.Drawing.Point(182, 98)
      Me.txtRadPipeEstWallLoss3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstWallLoss3.Name = "txtRadPipeEstWallLoss3"
      Me.txtRadPipeEstWallLoss3.Size = New System.Drawing.Size(68, 20)
      Me.txtRadPipeEstWallLoss3.TabIndex = 28
      '
      'txtRadPipeEstWallLoss2
      '
      Me.txtRadPipeEstWallLoss2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstWallLoss2.Location = New System.Drawing.Point(182, 78)
      Me.txtRadPipeEstWallLoss2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstWallLoss2.Name = "txtRadPipeEstWallLoss2"
      Me.txtRadPipeEstWallLoss2.Size = New System.Drawing.Size(68, 20)
      Me.txtRadPipeEstWallLoss2.TabIndex = 20
      '
      'txtRadPipeEstWallLoss1
      '
      Me.txtRadPipeEstWallLoss1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeEstWallLoss1.Location = New System.Drawing.Point(182, 58)
      Me.txtRadPipeEstWallLoss1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeEstWallLoss1.Name = "txtRadPipeEstWallLoss1"
      Me.txtRadPipeEstWallLoss1.Size = New System.Drawing.Size(68, 20)
      Me.txtRadPipeEstWallLoss1.TabIndex = 12
      '
      'txtRadPipeLocation7
      '
      Me.txtRadPipeLocation7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeLocation7.Location = New System.Drawing.Point(121, 178)
      Me.txtRadPipeLocation7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeLocation7.Name = "txtRadPipeLocation7"
      Me.txtRadPipeLocation7.Size = New System.Drawing.Size(61, 20)
      Me.txtRadPipeLocation7.TabIndex = 59
      '
      'txtRadPipeLocation6
      '
      Me.txtRadPipeLocation6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeLocation6.Location = New System.Drawing.Point(121, 158)
      Me.txtRadPipeLocation6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeLocation6.Name = "txtRadPipeLocation6"
      Me.txtRadPipeLocation6.Size = New System.Drawing.Size(61, 20)
      Me.txtRadPipeLocation6.TabIndex = 51
      '
      'txtRadPipeLocation5
      '
      Me.txtRadPipeLocation5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeLocation5.Location = New System.Drawing.Point(121, 138)
      Me.txtRadPipeLocation5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeLocation5.Name = "txtRadPipeLocation5"
      Me.txtRadPipeLocation5.Size = New System.Drawing.Size(61, 20)
      Me.txtRadPipeLocation5.TabIndex = 43
      '
      'txtRadPipeLocation4
      '
      Me.txtRadPipeLocation4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeLocation4.Location = New System.Drawing.Point(121, 118)
      Me.txtRadPipeLocation4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeLocation4.Name = "txtRadPipeLocation4"
      Me.txtRadPipeLocation4.Size = New System.Drawing.Size(61, 20)
      Me.txtRadPipeLocation4.TabIndex = 35
      '
      'txtRadPipeLocation3
      '
      Me.txtRadPipeLocation3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeLocation3.Location = New System.Drawing.Point(121, 98)
      Me.txtRadPipeLocation3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeLocation3.Name = "txtRadPipeLocation3"
      Me.txtRadPipeLocation3.Size = New System.Drawing.Size(61, 20)
      Me.txtRadPipeLocation3.TabIndex = 27
      '
      'txtRadPipeLocation2
      '
      Me.txtRadPipeLocation2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeLocation2.Location = New System.Drawing.Point(121, 78)
      Me.txtRadPipeLocation2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeLocation2.Name = "txtRadPipeLocation2"
      Me.txtRadPipeLocation2.Size = New System.Drawing.Size(61, 20)
      Me.txtRadPipeLocation2.TabIndex = 19
      '
      'txtRadPipeLocation1
      '
      Me.txtRadPipeLocation1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeLocation1.Location = New System.Drawing.Point(121, 58)
      Me.txtRadPipeLocation1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeLocation1.Name = "txtRadPipeLocation1"
      Me.txtRadPipeLocation1.Size = New System.Drawing.Size(61, 20)
      Me.txtRadPipeLocation1.TabIndex = 11
      '
      'txtRadPipeMinWall7
      '
      Me.txtRadPipeMinWall7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeMinWall7.Location = New System.Drawing.Point(58, 178)
      Me.txtRadPipeMinWall7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeMinWall7.Name = "txtRadPipeMinWall7"
      Me.txtRadPipeMinWall7.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeMinWall7.TabIndex = 58
      '
      'txtRadPipeMinWall6
      '
      Me.txtRadPipeMinWall6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeMinWall6.Location = New System.Drawing.Point(58, 158)
      Me.txtRadPipeMinWall6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeMinWall6.Name = "txtRadPipeMinWall6"
      Me.txtRadPipeMinWall6.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeMinWall6.TabIndex = 50
      '
      'txtRadPipeMinWall5
      '
      Me.txtRadPipeMinWall5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeMinWall5.Location = New System.Drawing.Point(58, 138)
      Me.txtRadPipeMinWall5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeMinWall5.Name = "txtRadPipeMinWall5"
      Me.txtRadPipeMinWall5.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeMinWall5.TabIndex = 42
      '
      'txtRadPipeMinWall4
      '
      Me.txtRadPipeMinWall4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeMinWall4.Location = New System.Drawing.Point(58, 118)
      Me.txtRadPipeMinWall4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeMinWall4.Name = "txtRadPipeMinWall4"
      Me.txtRadPipeMinWall4.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeMinWall4.TabIndex = 34
      '
      'txtRadPipeMinWall3
      '
      Me.txtRadPipeMinWall3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeMinWall3.Location = New System.Drawing.Point(58, 98)
      Me.txtRadPipeMinWall3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeMinWall3.Name = "txtRadPipeMinWall3"
      Me.txtRadPipeMinWall3.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeMinWall3.TabIndex = 26
      '
      'txtRadPipeMinWall2
      '
      Me.txtRadPipeMinWall2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeMinWall2.Location = New System.Drawing.Point(58, 78)
      Me.txtRadPipeMinWall2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeMinWall2.Name = "txtRadPipeMinWall2"
      Me.txtRadPipeMinWall2.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeMinWall2.TabIndex = 18
      '
      'txtRadPipeMinWall1
      '
      Me.txtRadPipeMinWall1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeMinWall1.Location = New System.Drawing.Point(58, 58)
      Me.txtRadPipeMinWall1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeMinWall1.Name = "txtRadPipeMinWall1"
      Me.txtRadPipeMinWall1.Size = New System.Drawing.Size(63, 20)
      Me.txtRadPipeMinWall1.TabIndex = 10
      '
      'txtRadPipeID7
      '
      Me.txtRadPipeID7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeID7.Location = New System.Drawing.Point(8, 178)
      Me.txtRadPipeID7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeID7.Name = "txtRadPipeID7"
      Me.txtRadPipeID7.Size = New System.Drawing.Size(50, 20)
      Me.txtRadPipeID7.TabIndex = 57
      '
      'txtRadPipeID6
      '
      Me.txtRadPipeID6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeID6.Location = New System.Drawing.Point(8, 158)
      Me.txtRadPipeID6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeID6.Name = "txtRadPipeID6"
      Me.txtRadPipeID6.Size = New System.Drawing.Size(50, 20)
      Me.txtRadPipeID6.TabIndex = 49
      '
      'txtRadPipeID5
      '
      Me.txtRadPipeID5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeID5.Location = New System.Drawing.Point(8, 138)
      Me.txtRadPipeID5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeID5.Name = "txtRadPipeID5"
      Me.txtRadPipeID5.Size = New System.Drawing.Size(50, 20)
      Me.txtRadPipeID5.TabIndex = 41
      '
      'txtRadPipeID4
      '
      Me.txtRadPipeID4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeID4.Location = New System.Drawing.Point(8, 118)
      Me.txtRadPipeID4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeID4.Name = "txtRadPipeID4"
      Me.txtRadPipeID4.Size = New System.Drawing.Size(50, 20)
      Me.txtRadPipeID4.TabIndex = 33
      '
      'txtRadPipeID3
      '
      Me.txtRadPipeID3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeID3.Location = New System.Drawing.Point(8, 98)
      Me.txtRadPipeID3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeID3.Name = "txtRadPipeID3"
      Me.txtRadPipeID3.Size = New System.Drawing.Size(50, 20)
      Me.txtRadPipeID3.TabIndex = 25
      '
      'txtRadPipeID2
      '
      Me.txtRadPipeID2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeID2.Location = New System.Drawing.Point(8, 78)
      Me.txtRadPipeID2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeID2.Name = "txtRadPipeID2"
      Me.txtRadPipeID2.Size = New System.Drawing.Size(50, 20)
      Me.txtRadPipeID2.TabIndex = 17
      '
      'txtRadPipeID0
      '
      Me.txtRadPipeID0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadPipeID0.Location = New System.Drawing.Point(8, 38)
      Me.txtRadPipeID0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadPipeID0.Name = "txtRadPipeID0"
      Me.txtRadPipeID0.Size = New System.Drawing.Size(50, 20)
      Me.txtRadPipeID0.TabIndex = 1
      '
      'uiSummaryRadPipeTextbox
      '
      Me.uiSummaryRadPipeTextbox.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
      Me.uiSummaryRadPipeTextbox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
      Me.uiSummaryRadPipeTextbox.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.uiSummaryRadPipeTextbox.Appearance.Text.Options.UseFont = True
      Me.uiSummaryRadPipeTextbox.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
      Me.uiSummaryRadPipeTextbox.Location = New System.Drawing.Point(-2, 153)
      Me.uiSummaryRadPipeTextbox.Margin = New System.Windows.Forms.Padding(0)
      Me.uiSummaryRadPipeTextbox.Name = "uiSummaryRadPipeTextbox"
      Me.uiSummaryRadPipeTextbox.Size = New System.Drawing.Size(588, 138)
      Me.uiSummaryRadPipeTextbox.TabIndex = 76
      '
      'uiSummaryTabRadBends
      '
      Me.uiSummaryTabRadBends.Controls.Add(Me.btnRadBendsUpdate)
      Me.uiSummaryTabRadBends.Controls.Add(Me.uiSummaryRadBendTextbox)
      Me.uiSummaryTabRadBends.Controls.Add(Me.Panel3)
      Me.uiSummaryTabRadBends.Controls.Add(Me.uiRadBendScheduleSize)
      Me.uiSummaryTabRadBends.Controls.Add(Me.uiRadBendBendSize)
      Me.uiSummaryTabRadBends.Controls.Add(Me.uiRadBendInternalOrExternal)
      Me.uiSummaryTabRadBends.Controls.Add(Me.Label25)
      Me.uiSummaryTabRadBends.Controls.Add(Me.uiRadBendGeneralOrLocal)
      Me.uiSummaryTabRadBends.Controls.Add(Me.Label26)
      Me.uiSummaryTabRadBends.Controls.Add(Me.Label27)
      Me.uiSummaryTabRadBends.Controls.Add(Me.Label28)
      Me.uiSummaryTabRadBends.Name = "uiSummaryTabRadBends"
      Me.uiSummaryTabRadBends.Size = New System.Drawing.Size(589, 320)
      Me.uiSummaryTabRadBends.Text = "Radiant - Bends"
      '
      'btnRadBendsUpdate
      '
      Me.btnRadBendsUpdate.Anchor = System.Windows.Forms.AnchorStyles.Bottom
      Me.btnRadBendsUpdate.Location = New System.Drawing.Point(162, 294)
      Me.btnRadBendsUpdate.Name = "btnRadBendsUpdate"
      Me.btnRadBendsUpdate.Size = New System.Drawing.Size(241, 23)
      Me.btnRadBendsUpdate.TabIndex = 1010
      Me.btnRadBendsUpdate.Text = "Update Radiant Bend Summary"
      '
      'uiSummaryRadBendTextbox
      '
      Me.uiSummaryRadBendTextbox.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
      Me.uiSummaryRadBendTextbox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
      Me.uiSummaryRadBendTextbox.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.uiSummaryRadBendTextbox.Appearance.Text.Options.UseFont = True
      Me.uiSummaryRadBendTextbox.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
      Me.uiSummaryRadBendTextbox.Location = New System.Drawing.Point(-2, 152)
      Me.uiSummaryRadBendTextbox.Margin = New System.Windows.Forms.Padding(0)
      Me.uiSummaryRadBendTextbox.Name = "uiSummaryRadBendTextbox"
      Me.uiSummaryRadBendTextbox.Size = New System.Drawing.Size(588, 141)
      Me.uiSummaryRadBendTextbox.TabIndex = 1009
      Me.uiSummaryRadBendTextbox.TabStop = False
      '
      'Panel3
      '
      Me.Panel3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
      Me.Panel3.AutoScroll = True
      Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
      Me.Panel3.Controls.Add(Me.Label18)
      Me.Panel3.Controls.Add(Me.Label19)
      Me.Panel3.Controls.Add(Me.Label20)
      Me.Panel3.Controls.Add(Me.Label22)
      Me.Panel3.Controls.Add(Me.txtRadBendID1)
      Me.Panel3.Controls.Add(Me.txtRadBendMinWall0)
      Me.Panel3.Controls.Add(Me.txtRadBendEstWallLoss0)
      Me.Panel3.Controls.Add(Me.txtRadBendEstRemWall0)
      Me.Panel3.Controls.Add(Me.txtRadBendEstRemWall7)
      Me.Panel3.Controls.Add(Me.txtRadBendEstRemWall6)
      Me.Panel3.Controls.Add(Me.txtRadBendEstRemWall5)
      Me.Panel3.Controls.Add(Me.txtRadBendEstRemWall4)
      Me.Panel3.Controls.Add(Me.txtRadBendEstRemWall3)
      Me.Panel3.Controls.Add(Me.txtRadBendEstRemWall2)
      Me.Panel3.Controls.Add(Me.txtRadBendEstRemWall1)
      Me.Panel3.Controls.Add(Me.txtRadBendEstWallLoss7)
      Me.Panel3.Controls.Add(Me.txtRadBendEstWallLoss6)
      Me.Panel3.Controls.Add(Me.txtRadBendEstWallLoss5)
      Me.Panel3.Controls.Add(Me.txtRadBendEstWallLoss4)
      Me.Panel3.Controls.Add(Me.txtRadBendEstWallLoss3)
      Me.Panel3.Controls.Add(Me.txtRadBendEstWallLoss2)
      Me.Panel3.Controls.Add(Me.txtRadBendEstWallLoss1)
      Me.Panel3.Controls.Add(Me.txtRadBendMinWall7)
      Me.Panel3.Controls.Add(Me.txtRadBendMinWall6)
      Me.Panel3.Controls.Add(Me.txtRadBendMinWall5)
      Me.Panel3.Controls.Add(Me.txtRadBendMinWall4)
      Me.Panel3.Controls.Add(Me.txtRadBendMinWall3)
      Me.Panel3.Controls.Add(Me.txtRadBendMinWall2)
      Me.Panel3.Controls.Add(Me.txtRadBendMinWall1)
      Me.Panel3.Controls.Add(Me.txtRadBendID7)
      Me.Panel3.Controls.Add(Me.txtRadBendID6)
      Me.Panel3.Controls.Add(Me.txtRadBendID5)
      Me.Panel3.Controls.Add(Me.txtRadBendID4)
      Me.Panel3.Controls.Add(Me.txtRadBendID3)
      Me.Panel3.Controls.Add(Me.txtRadBendID2)
      Me.Panel3.Controls.Add(Me.txtRadBendID0)
      Me.Panel3.Location = New System.Drawing.Point(-1, 52)
      Me.Panel3.Margin = New System.Windows.Forms.Padding(0)
      Me.Panel3.Name = "Panel3"
      Me.Panel3.Size = New System.Drawing.Size(587, 100)
      Me.Panel3.TabIndex = 1008
      '
      'Label18
      '
      Me.Label18.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label18.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label18.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label18.Location = New System.Drawing.Point(305, 10)
      Me.Label18.Name = "Label18"
      Me.Label18.Size = New System.Drawing.Size(96, 29)
      Me.Label18.TabIndex = 142
      Me.Label18.Text = "Estimated Remaining Wall (%)"
      Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'Label19
      '
      Me.Label19.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label19.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label19.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label19.Location = New System.Drawing.Point(243, 10)
      Me.Label19.Name = "Label19"
      Me.Label19.Size = New System.Drawing.Size(68, 29)
      Me.Label19.TabIndex = 140
      Me.Label19.Text = "Estimated Wall Loss"
      Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'Label20
      '
      Me.Label20.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label20.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label20.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label20.Location = New System.Drawing.Point(181, 10)
      Me.Label20.Name = "Label20"
      Me.Label20.Size = New System.Drawing.Size(63, 29)
      Me.Label20.TabIndex = 138
      Me.Label20.Text = "Minimum Wall"
      Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'Label22
      '
      Me.Label22.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label22.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label22.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label22.Location = New System.Drawing.Point(131, 10)
      Me.Label22.Name = "Label22"
      Me.Label22.Size = New System.Drawing.Size(50, 29)
      Me.Label22.TabIndex = 137
      Me.Label22.Text = "Bend ID"
      Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'txtRadBendID1
      '
      Me.txtRadBendID1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendID1.Location = New System.Drawing.Point(131, 59)
      Me.txtRadBendID1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendID1.Name = "txtRadBendID1"
      Me.txtRadBendID1.Size = New System.Drawing.Size(50, 20)
      Me.txtRadBendID1.TabIndex = 5
      '
      'txtRadBendMinWall0
      '
      Me.txtRadBendMinWall0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendMinWall0.Location = New System.Drawing.Point(181, 39)
      Me.txtRadBendMinWall0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendMinWall0.Name = "txtRadBendMinWall0"
      Me.txtRadBendMinWall0.Size = New System.Drawing.Size(63, 20)
      Me.txtRadBendMinWall0.TabIndex = 2
      '
      'txtRadBendEstWallLoss0
      '
      Me.txtRadBendEstWallLoss0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstWallLoss0.Location = New System.Drawing.Point(244, 39)
      Me.txtRadBendEstWallLoss0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstWallLoss0.Name = "txtRadBendEstWallLoss0"
      Me.txtRadBendEstWallLoss0.Size = New System.Drawing.Size(61, 20)
      Me.txtRadBendEstWallLoss0.TabIndex = 3
      '
      'txtRadBendEstRemWall0
      '
      Me.txtRadBendEstRemWall0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstRemWall0.Location = New System.Drawing.Point(305, 39)
      Me.txtRadBendEstRemWall0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstRemWall0.Name = "txtRadBendEstRemWall0"
      Me.txtRadBendEstRemWall0.Size = New System.Drawing.Size(96, 20)
      Me.txtRadBendEstRemWall0.TabIndex = 4
      '
      'txtRadBendEstRemWall7
      '
      Me.txtRadBendEstRemWall7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstRemWall7.Location = New System.Drawing.Point(305, 179)
      Me.txtRadBendEstRemWall7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstRemWall7.Name = "txtRadBendEstRemWall7"
      Me.txtRadBendEstRemWall7.Size = New System.Drawing.Size(96, 20)
      Me.txtRadBendEstRemWall7.TabIndex = 32
      '
      'txtRadBendEstRemWall6
      '
      Me.txtRadBendEstRemWall6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstRemWall6.Location = New System.Drawing.Point(305, 159)
      Me.txtRadBendEstRemWall6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstRemWall6.Name = "txtRadBendEstRemWall6"
      Me.txtRadBendEstRemWall6.Size = New System.Drawing.Size(96, 20)
      Me.txtRadBendEstRemWall6.TabIndex = 28
      '
      'txtRadBendEstRemWall5
      '
      Me.txtRadBendEstRemWall5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstRemWall5.Location = New System.Drawing.Point(305, 139)
      Me.txtRadBendEstRemWall5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstRemWall5.Name = "txtRadBendEstRemWall5"
      Me.txtRadBendEstRemWall5.Size = New System.Drawing.Size(96, 20)
      Me.txtRadBendEstRemWall5.TabIndex = 24
      '
      'txtRadBendEstRemWall4
      '
      Me.txtRadBendEstRemWall4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstRemWall4.Location = New System.Drawing.Point(305, 119)
      Me.txtRadBendEstRemWall4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstRemWall4.Name = "txtRadBendEstRemWall4"
      Me.txtRadBendEstRemWall4.Size = New System.Drawing.Size(96, 20)
      Me.txtRadBendEstRemWall4.TabIndex = 20
      '
      'txtRadBendEstRemWall3
      '
      Me.txtRadBendEstRemWall3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstRemWall3.Location = New System.Drawing.Point(305, 99)
      Me.txtRadBendEstRemWall3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstRemWall3.Name = "txtRadBendEstRemWall3"
      Me.txtRadBendEstRemWall3.Size = New System.Drawing.Size(96, 20)
      Me.txtRadBendEstRemWall3.TabIndex = 16
      '
      'txtRadBendEstRemWall2
      '
      Me.txtRadBendEstRemWall2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstRemWall2.Location = New System.Drawing.Point(305, 79)
      Me.txtRadBendEstRemWall2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstRemWall2.Name = "txtRadBendEstRemWall2"
      Me.txtRadBendEstRemWall2.Size = New System.Drawing.Size(96, 20)
      Me.txtRadBendEstRemWall2.TabIndex = 12
      '
      'txtRadBendEstRemWall1
      '
      Me.txtRadBendEstRemWall1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstRemWall1.Location = New System.Drawing.Point(305, 59)
      Me.txtRadBendEstRemWall1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstRemWall1.Name = "txtRadBendEstRemWall1"
      Me.txtRadBendEstRemWall1.Size = New System.Drawing.Size(96, 20)
      Me.txtRadBendEstRemWall1.TabIndex = 8
      '
      'txtRadBendEstWallLoss7
      '
      Me.txtRadBendEstWallLoss7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstWallLoss7.Location = New System.Drawing.Point(244, 179)
      Me.txtRadBendEstWallLoss7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstWallLoss7.Name = "txtRadBendEstWallLoss7"
      Me.txtRadBendEstWallLoss7.Size = New System.Drawing.Size(61, 20)
      Me.txtRadBendEstWallLoss7.TabIndex = 31
      '
      'txtRadBendEstWallLoss6
      '
      Me.txtRadBendEstWallLoss6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstWallLoss6.Location = New System.Drawing.Point(244, 159)
      Me.txtRadBendEstWallLoss6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstWallLoss6.Name = "txtRadBendEstWallLoss6"
      Me.txtRadBendEstWallLoss6.Size = New System.Drawing.Size(61, 20)
      Me.txtRadBendEstWallLoss6.TabIndex = 27
      '
      'txtRadBendEstWallLoss5
      '
      Me.txtRadBendEstWallLoss5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstWallLoss5.Location = New System.Drawing.Point(244, 139)
      Me.txtRadBendEstWallLoss5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstWallLoss5.Name = "txtRadBendEstWallLoss5"
      Me.txtRadBendEstWallLoss5.Size = New System.Drawing.Size(61, 20)
      Me.txtRadBendEstWallLoss5.TabIndex = 23
      '
      'txtRadBendEstWallLoss4
      '
      Me.txtRadBendEstWallLoss4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstWallLoss4.Location = New System.Drawing.Point(244, 119)
      Me.txtRadBendEstWallLoss4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstWallLoss4.Name = "txtRadBendEstWallLoss4"
      Me.txtRadBendEstWallLoss4.Size = New System.Drawing.Size(61, 20)
      Me.txtRadBendEstWallLoss4.TabIndex = 19
      '
      'txtRadBendEstWallLoss3
      '
      Me.txtRadBendEstWallLoss3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstWallLoss3.Location = New System.Drawing.Point(244, 99)
      Me.txtRadBendEstWallLoss3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstWallLoss3.Name = "txtRadBendEstWallLoss3"
      Me.txtRadBendEstWallLoss3.Size = New System.Drawing.Size(61, 20)
      Me.txtRadBendEstWallLoss3.TabIndex = 15
      '
      'txtRadBendEstWallLoss2
      '
      Me.txtRadBendEstWallLoss2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstWallLoss2.Location = New System.Drawing.Point(244, 79)
      Me.txtRadBendEstWallLoss2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstWallLoss2.Name = "txtRadBendEstWallLoss2"
      Me.txtRadBendEstWallLoss2.Size = New System.Drawing.Size(61, 20)
      Me.txtRadBendEstWallLoss2.TabIndex = 11
      '
      'txtRadBendEstWallLoss1
      '
      Me.txtRadBendEstWallLoss1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendEstWallLoss1.Location = New System.Drawing.Point(244, 59)
      Me.txtRadBendEstWallLoss1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendEstWallLoss1.Name = "txtRadBendEstWallLoss1"
      Me.txtRadBendEstWallLoss1.Size = New System.Drawing.Size(61, 20)
      Me.txtRadBendEstWallLoss1.TabIndex = 7
      '
      'txtRadBendMinWall7
      '
      Me.txtRadBendMinWall7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendMinWall7.Location = New System.Drawing.Point(181, 179)
      Me.txtRadBendMinWall7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendMinWall7.Name = "txtRadBendMinWall7"
      Me.txtRadBendMinWall7.Size = New System.Drawing.Size(63, 20)
      Me.txtRadBendMinWall7.TabIndex = 30
      '
      'txtRadBendMinWall6
      '
      Me.txtRadBendMinWall6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendMinWall6.Location = New System.Drawing.Point(181, 159)
      Me.txtRadBendMinWall6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendMinWall6.Name = "txtRadBendMinWall6"
      Me.txtRadBendMinWall6.Size = New System.Drawing.Size(63, 20)
      Me.txtRadBendMinWall6.TabIndex = 26
      '
      'txtRadBendMinWall5
      '
      Me.txtRadBendMinWall5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendMinWall5.Location = New System.Drawing.Point(181, 139)
      Me.txtRadBendMinWall5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendMinWall5.Name = "txtRadBendMinWall5"
      Me.txtRadBendMinWall5.Size = New System.Drawing.Size(63, 20)
      Me.txtRadBendMinWall5.TabIndex = 22
      '
      'txtRadBendMinWall4
      '
      Me.txtRadBendMinWall4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendMinWall4.Location = New System.Drawing.Point(181, 119)
      Me.txtRadBendMinWall4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendMinWall4.Name = "txtRadBendMinWall4"
      Me.txtRadBendMinWall4.Size = New System.Drawing.Size(63, 20)
      Me.txtRadBendMinWall4.TabIndex = 18
      '
      'txtRadBendMinWall3
      '
      Me.txtRadBendMinWall3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendMinWall3.Location = New System.Drawing.Point(181, 99)
      Me.txtRadBendMinWall3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendMinWall3.Name = "txtRadBendMinWall3"
      Me.txtRadBendMinWall3.Size = New System.Drawing.Size(63, 20)
      Me.txtRadBendMinWall3.TabIndex = 14
      '
      'txtRadBendMinWall2
      '
      Me.txtRadBendMinWall2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendMinWall2.Location = New System.Drawing.Point(181, 79)
      Me.txtRadBendMinWall2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendMinWall2.Name = "txtRadBendMinWall2"
      Me.txtRadBendMinWall2.Size = New System.Drawing.Size(63, 20)
      Me.txtRadBendMinWall2.TabIndex = 10
      '
      'txtRadBendMinWall1
      '
      Me.txtRadBendMinWall1.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendMinWall1.Location = New System.Drawing.Point(181, 59)
      Me.txtRadBendMinWall1.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendMinWall1.Name = "txtRadBendMinWall1"
      Me.txtRadBendMinWall1.Size = New System.Drawing.Size(63, 20)
      Me.txtRadBendMinWall1.TabIndex = 6
      '
      'txtRadBendID7
      '
      Me.txtRadBendID7.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendID7.Location = New System.Drawing.Point(131, 179)
      Me.txtRadBendID7.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendID7.Name = "txtRadBendID7"
      Me.txtRadBendID7.Size = New System.Drawing.Size(50, 20)
      Me.txtRadBendID7.TabIndex = 29
      '
      'txtRadBendID6
      '
      Me.txtRadBendID6.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendID6.Location = New System.Drawing.Point(131, 159)
      Me.txtRadBendID6.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendID6.Name = "txtRadBendID6"
      Me.txtRadBendID6.Size = New System.Drawing.Size(50, 20)
      Me.txtRadBendID6.TabIndex = 25
      '
      'txtRadBendID5
      '
      Me.txtRadBendID5.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendID5.Location = New System.Drawing.Point(131, 139)
      Me.txtRadBendID5.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendID5.Name = "txtRadBendID5"
      Me.txtRadBendID5.Size = New System.Drawing.Size(50, 20)
      Me.txtRadBendID5.TabIndex = 21
      '
      'txtRadBendID4
      '
      Me.txtRadBendID4.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendID4.Location = New System.Drawing.Point(131, 119)
      Me.txtRadBendID4.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendID4.Name = "txtRadBendID4"
      Me.txtRadBendID4.Size = New System.Drawing.Size(50, 20)
      Me.txtRadBendID4.TabIndex = 17
      '
      'txtRadBendID3
      '
      Me.txtRadBendID3.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendID3.Location = New System.Drawing.Point(131, 99)
      Me.txtRadBendID3.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendID3.Name = "txtRadBendID3"
      Me.txtRadBendID3.Size = New System.Drawing.Size(50, 20)
      Me.txtRadBendID3.TabIndex = 13
      '
      'txtRadBendID2
      '
      Me.txtRadBendID2.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendID2.Location = New System.Drawing.Point(131, 79)
      Me.txtRadBendID2.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendID2.Name = "txtRadBendID2"
      Me.txtRadBendID2.Size = New System.Drawing.Size(50, 20)
      Me.txtRadBendID2.TabIndex = 9
      '
      'txtRadBendID0
      '
      Me.txtRadBendID0.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.txtRadBendID0.Location = New System.Drawing.Point(131, 39)
      Me.txtRadBendID0.Margin = New System.Windows.Forms.Padding(0)
      Me.txtRadBendID0.Name = "txtRadBendID0"
      Me.txtRadBendID0.Size = New System.Drawing.Size(50, 20)
      Me.txtRadBendID0.TabIndex = 1
      '
      'uiRadBendScheduleSize
      '
      Me.uiRadBendScheduleSize.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiRadBendScheduleSize.Location = New System.Drawing.Point(387, 27)
      Me.uiRadBendScheduleSize.Name = "uiRadBendScheduleSize"
      Me.uiRadBendScheduleSize.Properties.AutoHeight = False
      Me.uiRadBendScheduleSize.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiRadBendScheduleSize.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiRadBendScheduleSize.Properties.Items.AddRange(New Object() {"Tube", "5", "10", "20", "30", "40", "50", "60", "80", "120", "140", "160", "XXS"})
      Me.uiRadBendScheduleSize.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiRadBendScheduleSize.Size = New System.Drawing.Size(92, 24)
      Me.uiRadBendScheduleSize.TabIndex = 4
      '
      'uiRadBendBendSize
      '
      Me.uiRadBendBendSize.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiRadBendBendSize.Location = New System.Drawing.Point(318, 27)
      Me.uiRadBendBendSize.Name = "uiRadBendBendSize"
      Me.uiRadBendBendSize.Properties.AutoHeight = False
      Me.uiRadBendBendSize.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiRadBendBendSize.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiRadBendBendSize.Properties.Items.AddRange(New Object() {"Other", "2", "2.5", "3", "3.5", "4", "5", "6", "8", "10", "12"})
      Me.uiRadBendBendSize.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiRadBendBendSize.Size = New System.Drawing.Size(63, 24)
      Me.uiRadBendBendSize.TabIndex = 3
      '
      'uiRadBendInternalOrExternal
      '
      Me.uiRadBendInternalOrExternal.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiRadBendInternalOrExternal.Location = New System.Drawing.Point(188, 27)
      Me.uiRadBendInternalOrExternal.Name = "uiRadBendInternalOrExternal"
      Me.uiRadBendInternalOrExternal.Properties.AutoHeight = False
      Me.uiRadBendInternalOrExternal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiRadBendInternalOrExternal.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiRadBendInternalOrExternal.Properties.Items.AddRange(New Object() {"internal", "external", "internal and external"})
      Me.uiRadBendInternalOrExternal.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiRadBendInternalOrExternal.Size = New System.Drawing.Size(123, 24)
      Me.uiRadBendInternalOrExternal.TabIndex = 2
      '
      'Label25
      '
      Me.Label25.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label25.AutoSize = True
      Me.Label25.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label25.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label25.Location = New System.Drawing.Point(78, 8)
      Me.Label25.Name = "Label25"
      Me.Label25.Size = New System.Drawing.Size(104, 17)
      Me.Label25.TabIndex = 1005
      Me.Label25.Text = "General or Local"
      Me.Label25.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'uiRadBendGeneralOrLocal
      '
      Me.uiRadBendGeneralOrLocal.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.uiRadBendGeneralOrLocal.Location = New System.Drawing.Point(79, 27)
      Me.uiRadBendGeneralOrLocal.Name = "uiRadBendGeneralOrLocal"
      Me.uiRadBendGeneralOrLocal.Properties.AutoHeight = False
      Me.uiRadBendGeneralOrLocal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiRadBendGeneralOrLocal.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiRadBendGeneralOrLocal.Properties.Items.AddRange(New Object() {"General", "Local", "General and local"})
      Me.uiRadBendGeneralOrLocal.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiRadBendGeneralOrLocal.Size = New System.Drawing.Size(103, 24)
      Me.uiRadBendGeneralOrLocal.TabIndex = 1
      '
      'Label26
      '
      Me.Label26.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label26.AutoSize = True
      Me.Label26.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label26.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label26.Location = New System.Drawing.Point(191, 8)
      Me.Label26.Name = "Label26"
      Me.Label26.Size = New System.Drawing.Size(118, 17)
      Me.Label26.TabIndex = 1000
      Me.Label26.Text = "Internal or External"
      Me.Label26.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label27
      '
      Me.Label27.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label27.AutoSize = True
      Me.Label27.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label27.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label27.Location = New System.Drawing.Point(318, 8)
      Me.Label27.Name = "Label27"
      Me.Label27.Size = New System.Drawing.Size(60, 17)
      Me.Label27.TabIndex = 1006
      Me.Label27.Text = "Pipe Size"
      Me.Label27.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label28
      '
      Me.Label28.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.Label28.AutoSize = True
      Me.Label28.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label28.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.Label28.Location = New System.Drawing.Point(387, 8)
      Me.Label28.Name = "Label28"
      Me.Label28.Size = New System.Drawing.Size(89, 17)
      Me.Label28.TabIndex = 1007
      Me.Label28.Text = "Pipe Schedule"
      Me.Label28.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'lblGeneralOptions
      '
      Me.lblGeneralOptions.Anchor = System.Windows.Forms.AnchorStyles.Top
      Me.lblGeneralOptions.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lblGeneralOptions.ForeColor = System.Drawing.Color.DarkSlateBlue
      Me.lblGeneralOptions.Location = New System.Drawing.Point(142, 0)
      Me.lblGeneralOptions.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
      Me.lblGeneralOptions.Name = "lblGeneralOptions"
      Me.lblGeneralOptions.Padding = New System.Windows.Forms.Padding(6, 0, 0, 0)
      Me.lblGeneralOptions.Size = New System.Drawing.Size(279, 28)
      Me.lblGeneralOptions.TabIndex = 32
      Me.lblGeneralOptions.Text = "General Options"
      Me.lblGeneralOptions.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'uiNewUsedPipes
      '
      Me.uiNewUsedPipes.Location = New System.Drawing.Point(126, 60)
      Me.uiNewUsedPipes.Name = "uiNewUsedPipes"
      Me.uiNewUsedPipes.Properties.AutoHeight = False
      Me.uiNewUsedPipes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiNewUsedPipes.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiNewUsedPipes.Properties.Items.AddRange(New Object() {"New Pipes (Baseline)", "In-Service Pipes"})
      Me.uiNewUsedPipes.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiNewUsedPipes.Size = New System.Drawing.Size(109, 24)
      Me.uiNewUsedPipes.TabIndex = 2
      '
      'uiSummaryWriteUp
      '
      Me.uiSummaryWriteUp.Location = New System.Drawing.Point(126, 29)
      Me.uiSummaryWriteUp.Name = "uiSummaryWriteUp"
      Me.uiSummaryWriteUp.Properties.AutoHeight = False
      Me.uiSummaryWriteUp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiSummaryWriteUp.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiSummaryWriteUp.Properties.Items.AddRange(New Object() {"Section", "General"})
      Me.uiSummaryWriteUp.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiSummaryWriteUp.Size = New System.Drawing.Size(109, 24)
      Me.uiSummaryWriteUp.TabIndex = 1
      '
      'uiUnits
      '
      Me.uiUnits.Location = New System.Drawing.Point(126, 91)
      Me.uiUnits.Name = "uiUnits"
      Me.uiUnits.Properties.AutoHeight = False
      Me.uiUnits.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiUnits.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiUnits.Properties.Items.AddRange(New Object() {"Imperial", "Metric"})
      Me.uiUnits.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiUnits.Size = New System.Drawing.Size(109, 24)
      Me.uiUnits.TabIndex = 3
      '
      'uiPassNumber
      '
      Me.uiPassNumber.Location = New System.Drawing.Point(342, 29)
      Me.uiPassNumber.Name = "uiPassNumber"
      Me.uiPassNumber.Properties.AutoHeight = False
      Me.uiPassNumber.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiPassNumber.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiPassNumber.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "A", "B", "C", "D", "E", "F", "G", "H", "North", "East", "South", "West", "Other"})
      Me.uiPassNumber.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiPassNumber.Size = New System.Drawing.Size(79, 24)
      Me.uiPassNumber.TabIndex = 4
      '
      'uiAppendix
      '
      Me.uiAppendix.Location = New System.Drawing.Point(342, 59)
      Me.uiAppendix.Name = "uiAppendix"
      Me.uiAppendix.Properties.AutoHeight = False
      Me.uiAppendix.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
      Me.uiAppendix.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
      Me.uiAppendix.Properties.Items.AddRange(New Object() {"A", "B", "C", "D", "E", "F", "G", "H", "Other"})
      Me.uiAppendix.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
      Me.uiAppendix.Size = New System.Drawing.Size(79, 24)
      Me.uiAppendix.TabIndex = 5
      '
      'ucProjectSummary
      '
      Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
      Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
      Me.AutoScroll = True
      Me.AutoSize = True
      Me.BackColor = System.Drawing.Color.Transparent
      Me.Controls.Add(Me.uiAppendix)
      Me.Controls.Add(Me.uiUnits)
      Me.Controls.Add(Me.uiPassNumber)
      Me.Controls.Add(Me.uiSummaryWriteUp)
      Me.Controls.Add(Me.uiNewUsedPipes)
      Me.Controls.Add(Me.lblGeneralOptions)
      Me.Controls.Add(Me.uilblAppendix)
      Me.Controls.Add(Me.uilblNewUsedPipes)
      Me.Controls.Add(Me.uiSummaryTabs)
      Me.Controls.Add(Me.uilblSummaryWriteup)
      Me.Controls.Add(Me.uilblUnits)
      Me.Controls.Add(Me.uilblPassNumber)
      Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Name = "ucProjectSummary"
      Me.Size = New System.Drawing.Size(598, 470)
      CType(Me.uiSummaryTabs, System.ComponentModel.ISupportInitialize).EndInit()
      Me.uiSummaryTabs.ResumeLayout(False)
      Me.uiSummaryTabConPipes.ResumeLayout(False)
      Me.uiSummaryTabConPipes.PerformLayout()
      CType(Me.uiConPipeScheduleSize.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiConPipePipeSize.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiConPipeInternalOrExternal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiConPipeGeneralOrLocal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      Me.PipePanel.ResumeLayout(False)
      CType(Me.txtConPipeAvgDiameter0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeAvgDiameter7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeAvgDiameter6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeAvgDiameter5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeMinWall0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeAvgDiameter4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeLocation0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeAvgDiameter3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeDesWall0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeAvgDiameter2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstWallLoss0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeAvgDiameter1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeAvgWall0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeDesWall7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstRemWall0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeDesWall6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeDesWall5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeDesWall4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeDesWall3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeDesWall2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeDesWall1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeAvgWall7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeAvgWall6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeAvgWall5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeAvgWall4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeAvgWall3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeAvgWall2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeAvgWall1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstRemWall7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstRemWall6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstRemWall5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstRemWall4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstRemWall3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstRemWall2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstRemWall1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstWallLoss7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstWallLoss6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstWallLoss5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstWallLoss4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstWallLoss3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstWallLoss2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeEstWallLoss1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeLocation7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeLocation6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeLocation5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeLocation4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeLocation3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeLocation2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeLocation1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeMinWall7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeMinWall6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeMinWall5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeMinWall4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeMinWall3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeMinWall2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeMinWall1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeID7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeID6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeID5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeID4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeID3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeID2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConPipeID0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      Me.uiSummaryTabConBends.ResumeLayout(False)
      Me.uiSummaryTabConBends.PerformLayout()
      Me.Panel2.ResumeLayout(False)
      CType(Me.txtConBendID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendMinWall0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstWallLoss0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstRemWall0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstRemWall7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstRemWall6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstRemWall5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstRemWall4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstRemWall3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstRemWall2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstRemWall1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstWallLoss7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstWallLoss6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstWallLoss5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstWallLoss4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstWallLoss3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstWallLoss2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendEstWallLoss1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendMinWall7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendMinWall6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendMinWall5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendMinWall4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendMinWall3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendMinWall2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendMinWall1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendID7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendID6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendID5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendID4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendID3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendID2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtConBendID0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiConBendScheduleSize.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiConBendBendSize.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiConBendInternalOrExternal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiConBendGeneralOrLocal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      Me.uiSummaryTabRadPipes.ResumeLayout(False)
      Me.uiSummaryTabRadPipes.PerformLayout()
      CType(Me.uiRadPipeScheduleSize.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiRadPipePipeSize.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiRadPipeInternalOrExternal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiRadPipeGeneralOrLocal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      Me.Panel1.ResumeLayout(False)
      CType(Me.txtRadPipeAvgDiameter0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeAvgDiameter7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeAvgDiameter6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeAvgDiameter5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeMinWall0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeAvgDiameter4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeLocation0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeAvgDiameter3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeDesWall0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeAvgDiameter2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstWallLoss0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeAvgDiameter1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeAvgWall0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeDesWall7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstRemWall0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeDesWall6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeDesWall5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeDesWall4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeDesWall3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeDesWall2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeDesWall1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeAvgWall7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeAvgWall6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeAvgWall5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeAvgWall4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeAvgWall3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeAvgWall2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeAvgWall1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstRemWall7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstRemWall6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstRemWall5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstRemWall4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstRemWall3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstRemWall2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstRemWall1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstWallLoss7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstWallLoss6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstWallLoss5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstWallLoss4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstWallLoss3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstWallLoss2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeEstWallLoss1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeLocation7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeLocation6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeLocation5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeLocation4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeLocation3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeLocation2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeLocation1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeMinWall7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeMinWall6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeMinWall5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeMinWall4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeMinWall3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeMinWall2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeMinWall1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeID7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeID6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeID5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeID4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeID3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeID2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadPipeID0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      Me.uiSummaryTabRadBends.ResumeLayout(False)
      Me.uiSummaryTabRadBends.PerformLayout()
      Me.Panel3.ResumeLayout(False)
      CType(Me.txtRadBendID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendMinWall0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstWallLoss0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstRemWall0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstRemWall7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstRemWall6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstRemWall5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstRemWall4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstRemWall3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstRemWall2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstRemWall1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstWallLoss7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstWallLoss6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstWallLoss5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstWallLoss4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstWallLoss3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstWallLoss2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendEstWallLoss1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendMinWall7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendMinWall6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendMinWall5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendMinWall4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendMinWall3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendMinWall2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendMinWall1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendID7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendID6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendID5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendID4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendID3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendID2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.txtRadBendID0.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiRadBendScheduleSize.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiRadBendBendSize.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiRadBendInternalOrExternal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiRadBendGeneralOrLocal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiNewUsedPipes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiSummaryWriteUp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiUnits.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiPassNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.uiAppendix.Properties, System.ComponentModel.ISupportInitialize).EndInit()
      Me.ResumeLayout(False)
      Me.PerformLayout()

   End Sub
    Friend WithEvents uilblSummaryWriteup As System.Windows.Forms.Label
    Friend WithEvents uilblNewUsedPipes As System.Windows.Forms.Label
    Friend WithEvents uilblPassNumber As System.Windows.Forms.Label
    Friend WithEvents uilblUnits As System.Windows.Forms.Label
    Friend WithEvents uilblAppendix As System.Windows.Forms.Label
    Friend WithEvents uiSummaryTabs As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents uiSummaryTabConPipes As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uiSummaryConPipeTextbox As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents uiSummaryTabConBends As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents lblGeneralOptions As System.Windows.Forms.Label
    Friend WithEvents uiNewUsedPipes As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiSummaryWriteUp As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiUnits As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiPassNumber As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiAppendix As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents PipePanel As System.Windows.Forms.Panel
    Friend WithEvents lblPipeInputAvgDiameter As System.Windows.Forms.Label
    Friend WithEvents lblPipeInputDesignedWall As System.Windows.Forms.Label
    Friend WithEvents lblPipeInputAvgWall As System.Windows.Forms.Label
    Friend WithEvents lblPipeInputEstWallRemaining As System.Windows.Forms.Label
    Friend WithEvents lblPipeInputWallLoss As System.Windows.Forms.Label
    Friend WithEvents lblPipeInputLocation As System.Windows.Forms.Label
    Friend WithEvents lblPipeInputMinWall As System.Windows.Forms.Label
    Friend WithEvents lblPipeInputID As System.Windows.Forms.Label
    Friend WithEvents txtConPipeAvgDiameter0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeAvgDiameter7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeAvgDiameter6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeAvgDiameter5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeAvgDiameter4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeAvgDiameter3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeAvgDiameter2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeAvgDiameter1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeDesWall0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeDesWall7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeDesWall6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeDesWall5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeDesWall4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeDesWall3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeDesWall2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeDesWall1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeAvgWall0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeAvgWall7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeAvgWall6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeAvgWall5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeAvgWall4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeAvgWall3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeAvgWall2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeAvgWall1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstRemWall0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstRemWall7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstRemWall6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstRemWall5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstRemWall4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstRemWall3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstRemWall2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstRemWall1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstWallLoss0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstWallLoss7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstWallLoss6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstWallLoss5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstWallLoss4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstWallLoss3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstWallLoss2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeEstWallLoss1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeLocation0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeLocation7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeLocation6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeLocation5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeLocation4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeLocation3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeLocation2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeLocation1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeMinWall0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeMinWall7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeMinWall6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeMinWall5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeMinWall4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeMinWall3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeMinWall2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeMinWall1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeID1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeID7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeID6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeID5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeID4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeID3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeID2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConPipeID0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiSummaryTabRadPipes As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uiSummaryTabRadBends As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uiConPipeScheduleSize As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiConPipePipeSize As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiConPipeInternalOrExternal As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uilblGeneralOrLocal As System.Windows.Forms.Label
    Friend WithEvents uiConPipeGeneralOrLocal As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uilblInternalOrExternal As System.Windows.Forms.Label
    Friend WithEvents uilblPipeSize As System.Windows.Forms.Label
    Friend WithEvents uilblPipeSchedule As System.Windows.Forms.Label
    Friend WithEvents btnConPipesUpdate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiRadPipeScheduleSize As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiRadPipePipeSize As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiRadPipeInternalOrExternal As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents uiRadPipeGeneralOrLocal As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnRadPipesUpdate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtRadPipeAvgDiameter0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeAvgDiameter7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeAvgDiameter6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeID1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeAvgDiameter5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeMinWall0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeAvgDiameter4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeLocation0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeAvgDiameter3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeDesWall0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeAvgDiameter2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstWallLoss0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeAvgDiameter1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeAvgWall0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeDesWall7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstRemWall0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeDesWall6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeDesWall5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeDesWall4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeDesWall3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeDesWall2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeDesWall1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeAvgWall7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeAvgWall6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeAvgWall5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeAvgWall4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeAvgWall3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeAvgWall2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeAvgWall1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstRemWall7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstRemWall6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstRemWall5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstRemWall4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstRemWall3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstRemWall2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstRemWall1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstWallLoss7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstWallLoss6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstWallLoss5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstWallLoss4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstWallLoss3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstWallLoss2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeEstWallLoss1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeLocation7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeLocation6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeLocation5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeLocation4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeLocation3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeLocation2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeLocation1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeMinWall7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeMinWall6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeMinWall5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeMinWall4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeMinWall3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeMinWall2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeMinWall1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeID7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeID6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeID5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeID4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeID3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeID2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadPipeID0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiSummaryRadPipeTextbox As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents btnConBendsUpdate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiSummaryConBendTextbox As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtConBendID1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendMinWall0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstWallLoss0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstRemWall0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstRemWall7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstRemWall6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstRemWall5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstRemWall4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstRemWall3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstRemWall2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstRemWall1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstWallLoss7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstWallLoss6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstWallLoss5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstWallLoss4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstWallLoss3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstWallLoss2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendEstWallLoss1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendMinWall7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendMinWall6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendMinWall5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendMinWall4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendMinWall3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendMinWall2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendMinWall1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendID7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendID6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendID5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendID4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendID3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendID2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConBendID0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiConBendScheduleSize As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiConBendBendSize As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiConBendInternalOrExternal As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents uiConBendGeneralOrLocal As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents btnRadBendsUpdate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiSummaryRadBendTextbox As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtRadBendID1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendMinWall0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstWallLoss0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstRemWall0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstRemWall7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstRemWall6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstRemWall5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstRemWall4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstRemWall3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstRemWall2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstRemWall1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstWallLoss7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstWallLoss6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstWallLoss5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstWallLoss4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstWallLoss3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstWallLoss2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendEstWallLoss1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendMinWall7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendMinWall6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendMinWall5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendMinWall4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendMinWall3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendMinWall2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendMinWall1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendID7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendID6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendID5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendID4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendID3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendID2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRadBendID0 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiRadBendScheduleSize As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiRadBendBendSize As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiRadBendInternalOrExternal As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents uiRadBendGeneralOrLocal As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label

End Class
