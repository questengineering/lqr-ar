﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucSectionSummary
   Inherits System.Windows.Forms.UserControl

   'UserControl overrides dispose to clean up the component list.
   <System.Diagnostics.DebuggerNonUserCode()> _
   Protected Overrides Sub Dispose(ByVal disposing As Boolean)
      Try
         If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
         End If
      Finally
         MyBase.Dispose(disposing)
      End Try
   End Sub

   'Required by the Windows Form Designer
   Private components As System.ComponentModel.IContainer

   'NOTE: The following procedure is required by the Windows Form Designer
   'It can be modified using the Windows Form Designer.  
   'Do not modify it using the code editor.
   <System.Diagnostics.DebuggerStepThrough()> _
   Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject2 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject3 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject4 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject5 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject6 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject7 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject8 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject9 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject10 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject11 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject12 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject13 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject14 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucSectionSummary))
        Dim SerializableAppearanceObject15 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject16 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject17 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject18 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.seSwellingCount = New DevExpress.XtraEditors.SpinEdit()
        Me.chkInternalFouling = New DevExpress.XtraEditors.CheckEdit()
        Me.bsSummaryPipeOrBendData = New System.Windows.Forms.BindingSource(Me.components)
        Me.QTT_SummaryData = New QIG.AutoReporter.QTT_SummaryData()
        Me.btnPaste = New DevExpress.XtraEditors.SimpleButton()
        Me.radPipeOrBend = New DevExpress.XtraEditors.RadioGroup()
        Me.uiPipeOrBendDataInfo = New DevExpress.XtraGrid.GridControl()
        Me.uiPipeOrBendDataInfoView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFK_SummaryPipeData = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPipeID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMinWall = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLocation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEstWallLoss = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEstRemWall = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAvgWall = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDesWall = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAvgDiam = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colConvectionOrRadiant = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PasteButtons = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.bsSummaryPipeDataInfo = New System.Windows.Forms.BindingSource(Me.components)
        Me.uiPipeSection = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiInternalOrExternal = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uilblGeneralOrLocal = New System.Windows.Forms.Label()
        Me.uiGeneralOrLocal = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uilblInternalOrExternal = New System.Windows.Forms.Label()
        Me.btnWriteSectionSummary = New DevExpress.XtraEditors.SimpleButton()
        Me.uiSectionSummaryTextbox = New DevExpress.XtraRichEdit.RichEditControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.lblAboveThreshold = New DevExpress.XtraEditors.LabelControl()
        Me.seDentThreshold = New DevExpress.XtraEditors.SpinEdit()
        Me.seOvalityThreshold = New DevExpress.XtraEditors.SpinEdit()
        Me.seSwellThreshold = New DevExpress.XtraEditors.SpinEdit()
        Me.seBulgeThreshold = New DevExpress.XtraEditors.SpinEdit()
        Me.lblDeformations = New System.Windows.Forms.Label()
        Me.SEDenting = New DevExpress.XtraEditors.SpinEdit()
        Me.lblDent = New DevExpress.XtraEditors.LabelControl()
        Me.SEOvality = New DevExpress.XtraEditors.SpinEdit()
        Me.lblOvality = New DevExpress.XtraEditors.LabelControl()
        Me.SESwelling = New DevExpress.XtraEditors.SpinEdit()
        Me.lblSwell = New DevExpress.XtraEditors.LabelControl()
        Me.SEBulging = New DevExpress.XtraEditors.SpinEdit()
        Me.lblBulge = New DevExpress.XtraEditors.LabelControl()
        Me.uiUnits = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.bsSectionSummary = New System.Windows.Forms.BindingSource(Me.components)
        Me.uiNewUsedPipes = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uilblNewUsedPipes = New System.Windows.Forms.Label()
        Me.uilblUnits = New System.Windows.Forms.Label()
        Me.taTblSummary = New QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryTableAdapter()
        Me.taTblSummaryPipeData = New QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryPipeDataTableAdapter()
        Me.taTblSummaryBendData = New QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryBendDataTableAdapter()
        Me.taTblSummaryBendDataInfo = New QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryBendDataInfoTableAdapter()
        Me.taTblSummaryPipeDataInfo = New QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryPipeDataInfoTableAdapter()
        Me.bsSummaryBendDataInfo = New System.Windows.Forms.BindingSource(Me.components)
        Me.uiSecSumShowHideButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiSectionSummaryEditor = New DevExpress.XtraRichEdit.RichEditControl()
        Me.bsSectionSummaries = New System.Windows.Forms.BindingSource(Me.components)
        Me.uiClearSectionSummaryButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiAppendSectionSummaryButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiReplaceSectionSummaryButton = New DevExpress.XtraEditors.SimpleButton()
        Me.taTblPipeTubeItemSummaries = New QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblPipeTubeItemSummariesTableAdapter()
        Me.taTblPipeTubeItems = New QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblPipeTubeItemsTableAdapter()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.seDentingBelow = New DevExpress.XtraEditors.SpinEdit()
        Me.seOvalityBelow = New DevExpress.XtraEditors.SpinEdit()
        Me.seSwellingBelow = New DevExpress.XtraEditors.SpinEdit()
        Me.seBulgingBelow = New DevExpress.XtraEditors.SpinEdit()
        Me.QTT_PipeTubeDetails = New QIG.AutoReporter.QTT_PipeTubeDetails()
        Me.TblPipeTubeItemsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblPipeTubeItemsTableAdapter = New QIG.AutoReporter.QTT_PipeTubeDetailsTableAdapters.tblPipeTubeItemsTableAdapter()
        Me.uiSectionSelection = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.seSwellingCount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkInternalFouling.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsSummaryPipeOrBendData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.QTT_SummaryData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radPipeOrBend.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiPipeOrBendDataInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiPipeOrBendDataInfoView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PasteButtons, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsSummaryPipeDataInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiPipeSection.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiInternalOrExternal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiGeneralOrLocal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDentThreshold.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seOvalityThreshold.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seSwellThreshold.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seBulgeThreshold.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SEDenting.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SEOvality.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SESwelling.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SEBulging.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiUnits.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsSectionSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiNewUsedPipes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsSummaryBendDataInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsSectionSummaries, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDentingBelow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seOvalityBelow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seSwellingBelow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seBulgingBelow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.QTT_PipeTubeDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblPipeTubeItemsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiSectionSelection.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label93
        '
        Me.Label93.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label93.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label93.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label93.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label93.Location = New System.Drawing.Point(0, 0)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(871, 15)
        Me.Label93.TabIndex = 41
        Me.Label93.Text = "SECTION SUMMARIES WRITER"
        Me.Label93.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'seSwellingCount
        '
        Me.seSwellingCount.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seSwellingCount.Location = New System.Drawing.Point(89, 222)
        Me.seSwellingCount.Name = "seSwellingCount"
        Me.seSwellingCount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.seSwellingCount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seSwellingCount.Size = New System.Drawing.Size(43, 20)
        Me.seSwellingCount.TabIndex = 47
        '
        'chkInternalFouling
        '
        Me.chkInternalFouling.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "InternalFoulingOrNot", True))
        Me.chkInternalFouling.Location = New System.Drawing.Point(114, 277)
        Me.chkInternalFouling.Name = "chkInternalFouling"
        Me.chkInternalFouling.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.chkInternalFouling.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.chkInternalFouling.Properties.Appearance.Options.UseFont = True
        Me.chkInternalFouling.Properties.Appearance.Options.UseForeColor = True
        Me.chkInternalFouling.Properties.Caption = "Internal Fouling"
        Me.chkInternalFouling.Properties.ValueChecked = "True"
        Me.chkInternalFouling.Properties.ValueGrayed = "False"
        Me.chkInternalFouling.Properties.ValueUnchecked = "False"
        Me.chkInternalFouling.Size = New System.Drawing.Size(115, 22)
        Me.chkInternalFouling.TabIndex = 102
        '
        'bsSummaryPipeOrBendData
        '
        Me.bsSummaryPipeOrBendData.DataMember = "tblSummaryPipeData"
        Me.bsSummaryPipeOrBendData.DataSource = Me.QTT_SummaryData
        '
        'QTT_SummaryData
        '
        Me.QTT_SummaryData.DataSetName = "QTT_SummaryData"
        Me.QTT_SummaryData.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'btnPaste
        '
        Me.btnPaste.Appearance.BackColor = System.Drawing.Color.White
        Me.btnPaste.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.btnPaste.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.btnPaste.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPaste.Appearance.Options.UseBackColor = True
        Me.btnPaste.Appearance.Options.UseBorderColor = True
        Me.btnPaste.Appearance.Options.UseFont = True
        Me.btnPaste.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.btnPaste.Location = New System.Drawing.Point(761, 26)
        Me.btnPaste.Name = "btnPaste"
        Me.btnPaste.Size = New System.Drawing.Size(110, 33)
        Me.btnPaste.TabIndex = 198
        Me.btnPaste.Text = "Paste Excel Data"
        '
        'radPipeOrBend
        '
        Me.radPipeOrBend.EditValue = "Pipe"
        Me.radPipeOrBend.Location = New System.Drawing.Point(598, 72)
        Me.radPipeOrBend.Name = "radPipeOrBend"
        Me.radPipeOrBend.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.radPipeOrBend.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radPipeOrBend.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.radPipeOrBend.Properties.Appearance.Options.UseBackColor = True
        Me.radPipeOrBend.Properties.Appearance.Options.UseFont = True
        Me.radPipeOrBend.Properties.Appearance.Options.UseForeColor = True
        Me.radPipeOrBend.Properties.Columns = 2
        Me.radPipeOrBend.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem("Pipe", "Pipe"), New DevExpress.XtraEditors.Controls.RadioGroupItem("Bend", "Bend")})
        Me.radPipeOrBend.Size = New System.Drawing.Size(119, 24)
        Me.radPipeOrBend.TabIndex = 197
        '
        'uiPipeOrBendDataInfo
        '
        Me.uiPipeOrBendDataInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.CancelEdit.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Edit.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.EndEdit.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.First.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.First.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Last.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Last.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Next.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Next.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.NextPage.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.NextPage.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Prev.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.Prev.Visible = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.PrevPage.Enabled = False
        Me.uiPipeOrBendDataInfo.EmbeddedNavigator.Buttons.PrevPage.Visible = False
        Me.uiPipeOrBendDataInfo.Location = New System.Drawing.Point(326, 102)
        Me.uiPipeOrBendDataInfo.MainView = Me.uiPipeOrBendDataInfoView
        Me.uiPipeOrBendDataInfo.Name = "uiPipeOrBendDataInfo"
        Me.uiPipeOrBendDataInfo.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.PasteButtons})
        Me.uiPipeOrBendDataInfo.Size = New System.Drawing.Size(545, 104)
        Me.uiPipeOrBendDataInfo.TabIndex = 193
        Me.uiPipeOrBendDataInfo.UseEmbeddedNavigator = True
        Me.uiPipeOrBendDataInfo.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uiPipeOrBendDataInfoView})
        '
        'uiPipeOrBendDataInfoView
        '
        Me.uiPipeOrBendDataInfoView.Appearance.ColumnFilterButton.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPipeOrBendDataInfoView.Appearance.ColumnFilterButton.Options.UseFont = True
        Me.uiPipeOrBendDataInfoView.ColumnPanelRowHeight = 35
        Me.uiPipeOrBendDataInfoView.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID, Me.colFK_SummaryPipeData, Me.colPipeID, Me.colMinWall, Me.colLocation, Me.colEstWallLoss, Me.colEstRemWall, Me.colAvgWall, Me.colDesWall, Me.colAvgDiam, Me.colConvectionOrRadiant})
        Me.uiPipeOrBendDataInfoView.GridControl = Me.uiPipeOrBendDataInfo
        Me.uiPipeOrBendDataInfoView.Name = "uiPipeOrBendDataInfoView"
        Me.uiPipeOrBendDataInfoView.OptionsCustomization.AllowColumnMoving = False
        Me.uiPipeOrBendDataInfoView.OptionsMenu.EnableColumnMenu = False
        Me.uiPipeOrBendDataInfoView.OptionsView.ColumnAutoWidth = False
        Me.uiPipeOrBendDataInfoView.OptionsView.EnableAppearanceEvenRow = True
        Me.uiPipeOrBendDataInfoView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.uiPipeOrBendDataInfoView.OptionsView.ShowGroupPanel = False
        '
        'colID
        '
        Me.colID.FieldName = "ID"
        Me.colID.Name = "colID"
        Me.colID.OptionsColumn.ReadOnly = True
        Me.colID.Visible = True
        Me.colID.VisibleIndex = 0
        '
        'colFK_SummaryPipeData
        '
        Me.colFK_SummaryPipeData.FieldName = "FK_SummaryPipeData"
        Me.colFK_SummaryPipeData.Name = "colFK_SummaryPipeData"
        Me.colFK_SummaryPipeData.Visible = True
        Me.colFK_SummaryPipeData.VisibleIndex = 1
        '
        'colPipeID
        '
        Me.colPipeID.FieldName = "PipeID"
        Me.colPipeID.Name = "colPipeID"
        Me.colPipeID.Visible = True
        Me.colPipeID.VisibleIndex = 2
        '
        'colMinWall
        '
        Me.colMinWall.FieldName = "MinWall"
        Me.colMinWall.Name = "colMinWall"
        Me.colMinWall.Visible = True
        Me.colMinWall.VisibleIndex = 3
        '
        'colLocation
        '
        Me.colLocation.FieldName = "Location"
        Me.colLocation.Name = "colLocation"
        Me.colLocation.Visible = True
        Me.colLocation.VisibleIndex = 4
        '
        'colEstWallLoss
        '
        Me.colEstWallLoss.FieldName = "EstWallLoss"
        Me.colEstWallLoss.Name = "colEstWallLoss"
        Me.colEstWallLoss.Visible = True
        Me.colEstWallLoss.VisibleIndex = 5
        '
        'colEstRemWall
        '
        Me.colEstRemWall.FieldName = "EstRemWall"
        Me.colEstRemWall.Name = "colEstRemWall"
        Me.colEstRemWall.Visible = True
        Me.colEstRemWall.VisibleIndex = 6
        '
        'colAvgWall
        '
        Me.colAvgWall.FieldName = "AvgWall"
        Me.colAvgWall.Name = "colAvgWall"
        Me.colAvgWall.Visible = True
        Me.colAvgWall.VisibleIndex = 7
        '
        'colDesWall
        '
        Me.colDesWall.FieldName = "DesWall"
        Me.colDesWall.Name = "colDesWall"
        Me.colDesWall.Visible = True
        Me.colDesWall.VisibleIndex = 8
        '
        'colAvgDiam
        '
        Me.colAvgDiam.FieldName = "AvgDiam"
        Me.colAvgDiam.Name = "colAvgDiam"
        Me.colAvgDiam.Visible = True
        Me.colAvgDiam.VisibleIndex = 9
        '
        'colConvectionOrRadiant
        '
        Me.colConvectionOrRadiant.FieldName = "ConvectionOrRadiant"
        Me.colConvectionOrRadiant.Name = "colConvectionOrRadiant"
        Me.colConvectionOrRadiant.Visible = True
        Me.colConvectionOrRadiant.VisibleIndex = 10
        '
        'PasteButtons
        '
        Me.PasteButtons.AutoHeight = False
        Me.PasteButtons.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.PasteButtons.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Paste", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V)), SerializableAppearanceObject1, "Paste a row of Excel data into the table.", Nothing, Nothing, True)})
        Me.PasteButtons.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.PasteButtons.Name = "PasteButtons"
        '
        'bsSummaryPipeDataInfo
        '
        Me.bsSummaryPipeDataInfo.DataMember = "tblSummaryPipeDataInfo"
        Me.bsSummaryPipeDataInfo.DataSource = Me.QTT_SummaryData
        '
        'uiPipeSection
        '
        Me.uiPipeSection.Location = New System.Drawing.Point(530, 26)
        Me.uiPipeSection.Name = "uiPipeSection"
        Me.uiPipeSection.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPipeSection.Properties.Appearance.Options.UseFont = True
        Me.uiPipeSection.Properties.Appearance.Options.UseTextOptions = True
        Me.uiPipeSection.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiPipeSection.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPipeSection.Properties.AppearanceDisabled.Options.UseFont = True
        Me.uiPipeSection.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPipeSection.Properties.AppearanceDropDown.Options.UseFont = True
        Me.uiPipeSection.Properties.AppearanceDropDown.Options.UseTextOptions = True
        Me.uiPipeSection.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiPipeSection.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPipeSection.Properties.AppearanceFocused.Options.UseFont = True
        Me.uiPipeSection.Properties.AppearanceFocused.Options.UseTextOptions = True
        Me.uiPipeSection.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiPipeSection.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiPipeSection.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.uiPipeSection.Properties.AutoHeight = False
        SerializableAppearanceObject2.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject2.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject2.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject2.Options.UseBackColor = True
        SerializableAppearanceObject2.Options.UseBorderColor = True
        Me.uiPipeSection.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject2, "", Nothing, Nothing, True)})
        Me.uiPipeSection.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiPipeSection.Properties.DropDownRows = 11
        Me.uiPipeSection.Properties.Items.AddRange(New Object() {"Other", "2", "2.5", "3", "3.5", "4", "5", "6", "8", "10", "12"})
        Me.uiPipeSection.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiPipeSection.Size = New System.Drawing.Size(147, 24)
        Me.uiPipeSection.TabIndex = 157
        '
        'uiInternalOrExternal
        '
        Me.uiInternalOrExternal.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "InternalOrExternal", True))
        Me.uiInternalOrExternal.Location = New System.Drawing.Point(454, 72)
        Me.uiInternalOrExternal.Name = "uiInternalOrExternal"
        Me.uiInternalOrExternal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiInternalOrExternal.Properties.Appearance.Options.UseFont = True
        Me.uiInternalOrExternal.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiInternalOrExternal.Properties.AppearanceDisabled.Options.UseFont = True
        Me.uiInternalOrExternal.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiInternalOrExternal.Properties.AppearanceDropDown.Options.UseFont = True
        Me.uiInternalOrExternal.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiInternalOrExternal.Properties.AppearanceFocused.Options.UseFont = True
        Me.uiInternalOrExternal.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiInternalOrExternal.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.uiInternalOrExternal.Properties.AutoHeight = False
        SerializableAppearanceObject3.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject3.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject3.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject3.Options.UseBackColor = True
        SerializableAppearanceObject3.Options.UseBorderColor = True
        Me.uiInternalOrExternal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject3, "", Nothing, Nothing, True)})
        Me.uiInternalOrExternal.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiInternalOrExternal.Properties.Items.AddRange(New Object() {"internal", "external", "internal and external"})
        Me.uiInternalOrExternal.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiInternalOrExternal.Size = New System.Drawing.Size(138, 24)
        Me.uiInternalOrExternal.TabIndex = 154
        '
        'uilblGeneralOrLocal
        '
        Me.uilblGeneralOrLocal.AutoSize = True
        Me.uilblGeneralOrLocal.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uilblGeneralOrLocal.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uilblGeneralOrLocal.Location = New System.Drawing.Point(326, 54)
        Me.uilblGeneralOrLocal.Name = "uilblGeneralOrLocal"
        Me.uilblGeneralOrLocal.Size = New System.Drawing.Size(95, 15)
        Me.uilblGeneralOrLocal.TabIndex = 163
        Me.uilblGeneralOrLocal.Text = "General or Local:"
        Me.uilblGeneralOrLocal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiGeneralOrLocal
        '
        Me.uiGeneralOrLocal.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "GenOrLocal", True))
        Me.uiGeneralOrLocal.Location = New System.Drawing.Point(326, 72)
        Me.uiGeneralOrLocal.Name = "uiGeneralOrLocal"
        Me.uiGeneralOrLocal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiGeneralOrLocal.Properties.Appearance.Options.UseFont = True
        Me.uiGeneralOrLocal.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiGeneralOrLocal.Properties.AppearanceDisabled.Options.UseFont = True
        Me.uiGeneralOrLocal.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiGeneralOrLocal.Properties.AppearanceDropDown.Options.UseFont = True
        Me.uiGeneralOrLocal.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiGeneralOrLocal.Properties.AppearanceFocused.Options.UseFont = True
        Me.uiGeneralOrLocal.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiGeneralOrLocal.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.uiGeneralOrLocal.Properties.AutoHeight = False
        SerializableAppearanceObject4.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject4.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject4.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject4.Options.UseBackColor = True
        SerializableAppearanceObject4.Options.UseBorderColor = True
        Me.uiGeneralOrLocal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject4, "", Nothing, Nothing, True)})
        Me.uiGeneralOrLocal.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiGeneralOrLocal.Properties.Items.AddRange(New Object() {"General", "Localized", "General and localized"})
        Me.uiGeneralOrLocal.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiGeneralOrLocal.Size = New System.Drawing.Size(122, 24)
        Me.uiGeneralOrLocal.TabIndex = 152
        '
        'uilblInternalOrExternal
        '
        Me.uilblInternalOrExternal.AutoSize = True
        Me.uilblInternalOrExternal.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uilblInternalOrExternal.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uilblInternalOrExternal.Location = New System.Drawing.Point(454, 54)
        Me.uilblInternalOrExternal.Name = "uilblInternalOrExternal"
        Me.uilblInternalOrExternal.Size = New System.Drawing.Size(108, 15)
        Me.uilblInternalOrExternal.TabIndex = 164
        Me.uilblInternalOrExternal.Text = "Internal or External:"
        Me.uilblInternalOrExternal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnWriteSectionSummary
        '
        Me.btnWriteSectionSummary.Appearance.BackColor = System.Drawing.Color.White
        Me.btnWriteSectionSummary.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.btnWriteSectionSummary.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.btnWriteSectionSummary.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnWriteSectionSummary.Appearance.Options.UseBackColor = True
        Me.btnWriteSectionSummary.Appearance.Options.UseBorderColor = True
        Me.btnWriteSectionSummary.Appearance.Options.UseFont = True
        Me.btnWriteSectionSummary.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.btnWriteSectionSummary.Location = New System.Drawing.Point(761, 63)
        Me.btnWriteSectionSummary.Name = "btnWriteSectionSummary"
        Me.btnWriteSectionSummary.Size = New System.Drawing.Size(110, 33)
        Me.btnWriteSectionSummary.TabIndex = 159
        Me.btnWriteSectionSummary.Text = "Write Summary"
        '
        'uiSectionSummaryTextbox
        '
        Me.uiSectionSummaryTextbox.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
        Me.uiSectionSummaryTextbox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uiSectionSummaryTextbox.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiSectionSummaryTextbox.Appearance.Text.Options.UseFont = True
        Me.uiSectionSummaryTextbox.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.uiSectionSummaryTextbox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsSummaryPipeOrBendData, "Paragraph", True))
        Me.uiSectionSummaryTextbox.Location = New System.Drawing.Point(326, 209)
        Me.uiSectionSummaryTextbox.Margin = New System.Windows.Forms.Padding(0)
        Me.uiSectionSummaryTextbox.Name = "uiSectionSummaryTextbox"
        Me.uiSectionSummaryTextbox.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden
        Me.uiSectionSummaryTextbox.Options.HorizontalScrollbar.Visibility = DevExpress.XtraRichEdit.RichEditScrollbarVisibility.Hidden
        Me.uiSectionSummaryTextbox.ReadOnly = True
        Me.uiSectionSummaryTextbox.Size = New System.Drawing.Size(545, 131)
        Me.uiSectionSummaryTextbox.TabIndex = 153
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.LabelControl2.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.LabelControl2.Location = New System.Drawing.Point(214, 144)
        Me.LabelControl2.LookAndFeel.SkinName = "Blue"
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(58, 17)
        Me.LabelControl2.TabIndex = 181
        Me.LabelControl2.Text = "Threshold"
        '
        'lblAboveThreshold
        '
        Me.lblAboveThreshold.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.lblAboveThreshold.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblAboveThreshold.Location = New System.Drawing.Point(120, 144)
        Me.lblAboveThreshold.Name = "lblAboveThreshold"
        Me.lblAboveThreshold.Size = New System.Drawing.Size(37, 17)
        Me.lblAboveThreshold.TabIndex = 180
        Me.lblAboveThreshold.Text = "Above" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'seDentThreshold
        '
        Me.seDentThreshold.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "DentingThreshold", True))
        Me.seDentThreshold.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDentThreshold.Location = New System.Drawing.Point(214, 249)
        Me.seDentThreshold.Name = "seDentThreshold"
        Me.seDentThreshold.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.seDentThreshold.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.seDentThreshold.Properties.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.seDentThreshold.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.seDentThreshold.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seDentThreshold.Properties.Appearance.Options.UseBackColor = True
        Me.seDentThreshold.Properties.Appearance.Options.UseBorderColor = True
        Me.seDentThreshold.Properties.Appearance.Options.UseFont = True
        Me.seDentThreshold.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seDentThreshold.Properties.AppearanceDisabled.Options.UseFont = True
        Me.seDentThreshold.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seDentThreshold.Properties.AppearanceFocused.Options.UseFont = True
        Me.seDentThreshold.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seDentThreshold.Properties.AppearanceReadOnly.Options.UseFont = True
        SerializableAppearanceObject5.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject5.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject5.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject5.Options.UseBackColor = True
        SerializableAppearanceObject5.Options.UseBorderColor = True
        Me.seDentThreshold.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject5, "", Nothing, Nothing, True)})
        Me.seDentThreshold.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.seDentThreshold.Properties.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.seDentThreshold.Properties.Mask.EditMask = "n1"
        Me.seDentThreshold.Properties.MaxValue = New Decimal(New Integer() {50, 0, 0, 0})
        Me.seDentThreshold.Size = New System.Drawing.Size(58, 22)
        Me.seDentThreshold.TabIndex = 179
        '
        'seOvalityThreshold
        '
        Me.seOvalityThreshold.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "OvalityThreshold", True))
        Me.seOvalityThreshold.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seOvalityThreshold.Location = New System.Drawing.Point(213, 221)
        Me.seOvalityThreshold.Name = "seOvalityThreshold"
        Me.seOvalityThreshold.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.seOvalityThreshold.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.seOvalityThreshold.Properties.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.seOvalityThreshold.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.seOvalityThreshold.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seOvalityThreshold.Properties.Appearance.Options.UseBackColor = True
        Me.seOvalityThreshold.Properties.Appearance.Options.UseBorderColor = True
        Me.seOvalityThreshold.Properties.Appearance.Options.UseFont = True
        Me.seOvalityThreshold.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seOvalityThreshold.Properties.AppearanceDisabled.Options.UseFont = True
        Me.seOvalityThreshold.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seOvalityThreshold.Properties.AppearanceFocused.Options.UseFont = True
        Me.seOvalityThreshold.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seOvalityThreshold.Properties.AppearanceReadOnly.Options.UseFont = True
        SerializableAppearanceObject6.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject6.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject6.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject6.Options.UseBackColor = True
        SerializableAppearanceObject6.Options.UseBorderColor = True
        Me.seOvalityThreshold.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject6, "", Nothing, Nothing, True)})
        Me.seOvalityThreshold.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.seOvalityThreshold.Properties.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.seOvalityThreshold.Properties.Mask.EditMask = "n1"
        Me.seOvalityThreshold.Properties.MaxValue = New Decimal(New Integer() {50, 0, 0, 0})
        Me.seOvalityThreshold.Size = New System.Drawing.Size(58, 22)
        Me.seOvalityThreshold.TabIndex = 178
        '
        'seSwellThreshold
        '
        Me.seSwellThreshold.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "SwellingThreshold", True))
        Me.seSwellThreshold.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seSwellThreshold.Location = New System.Drawing.Point(213, 193)
        Me.seSwellThreshold.Name = "seSwellThreshold"
        Me.seSwellThreshold.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.seSwellThreshold.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.seSwellThreshold.Properties.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.seSwellThreshold.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.seSwellThreshold.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seSwellThreshold.Properties.Appearance.Options.UseBackColor = True
        Me.seSwellThreshold.Properties.Appearance.Options.UseBorderColor = True
        Me.seSwellThreshold.Properties.Appearance.Options.UseFont = True
        Me.seSwellThreshold.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seSwellThreshold.Properties.AppearanceDisabled.Options.UseFont = True
        Me.seSwellThreshold.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seSwellThreshold.Properties.AppearanceFocused.Options.UseFont = True
        Me.seSwellThreshold.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seSwellThreshold.Properties.AppearanceReadOnly.Options.UseFont = True
        SerializableAppearanceObject7.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject7.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject7.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject7.Options.UseBackColor = True
        SerializableAppearanceObject7.Options.UseBorderColor = True
        Me.seSwellThreshold.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject7, "", Nothing, Nothing, True)})
        Me.seSwellThreshold.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.seSwellThreshold.Properties.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.seSwellThreshold.Properties.Mask.EditMask = "n1"
        Me.seSwellThreshold.Properties.MaxValue = New Decimal(New Integer() {50, 0, 0, 0})
        Me.seSwellThreshold.Size = New System.Drawing.Size(58, 22)
        Me.seSwellThreshold.TabIndex = 177
        '
        'seBulgeThreshold
        '
        Me.seBulgeThreshold.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "BulgingThreshold", True))
        Me.seBulgeThreshold.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seBulgeThreshold.Location = New System.Drawing.Point(213, 165)
        Me.seBulgeThreshold.Name = "seBulgeThreshold"
        Me.seBulgeThreshold.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.seBulgeThreshold.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.seBulgeThreshold.Properties.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.seBulgeThreshold.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.seBulgeThreshold.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seBulgeThreshold.Properties.Appearance.Options.UseBackColor = True
        Me.seBulgeThreshold.Properties.Appearance.Options.UseBorderColor = True
        Me.seBulgeThreshold.Properties.Appearance.Options.UseFont = True
        Me.seBulgeThreshold.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seBulgeThreshold.Properties.AppearanceDisabled.Options.UseFont = True
        Me.seBulgeThreshold.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seBulgeThreshold.Properties.AppearanceFocused.Options.UseFont = True
        Me.seBulgeThreshold.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seBulgeThreshold.Properties.AppearanceReadOnly.Options.UseFont = True
        SerializableAppearanceObject8.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject8.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject8.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject8.Options.UseBackColor = True
        SerializableAppearanceObject8.Options.UseBorderColor = True
        Me.seBulgeThreshold.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject8, "", Nothing, Nothing, True)})
        Me.seBulgeThreshold.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.seBulgeThreshold.Properties.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.seBulgeThreshold.Properties.Mask.EditMask = "n1"
        Me.seBulgeThreshold.Properties.MaxValue = New Decimal(New Integer() {50, 0, 0, 0})
        Me.seBulgeThreshold.Size = New System.Drawing.Size(58, 22)
        Me.seBulgeThreshold.TabIndex = 176
        '
        'lblDeformations
        '
        Me.lblDeformations.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.lblDeformations.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeformations.ForeColor = System.Drawing.Color.GhostWhite
        Me.lblDeformations.Location = New System.Drawing.Point(0, 120)
        Me.lblDeformations.Name = "lblDeformations"
        Me.lblDeformations.Size = New System.Drawing.Size(310, 15)
        Me.lblDeformations.TabIndex = 175
        Me.lblDeformations.Text = "DEFORMATIONS"
        Me.lblDeformations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SEDenting
        '
        Me.SEDenting.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "DentingExceedingCount", True))
        Me.SEDenting.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.SEDenting.Location = New System.Drawing.Point(116, 249)
        Me.SEDenting.Name = "SEDenting"
        Me.SEDenting.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.SEDenting.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.SEDenting.Properties.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.SEDenting.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.SEDenting.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SEDenting.Properties.Appearance.Options.UseBackColor = True
        Me.SEDenting.Properties.Appearance.Options.UseBorderColor = True
        Me.SEDenting.Properties.Appearance.Options.UseFont = True
        Me.SEDenting.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SEDenting.Properties.AppearanceDisabled.Options.UseFont = True
        Me.SEDenting.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SEDenting.Properties.AppearanceFocused.Options.UseFont = True
        Me.SEDenting.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SEDenting.Properties.AppearanceReadOnly.Options.UseFont = True
        SerializableAppearanceObject9.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject9.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject9.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject9.Options.UseBackColor = True
        SerializableAppearanceObject9.Options.UseBorderColor = True
        Me.SEDenting.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject9, "", Nothing, Nothing, True)})
        Me.SEDenting.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.SEDenting.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.SEDenting.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.SEDenting.Properties.IsFloatValue = False
        Me.SEDenting.Properties.Mask.EditMask = "n0"
        Me.SEDenting.Properties.MaxValue = New Decimal(New Integer() {75, 0, 0, 0})
        Me.SEDenting.Size = New System.Drawing.Size(43, 22)
        Me.SEDenting.TabIndex = 174
        '
        'lblDent
        '
        Me.lblDent.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.lblDent.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblDent.Location = New System.Drawing.Point(62, 250)
        Me.lblDent.Name = "lblDent"
        Me.lblDent.Size = New System.Drawing.Size(48, 17)
        Me.lblDent.TabIndex = 173
        Me.lblDent.Text = "Denting:"
        '
        'SEOvality
        '
        Me.SEOvality.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "OvalityExceedingCount", True))
        Me.SEOvality.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.SEOvality.Location = New System.Drawing.Point(116, 221)
        Me.SEOvality.Name = "SEOvality"
        Me.SEOvality.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.SEOvality.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.SEOvality.Properties.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.SEOvality.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.SEOvality.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SEOvality.Properties.Appearance.Options.UseBackColor = True
        Me.SEOvality.Properties.Appearance.Options.UseBorderColor = True
        Me.SEOvality.Properties.Appearance.Options.UseFont = True
        Me.SEOvality.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SEOvality.Properties.AppearanceDisabled.Options.UseFont = True
        Me.SEOvality.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SEOvality.Properties.AppearanceFocused.Options.UseFont = True
        Me.SEOvality.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SEOvality.Properties.AppearanceReadOnly.Options.UseFont = True
        SerializableAppearanceObject10.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject10.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject10.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject10.Options.UseBackColor = True
        SerializableAppearanceObject10.Options.UseBorderColor = True
        Me.SEOvality.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject10, "", Nothing, Nothing, True)})
        Me.SEOvality.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.SEOvality.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.SEOvality.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.SEOvality.Properties.IsFloatValue = False
        Me.SEOvality.Properties.Mask.EditMask = "n0"
        Me.SEOvality.Properties.MaxValue = New Decimal(New Integer() {75, 0, 0, 0})
        Me.SEOvality.Size = New System.Drawing.Size(43, 22)
        Me.SEOvality.TabIndex = 172
        '
        'lblOvality
        '
        Me.lblOvality.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.lblOvality.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblOvality.Location = New System.Drawing.Point(68, 222)
        Me.lblOvality.Name = "lblOvality"
        Me.lblOvality.Size = New System.Drawing.Size(42, 17)
        Me.lblOvality.TabIndex = 171
        Me.lblOvality.Text = "Ovality:"
        '
        'SESwelling
        '
        Me.SESwelling.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "SwellingExceedingCount", True))
        Me.SESwelling.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.SESwelling.Location = New System.Drawing.Point(116, 193)
        Me.SESwelling.Name = "SESwelling"
        Me.SESwelling.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.SESwelling.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.SESwelling.Properties.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.SESwelling.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.SESwelling.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SESwelling.Properties.Appearance.Options.UseBackColor = True
        Me.SESwelling.Properties.Appearance.Options.UseBorderColor = True
        Me.SESwelling.Properties.Appearance.Options.UseFont = True
        Me.SESwelling.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SESwelling.Properties.AppearanceDisabled.Options.UseFont = True
        Me.SESwelling.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SESwelling.Properties.AppearanceFocused.Options.UseFont = True
        Me.SESwelling.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SESwelling.Properties.AppearanceReadOnly.Options.UseFont = True
        SerializableAppearanceObject11.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject11.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject11.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject11.Options.UseBackColor = True
        SerializableAppearanceObject11.Options.UseBorderColor = True
        Me.SESwelling.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject11, "", Nothing, Nothing, True)})
        Me.SESwelling.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.SESwelling.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.SESwelling.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.SESwelling.Properties.IsFloatValue = False
        Me.SESwelling.Properties.Mask.EditMask = "n0"
        Me.SESwelling.Properties.MaxValue = New Decimal(New Integer() {75, 0, 0, 0})
        Me.SESwelling.Size = New System.Drawing.Size(43, 22)
        Me.SESwelling.TabIndex = 170
        '
        'lblSwell
        '
        Me.lblSwell.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.lblSwell.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblSwell.Location = New System.Drawing.Point(60, 194)
        Me.lblSwell.Name = "lblSwell"
        Me.lblSwell.Size = New System.Drawing.Size(50, 17)
        Me.lblSwell.TabIndex = 169
        Me.lblSwell.Text = "Swelling:"
        '
        'SEBulging
        '
        Me.SEBulging.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "BulgingExceedingCount", True))
        Me.SEBulging.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.SEBulging.Location = New System.Drawing.Point(116, 165)
        Me.SEBulging.Name = "SEBulging"
        Me.SEBulging.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.SEBulging.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.SEBulging.Properties.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.SEBulging.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.SEBulging.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SEBulging.Properties.Appearance.Options.UseBackColor = True
        Me.SEBulging.Properties.Appearance.Options.UseBorderColor = True
        Me.SEBulging.Properties.Appearance.Options.UseFont = True
        Me.SEBulging.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SEBulging.Properties.AppearanceDisabled.Options.UseFont = True
        Me.SEBulging.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SEBulging.Properties.AppearanceFocused.Options.UseFont = True
        Me.SEBulging.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SEBulging.Properties.AppearanceReadOnly.Options.UseFont = True
        SerializableAppearanceObject12.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject12.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject12.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject12.Options.UseBackColor = True
        SerializableAppearanceObject12.Options.UseBorderColor = True
        Me.SEBulging.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject12, "", Nothing, Nothing, True)})
        Me.SEBulging.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.SEBulging.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.SEBulging.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.SEBulging.Properties.IsFloatValue = False
        Me.SEBulging.Properties.Mask.EditMask = "n0"
        Me.SEBulging.Properties.MaxValue = New Decimal(New Integer() {75, 0, 0, 0})
        Me.SEBulging.Size = New System.Drawing.Size(43, 22)
        Me.SEBulging.TabIndex = 168
        '
        'lblBulge
        '
        Me.lblBulge.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.lblBulge.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblBulge.Location = New System.Drawing.Point(64, 166)
        Me.lblBulge.Name = "lblBulge"
        Me.lblBulge.Size = New System.Drawing.Size(46, 17)
        Me.lblBulge.TabIndex = 167
        Me.lblBulge.Text = "Bulging:"
        '
        'uiUnits
        '
        Me.uiUnits.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSectionSummary, "LeadingUnits", True))
        Me.uiUnits.Location = New System.Drawing.Point(113, 57)
        Me.uiUnits.Name = "uiUnits"
        Me.uiUnits.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiUnits.Properties.Appearance.Options.UseFont = True
        Me.uiUnits.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiUnits.Properties.AppearanceDisabled.Options.UseFont = True
        Me.uiUnits.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiUnits.Properties.AppearanceDropDown.Options.UseFont = True
        Me.uiUnits.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiUnits.Properties.AppearanceFocused.Options.UseFont = True
        Me.uiUnits.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiUnits.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.uiUnits.Properties.AutoHeight = False
        SerializableAppearanceObject13.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject13.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject13.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject13.Options.UseBackColor = True
        SerializableAppearanceObject13.Options.UseBorderColor = True
        Me.uiUnits.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject13, "", Nothing, Nothing, True)})
        Me.uiUnits.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiUnits.Properties.Items.AddRange(New Object() {"Imperial", "Metric"})
        Me.uiUnits.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiUnits.Size = New System.Drawing.Size(150, 24)
        Me.uiUnits.TabIndex = 155
        '
        'bsSectionSummary
        '
        Me.bsSectionSummary.DataMember = "tblSummary"
        Me.bsSectionSummary.DataSource = Me.QTT_SummaryData
        '
        'uiNewUsedPipes
        '
        Me.uiNewUsedPipes.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSectionSummary, "NewOrUsedPipes", True))
        Me.uiNewUsedPipes.Location = New System.Drawing.Point(113, 26)
        Me.uiNewUsedPipes.Name = "uiNewUsedPipes"
        Me.uiNewUsedPipes.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiNewUsedPipes.Properties.Appearance.Options.UseFont = True
        Me.uiNewUsedPipes.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiNewUsedPipes.Properties.AppearanceDisabled.Options.UseFont = True
        Me.uiNewUsedPipes.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiNewUsedPipes.Properties.AppearanceDropDown.Options.UseFont = True
        Me.uiNewUsedPipes.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiNewUsedPipes.Properties.AppearanceFocused.Options.UseFont = True
        Me.uiNewUsedPipes.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiNewUsedPipes.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.uiNewUsedPipes.Properties.AutoHeight = False
        SerializableAppearanceObject14.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject14.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject14.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject14.Options.UseBackColor = True
        SerializableAppearanceObject14.Options.UseBorderColor = True
        Me.uiNewUsedPipes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject14, "", Nothing, Nothing, True)})
        Me.uiNewUsedPipes.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiNewUsedPipes.Properties.Items.AddRange(New Object() {"New Pipes (Baseline)", "In-Service Pipes"})
        Me.uiNewUsedPipes.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiNewUsedPipes.Size = New System.Drawing.Size(150, 24)
        Me.uiNewUsedPipes.TabIndex = 151
        '
        'uilblNewUsedPipes
        '
        Me.uilblNewUsedPipes.AutoSize = True
        Me.uilblNewUsedPipes.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uilblNewUsedPipes.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uilblNewUsedPipes.Location = New System.Drawing.Point(3, 26)
        Me.uilblNewUsedPipes.Name = "uilblNewUsedPipes"
        Me.uilblNewUsedPipes.Size = New System.Drawing.Size(107, 17)
        Me.uilblNewUsedPipes.TabIndex = 160
        Me.uilblNewUsedPipes.Text = "New/Used Pipes:"
        Me.uilblNewUsedPipes.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uilblUnits
        '
        Me.uilblUnits.AutoSize = True
        Me.uilblUnits.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uilblUnits.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uilblUnits.Location = New System.Drawing.Point(20, 57)
        Me.uilblUnits.Name = "uilblUnits"
        Me.uilblUnits.Size = New System.Drawing.Size(90, 17)
        Me.uilblUnits.TabIndex = 161
        Me.uilblUnits.Text = "Leading Units:"
        Me.uilblUnits.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'taTblSummary
        '
        Me.taTblSummary.ClearBeforeFill = True
        '
        'taTblSummaryPipeData
        '
        Me.taTblSummaryPipeData.ClearBeforeFill = True
        '
        'taTblSummaryBendData
        '
        Me.taTblSummaryBendData.ClearBeforeFill = True
        '
        'taTblSummaryBendDataInfo
        '
        Me.taTblSummaryBendDataInfo.ClearBeforeFill = True
        '
        'taTblSummaryPipeDataInfo
        '
        Me.taTblSummaryPipeDataInfo.ClearBeforeFill = True
        '
        'bsSummaryBendDataInfo
        '
        Me.bsSummaryBendDataInfo.DataMember = "tblSummaryBendDataInfo"
        Me.bsSummaryBendDataInfo.DataSource = Me.QTT_SummaryData
        '
        'uiSecSumShowHideButton
        '
        Me.uiSecSumShowHideButton.Image = CType(resources.GetObject("uiSecSumShowHideButton.Image"), System.Drawing.Image)
        Me.uiSecSumShowHideButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiSecSumShowHideButton.Location = New System.Drawing.Point(11, 407)
        Me.uiSecSumShowHideButton.Name = "uiSecSumShowHideButton"
        Me.uiSecSumShowHideButton.Size = New System.Drawing.Size(27, 27)
        Me.uiSecSumShowHideButton.TabIndex = 204
        '
        'uiSectionSummaryEditor
        '
        Me.uiSectionSummaryEditor.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
        Me.uiSectionSummaryEditor.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uiSectionSummaryEditor.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 9.5!)
        Me.uiSectionSummaryEditor.Appearance.Text.Options.UseFont = True
        Me.uiSectionSummaryEditor.DataBindings.Add(New System.Windows.Forms.Binding("RtfText", Me.bsSectionSummaries, "PipeTubeSummary", True))
        Me.uiSectionSummaryEditor.Location = New System.Drawing.Point(210, 346)
        Me.uiSectionSummaryEditor.Name = "uiSectionSummaryEditor"
        Me.uiSectionSummaryEditor.Size = New System.Drawing.Size(661, 168)
        Me.uiSectionSummaryEditor.TabIndex = 203
        Me.uiSectionSummaryEditor.Views.SimpleView.Padding = New System.Windows.Forms.Padding(4, 4, 4, 0)
        '
        'bsSectionSummaries
        '
        Me.bsSectionSummaries.DataMember = "tblPipeTubeItemSummaries"
        Me.bsSectionSummaries.DataSource = Me.QTT_SummaryData
        '
        'uiClearSectionSummaryButton
        '
        Me.uiClearSectionSummaryButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiClearSectionSummaryButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiClearSectionSummaryButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiClearSectionSummaryButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiClearSectionSummaryButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiClearSectionSummaryButton.Appearance.Options.UseBackColor = True
        Me.uiClearSectionSummaryButton.Appearance.Options.UseBorderColor = True
        Me.uiClearSectionSummaryButton.Appearance.Options.UseFont = True
        Me.uiClearSectionSummaryButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiClearSectionSummaryButton.Location = New System.Drawing.Point(49, 440)
        Me.uiClearSectionSummaryButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiClearSectionSummaryButton.Name = "uiClearSectionSummaryButton"
        Me.uiClearSectionSummaryButton.Size = New System.Drawing.Size(155, 32)
        Me.uiClearSectionSummaryButton.TabIndex = 202
        Me.uiClearSectionSummaryButton.Text = "Clear All Text"
        '
        'uiAppendSectionSummaryButton
        '
        Me.uiAppendSectionSummaryButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiAppendSectionSummaryButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiAppendSectionSummaryButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiAppendSectionSummaryButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiAppendSectionSummaryButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiAppendSectionSummaryButton.Appearance.Options.UseBackColor = True
        Me.uiAppendSectionSummaryButton.Appearance.Options.UseBorderColor = True
        Me.uiAppendSectionSummaryButton.Appearance.Options.UseFont = True
        Me.uiAppendSectionSummaryButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiAppendSectionSummaryButton.Location = New System.Drawing.Point(49, 368)
        Me.uiAppendSectionSummaryButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiAppendSectionSummaryButton.Name = "uiAppendSectionSummaryButton"
        Me.uiAppendSectionSummaryButton.Size = New System.Drawing.Size(155, 32)
        Me.uiAppendSectionSummaryButton.TabIndex = 200
        Me.uiAppendSectionSummaryButton.Text = "Add Paragraph"
        '
        'uiReplaceSectionSummaryButton
        '
        Me.uiReplaceSectionSummaryButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiReplaceSectionSummaryButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiReplaceSectionSummaryButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiReplaceSectionSummaryButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiReplaceSectionSummaryButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiReplaceSectionSummaryButton.Appearance.Options.UseBackColor = True
        Me.uiReplaceSectionSummaryButton.Appearance.Options.UseBorderColor = True
        Me.uiReplaceSectionSummaryButton.Appearance.Options.UseFont = True
        Me.uiReplaceSectionSummaryButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiReplaceSectionSummaryButton.Location = New System.Drawing.Point(49, 404)
        Me.uiReplaceSectionSummaryButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiReplaceSectionSummaryButton.Name = "uiReplaceSectionSummaryButton"
        Me.uiReplaceSectionSummaryButton.Size = New System.Drawing.Size(155, 32)
        Me.uiReplaceSectionSummaryButton.TabIndex = 201
        Me.uiReplaceSectionSummaryButton.Text = "Replace Paragraph"
        '
        'taTblPipeTubeItemSummaries
        '
        Me.taTblPipeTubeItemSummaries.ClearBeforeFill = True
        '
        'taTblPipeTubeItems
        '
        Me.taTblPipeTubeItems.ClearBeforeFill = True
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.LabelControl3.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.LabelControl3.Location = New System.Drawing.Point(170, 144)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(34, 17)
        Me.LabelControl3.TabIndex = 210
        Me.LabelControl3.Text = "Below"
        '
        'seDentingBelow
        '
        Me.seDentingBelow.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "DentingNotExceedingCount", True))
        Me.seDentingBelow.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDentingBelow.Location = New System.Drawing.Point(165, 249)
        Me.seDentingBelow.Name = "seDentingBelow"
        Me.seDentingBelow.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.seDentingBelow.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.seDentingBelow.Properties.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.seDentingBelow.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.seDentingBelow.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seDentingBelow.Properties.Appearance.Options.UseBackColor = True
        Me.seDentingBelow.Properties.Appearance.Options.UseBorderColor = True
        Me.seDentingBelow.Properties.Appearance.Options.UseFont = True
        Me.seDentingBelow.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seDentingBelow.Properties.AppearanceDisabled.Options.UseFont = True
        Me.seDentingBelow.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seDentingBelow.Properties.AppearanceFocused.Options.UseFont = True
        Me.seDentingBelow.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seDentingBelow.Properties.AppearanceReadOnly.Options.UseFont = True
        SerializableAppearanceObject15.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject15.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject15.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject15.Options.UseBackColor = True
        SerializableAppearanceObject15.Options.UseBorderColor = True
        Me.seDentingBelow.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject15, "", Nothing, Nothing, True)})
        Me.seDentingBelow.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.seDentingBelow.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seDentingBelow.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seDentingBelow.Properties.IsFloatValue = False
        Me.seDentingBelow.Properties.Mask.EditMask = "n0"
        Me.seDentingBelow.Properties.MaxValue = New Decimal(New Integer() {75, 0, 0, 0})
        Me.seDentingBelow.Size = New System.Drawing.Size(43, 22)
        Me.seDentingBelow.TabIndex = 209
        '
        'seOvalityBelow
        '
        Me.seOvalityBelow.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "OvalityNotExceedingCount", True))
        Me.seOvalityBelow.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seOvalityBelow.Location = New System.Drawing.Point(165, 221)
        Me.seOvalityBelow.Name = "seOvalityBelow"
        Me.seOvalityBelow.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.seOvalityBelow.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.seOvalityBelow.Properties.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.seOvalityBelow.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.seOvalityBelow.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seOvalityBelow.Properties.Appearance.Options.UseBackColor = True
        Me.seOvalityBelow.Properties.Appearance.Options.UseBorderColor = True
        Me.seOvalityBelow.Properties.Appearance.Options.UseFont = True
        Me.seOvalityBelow.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seOvalityBelow.Properties.AppearanceDisabled.Options.UseFont = True
        Me.seOvalityBelow.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seOvalityBelow.Properties.AppearanceFocused.Options.UseFont = True
        Me.seOvalityBelow.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seOvalityBelow.Properties.AppearanceReadOnly.Options.UseFont = True
        SerializableAppearanceObject16.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject16.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject16.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject16.Options.UseBackColor = True
        SerializableAppearanceObject16.Options.UseBorderColor = True
        Me.seOvalityBelow.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject16, "", Nothing, Nothing, True)})
        Me.seOvalityBelow.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.seOvalityBelow.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seOvalityBelow.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seOvalityBelow.Properties.IsFloatValue = False
        Me.seOvalityBelow.Properties.Mask.EditMask = "n0"
        Me.seOvalityBelow.Properties.MaxValue = New Decimal(New Integer() {75, 0, 0, 0})
        Me.seOvalityBelow.Size = New System.Drawing.Size(43, 22)
        Me.seOvalityBelow.TabIndex = 208
        '
        'seSwellingBelow
        '
        Me.seSwellingBelow.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "SwellingNotExceedingCount", True))
        Me.seSwellingBelow.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seSwellingBelow.Location = New System.Drawing.Point(165, 193)
        Me.seSwellingBelow.Name = "seSwellingBelow"
        Me.seSwellingBelow.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.seSwellingBelow.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.seSwellingBelow.Properties.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.seSwellingBelow.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.seSwellingBelow.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seSwellingBelow.Properties.Appearance.Options.UseBackColor = True
        Me.seSwellingBelow.Properties.Appearance.Options.UseBorderColor = True
        Me.seSwellingBelow.Properties.Appearance.Options.UseFont = True
        Me.seSwellingBelow.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seSwellingBelow.Properties.AppearanceDisabled.Options.UseFont = True
        Me.seSwellingBelow.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seSwellingBelow.Properties.AppearanceFocused.Options.UseFont = True
        Me.seSwellingBelow.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seSwellingBelow.Properties.AppearanceReadOnly.Options.UseFont = True
        SerializableAppearanceObject17.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject17.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject17.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject17.Options.UseBackColor = True
        SerializableAppearanceObject17.Options.UseBorderColor = True
        Me.seSwellingBelow.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject17, "", Nothing, Nothing, True)})
        Me.seSwellingBelow.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.seSwellingBelow.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seSwellingBelow.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seSwellingBelow.Properties.IsFloatValue = False
        Me.seSwellingBelow.Properties.Mask.EditMask = "n0"
        Me.seSwellingBelow.Properties.MaxValue = New Decimal(New Integer() {75, 0, 0, 0})
        Me.seSwellingBelow.Size = New System.Drawing.Size(43, 22)
        Me.seSwellingBelow.TabIndex = 207
        '
        'seBulgingBelow
        '
        Me.seBulgingBelow.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsSummaryPipeOrBendData, "BulgingNotExceedingCount", True))
        Me.seBulgingBelow.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seBulgingBelow.Location = New System.Drawing.Point(165, 165)
        Me.seBulgingBelow.Name = "seBulgingBelow"
        Me.seBulgingBelow.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.seBulgingBelow.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.seBulgingBelow.Properties.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.seBulgingBelow.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.seBulgingBelow.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seBulgingBelow.Properties.Appearance.Options.UseBackColor = True
        Me.seBulgingBelow.Properties.Appearance.Options.UseBorderColor = True
        Me.seBulgingBelow.Properties.Appearance.Options.UseFont = True
        Me.seBulgingBelow.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seBulgingBelow.Properties.AppearanceDisabled.Options.UseFont = True
        Me.seBulgingBelow.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seBulgingBelow.Properties.AppearanceFocused.Options.UseFont = True
        Me.seBulgingBelow.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seBulgingBelow.Properties.AppearanceReadOnly.Options.UseFont = True
        SerializableAppearanceObject18.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject18.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject18.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject18.Options.UseBackColor = True
        SerializableAppearanceObject18.Options.UseBorderColor = True
        Me.seBulgingBelow.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject18, "", Nothing, Nothing, True)})
        Me.seBulgingBelow.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.seBulgingBelow.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seBulgingBelow.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seBulgingBelow.Properties.IsFloatValue = False
        Me.seBulgingBelow.Properties.Mask.EditMask = "n0"
        Me.seBulgingBelow.Properties.MaxValue = New Decimal(New Integer() {75, 0, 0, 0})
        Me.seBulgingBelow.Size = New System.Drawing.Size(43, 22)
        Me.seBulgingBelow.TabIndex = 206
        '
        'QTT_PipeTubeDetails
        '
        Me.QTT_PipeTubeDetails.DataSetName = "QTT_PipeTubeDetails"
        Me.QTT_PipeTubeDetails.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblPipeTubeItemsBindingSource
        '
        Me.TblPipeTubeItemsBindingSource.DataMember = "tblPipeTubeItems"
        Me.TblPipeTubeItemsBindingSource.DataSource = Me.QTT_SummaryData
        '
        'TblPipeTubeItemsTableAdapter
        '
        Me.TblPipeTubeItemsTableAdapter.ClearBeforeFill = True
        '
        'uiSectionSelection
        '
        Me.uiSectionSelection.Location = New System.Drawing.Point(329, 26)
        Me.uiSectionSelection.Name = "uiSectionSelection"
        Me.uiSectionSelection.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiSectionSelection.Properties.Appearance.Options.UseFont = True
        Me.uiSectionSelection.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.uiSectionSelection.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiSectionSelection.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PipeSchedule", 65, "Pipe Name"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 65, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.uiSectionSelection.Properties.DataSource = Me.TblPipeTubeItemsBindingSource
        Me.uiSectionSelection.Properties.DisplayMember = "Description"
        Me.uiSectionSelection.Properties.NullText = ""
        Me.uiSectionSelection.Properties.ValueMember = "ID"
        Me.uiSectionSelection.Size = New System.Drawing.Size(195, 24)
        Me.uiSectionSelection.TabIndex = 211
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label1.Location = New System.Drawing.Point(3, 346)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(201, 15)
        Me.Label1.TabIndex = 212
        Me.Label1.Text = "FORMATTED SECTION SUMMARY"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ucSectionSummary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.uiSectionSelection)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.seDentingBelow)
        Me.Controls.Add(Me.seOvalityBelow)
        Me.Controls.Add(Me.seSwellingBelow)
        Me.Controls.Add(Me.seBulgingBelow)
        Me.Controls.Add(Me.uiSecSumShowHideButton)
        Me.Controls.Add(Me.uiSectionSummaryEditor)
        Me.Controls.Add(Me.uiClearSectionSummaryButton)
        Me.Controls.Add(Me.uiAppendSectionSummaryButton)
        Me.Controls.Add(Me.uiReplaceSectionSummaryButton)
        Me.Controls.Add(Me.btnPaste)
        Me.Controls.Add(Me.radPipeOrBend)
        Me.Controls.Add(Me.uiPipeOrBendDataInfo)
        Me.Controls.Add(Me.uiPipeSection)
        Me.Controls.Add(Me.uiInternalOrExternal)
        Me.Controls.Add(Me.uilblGeneralOrLocal)
        Me.Controls.Add(Me.uiGeneralOrLocal)
        Me.Controls.Add(Me.uilblInternalOrExternal)
        Me.Controls.Add(Me.btnWriteSectionSummary)
        Me.Controls.Add(Me.uiSectionSummaryTextbox)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.lblAboveThreshold)
        Me.Controls.Add(Me.seDentThreshold)
        Me.Controls.Add(Me.seOvalityThreshold)
        Me.Controls.Add(Me.seSwellThreshold)
        Me.Controls.Add(Me.seBulgeThreshold)
        Me.Controls.Add(Me.lblDeformations)
        Me.Controls.Add(Me.SEDenting)
        Me.Controls.Add(Me.lblDent)
        Me.Controls.Add(Me.SEOvality)
        Me.Controls.Add(Me.lblOvality)
        Me.Controls.Add(Me.SESwelling)
        Me.Controls.Add(Me.lblSwell)
        Me.Controls.Add(Me.SEBulging)
        Me.Controls.Add(Me.lblBulge)
        Me.Controls.Add(Me.uiUnits)
        Me.Controls.Add(Me.uiNewUsedPipes)
        Me.Controls.Add(Me.uilblNewUsedPipes)
        Me.Controls.Add(Me.uilblUnits)
        Me.Controls.Add(Me.chkInternalFouling)
        Me.Controls.Add(Me.Label93)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucSectionSummary"
        Me.Size = New System.Drawing.Size(874, 524)
        CType(Me.seSwellingCount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkInternalFouling.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsSummaryPipeOrBendData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.QTT_SummaryData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radPipeOrBend.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiPipeOrBendDataInfo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiPipeOrBendDataInfoView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PasteButtons, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsSummaryPipeDataInfo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiPipeSection.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiInternalOrExternal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiGeneralOrLocal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDentThreshold.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seOvalityThreshold.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seSwellThreshold.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seBulgeThreshold.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SEDenting.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SEOvality.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SESwelling.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SEBulging.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiUnits.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsSectionSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiNewUsedPipes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsSummaryBendDataInfo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsSectionSummaries, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDentingBelow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seOvalityBelow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seSwellingBelow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seBulgingBelow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.QTT_PipeTubeDetails, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblPipeTubeItemsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiSectionSelection.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents seSwellingCount As DevExpress.XtraEditors.SpinEdit
    Public WithEvents chkInternalFouling As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents btnPaste As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents radPipeOrBend As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents uiPipeOrBendDataInfo As DevExpress.XtraGrid.GridControl
    Friend WithEvents uiPipeOrBendDataInfoView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PasteButtons As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents uiPipeSection As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiInternalOrExternal As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uilblGeneralOrLocal As System.Windows.Forms.Label
    Friend WithEvents uiGeneralOrLocal As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uilblInternalOrExternal As System.Windows.Forms.Label
    Friend WithEvents btnWriteSectionSummary As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiSectionSummaryTextbox As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblAboveThreshold As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seDentThreshold As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seOvalityThreshold As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seSwellThreshold As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seBulgeThreshold As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents lblDeformations As System.Windows.Forms.Label
    Friend WithEvents SEDenting As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents lblDent As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SEOvality As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents lblOvality As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SESwelling As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents lblSwell As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SEBulging As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents lblBulge As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uiUnits As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiNewUsedPipes As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uilblNewUsedPipes As System.Windows.Forms.Label
    Friend WithEvents uilblUnits As System.Windows.Forms.Label
    Friend WithEvents bsSummaryPipeDataInfo As System.Windows.Forms.BindingSource
    Friend WithEvents QTT_SummaryData As QIG.AutoReporter.QTT_SummaryData
    Friend WithEvents bsSummaryPipeOrBendData As System.Windows.Forms.BindingSource
    Friend WithEvents bsSectionSummary As System.Windows.Forms.BindingSource
    Friend WithEvents taTblSummary As QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryTableAdapter
    Friend WithEvents taTblSummaryPipeData As QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryPipeDataTableAdapter
    Friend WithEvents taTblSummaryBendData As QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryBendDataTableAdapter
    Friend WithEvents taTblSummaryBendDataInfo As QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryBendDataInfoTableAdapter
    Friend WithEvents taTblSummaryPipeDataInfo As QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblSummaryPipeDataInfoTableAdapter
    Friend WithEvents bsSummaryBendDataInfo As System.Windows.Forms.BindingSource
    Friend WithEvents uiSecSumShowHideButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiSectionSummaryEditor As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents uiClearSectionSummaryButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiAppendSectionSummaryButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiReplaceSectionSummaryButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bsSectionSummaries As System.Windows.Forms.BindingSource
    Friend WithEvents taTblPipeTubeItemSummaries As QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblPipeTubeItemSummariesTableAdapter
    Friend WithEvents taTblPipeTubeItems As QIG.AutoReporter.QTT_SummaryDataTableAdapters.tblPipeTubeItemsTableAdapter
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seDentingBelow As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seOvalityBelow As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seSwellingBelow As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seBulgingBelow As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents colID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFK_SummaryPipeData As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPipeID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMinWall As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLocation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEstWallLoss As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEstRemWall As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAvgWall As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDesWall As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAvgDiam As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colConvectionOrRadiant As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblPipeTubeItemsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents QTT_PipeTubeDetails As QIG.AutoReporter.QTT_PipeTubeDetails
    Friend WithEvents TblPipeTubeItemsTableAdapter As QIG.AutoReporter.QTT_PipeTubeDetailsTableAdapters.tblPipeTubeItemsTableAdapter
    Friend WithEvents uiSectionSelection As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label

End Class
